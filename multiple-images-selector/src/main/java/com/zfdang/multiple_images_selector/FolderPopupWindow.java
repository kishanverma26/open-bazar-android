package com.zfdang.multiple_images_selector;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupWindow;

import com.zfdang.multiple_images_selector.models.FolderListContent;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by zfdang on 2016-4-16.
 */
public class FolderPopupWindow extends PopupWindow {

    private static final String TAG = "FolderPopupWindow";
    private Context mContext;
    private View conentView;
    private RecyclerView recyclerView;
    private OnFolderRecyclerViewInteractionListener mListener = null;
    ImageView selector_button_back;
    // http://stackoverflow.com/questions/23464232/how-would-you-create-a-popover-view-in-android-like-facebook-comments
    public void initPopupWindow(final Activity context) {
        mContext = context;
        if (context instanceof OnFolderRecyclerViewInteractionListener) {
            mListener = (OnFolderRecyclerViewInteractionListener) context;
        } else {
            Log.d(TAG, "initPopupWindow: " + "context does not implement OnFolderRecyclerViewInteractionListener");
        }

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        conentView = layoutInflater.inflate(R.layout.popup_folder_recyclerview, null, false);
         selector_button_back=conentView.findViewById(R.id.selector_button_back);
        View rview = conentView.findViewById(R.id.folder_recyclerview);
        // Set the adapter
        if (rview instanceof RecyclerView) {
            recyclerView = (RecyclerView) rview;
            recyclerView.setLayoutManager(new GridLayoutManager(context,2));
            int spacingInPixels = mContext.getResources().getDimensionPixelSize(R.dimen.d_2dp);
            recyclerView.addItemDecoration(new SpacesItemDecoration(2,spacingInPixels,true));
            recyclerView.setAdapter(new FolderRecyclerViewAdapter(FolderListContent.FOLDERS, mListener));
        }

        // get device size
        Display display = context.getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);

        this.setContentView(conentView);
        this.setWidth(size.x);
        this.setHeight(size.y);
        // http://stackoverflow.com/questions/12232724/popupwindow-dismiss-when-clicked-outside
        this.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.popup_background));
        this.setOutsideTouchable(true);
        this.setFocusable(true);
        this.setAnimationStyle(R.style.AnimationPreview);

         selector_button_back.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 context.finish();
             }
         });
    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;
        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public SpacesItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
        }
    }



