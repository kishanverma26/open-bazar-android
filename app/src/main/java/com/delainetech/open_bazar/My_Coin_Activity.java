package com.delainetech.open_bazar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import io.paperdb.Paper;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.delainetech.open_bazar.Adapter.My_Coin_Adapter;
import com.delainetech.open_bazar.Custom_classes.EndlessRecyclerViewScrollListener;
import com.delainetech.open_bazar.Models.MYCOINS;
import com.delainetech.open_bazar.Models.Offers;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import static com.delainetech.open_bazar.Utils.Common.ParseString;
import static com.delainetech.open_bazar.Utils.Common.logg;

public class My_Coin_Activity extends AppCompatActivity implements  ParamKeys,View.OnClickListener , SwipeRefreshLayout.OnRefreshListener {

    Context mcontext;
    ImageView iv_faq;
    RecyclerView recyclerview;
    LinearLayoutManager linearLayoutManager;
    My_Coin_Adapter adapter;
    Toolbar toolbar;
    TextView tv_date,tv_coins;
    EndlessRecyclerViewScrollListener scrollListener;
    int index=0;
    SwipeRefreshLayout swipeContainer;
    UserSharedPreferences preferences;
    ArrayList<MYCOINS> mycoinsArrayList=new ArrayList<>();;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_coin_activity);
        mcontext=My_Coin_Activity.this;
        inti();
    }

    private void inti() {

        preferences=UserSharedPreferences.getInstance(mcontext);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));

        tv_coins=(TextView) findViewById(R.id.tv_coins);
        tv_date=(TextView) findViewById(R.id.tv_date);
        recyclerview=(RecyclerView) findViewById(R.id.recyclerview);
        linearLayoutManager=new LinearLayoutManager(mcontext);
        recyclerview.setLayoutManager(linearLayoutManager);
        recyclerview.setNestedScrollingEnabled(false);
        adapter=new My_Coin_Adapter(this,mycoinsArrayList);
        recyclerview.setAdapter(adapter);

        iv_faq=(ImageView)findViewById(R.id.iv_faq);
        iv_faq.setOnClickListener(this);

        tv_date.setText("Today,"+date());

        swipeContainer=(SwipeRefreshLayout)findViewById(R.id.Swip_refreshlayout);
        swipeContainer.setOnRefreshListener(this);

        if (Paper.book("get_mycoins").contains("response")){

            setdata(Paper.book("get_mycoins").read("response").toString());
        }

        fetech_mycoins(index);
        scrollListener=new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if ( (mycoinsArrayList.size()%10)==0){
                    index++;
                    fetech_mycoins(index);
                }
            }
        };

        recyclerview.addOnScrollListener(scrollListener);

        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorPrimary),getResources().getColor(R.color.colorPrimary));


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.iv_faq:
                Intent i=new Intent(mcontext,Drawer_Frame_Activity.class);
                i.putExtra(Pos,4);
                i.putExtra(Title,"My Coins FAQ");
                startActivity(i);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId()==android.R.id.home)
        {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }


    private String date(){
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => "+c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("dd MMM", Locale.US);
        String formattedDate = df.format(c.getTime());

        return formattedDate;
    }

    @Override
    public void onRefresh() {
        index=0;
        mycoinsArrayList.clear();
        fetech_mycoins(index);
    }

    public void  fetech_mycoins(final int index){


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "get_mycoins", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                pro.cancel();
                logg("get_mycoins",response);
                if (index==0){
                    Paper.book("get_mycoins").write("response",response);
                    mycoinsArrayList.clear();
                }
                swipeContainer.setRefreshing(false);
                setdata(response);
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),recyclerview);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put("index",index+"");
                params.put("type","0");
                params.put(User_id,preferences.getuserid());




                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }


    private void setdata(String res){
        try {
            JSONObject object=new JSONObject(res);
            if (object.getString("status").equals("success")){

                JSONArray jsonArray=object.getJSONArray("data");

                for (int i=0;i<jsonArray.length();i++){
                    JSONObject object1=jsonArray.getJSONObject(i);

                    mycoinsArrayList.add(new MYCOINS(ParseString(object1,"id"),ParseString(object1,"user_id"),
                            ParseString(object1,"tital"),ParseString(object1,"refer_id"),
                            ParseString(object1,"coins"),ParseString(object1,"timestamp"),ParseString(object1,"type")));
                }
                adapter.notifyDataSetChanged();

                tv_coins.setText(object.getString("mycoins"));
            }


        }
        catch (Exception e){
//                    pro.cancel();
            Log.e("error","e",e);

        }
    }

}
