//package com.delainetech.open_bazar;
//
//import android.content.ContentResolver;
//import android.content.Context;
//import android.database.Cursor;
//import android.net.Uri;
//import android.os.AsyncTask;
//import android.provider.ContactsContract;
//import android.util.Log;
//
//import com.android.volley.DefaultRetryPolicy;
//import com.android.volley.Request;
//import com.android.volley.Response;
//import com.delainetech.open_bazar.SQL.ContactDTO;
//import com.delainetech.open_bazar.SQL.Databasehelper;
//import com.delainetech.open_bazar.Utils.Common;
//import com.delainetech.open_bazar.Utils.UserSharedPreferences;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//
//public class Fatch_contact extends AsyncTask<String, Void, String> {
//    UserSharedPreferences preferences;
//    String numbers="",dataValue="";
//    Databasehelper databasehelper;
//    Context context;
//
//    public Fatch_contact(Context context) {
//        this.context = context;
//    }
//
//    @Override
//    protected void onPostExecute(String s) {
//
//        super.onPostExecute(s);
//    }
//
//    @Override
//    protected String doInBackground(String... strings) {
//      ArrayList<String> list=  getNameEmailDetails();
//        return null;
//    }
//
//    @Override
//    protected void onPreExecute() {
//        super.onPreExecute();
//        preferences=new UserSharedPreferences(context);
//        databasehelper=new Databasehelper(context);
//    }
//
//    public ArrayList<String> getNameEmailDetails(){
//        ArrayList<String> names = new ArrayList<String>();
//        ContentResolver cr = context.getContentResolver();
//        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,null, null, null, null);
//        if (cur.getCount() > 0) {
//            while (cur.moveToNext()) {
//                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
//                Cursor cur1 = cr.query(
//                        ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
//                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
//                        new String[]{id}, null);
//                while (cur1.moveToNext()) {
//                    //to get the contact names
//                    String name=cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
//                    Log.e("Name :", name);
//                    String email = cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
//                    String uri = cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Email.PHOTO_URI));
//                    Log.e("Email", email+", uri="+uri);
//
//                    if(email!=null){
//                        databasehelper.insertContactdata(name,  uri,email);
//
//                        names.add(name);
//                    }
//                }
//                cur1.close();
//            }
//        }
//        return names;
//    }
//
//
//    private List<ContactDTO> getAllContacts()
//    {
//        databasehelper=new Databasehelper(context);
//        List<ContactDTO> ret = new ArrayList<ContactDTO>();
//
//        // Get all raw contacts id list.
//        List<Integer> rawContactsIdList = getRawContactsIdList();
//
//        int contactListSize = rawContactsIdList.size();
//
//        ContentResolver contentResolver = context.getContentResolver();
//
//        // Loop in the raw contacts list.
//        for(int i=0;i<contactListSize;i++)
//        {
//            // Get the raw contact id.
//            Integer rawContactId = rawContactsIdList.get(i);
//
//            // Data content uri (access data table. )
//            Uri dataContentUri = ContactsContract.Data.CONTENT_URI;
//
//            // Build query columns name array.
//            List<String> queryColumnList = new ArrayList<String>();
//
//            // ContactsContract.Data.CONTACT_ID = "contact_id";
//            queryColumnList.add(ContactsContract.Data.CONTACT_ID);
//
//            // ContactsContract.Data.MIMETYPE = "mimetype";
//            queryColumnList.add(ContactsContract.Data.MIMETYPE);
//            queryColumnList.add(ContactsContract.Data.NAME_RAW_CONTACT_ID);
//            queryColumnList.add(ContactsContract.Data.PHOTO_URI);
//            queryColumnList.add(ContactsContract.CommonDataKinds.Email.DATA);
//            queryColumnList.add(ContactsContract.CommonDataKinds.Email.DISPLAY_NAME);
//           queryColumnList.add(ContactsContract.CommonDataKinds.Email.DATA1);
//            queryColumnList.add(ContactsContract.Data.DISPLAY_NAME);
//            queryColumnList.add(ContactsContract.Data.DATA1);
//            queryColumnList.add(ContactsContract.Data.DATA2);
//            queryColumnList.add(ContactsContract.Data.DATA3);
//            queryColumnList.add(ContactsContract.Data.DATA4);
//            queryColumnList.add(ContactsContract.Data.DATA5);
//
//            // Translate column name list to array.
//            String queryColumnArr[] = queryColumnList.toArray(new String[queryColumnList.size()]);
//
//            // Build query condition string. Query rows by contact id.
//            StringBuffer whereClauseBuf = new StringBuffer();
//            whereClauseBuf.append(ContactsContract.Data.RAW_CONTACT_ID);
//            whereClauseBuf.append("=");
//            whereClauseBuf.append(rawContactId);
//
//            // Query data table and return related contact data.
//            Cursor cursor = contentResolver.query(dataContentUri, queryColumnArr, whereClauseBuf.toString(), null, null);
//
//            /* If this cursor return database table row data.
//               If do not check cursor.getCount() then it will throw error
//               android.database.CursorIndexOutOfBoundsException: Index 0 requested, with a size of 0.
//               */
//            try{
//            if(cursor!=null && cursor.getCount() > 0)
//            {
//                StringBuffer lineBuf = new StringBuffer();
//                cursor.moveToFirst();
//
//                do{
//                    // First get mimetype column value.
//                    String mimeType = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.MIMETYPE));
//                    lineBuf.append(" \r\n , MimeType : ");
//                    lineBuf.append(mimeType);
//                    String name=cursor.getString(cursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
//                    String NAME_RAW_CONTACT_ID=cursor.getString(cursor.getColumnIndex(ContactsContract.Data.NAME_RAW_CONTACT_ID));
//                    String MIMETYPE=cursor.getString(cursor.getColumnIndex(ContactsContract.Data.MIMETYPE));
//                    String PHOTO_URI=cursor.getString(cursor.getColumnIndex(ContactsContract.Data.PHOTO_URI));
//                    String CONTACT_ID=cursor.getString(cursor.getColumnIndex(ContactsContract.Data.CONTACT_ID));
//                    String email=cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA1));
//                    Log.e("email",email+"");
//                    String DATA1="";
//                     DATA1=cursor.getString(cursor.getColumnIndex(ContactsContract.Data.DATA1));
//                    if (MIMETYPE.equals("vnd.android.cursor.item/phone_v2") && DATA1!=null && Common.isValidMobile(DATA1)) {
//                      //  Log.e("data",DATA1+"");
//                        if (DATA1.length()>8) {
////                            databasehelper.insertContactdata(name, NAME_RAW_CONTACT_ID, MIMETYPE, PHOTO_URI, DATA1.trim(), CONTACT_ID);
//                        }
//                    }
//                }
//                while(cursor.moveToNext());
//
//            }}finally {
//                cursor.close();
//            }
//            int index=dataValue.indexOf(":");
//            String value=dataValue.substring(index+1,dataValue.length());
//            numbers=numbers+","+value;
//            numbers=numbers.trim();
//
//        }
//        Cursor c=databasehelper.get_alldata();
//
//
//
//        if(c!=null &&c.moveToFirst() ){
//            do{
//                String value=c.getString(c.getColumnIndex(Databasehelper.col6));
//                numbers=numbers+","+value;
//                numbers=numbers.trim();
//                //  Log.e("contactno",numbers+"");
//                //         c.getString(c.getColumnIndex(Databasehelper.col6));
//            } while(c.moveToNext());
//        }
//
//
//      Log.e("jsonarray",contact_in_jsonformate()+"")  ;
//      Log.e("jsonarray",contact_in_jsonformate().length()+"")  ;
//      System.out.print(contact_in_jsonformate()+"");
//
//        return ret;
//    }
//
//    // Return all raw_contacts _id in a list.
//    private List<Integer> getRawContactsIdList()
//    {
//        List<Integer> ret = new ArrayList<Integer>();
//
//        ContentResolver contentResolver =context. getContentResolver();
//
//        // Row contacts content uri( access raw_contacts table. ).
//        Uri rawContactUri = ContactsContract.RawContacts.CONTENT_URI;
//        // Return _id column in contacts raw_contacts table.
//        String queryColumnArr[] = {ContactsContract.RawContacts._ID};
//        // Query raw_contacts table and return raw_contacts table _id.
//        Cursor cursor = contentResolver.query(rawContactUri,queryColumnArr, null, null, null);
//        Log.e("cursor", cursor.getCount()+"" );
//        if(cursor!=null)
//        {
//            if (cursor.getCount()>0) {
//                cursor.moveToFirst();
//                do {
//                    int idColumnIndex = cursor.getColumnIndex(ContactsContract.RawContacts._ID);
//                    int rawContactsId = cursor.getInt(idColumnIndex);
//                    ret.add(new Integer(rawContactsId));
//                } while (cursor.moveToNext());
//            }
//            }
//
//        cursor.close();
//
//        return ret;
//    }
//
//    private JSONArray contact_in_jsonformate(){
//        Cursor c= databasehelper.get_alldata();
//        JSONArray jsonArray=new JSONArray();
//        JSONObject jsonObject;
//        if(c!=null &&c.moveToFirst() ){
//            do{
//                jsonObject=new JSONObject();
//                String value=c.getString(c.getColumnIndex(Databasehelper.col6));
//                String name=c.getString(c.getColumnIndex(Databasehelper.col2));
//                String number=c.getString(c.getColumnIndex(Databasehelper.col6));
//                if (name.length()>1){
//                    name = name.substring(0, 1).toUpperCase() + name.substring(1);
//
//                }
//                try {
//                    jsonObject.put("name",name+"");
//                    jsonObject.put("number",number+"");
//
//                    jsonArray.put(jsonObject);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//                numbers=numbers+","+value;
//                numbers=numbers.trim();
//                Log.e("contactno1",numbers+"");
//                //         c.getString(c.getColumnIndex(Databasehelper.col6));
//            } while(c.moveToNext());
//        }
//        return jsonArray;
//    }
//
//
//
//}
