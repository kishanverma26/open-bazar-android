package com.delainetech.open_bazar;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.delainetech.open_bazar.Adapter.Category_adapter_filter;
import com.delainetech.open_bazar.Category.Category_Activity;
import com.delainetech.open_bazar.Custom_classes.DelayedProgressDialog;
import com.delainetech.open_bazar.Custom_classes.MySingleton;
import com.delainetech.open_bazar.Models.Category;
import com.delainetech.open_bazar.Models.Category_Filter;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;
import com.zfdang.multiple_images_selector.DividerItemDecoration;
import com.zfdang.multiple_images_selector.FolderPopupWindow;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import static com.delainetech.open_bazar.Utils.Common.ParseString;
import static com.delainetech.open_bazar.Utils.Common.logg;
import static com.delainetech.open_bazar.Utils.ParamKeys.baseurl;

public class Category_Activity_filter extends AppCompatActivity {

    Category_adapter_filter adapter;
    RecyclerView recyclerview;;
    GridLayoutManager gridLayoutManager;
    Context mcoxt;
    Toolbar toolbar;
    ArrayList<Category_Filter> categoryArrayList=new ArrayList<Category_Filter>();
    String id;
    View v;
    String main_cat_id="0";
    UserSharedPreferences preferences;
    TextView tv_tital;
    String str_tital="";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_activity_filter);
        mcoxt=Category_Activity_filter.this;
        preferences=new UserSharedPreferences(mcoxt);
        main_cat_id=Common.Parsintentvalue("id",Category_Activity_filter.this);
        initializ();
    }


    private void initializ() {

        toolbar=(Toolbar) findViewById(R.id.toolbar);
        tv_tital=(TextView) findViewById(R.id.tv_tital);

        if (main_cat_id.equals("1")){
            str_tital="Sell Categories";
        }else if (main_cat_id.equals("2")){
            str_tital="Rent Categories";
        }else if (main_cat_id.equals("3")){
            str_tital="Services Categories";
        }else if (main_cat_id.equals("4")){
            str_tital="Shops Categories";
        }

        tv_tital.setText(str_tital);

        recyclerview=(RecyclerView) findViewById(R.id.recyclerview);
        gridLayoutManager=new GridLayoutManager(mcoxt,2);
        recyclerview.setLayoutManager(gridLayoutManager);
        int spacingInPixels = mcoxt.getResources().getDimensionPixelSize(R.dimen.d_5dp);
        recyclerview.addItemDecoration(new SpacesItemDecoration(2,spacingInPixels,true));
        adapter=new Category_adapter_filter(mcoxt,categoryArrayList);
        recyclerview.setAdapter(adapter);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);

        fatch_category(mcoxt);



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void  fatch_category(final Context context){

        final DelayedProgressDialog pro=new DelayedProgressDialog();

        pro.show(getSupportFragmentManager(),"");


        logg("urll",baseurl + "ad_counts");
        StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "ad_counts", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                pro.cancel();
                logg("get_category11",response);

                add_data_in_list(response);
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),recyclerview);
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("main_category",main_cat_id);

                return params;
            }
        };

        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(context).addToRequestQueue(stringRequest);

    }

    private void add_data_in_list(String response) {
        try {
            JSONObject jsonObject=new JSONObject(response);
            if (jsonObject.getString("status").equals("true")){
                JSONArray jsonArray=jsonObject.getJSONArray("data");
                for (int i=0;i<jsonArray.length();i++){
                    JSONObject object=jsonArray.getJSONObject(i);
                    categoryArrayList.add(new Category_Filter(ParseString(object,"id"),ParseString(object,"en_name"),
                            ParseString(object,"ad_count"),ParseString(object,"image")));
                }
                adapter.notifyDataSetChanged();


            }

        } catch (JSONException e) {
            e.printStackTrace();
            logg("error",e+"");
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;
        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public SpacesItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }
}
