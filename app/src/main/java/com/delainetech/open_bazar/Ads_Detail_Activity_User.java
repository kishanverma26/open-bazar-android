package com.delainetech.open_bazar;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.delainetech.open_bazar.Adapter.Detail_Adapter_recyclerview;
import com.delainetech.open_bazar.Adapter.Image_Viewpage_Adapter;
import com.delainetech.open_bazar.Custom_classes.CTextView;
import com.delainetech.open_bazar.Custom_classes.CustomHorizontalProgresWithNum;
import com.delainetech.open_bazar.Custom_classes.DelayedProgressDialog;
import com.delainetech.open_bazar.Models.ADS;
import com.delainetech.open_bazar.Models.Category_values;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.facebook.accountkit.ui.SkinManager;
import com.facebook.accountkit.ui.UIManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.delainetech.open_bazar.Utils.Common.ParseString;
import static com.delainetech.open_bazar.Utils.Common.logg;

public class Ads_Detail_Activity_User extends AppCompatActivity implements View.OnClickListener,ParamKeys {


    Context mcontext;

    ImageView iv_back,iv_call,iv_bid,iv_chat,iv_edit,iv_share;
    TextView tv_call,tv_bid,tv_chat;
    RecyclerView recyclerview;
    Detail_Adapter_recyclerview detail_adapter_recyclerview;
    LinearLayoutManager linearLayoutManager;
    ViewPager view_pager;
    Image_Viewpage_Adapter image_viewpage_adapter;
    private ArrayList<String> postimages = new ArrayList<>();
//    EditText et_bidamount;
    NestedScrollView nested_scroll;
    ADS ads=new ADS();
    TextView tv_tital,tv_time,tv_views,tv_favorite_count,tv_location,tv_price,tv_des,tv_imagecount,tv_expired,
            tv_ades_text,tv_myofferads_counts,tv_myofferads,tv_adquality;
    CTextView tv_like;
    UserSharedPreferences preferences;
    Button bt_edit;

    LinearLayout ll_empty;

    de.hdodenhof.circleimageview.CircleImageView iv_circle;
    TextView tv_username;

    CustomHorizontalProgresWithNum horizontalProgress;
    Timer timer;

    int progress=70;
    Intent i;
    RelativeLayout rl_edit,rl_phonenoverify;
    CardView cv_offer;
    RelativeLayout rl_boost,rl_delete,rl_chat,rl_share;

    ArrayList<Category_values> category_values=new ArrayList<Category_values>();



    String bid_type="";
    Button bt_verify;

    TextView tv_shopname;
    String bidtype="0";

    ImageView iv_feedback;

//    Boost
    ImageView iv_boost;

//    Loader
    RelativeLayout rl_view;
    ProgressBar pb_loader;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_page_activity_user);
        mcontext= Ads_Detail_Activity_User.this;
        init();

    }


    private void init() {
        cv_offer=(CardView) findViewById(R.id.cv_offer);
        rl_view=(RelativeLayout)findViewById(R.id.rl_view);
        pb_loader=(ProgressBar) findViewById(R.id.pb_loader);



        tv_shopname=(TextView) findViewById(R.id.tv_shopname);
        rl_delete=(RelativeLayout) findViewById(R.id.rl_delete);
        rl_delete.setOnClickListener(this);
        bt_verify=(Button) findViewById(R.id.bt_verify);
        bt_verify.setOnClickListener(this);

        iv_boost=(ImageView) findViewById(R.id.iv_boost);

        iv_feedback=(ImageView) findViewById(R.id.iv_feedback);
        iv_feedback.setOnClickListener(this);

        rl_boost=(RelativeLayout)  findViewById(R.id.rl_boost);
        rl_boost.setOnClickListener(this);

        rl_share=(RelativeLayout)  findViewById(R.id.rl_share);
        rl_share.setOnClickListener(this);

        iv_edit=(ImageView)  findViewById(R.id.iv_edit);
        iv_edit.setOnClickListener(this);

        iv_share=(ImageView)  findViewById(R.id.iv_share);
        iv_share.setOnClickListener(this);

        rl_edit=(RelativeLayout)  findViewById(R.id.rl_edit);
        rl_edit.setOnClickListener(this);
        rl_phonenoverify=(RelativeLayout)findViewById(R.id.rl_phonenoverify);
        rl_phonenoverify.setOnClickListener(this);

        bt_edit=(Button)  findViewById(R.id.bt_edit);
        bt_edit.setOnClickListener(this);

        ll_empty=(LinearLayout) findViewById(R.id.ll_empty);
        iv_circle=(CircleImageView) findViewById(R.id.iv_circle);
        tv_username=(TextView) findViewById(R.id.tv_username);

        preferences=UserSharedPreferences.getInstance(mcontext);
        tv_tital=(TextView) findViewById(R.id.tv_tital);
        tv_time=(TextView)findViewById(R.id.tv_time);
        tv_views=(TextView)findViewById(R.id.tv_views);
        tv_like=(CTextView) findViewById(R.id.tv_like);
        tv_favorite_count=(TextView)findViewById(R.id.tv_favorite_count);
        tv_location=(TextView)findViewById(R.id.tv_location);
        tv_price=(TextView)findViewById(R.id.tv_price);
        tv_des=(TextView)findViewById(R.id.tv_des);
        tv_imagecount=(TextView)findViewById(R.id.tv_imagecount);
        tv_expired=(TextView)findViewById(R.id.tv_expired);
        tv_ades_text=(TextView)findViewById(R.id.tv_ades_text);
        tv_myofferads_counts=(TextView)findViewById(R.id.tv_myofferads_counts);
        tv_myofferads=(TextView)findViewById(R.id.tv_myofferads);
        tv_adquality=(TextView)findViewById(R.id.tv_adquality);
        tv_myofferads.setOnClickListener(this);

        nested_scroll=(NestedScrollView) findViewById(R.id.nested_scroll);
//        et_bidamount=(EditText) findViewById(R.id.et_bidamount);
        iv_call=(ImageView) findViewById(R.id.iv_call);
        iv_bid=(ImageView) findViewById(R.id.iv_bid);
        iv_chat=(ImageView) findViewById(R.id.iv_chat);
        tv_call=(TextView) findViewById(R.id.tv_call);
        tv_bid=(TextView) findViewById(R.id.tv_bid);
        tv_chat=(TextView) findViewById(R.id.tv_chat);

        rl_chat=(RelativeLayout) findViewById(R.id.rl_chat);
        rl_chat.setOnClickListener(this);

        view_pager=(ViewPager) findViewById(R.id.view_pager);
         view_pager.setOnClickListener(this);


        recyclerview=(RecyclerView) findViewById(R.id.recyclerview);
        linearLayoutManager=new LinearLayoutManager(mcontext);
        recyclerview.setLayoutManager(linearLayoutManager);

        recyclerview.setNestedScrollingEnabled(false);
        iv_back=(ImageView) findViewById(R.id.iv_back);
        iv_back.setOnClickListener(this);



        fetech_ads(getIntent().getStringExtra("id"));

    }




    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.iv_feedback:
                i=new Intent(Ads_Detail_Activity_User.this,Drawer_Frame_Activity.class);
                i.putExtra(Pos,2);
                i.putExtra(Title,"Contact Us");
                startActivity(i);
                break;
            case R.id.rl_edit:
                editprofile();
                break;
            case R.id.rl_phonenoverify:
                onSMSLoginFlow();
                break;
            case R.id.bt_verify:
                onSMSLoginFlow();
                break;
            case R.id.bt_edit:
                editprofile();
                break;
        case R.id.iv_edit:
                editprofile();
                break;
        case R.id.rl_boost:
            i=new Intent(mcontext, Drawer_Frame_Activity.class);
            i.putExtra(Pos,17);
            i.putExtra(Title,"Boost Ad");
            i.putExtra("add_id",ads.getId());
            startActivity(i);
                break;
        case R.id.rl_share:
            create_dynamic_link(ads.getAd_title(),"adpost/"+ads.getId()+","+preferences.getuserid());

                break;
         case R.id.iv_share:
            create_dynamic_link(ads.getAd_title(),"adpost/"+ads.getId()+","+preferences.getuserid());

                break;
         case R.id.rl_delete:
              alert(Ads_Detail_Activity_User.this);
                break;


            case R.id.iv_back:
            onBackPressed();
                break;
            case R.id.rl_chat:
                Intent i=new Intent(mcontext, Home_Screen_Activity.class);
                i.putExtra("from","chat");
                mcontext.startActivity(i);
//                Intent chatIntent = new Intent(getActivity(), ChatActivity.class);
//                chatIntent.putExtra("user_id", ads.getUser_id());
//                chatIntent.putExtra("user_name", ads.getUser_name());
//                chatIntent.putExtra("name", preferences.getname());
//                chatIntent.putExtra("id",preferences.getuserid());
//                Log.e("ides", ads.getUser_id() + ",,"  + ",," + preferences.getuserid());
//                startActivity(chatIntent);

                customnavbar(2);
                break;

            case R.id.rl_call:
                if (ads.getHide_mobileno().equals("true")) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:00000000"));
                    startActivity(intent);
                    customnavbar(0);
                }else {

                }
                break;

            case R.id.rl_bid:
                customnavbar(1);
                break;
            case R.id.tv_myofferads:
                i=new Intent(mcontext, All_offers_Activity.class);
                i.putExtra("count",Integer.parseInt(ads.getOffer_count()));
                i.putExtra("id",ads.getId());
                i.putExtra("bid_type",bidtype);
                i.putExtra("type","user");
                startActivityForResult(i,OFFER_COUNT);
                break;
         case R.id.tv_call:
                i=new Intent(mcontext, Drawer_Frame_Activity.class);
             i.putExtra(Pos,17);
             i.putExtra(Title,"Boost Ad");
             i.putExtra("add_id",ads.getId());
                startActivity(i);
                break;

        }
    }



    //custom navigation bar

    private void customnavbar(int position){
        int active_color=R.color.on_click_icon_color;
        int inactive_color=R.color.white;
        tv_call.setTextColor(getResources().getColor(inactive_color));
        tv_bid.setTextColor(getResources().getColor(inactive_color));
        tv_chat.setTextColor(getResources().getColor(inactive_color));
        final float scale = getResources().getDisplayMetrics().density;
        int active_height_ = (int) (26 * scale + 0.5f);
        int active_width_ = (int) (26 * scale + 0.5f);
        int inactive_height_ = (int) (24 * scale + 0.5f);
        int inactive_width_ = (int) (24 * scale + 0.5f);

        iv_call.setColorFilter(ContextCompat.getColor(mcontext, inactive_color), android.graphics.PorterDuff.Mode.SRC_IN);
        iv_bid.setColorFilter(ContextCompat.getColor(mcontext, inactive_color), android.graphics.PorterDuff.Mode.SRC_IN);
        iv_chat.setColorFilter(ContextCompat.getColor(mcontext, inactive_color), android.graphics.PorterDuff.Mode.SRC_IN);

        iv_call.getLayoutParams().height = inactive_height_;
        iv_call.getLayoutParams().width =inactive_width_;
        iv_bid.getLayoutParams().height = inactive_height_;
        iv_bid.getLayoutParams().width =inactive_width_;
        iv_chat.getLayoutParams().height = inactive_height_;
        iv_chat.getLayoutParams().width =inactive_width_;


        switch (position){
            case 0:
            tv_call.setTextColor(getResources().getColor(active_color));
          iv_call.setColorFilter(ContextCompat.getColor(mcontext, active_color), android.graphics.PorterDuff.Mode.SRC_IN);
                iv_call.getLayoutParams().height = active_height_;
               iv_call.getLayoutParams().width =active_width_;

                //Action
//                et_bidamount.setFocusable(false);

                break;

            case 1:
                tv_bid.setTextColor(getResources().getColor(active_color));
                iv_bid.setColorFilter(ContextCompat.getColor(mcontext, active_color), android.graphics.PorterDuff.Mode.SRC_IN);

                iv_bid.getLayoutParams().height = active_height_;
                iv_bid.getLayoutParams().width = active_width_;

//              Action
//                et_bidamount.setFocusable(true);
                nested_scroll.post(new Runnable() {
                    @Override
                    public void run() {
                        nested_scroll.fullScroll(View.FOCUS_DOWN);
                    }
                });
                break;


            case 2:
             tv_chat.setTextColor(getResources().getColor(active_color));
          iv_chat.setColorFilter(ContextCompat.getColor(mcontext, active_color), android.graphics.PorterDuff.Mode.SRC_IN);

                iv_chat.getLayoutParams().height = active_height_;
                iv_chat.getLayoutParams().width = active_width_;
//                et_bidamount.setFocusable(false);

                break;
        }



    }

    @Override
    public void onResume() {
        super.onResume();
        customnavbar(4);
    }



    public void  view(final String postid, final String type){


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "ad_view", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                logg("insert_ads",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){

                    }

                }
                catch (Exception e){
                    Log.e("error","e",e);
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(User_id,preferences.getuserid());
                params.put("post_id",postid);
                params.put("type",type);

                Log.e("params",params+"");
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }


    private void getdatediffrance(String timestamp){

      printDifference( Common.getDate(System.currentTimeMillis()),Common.getDate(Long.parseLong(timestamp)*1000));


    }


    public void printDifference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : "+ endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);
        Log.e("date= ","days:"+elapsedDays);
        tv_expired.setText(Html.fromHtml("<font color=#58697B>Expired In: </font> <font color=#c62828>"+elapsedDays+"</font> <font color=#58697B>Days</font>"));

    }


    private void progresscount(){

        horizontalProgress=(CustomHorizontalProgresWithNum) findViewById(R.id.horizontalProgress);
        horizontalProgress.setProgress(0);
        horizontalProgress.setMax(100);

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                horizontalProgress.setProgress(horizontalProgress.getProgress()+1);
                if (horizontalProgress.getProgress() >= progress) {
                    timer.cancel();
                }


            }
        }, 50, 50);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
            logg("activityresult",requestCode+","+resultCode+"okresult="+(resultCode == RESULT_OK));
        if ((requestCode==11) && (resultCode == RESULT_OK)){


                ads.setAd_title(data.getStringExtra("tital"));
                ads.setHide_mobileno(data.getStringExtra("hide_mobileno"));
                ads.setAd_description(data.getStringExtra("ad_description"));
                ads.setMobile(data.getStringExtra("mobile"));
                ads.setPrice(data.getStringExtra("price"));
                ads.setPrice_type(data.getStringExtra("price_type"));
                ads.setImages(data.getStringExtra("images"));
                ads.setCity(data.getStringExtra("city"));
                ads.setShop_name(data.getStringExtra("shop_name"));
                ads.setCategory(data.getStringExtra(Category));

            category_values.clear();

            //        category
            if (ads.getCategory().length()>0){
                try {
                    JSONArray jsonArray=new JSONArray(ads.getCategory());
                    for (int i=2;i<jsonArray.length();i++){
                        JSONObject jsonObject=jsonArray.getJSONObject(i);
                        category_values.add(new Category_values(ParseString(jsonObject,"tital"),ParseString(jsonObject,"seletecvalue"),ParseString(jsonObject,"id"),
                                ParseString(jsonObject,"value"),ParseString(jsonObject,"image")));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (!(category_values.size()>0)){
                    recyclerview.setVisibility(View.GONE);
                }
                Log.e("detailsize",category_values.size()+"");
                detail_adapter_recyclerview=new Detail_Adapter_recyclerview(mcontext,category_values);
                recyclerview.setAdapter(detail_adapter_recyclerview);
            }
                setupdata_onui();


        }

        if (requestCode == Mobile_RESULT_CODE && resultCode==RESULT_OK) { // confirm that this response matches your request
            AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
            String toastMessage;
            if (loginResult.getError() != null) {
                toastMessage = loginResult.getError().getErrorType().getMessage();
            } else if (loginResult.wasCancelled()) {
                toastMessage = "Login Cancelled";
            } else {
                getaccount_info();
            }
        }else if (requestCode == Mobile_RESULT_CODE && resultCode==RESULT_CANCELED){

            Toast.makeText(mcontext, "Your ad is pending because mobile number is not verify", Toast.LENGTH_LONG).show();
        }
    }


    private void getaccount_info() {
        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(Account account) {
                Log.e("mobileno",account.getPhoneNumber()+"");
                if (account.getPhoneNumber().toString().contains(ads.getMobile())){
                    verify_ad(ads.getId());
                } else {

                    Toast.makeText(mcontext, "You have enter diffrent phone number", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onError(AccountKitError accountKitError) {

            }
        });
    }


    private void editprofile(){
        i=new Intent(mcontext, Adpost_Edit_Activity.class);
        i.putExtra("id",ads.getId());
        i.putExtra("tital",ads.getAd_title());
        i.putExtra("mobile",ads.getMobile());
        i.putExtra("price",ads.getPrice());
        i.putExtra("des",ads.getAd_description());
        i.putExtra("city",ads.getCity());
        i.putExtra("image",ads.getImages());
        i.putExtra("price_type",ads.getPrice_type());
        i.putExtra("category",ads.getCategory());
        i.putExtra("main_cat_id",ads.getMain_category());
        i.putExtra("hide_mobileno",ads.getHide_mobileno());
       i.putExtra("shop_name",ads.getShop_name());

        startActivityForResult(i,11);
    }


    private void setupdata_onui(){
        progress=70;
        tv_ades_text.setText(Html.fromHtml("<font color=#bcbcbc>Add more pictures and information about your Ad to </font> <font color=#c62828>Get</font> <font color=#bcbcbc> more views and sell it faster</font>"));
        tv_tital.setText(ads.getAd_title());
        tv_time.setText(Common.getTimeAgo(Long.parseLong(ads.getTimestamp()),mcontext));
        tv_like.setText(ads.getLike_count());
        tv_location.setText(ads.getCity());
        tv_views.setText(ads.getViews_count());
        tv_des.setText(ads.getAd_description());
        tv_favorite_count.setText(ads.getFavorite_count());
        tv_myofferads_counts.setText(ads.getOffer_count());


        if (ads.getPrice_type().equals("price")){
            bidtype="0";
            tv_price.setText("€ "+ads.getPrice());
        }else  if (ads.getPrice_type().equals("ask for price")){
            bidtype="1";
            tv_price.setText("Ask For Price");
        }else  if (ads.getPrice_type().equals("exchange")){
            bidtype="1";
            tv_price.setText("Exchange");
        }else  if (ads.getPrice_type().equals("free")){
            bidtype="1";
            tv_price.setText("Free");
        }


        logg("length",ads.getExpired_date().length()+"");
        if (ads.getExpired_date().length()>0){
            getdatediffrance(ads.getExpired_date());
        }

        if (ads.getImages().length()>0) {
            try {
                postimages.clear();
                ll_empty.setVisibility(View.GONE);
                view_pager.setVisibility(View.VISIBLE);
                JSONArray jsonArray = new JSONArray(ads.getImages());
                for (int i=0;i<jsonArray.length();i++) {

                    postimages.add(jsonArray.getJSONObject(i).getString("image"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
                logg("excep", e + "");
            }
            tv_imagecount.setText("1 / "+postimages.size());
        }else {
            view_pager.setVisibility(View.GONE);
            ll_empty.setVisibility(View.VISIBLE);
            tv_imagecount.setText("0");
        }

        image_viewpage_adapter=new Image_Viewpage_Adapter(mcontext,postimages,ads);
        view_pager.setAdapter(image_viewpage_adapter);
        view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (ads.getImages().length()>0) {

                    tv_imagecount.setText((position + 1) + " / " + postimages.size());
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        RequestOptions options=new RequestOptions().centerCrop().error(R.drawable.userimage);

        Glide.with(mcontext).load(Common.Image_Loading_Url(preferences.getimage())).apply(options).into(iv_circle);
        tv_username.setText(preferences.getname());

        if (postimages.size()>14){
            progress=progress+15;
        }else
        if (postimages.size()>5){
            progress=progress+10;
        }else if (postimages.size()>0){
            progress=progress+5;
        }

        if (tv_des.getText().length()>300){
            progress=progress+15;
        }else
        if (tv_des.getText().length()>200){
            progress=progress+10;
        }else if (tv_des.getText().length()>100){
            progress=progress+5;
        }

        if (progress==100){
            tv_ades_text.setVisibility(View.GONE);
//            tv_adquality.setVisibility(View.GONE);
        }else {
            tv_ades_text.setVisibility(View.VISIBLE);
//            tv_adquality.setVisibility(View.VISIBLE);
        }

        progresscount();

        rl_view.setVisibility(View.VISIBLE);
        pb_loader.setVisibility(View.GONE);
    }


    public void alert( final Context mcoxt){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mcoxt);

        alertDialog.setMessage("Are your sure you want to delete?");
        alertDialog.setPositiveButton(mcoxt.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
                delete_ad(ads.getId());
            }
        });
        alertDialog.setNegativeButton(mcoxt.getResources().getString(R.string.No), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog1= alertDialog.create();
        alertDialog1.show();

        alertDialog1.getButton(alertDialog1.BUTTON_NEGATIVE).setTextColor(mcoxt.getResources().getColor(R.color.colorAccent));
        alertDialog1.getButton(alertDialog1.BUTTON_POSITIVE).setTextColor(mcoxt.getResources().getColor(R.color.colorAccent));
    }



    public void  delete_ad(final String id){


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "delete_ads", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                pro.cancel();
                logg("insert_ads",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){



                    }

                }
                catch (Exception e){
//                    pro.cancel();
                    Log.e("error","e",e);
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(Id,id);
                params.put(User_id,preferences.getuserid());

                Log.e("params",params+"");
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }



    public void onSMSLoginFlow() {
        String locale = getResources().getConfiguration().locale.getCountry();
        logg("countrycode",locale+"");
        PhoneNumber number=new PhoneNumber(Country_CODE,ads.getMobile());
        AccountKit.logOut();
        final Intent intent = new Intent(mcontext, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN); // or .ResponseType.TOKEN
        // ... perform additional configuration ...

        UIManager uiManager;
        uiManager = new SkinManager(
                SkinManager.Skin.CLASSIC, Color.parseColor("#E83957"));

        configurationBuilder.setUIManager(uiManager);
        configurationBuilder.setInitialPhoneNumber(number);
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
         startActivityForResult(intent, Mobile_RESULT_CODE);
    }

    ProgressDialog dialog;

    public void  verify_ad(final String postid){
        dialog=new ProgressDialog(mcontext);
        dialog.setMessage("Please wait......");

        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "update_mobileverification", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                dialog.dismiss();
                logg("update_mobileverification",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success"))
                    {
                       rl_phonenoverify.setVisibility(View.GONE);
                       ads.setVerify_phone("true");
                       Toast.makeText(mcontext, "Your mobile number is verifyed now", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        rl_phonenoverify.setVisibility(View.VISIBLE);
                    }

                }
                catch (Exception e){
                    Log.e("error","e",e);
                    dialog.dismiss();
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),rl_delete);
                dialog.dismiss();
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put("id",postid);

                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        MySingleton.getInstance(context).addToRequestQueue(stringRequest);
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }


    public void create_dynamic_link(final String msg, String str){

        Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse("https://openbazar.page.link?="+str))
                .setDomainUriPrefix("https://openbazar.page.link")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder("com.open_bazar")
                        .setFallbackUrl(Uri.parse("https://www.dropbox.com/s/o1z7e9w94g7twqz/app-debug.apk?dl=0"))
                        .build())
                // Set parameters
                // ...
                .buildShortDynamicLink()

                .addOnCompleteListener(Ads_Detail_Activity_User.this, new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {
                            // Short link created
                            Uri shortLink = task.getResult().getShortLink();
                            Uri flowchartLink = task.getResult().getPreviewLink();

                            logg("shortLink",shortLink+" flowchartLink"+flowchartLink);


                            String shareBody2 = msg+" "+shortLink;
                            Intent sharingIntent2 = new Intent(Intent.ACTION_SEND);
                            sharingIntent2.setType("text/plain");
                            sharingIntent2.putExtra(Intent.EXTRA_SUBJECT, "Open Bazar");
                            sharingIntent2.putExtra(Intent.EXTRA_TEXT, shareBody2);
                            startActivity(Intent.createChooser(sharingIntent2, ""));

                        } else {
                            // Error
                            // ...
                            Toast.makeText(mcontext, "Somthing went wrong please try again", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    public void  fetech_ads(final String id){
//
//        final DelayedProgressDialog pro=new DelayedProgressDialog();
//
//        pro.show(getSupportFragmentManager(),"");


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "getpost_detail", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                logg("getpost_detail",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){

                        JSONArray jsonArray=object.getJSONArray("data");

                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject object1=jsonArray.getJSONObject(i);
                            ads.setId(ParseString(object1,"id"));
                            ads.setUser_id(ParseString(object1,"user_id"));
                            ads.setAd_title(ParseString(object1,"ad_title"));
                            ads.setAd_description(ParseString(object1,"ad_description"));
                            ads.setMobile(ParseString(object1,"mobile"));
                            ads.setPrice(ParseString(object1,"price"));
                            ads.setImages(ParseString(object1,"images"));
                            ads.setCategory(ParseString(object1,"category"));
                            ads.setCity(ParseString(object1,"city"));
                            ads.setFavorite_count(ParseString(object1,"favorite_count"));
                            ads.setViews_count(ParseString(object1,"views_count"));
                            ads.setLike_count(ParseString(object1,"like_count"));
                            ads.setTimestamp(ParseString(object1,"timestamp"));
                            ads.setFav_status(ParseString(object1,"fav_stattus"));
                            ads.setLike_status(ParseString(object1,"like_stattus"));
                            ads.setUser_name(ParseString(object1,"user_name"));
                            ads.setUser_image(ParseString(object1,"user_image"));
                            ads.setExpired_date(ParseString(object1,"expired_date"));
                            ads.setOffer_count(ParseString(object1,"offer_count"));
                            ads.setPrice_type(ParseString(object1,"price_type"));
                            ads.setMain_category(ParseString(object1,"main_category"));
                            ads.setUser_regiterdate(ParseString(object1,"user_regiterdate"));
                            ads.setAds_count(ParseString(object1,"ads_count"));
                            ads.setFollow_user(ParseString(object1,"follow_user"));
                            ads.setVerify_phone(ParseString(object1,"mobile_verify"));
                            ads.setHide_mobileno(ParseString(object1,"hide_mobileno"));
                          ads.setBoost_type(ParseString(object1,"boost_type"));
                        }

                        setdata();
//                        pro.cancel();
                    }


                }
                catch (Exception e){
//                    pro.cancel();
                    Log.e("error","e",e);

                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),rl_delete);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put("id",id);
                params.put(User_id,preferences.getuserid()+"");



                logg("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        MySingleton.getInstance(context).addToRequestQueue(stringRequest);
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }

    private void setdata() {

        if (ads.getOffer_count().equals("0")){
            cv_offer.setVisibility(View.GONE);
        }
        setupdata_onui();

//        view(ads.getId(),"view");

        Log.e("dataaa",ads.getPrice_type());
        if (ads.getPrice_type().equals("price"))
        {
            tv_myofferads.setText(mcontext.getResources().getString(R.string.seealloffers));
        }
        else {
            tv_myofferads.setText(mcontext.getResources().getString(R.string.seeallcomments));
        }


        //        category
        //        category
        category_values.add(new Category_values("Ad id",ads.getId(),"",
                "",""));
        if (ads.getCategory().length()>0){
            try {
                JSONArray jsonArray=new JSONArray(ads.getCategory());
                for (int i=2;i<jsonArray.length();i++){
                    JSONObject jsonObject=jsonArray.getJSONObject(i);
                    category_values.add(new Category_values(ParseString(jsonObject,"tital"),ParseString(jsonObject,"seletecvalue"),ParseString(jsonObject,"id"),
                            ParseString(jsonObject,"value"),ParseString(jsonObject,"image")));

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            detail_adapter_recyclerview=new Detail_Adapter_recyclerview(mcontext,category_values);
            recyclerview.setAdapter(detail_adapter_recyclerview);
        }

        logg("ads.getVerify_phone()",ads.getVerify_phone()+"--");
        if (ads.getVerify_phone().equals("true"))
        {
            rl_phonenoverify.setVisibility(View.GONE);
        }else {
            rl_phonenoverify.setVisibility(View.VISIBLE);
        }


        if (ads.getMain_category().equals("4")){
            tv_shopname.setText(ads.getShop_name());
        }

        if (ads.getBoost_type().equals("0")){
            iv_boost.setVisibility(View.VISIBLE);
            iv_boost.setImageDrawable(getResources().getDrawable(R.drawable.boost_ic_rocket));
        }else if (ads.getBoost_type().equals("1")){
            iv_boost.setVisibility(View.VISIBLE);
            iv_boost.setImageDrawable(getResources().getDrawable(R.drawable.boost_ic_feature));
        }else {
            iv_boost.setVisibility(View.GONE);
        }

    }


    @Override
    public void onBackPressed() {
        finish();
    }
}
