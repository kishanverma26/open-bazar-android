package com.delainetech.open_bazar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import io.paperdb.Paper;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.delainetech.open_bazar.Custom_classes.DelayedProgressDialog;
import com.delainetech.open_bazar.Custom_classes.MySingleton;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;
import com.facebook.accountkit.AccountKit;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.delainetech.open_bazar.Utils.Common.logg;

public class Setting_Activity extends AppCompatActivity implements View.OnClickListener,ParamKeys {

    RecyclerView recyclerview;;
    Context mcoxt;
    Toolbar toolbar;
    TextView tv_logout,tv_cityname,tv_city,tv_blockuser,tv_term,tv_privay;

    UserSharedPreferences preferences,userSharedPreferences;
    private DatabaseReference mRootRef;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        mcoxt=Setting_Activity.this;
        initializ();
    }

    private void initializ() {
        mRootRef = FirebaseDatabase.getInstance().getReference();
        preferences=UserSharedPreferences.getInstance(this);
        userSharedPreferences=new UserSharedPreferences(this,"");
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);

        tv_logout=(TextView)findViewById(R.id.tv_logout);
        tv_logout.setOnClickListener(this);
        tv_blockuser=(TextView)findViewById(R.id.tv_blockuser);
        tv_blockuser.setOnClickListener(this);
        tv_term=(TextView)findViewById(R.id.tv_term);
        tv_term.setOnClickListener(this);
        tv_privay=(TextView)findViewById(R.id.tv_privay);
        tv_privay.setOnClickListener(this);
        tv_cityname=(TextView)findViewById(R.id.tv_cityname);
        tv_cityname.setOnClickListener(this);
        tv_city=(TextView)findViewById(R.id.tv_city);
        tv_city.setOnClickListener(this);


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    Intent intent;

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_logout:
                online_status(ServerValue.TIMESTAMP);
                logout(this);
                AccountKit.logOut();

                Intent i=new Intent(mcoxt,Login_Activity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                Paper.book().destroy();
                finish();
                break;
            case R.id.tv_city:
                startActivity(new Intent(mcoxt,Location_picker_Activity.class));
                break;
            case R.id.tv_privay:
                String url =  weburl+"privacy-policy";
                String title="Privacy Policy";
                intent=new Intent(mcoxt,WebView_Activity.class);
                intent.putExtra("text",title);
                intent.putExtra("url",url);
                startActivity(intent);
                break;
            case R.id.tv_term:
                String url1 = weburl+"terms-of-use";
                String title1="Terms & Conditions";
                Intent intent1=new Intent(mcoxt,WebView_Activity.class);
                intent1.putExtra("text",title1);
                intent1.putExtra("url",url1);
                startActivity(intent1);

                break;

            case R.id.tv_blockuser:
                startActivity(new Intent(mcoxt,Drawer_Frame_Activity.class).putExtra(Pos,15).putExtra(Title,"Blocked Users"));
                break;


        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        tv_cityname.setText(preferences.getaddress());
    }


    public void  logout(final Context context){

//        final DelayedProgressDialog pro=new DelayedProgressDialog();
//
//        pro.show(getSupportFragmentManager(),"");


        StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "logout", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                pro.cancel();
                preferences.Clear();
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
//                Common.Snakbar(Common.volleyerror(volleyError),tv_city);
                preferences.Clear();
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(Id,preferences.getuserid());
                params.put("push_id",userSharedPreferences.getplayerid());
                params.put("device_id",userSharedPreferences.getdeviceid());
                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(context).addToRequestQueue(stringRequest);


    }

    private void online_status(Object status){
        mRootRef.child("Users").child("user"+preferences.getuserid()).child("online").setValue(status);

    }
}
