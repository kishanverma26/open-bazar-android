package com.delainetech.open_bazar;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.SimpleMultiPartRequest;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.delainetech.open_bazar.Adapter.Ad_Category_Preview_adapter;
import com.delainetech.open_bazar.Adapter.Ad_Edit_Image_recycler_adapter;
import com.delainetech.open_bazar.Adapter.Custom_Spinner_Adapter;
import com.delainetech.open_bazar.Adapter.Edit_Ad_Category_Preview_adapter;
import com.delainetech.open_bazar.Category.Category_Activity;
import com.delainetech.open_bazar.Chat.FirbaseCommon;
import com.delainetech.open_bazar.Models.Category_values;
import com.delainetech.open_bazar.Models.Gallery;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputLayout;
import com.theartofdev.edmodo.cropper.CropImage;
import com.zfdang.multiple_images_selector.ImagesSelectorActivity;
import com.zfdang.multiple_images_selector.SelectorSettings;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static com.delainetech.open_bazar.Utils.Common.ParseString;
import static com.delainetech.open_bazar.Utils.Common.Parsintentvalue;
import static com.delainetech.open_bazar.Utils.Common.logg;


public class Adpost_Edit_Activity extends AppCompatActivity implements ParamKeys,View.OnClickListener {

    Toolbar toolbar;
    Context mcontext;
    RecyclerView image_recyclerview;
    LinearLayoutManager linearLayoutManager;
    Ad_Edit_Image_recycler_adapter adapter;
    HorizontalScrollView scrollView;
    ImageView iv_add;
    TextView tv_category,tv_tital,tv_location;
    RelativeLayout rl_category;
    Button bt_insertad;
//    Add image
    LinearLayout ll_addimage;
    List<Gallery> galleryMedias_images = new ArrayList<>();

    int Replace_REQUEST_CODE_GALLERY = 2;
    int REQUEST_CODE_GALLERY = 1;
    int Image_limit=30;
    UserSharedPreferences preferences;
    String[] permissionsRequired = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    EditText et_title,et_desc,et_mobile,et_price;

    ProgressDialog dialog;
    Spinner spinner;
    TextInputLayout it_price;
    LinearLayout ll_price;
    String price_type="price",price="0",id="",category="",shopname="";
    boolean image_change=false;

    //category array list
    ArrayList<Category_values> category_values=new ArrayList<Category_values>();
    RecyclerView rv_cat;
    LinearLayoutManager catlinearLayoutManager;
    Edit_Ad_Category_Preview_adapter ad_category_preview_adapter;

    RelativeLayout rl_cat_recyclerview;
   public String Main_cat_id="0";
    CardView cv_price,cv_location;
    Switch sw_mobileno;

    //    shop name
    RelativeLayout rl_shopname;
    EditText et_shopname;
    ImageView iv_shopname;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ads_add_activity);
        Common.hideSoftKeyboard(Adpost_Edit_Activity.this);
        mcontext= Adpost_Edit_Activity.this;
        initialize();
    }

    private void initialize() {
        dialog=new ProgressDialog(this);
        preferences=UserSharedPreferences.getInstance(mcontext);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);

        rl_shopname=(RelativeLayout)findViewById(R.id.rl_shopname);
        et_shopname=(EditText)findViewById(R.id.et_shopname);
        iv_shopname=(ImageView) findViewById(R.id.iv_shopname);
        iv_shopname.setOnClickListener(this);

        cv_location=(CardView) findViewById(R.id.cv_location);
        cv_price=(CardView) findViewById(R.id.cv_price);
        cv_location.setVisibility(View.VISIBLE);
        cv_price.setVisibility(View.VISIBLE);
        sw_mobileno=(Switch) findViewById(R.id.sw_mobileno);
        rl_cat_recyclerview=(RelativeLayout) findViewById(R.id.rl_cat_recyclerview);
        ll_price=(LinearLayout) findViewById(R.id.ll_price);
        it_price=(TextInputLayout) findViewById(R.id.it_price);
        bt_insertad=(Button) findViewById(R.id.bt_insertad);
       et_price=(EditText) findViewById(R.id.et_price);
        et_mobile=(EditText) findViewById(R.id.et_mobile);
        et_title=(EditText) findViewById(R.id.et_title);
        et_desc=(EditText) findViewById(R.id.et_desc);
        ll_addimage=(LinearLayout)findViewById(R.id.ll_addimage);
        ll_addimage.setOnClickListener(this);
        bt_insertad.setOnClickListener(this);

        bt_insertad.setText("SAVE AND UPDATE YOUR AD");

        //add category
        tv_category=(TextView) findViewById(R.id.tv_category);
        tv_category.setOnClickListener(this);
//        tv_insertad=(TextView) findViewById(R.id.tv_insertad);
//        tv_insertad.setOnClickListener(this);

        tv_tital=(TextView) findViewById(R.id.tv_tital);
        tv_tital.setText("Edit Ad");
        tv_location=(TextView) findViewById(R.id.tv_location);
        tv_location.setOnClickListener(this);

        //Category
        rl_category=(RelativeLayout) findViewById(R.id.rl_category);
        rl_category.setOnClickListener(this);

        //add image
        iv_add=(ImageView) findViewById(R.id.iv_add);
        iv_add.setOnClickListener(this);
        scrollView=(HorizontalScrollView)findViewById(R.id.horizantal_scroll);
        image_recyclerview=(RecyclerView)findViewById(R.id.image_recyclerview);
        linearLayoutManager=new LinearLayoutManager(mcontext,LinearLayoutManager.HORIZONTAL,false);
        image_recyclerview.setLayoutManager(linearLayoutManager);

        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0,iv_add.getBottom());
            }
        });

        spinner=(Spinner) findViewById(R.id.spinner);
        String ar[]={"Price", "Ask For Price", "Exchange", "Free"};
        Custom_Spinner_Adapter adapter1=new Custom_Spinner_Adapter(this,ar,R.color.dark_text_color);
        spinner.setAdapter(adapter1);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position>0){
                    ll_price.setVisibility(View.GONE);
                }else {
                    ll_price.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        et_title.setText(Parsintentvalue("tital", Adpost_Edit_Activity.this));
        et_mobile.setText(Parsintentvalue("mobile", Adpost_Edit_Activity.this));
        et_desc.setText(Parsintentvalue("des", Adpost_Edit_Activity.this));
        tv_location.setText(Parsintentvalue("city", Adpost_Edit_Activity.this));
        price_type=Parsintentvalue("price_type", Adpost_Edit_Activity.this);
        id=Parsintentvalue("id", Adpost_Edit_Activity.this);
        category=Parsintentvalue("category", Adpost_Edit_Activity.this);
        try {
        if (Parsintentvalue("image", Adpost_Edit_Activity.this).length()>0) {
            ll_addimage.setVisibility(View.GONE);
            scrollView.setVisibility(View.VISIBLE);
            JSONArray jsonArray = null;

                jsonArray = new JSONArray(Parsintentvalue("image", Adpost_Edit_Activity.this));

            for (int i = 0; i < jsonArray.length(); i++) {
                if (i==0) {
                    image0=jsonArray.getJSONObject(i).getString("image");
                    galleryMedias_images.add(new Gallery(jsonArray.getJSONObject(i).getString("image"), true));
                }else {
                    galleryMedias_images.add(new Gallery(jsonArray.getJSONObject(i).getString("image"), false));

                }
            }
        }


        if (category.length()>0){
            JSONArray jsonArray=new JSONArray(category);
            for (int i=0;i<jsonArray.length();i++){
                JSONObject jsonObject=jsonArray.getJSONObject(i);
                category_values.add(new Category_values(ParseString(jsonObject,"tital"),ParseString(jsonObject,"seletecvalue"),ParseString(jsonObject,"id"),
                        ParseString(jsonObject,"value"),ParseString(jsonObject,"image")));

            }

            //category recycler view
            rv_cat=(RecyclerView)findViewById(R.id.rv_cat);
            catlinearLayoutManager=new LinearLayoutManager(mcontext);
            rv_cat.setLayoutManager(catlinearLayoutManager);
            rv_cat.setNestedScrollingEnabled(false);

            if (category_values.size()>0){
                rl_category.setVisibility(View.GONE);
                rl_cat_recyclerview.setVisibility(View.VISIBLE);

                ad_category_preview_adapter=new Edit_Ad_Category_Preview_adapter(this,category_values);
                rv_cat.setAdapter(ad_category_preview_adapter);

            }
        }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Image_limit=Image_limit-galleryMedias_images.size();
        if (price_type.equals("price"))
        {
           spinner.setSelection(0);
           et_price.setText(Parsintentvalue("price", Adpost_Edit_Activity.this));
            ll_price.setVisibility(View.VISIBLE);
        }else if (price_type.equals("ask for price"))
        {
            spinner.setSelection(1);

            ll_price.setVisibility(View.GONE);
        }else if (price_type.equals("exchange"))
        {
            ll_price.setVisibility(View.GONE);
            spinner.setSelection(2);

        }else if (price_type.equals("free"))
        {
            ll_price.setVisibility(View.GONE);
            spinner.setSelection(3);

        }


        adapter=new Ad_Edit_Image_recycler_adapter(this,galleryMedias_images);
        image_recyclerview.setAdapter(adapter);
        image_recyclerview.setNestedScrollingEnabled(true);

//    maincat

        Main_cat_id=getIntent().getStringExtra("main_cat_id");
        if (getIntent().getStringExtra("hide_mobileno").equals("true")){
            sw_mobileno.setSelected(true);
        }else {
            sw_mobileno.setSelected(false);
        }

        if (Main_cat_id.equals("4")){
         rl_shopname.setVisibility(View.VISIBLE);
             et_shopname.setVisibility(View.VISIBLE);
             iv_shopname.setVisibility(View.GONE);
        et_shopname.setText(getIntent().getStringExtra("shop_name"));
        }

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.add_ad,menu);
        menu.findItem(R.id.item_reset).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
             finish();
                break;


            case R.id.item_post:
//                Common.Snakbar("Your ad is posted now",toolbar);
//                finish();

                checkvalidateion();
//                logg("logg",spinner.getSelectedIndex()+"");
                break;


             case R.id.item_reset:
//                 posted_poupup();
                 alert(mcontext);

                break;



        }
        return super.onOptionsItemSelected(item);
    }


    public void alert(Context mcontext){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mcontext);
        alertDialog.setTitle(R.string.reset_all_fields);
        alertDialog.setMessage(getResources().getString(R.string.areyoushure_reset));
        alertDialog.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                et_title.setText("");
                et_desc.setText("");
                et_price.setText("");
                et_mobile.setText("");
                ll_addimage.setVisibility(View.VISIBLE);
                scrollView.setVisibility(View.GONE);
                galleryMedias_images.clear();
                adapter.notifyDataSetChanged();
                Common.Snakbar(getResources().getString(R.string.reset_all_fields),toolbar);


            }
        });
        alertDialog.setNegativeButton(getResources().getString(R.string.No), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog1= alertDialog.create();
        alertDialog1.show();

        alertDialog1.getButton(alertDialog1.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorAccent));
        alertDialog1.getButton(alertDialog1.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorAccent));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_category:
                startActivity(new Intent(mcontext, Category_Activity.class));

                break;
            case R.id.rl_category:
                startActivity(new Intent(mcontext,Category_Activity.class));

                break;
            case R.id.ll_addimage:

              if ( Common.hasPermissions(this,permissionsRequired)){
                take_picture(mcontext);
              }else {
                  Common.askpermission(this,permissionsRequired);
              }
                break;
            case R.id.iv_add:
                take_picture(mcontext);
                break;

//            case R.id.tv_insertad:
//                checkvalidateion();
//                break;

            case R.id.tv_location:
                startActivityForResult(new Intent(mcontext,Location_picker_Activity.class),LOCTAION_CITY);

                break;
           case  R.id.bt_insertad:
               checkvalidateion();
             break;

        }
    }
    private ArrayList<String> mResults = new ArrayList<>();
    private void openGallery() {
//        take_album_or_gallry(mcontext);

        Intent intent = new Intent(Adpost_Edit_Activity.this, ImagesSelectorActivity.class);
        // max number of images to be selected
        intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, Image_limit);
        // min size of image which will be shown; to filter tiny images (mainly icons)
        intent.putExtra(SelectorSettings.SELECTOR_MIN_IMAGE_SIZE, 100000);
        // show camera or not
        intent.putExtra(SelectorSettings.SELECTOR_SHOW_CAMERA, false);
        // pass current selected images as the initial value
        intent.putStringArrayListExtra(SelectorSettings.SELECTOR_INITIAL_SELECTED_LIST, mResults);
        // start the selector
        startActivityForResult(intent, REQUEST_CODE_GALLERY);

//        startActivityForResult(new GalleryHelper().setMultiselection(true)
//                .setMaxSelectedItems(Image_limit)
//                .setShowVideos(false)
//                .getCallingIntent(this), REQUEST_CODE_GALLERY);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        logg("requestcode=",requestCode+", resultecode="+resultCode);




        if (requestCode == REQUEST_CODE_GALLERY ) {
            if (data!=null){

                if(resultCode == RESULT_OK) {
                    mResults = data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS);
                    assert mResults != null;

                    // show results in textview
                    StringBuilder sb = new StringBuilder();
                    sb.append(String.format("Totally %d images selected:", mResults.size())).append("\n");
                    for(String result : mResults) {
                        galleryMedias_images.add(new Gallery(result,false));
                        sb.append(result).append("\n");
                    }
                    mResults.clear();

                }

                Image_limit=30;
               Image_limit= Image_limit-galleryMedias_images.size();

                if (galleryMedias_images.size()>0){
                   ll_addimage.setVisibility(View.GONE);
                    scrollView.setVisibility(View.VISIBLE);
                    adapter=new Ad_Edit_Image_recycler_adapter(this,galleryMedias_images);
                    image_recyclerview.setAdapter(adapter);
                }else {
                    ll_addimage.setVisibility(View.VISIBLE);
                    scrollView.setVisibility(View.GONE);
                }


            }

        }else if (requestCode==Replace_REQUEST_CODE_GALLERY){
            if (data!=null){
                mResults = data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS);
                assert mResults != null;

                // show results in textview
                StringBuilder sb = new StringBuilder();
                sb.append(String.format("Totally %d images selected:", mResults.size())).append("\n");
                for(String result : mResults) {
                    galleryMedias_images.set(edit_imagepos,new Gallery(result,galleryMedias_images.get(edit_imagepos).isIs_main()));

                }
                mResults.clear();
              //                Image_limit=30;
//                Image_limit= Image_limit-galleryMedias_images.size();
        adapter.notifyDataSetChanged();
//                if (galleryMedias_images.size()>0){
//                    ll_addimage.setVisibility(View.GONE);
//                    scrollView.setVisibility(View.VISIBLE);
//                    adapter=new Ad_Image_recycler_adapter(this,galleryMedias_images);
//                    image_recyclerview.setAdapter(adapter);
//                }else {
//                    ll_addimage.setVisibility(View.VISIBLE);
//                    scrollView.setVisibility(View.GONE);
//                }

            }
        }

        if (requestCode == REQUEST_CODE_IMAGE_CAPTURE && resultCode== RESULT_OK) {
            if (mOutputFileUri != null) {
                String uri = mOutputFileUri.toString();
                Log.e("uri", uri);
                galleryMedias_images.add(new Gallery(mOutputFileUri.toString(),false));
                adapter.notifyDataSetChanged();

                if (galleryMedias_images.size()>0){
                    ll_addimage.setVisibility(View.GONE);
                    scrollView.setVisibility(View.VISIBLE);
                    adapter=new Ad_Edit_Image_recycler_adapter(this,galleryMedias_images);
                    image_recyclerview.setAdapter(adapter);

                }else {
                    ll_addimage.setVisibility(View.VISIBLE);
                    scrollView.setVisibility(View.GONE);
                }


//                imageUriList.add(new Post(UUID.randomUUID().toString(), mOutputFileUri, mOutputFileUri.toString()));
//                startCrop(imageUriList.get(0).getUri());
            }
        }else if (requestCode == Replace_REQUEST_CODE_IMAGE_CAPTURE && resultCode== RESULT_OK){
            if (mOutputFileUri != null) {
                String uri = mOutputFileUri.toString();
                Log.e("uri", uri);
                galleryMedias_images.set(edit_imagepos,new Gallery(mOutputFileUri.toString(),false));
                adapter.notifyDataSetChanged();

                if (galleryMedias_images.size()>0){
                    ll_addimage.setVisibility(View.GONE);
                    scrollView.setVisibility(View.VISIBLE);
                    adapter=new Ad_Edit_Image_recycler_adapter(this,galleryMedias_images);
                    image_recyclerview.setAdapter(adapter);

                }else {
                    ll_addimage.setVisibility(View.VISIBLE);
                    scrollView.setVisibility(View.GONE);
                }


//                imageUriList.add(new Post(UUID.randomUUID().toString(), mOutputFileUri, mOutputFileUri.toString()));
//                startCrop(imageUriList.get(0).getUri());
            }
        }



        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                galleryMedias_images.set(edit_imagepos,new Gallery(resultUri.toString(),false));
                adapter.notifyDataSetChanged();

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }

        if ((requestCode==LOCTAION_CITY) && (resultCode== RESULT_OK)){
            if (data!=null) {
                tv_location.setText(data.getStringExtra("city"));
            }
        }


        if (requestCode==ADS_CATEGORY_RESULT_CODE && resultCode==RESULT_OK){
            category_values.clear();
            category_values = (ArrayList<Category_values>) data.getSerializableExtra("list");
            logg("sizeitem",category_values.size()+"");
            if (category_values.size()>0){
                rl_category.setVisibility(View.GONE);
                rv_cat.setVisibility(View.VISIBLE);

                ad_category_preview_adapter=new Edit_Ad_Category_Preview_adapter(this,category_values);
                rv_cat.setAdapter(ad_category_preview_adapter);
//                ad_category_preview_adapter.notifyDataSetChanged();

            }

        }

    }



//    edit_image
        int edit_imagepos=0;
    public void edit_image(Context mcontext, final int position) {

        edit_imagepos=position;
        View view = getLayoutInflater().inflate(R.layout.edit_picture_bottom_sheet, null);

        final BottomSheetDialog dialog = new BottomSheetDialog(mcontext);
        LinearLayout ll=(LinearLayout)view.findViewById(R.id.ll);
        TextView tv_edit=(TextView)view.findViewById(R.id.tv_edit);
            tv_edit.setVisibility(View.GONE);
         TextView tv_camera=(TextView)view.findViewById(R.id.tv_camera);
        TextView tv_gallery=(TextView)view.findViewById(R.id.tv_gallery);
        TextView tv_delete=(TextView)view.findViewById(R.id.tv_delete);
        TextView tv_main_image=(TextView)view.findViewById(R.id.tv_main_image);
        WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.dimAmount = 0.0F;

        dialog.getWindow().setAttributes(wlmp);
//        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        tv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                Log.e("uri",galleryMedias_images.get(edit_imagepos).getUri()+"");
                Log.e("uri1",Uri.parse(galleryMedias_images.get(edit_imagepos).getUri())+"");

                if (galleryMedias_images.get(edit_imagepos).getUri().contains("file")) {
                    CropImage.activity(Uri.parse(galleryMedias_images.get(edit_imagepos).getUri()))
                            .start(Adpost_Edit_Activity.this);
                }else if (galleryMedias_images.get(edit_imagepos).getUri().contains("emulated/0")){
                    CropImage.activity(Uri.fromFile(new File(galleryMedias_images.get(edit_imagepos).getUri())))
                            .start(Adpost_Edit_Activity.this);

                }else {
                    logg("image",Uri.parse(imageurl+galleryMedias_images.get(edit_imagepos).getUri())+"");

                    CropImage.activity(Uri.parse(imageurl+galleryMedias_images.get(edit_imagepos).getUri()))
                            .start(Adpost_Edit_Activity.this);
                }
            }
        });
        tv_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                // start multiple photos selector
                Intent intent = new Intent(Adpost_Edit_Activity.this, ImagesSelectorActivity.class);
                // max number of images to be selected
                intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 1);
                // min size of image which will be shown; to filter tiny images (mainly icons)
                intent.putExtra(SelectorSettings.SELECTOR_MIN_IMAGE_SIZE, 100000);
                // show camera or not
                intent.putExtra(SelectorSettings.SELECTOR_SHOW_CAMERA, false);
                // pass current selected images as the initial value
                intent.putStringArrayListExtra(SelectorSettings.SELECTOR_INITIAL_SELECTED_LIST, mResults);
                // start the selector
                startActivityForResult(intent, Replace_REQUEST_CODE_GALLERY);


            }
        });
        tv_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File file = new File(Environment.getExternalStorageDirectory(),
                        UUID.randomUUID().toString() + "img.jpg");
                mOutputFileUri = FileProvider.getUriForFile(Adpost_Edit_Activity.this, "com.open_bazar.fileprovider", file);
                Log.e("uri", ("file:" + file.getAbsolutePath()) + "");
                intent.putExtra(MediaStore.EXTRA_OUTPUT, mOutputFileUri);
                mOutputFileUri = Uri.parse("file:" + file.getAbsolutePath());
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivityForResult(intent, Replace_REQUEST_CODE_IMAGE_CAPTURE);
            }
        });

        tv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            dialog.dismiss();
                galleryMedias_images.remove(position);
            adapter.notifyDataSetChanged();

                if (galleryMedias_images.size()==0){
                    ll_addimage.setVisibility(View.VISIBLE);
                    scrollView.setVisibility(View.GONE);
                }

            }
        });
        tv_main_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<galleryMedias_images.size();i++){
                    galleryMedias_images.set(i,new Gallery(galleryMedias_images.get(i).getUri(),false));

                }
                galleryMedias_images.add(0,new Gallery(galleryMedias_images.get(edit_imagepos).getUri(),true));
                galleryMedias_images.remove(edit_imagepos+1);
                adapter.notifyDataSetChanged();
                dialog.dismiss();

            }
        });


        dialog.setContentView(view);
        dialog.show();
    }



    public void take_picture(Context mcontext) {

        View view = getLayoutInflater().inflate(R.layout.take_picture_dialog_bottom_sheet, null);

        final BottomSheetDialog dialog = new BottomSheetDialog(mcontext);
        LinearLayout ll=(LinearLayout)view.findViewById(R.id.ll);
        TextView tv_camera=(TextView)view.findViewById(R.id.tv_camera);
        TextView tv_gallery=(TextView)view.findViewById(R.id.tv_gallery);
        WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.dimAmount = 0.0F;

        dialog.getWindow().setAttributes(wlmp);
//        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        tv_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                open_camera();
                dialog.dismiss();
            }
        });

        tv_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openGallery();
                dialog.dismiss();
            }
        });


        dialog.setContentView(view);
        dialog.show();
    }


    private Uri mOutputFileUri = null;
    private static final int REQUEST_CODE_IMAGE_CAPTURE = 12121;
    private static final int Replace_REQUEST_CODE_IMAGE_CAPTURE = 1211;

    private void open_camera(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStorageDirectory(),
                UUID.randomUUID().toString() + "img.jpg");
        mOutputFileUri = FileProvider.getUriForFile(Adpost_Edit_Activity.this, "com.open_bazar.fileprovider", file);
        Log.e("uri", ("file:" + file.getAbsolutePath()) + "");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mOutputFileUri);
        mOutputFileUri = Uri.parse("file:" + file.getAbsolutePath());
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(intent, REQUEST_CODE_IMAGE_CAPTURE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    int imageuploadcount=0,image_capture_count=0;

    private void upload_image(final int i){
        dialog.setTitle("Uploading image "+i+"/"+galleryMedias_images.size());

        SimpleMultiPartRequest request = new SimpleMultiPartRequest(Request.Method.POST, baseurl+"uploadimage", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                logg("response",response+"");
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){

                        image_object =new JSONObject();
                        image_object.put("image",object.getString("image"));
                        if (i==0){
                            image0=object.getString("image");
                        }
                        logg("jsonarrayimg",object.getString("image")+"");
                            jsonArray.put(i,image_object);
                        Common.logg("jsonarrayy1", jsonArray + "");
                        Common.logg("jsonarrayysize", i + "");
                        Common.logg("jsonarrayylistsize", galleryMedias_images.size() + "");
                        imageuploadcount++;
                      if (imageuploadcount==image_capture_count) {
                          Common.logg("jsonarrayy2", jsonArray + "");
                          post_ad();

                      }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    dialog.dismiss();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            Common.volleyerror(error);
                dialog.dismiss();
            }
        });
             request.addFile("image", Uri.parse(galleryMedias_images.get(i).getUri().replace("file:",""))+"");

        logg("params",request.getFilesToUpload()+"");
        request.setOnProgressListener(new Response.ProgressListener() {
            @Override
            public void onProgress(long transferredBytes, long totalSize) {
                int percentage = (int) ((transferredBytes / ((float) totalSize)) * 100);
            logg("percentage",percentage+"");
//                progress.setProgress(percentage);
            }
        });
        RequestQueue mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        mRequestQueue.add(request);



    }

    JSONObject image_object;
    JSONArray jsonArray=null;
    JSONArray category_array=null;
    private void checkvalidateion() {


            logg("size",category_values.size()+"");
        if (category_values.size() > 0) {
            category_array = new JSONArray();
            for (int i = 0; i < category_values.size(); i++) {
                JSONObject object = new JSONObject();
                try {
                    object.put("id", category_values.get(i).getId());
                    object.put("tital", category_values.get(i).getTital());
                    object.put("value", category_values.get(i).getValue());
                    object.put("image", category_values.get(i).getImage());
                    object.put("seletecvalue", category_values.get(i).getSeletedvalue());
                    category_array.put(object);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
            Log.e("jsonarraycategory", category_array + "");

            if (spinner.getSelectedItemPosition() == 0) {
                price_type = "price";
                if (et_price.getText().toString().trim().length() > 0) {
                    price = et_price.getText().toString().trim();
                } else {
                    price = "";
                }
            } else if (spinner.getSelectedItemPosition() == 1) {
                price = "0";
                price_type = "ask for price";

            } else if (spinner.getSelectedItemPosition() == 2) {
                price = "0";
                price_type = "exchange";
            } else if (spinner.getSelectedItemPosition() == 3) {
                price = "0";
                price_type = "free";
            }

            Common.hideSoftKeyboard(Adpost_Edit_Activity.this);

            if (Main_cat_id.equals("4") && !(et_shopname.getText().toString().length()>0) ){
                Common.Snakbar("Please enter shop name",et_shopname);
            }else {


                if (et_title.getText().toString().trim().length() > 0 && et_desc.getText().toString().trim().length() > 0 &&
                    et_mobile.getText().toString().trim().length() > 0 && price.trim().length() > 0) {
                if (!tv_location.getText().toString().equals("City")) {
                    dialog.setMessage("Please wait......");
                    dialog.show();
                    if (galleryMedias_images.size() > 0) {
                        imageuploadcount = 0;
                        try {
                            if (Parsintentvalue("image", Adpost_Edit_Activity.this).length() > 0) {
                                jsonArray = new JSONArray();
                                JSONObject jsonObject = null;
                                for (int i=0;i<galleryMedias_images.size();i++){
                                    jsonObject=new JSONObject();
                                    jsonObject.put("image",galleryMedias_images.get(i).getUri());
                                    jsonArray.put(jsonObject);
                                }

                                logg("array",jsonArray+"");
                            } else {
                                jsonArray = new JSONArray();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        for (int i = 0; i < galleryMedias_images.size(); i++) {
                            if (galleryMedias_images.get(i).getUri().contains("emulated/0")) {
                                image_capture_count++;
                                image_change = true;
                                upload_image(i);
                            }
                        }
                        if (!image_change) {
                            post_ad();
                        }


                    } else {
                        post_ad();
                    }
                } else {
                    Common.Snakbar("Please select city", et_title);
                }
            } else {
                Common.Snakbar("Please fill all fields", et_title);

            }
        }
        }else {
            Common.Snakbar("Please select what are you advertising?", et_title);

        }
    }

    String image0="";
    public void  post_ad(){

//        final DelayedProgressDialog pro=new DelayedProgressDialog();
//
//        pro.show(getSupportFragmentManager(),"");


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "update_ads", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                pro.cancel();
                logg("updated_ad",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){
                        dialog.dismiss();
                        Toast.makeText(mcontext, "Ad is Updated", Toast.LENGTH_SHORT).show();
                        FirbaseCommon.regiter_adpost(id,object.getJSONObject("data").getString("ad_title")
                                ,image0,object.getJSONObject("data").getString("price")
                                ,object.getJSONObject("data").getString("mobile")
                                ,object.getJSONObject("data").getString("hide_mobileno"));


                        Intent i=new Intent();
                        preferences.setadmobile(et_mobile.getText().toString());
                        i.putExtra("tital",object.getJSONObject("data").getString("ad_title"));
                        i.putExtra("ad_description",object.getJSONObject("data").getString("ad_description"));
                        i.putExtra("mobile",object.getJSONObject("data").getString("mobile"));
                        i.putExtra("price",object.getJSONObject("data").getString("price"));
                        i.putExtra("price_type",object.getJSONObject("data").getString("price_type"));
                        i.putExtra("images",object.getJSONObject("data").getString("images"));
                        i.putExtra("city",object.getJSONObject("data").getString("city"));
                        i.putExtra("hide_mobileno",object.getJSONObject("data").getString("hide_mobileno"));
                        i.putExtra("shop_name",object.getJSONObject("data").getString("shop_name"));
                      i.putExtra(Category,object.getJSONObject("data").getString(Category));


                      setResult(Activity.RESULT_OK,i);
                       finish();

                    }

                }
                catch (Exception e){
//                    pro.cancel();
                    Log.e("error","e",e);
                    dialog.dismiss();
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),et_desc);
                dialog.dismiss();
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(Id,id);
                params.put(City,preferences.getaddress());
                params.put(Ad_title,et_title.getText().toString().trim());
                params.put(Ad_description,et_desc.getText().toString().trim());
                params.put(Category,category_array+"");
                params.put(Mobile,et_mobile.getText().toString());
                if (jsonArray!=null) {
                    params.put(Images, jsonArray + "");
                }
                 params.put(Price,price);
                params.put("price_type",price_type);
                if (sw_mobileno.isSelected()) {
                    params.put("hide_mobileno", "true");
                }else {
                    params.put("hide_mobileno", "false");
                }

                if (Main_cat_id.equals("4")){
                    params.put("shop_name", et_shopname.getText().toString().trim());

                }

                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        MySingleton.getInstance(context).addToRequestQueue(stringRequest);
        RequestQueue mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        mRequestQueue.add(stringRequest);

    }


    @Override
    protected void onResume() {
        super.onResume();
        if (Image_limit==30){
            iv_add.setVisibility(View.GONE);
        }else {
            iv_add.setVisibility(View.VISIBLE);
        }
    }
}
