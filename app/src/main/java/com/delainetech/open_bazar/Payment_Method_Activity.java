package com.delainetech.open_bazar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.delainetech.open_bazar.Custom_classes.DelayedProgressDialog;
import com.delainetech.open_bazar.Custom_classes.MySingleton;
import com.delainetech.open_bazar.Models.ADS;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import io.paperdb.Paper;

import static com.delainetech.open_bazar.Utils.Common.ParseString;
import static com.delainetech.open_bazar.Utils.Common.logg;
import static com.delainetech.open_bazar.Utils.ParamKeys.User_id;
import static com.delainetech.open_bazar.Utils.ParamKeys.baseurl;

public class Payment_Method_Activity extends AppCompatActivity implements View.OnClickListener , ParamKeys {

    Context mcoxt;
    UserSharedPreferences preferences;
    ImageView iv_back;
    TextView tv_credit,tv_days,tv_subtotal,tv_total,tv_tc,tv_contactus;
    AppCompatButton bt_send;
    String credit="",days="",addid="",type="";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_method);

        mcoxt=Payment_Method_Activity.this;
        preferences=UserSharedPreferences.getInstance(mcoxt);
        credit=getIntent().getStringExtra("credit");
        days=getIntent().getStringExtra("days");
        addid=getIntent().getStringExtra("addid");
        type=getIntent().getStringExtra("type");

        tv_tc=(TextView) findViewById(R.id.tv_tc);
        tv_tc.setOnClickListener(this);

        tv_contactus=(TextView)findViewById(R.id.tv_contactus);
        tv_contactus.setOnClickListener(this);
        iv_back=findViewById(R.id.iv_back);
        iv_back.setOnClickListener(this);

        tv_credit=findViewById(R.id.tv_credit);
//        tv_credit.setText(credit+" Credits");
        bt_send=findViewById(R.id.bt_send);
        bt_send.setText("Pay "+credit+" Credits");
        bt_send.setOnClickListener(this);
        tv_days=findViewById(R.id.tv_days);

        tv_subtotal=findViewById(R.id.tv_subtotal);
        tv_subtotal.setText(credit+" Credits");
        tv_total=findViewById(R.id.tv_total);
        tv_total.setText(credit+" Credits");

            if (type.equals("0")){
                tv_days.setText("Rocket ("+days+" Days)");
            }else if (type.equals("1")){
                tv_days.setText("Feature ("+days+" Days)");
            }else {
                tv_days.setText("Repost ("+days+" Days)");
            }
        if (Paper.book("get_total_credits").contains("response")){

            setwallet_credit(Paper.book("get_total_credits").read("response").toString());
        }
        wallet_credit();
    }

    public void  payments(){

        final DelayedProgressDialog pro=new DelayedProgressDialog();

        pro.show(getSupportFragmentManager(),"");


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "boost_ads", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                pro.cancel();
                logg("boost_ads",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("true")){
                        Intent i=new Intent(Payment_Method_Activity.this, Home_Screen_Activity.class);

                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        finish();
                        Toast.makeText(mcoxt, "Your Ad Is Boosted Now....", Toast.LENGTH_SHORT).show();

                    }else {
                        Common.Snakbar(object.getString("message"),tv_credit);
                    }


                }
                catch (Exception e){
                    pro.cancel();
                    Log.e("error","e",e);

                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),tv_credit);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put("userid",preferences.getuserid()+"");
                params.put("ad_id",addid);
                params.put("credit",credit);
                params.put("type",type);
                params.put("days",days);



                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(mcoxt).addToRequestQueue(stringRequest);


    }

    Intent i;
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.bt_send:
                payments();
                break;
            case R.id.tv_tc:
                String url1 = weburl+"terms-of-use";
                String title1="Terms Conditions";
                Intent intent1=new Intent(mcoxt, WebView_Activity.class);
                intent1.putExtra("text",title1);
                intent1.putExtra("url",url1);
                startActivity(intent1);
                break;
            case R.id.tv_contactus:
                i=new Intent(mcoxt, Drawer_Frame_Activity.class);
                i.putExtra(Pos,2);
                i.putExtra(Title,"Contact Us");
                startActivity(i);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    public void  wallet_credit(){

        final DelayedProgressDialog pro=new DelayedProgressDialog();

        pro.show(getSupportFragmentManager(),"");


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "get_total_credits", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                pro.cancel();
                logg("boost_ads",response);
                Paper.book("get_total_credits").write("response",response);
                setwallet_credit(response);
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),tv_credit);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put("userid",preferences.getuserid()+"");


                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(mcoxt).addToRequestQueue(stringRequest);


    }

    private void setwallet_credit(String response) {
        try {
            JSONObject object=new JSONObject(response);
            if (object.getString("status").equals("success")){

                tv_credit.setText(object.getString("Credits")+" Credits");
            }


        }
        catch (Exception e){

            Log.e("error","e",e);

        }
    }


}
