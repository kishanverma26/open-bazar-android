package com.delainetech.open_bazar.onesignel_notification;

import android.content.Context;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;
import com.facebook.accountkit.AccountKit;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.onesignal.OneSignal;

import androidx.multidex.MultiDexApplication;
import io.fabric.sdk.android.Fabric;
import io.paperdb.Paper;

import static com.facebook.FacebookSdk.getApplicationContext;

import com.facebook.drawee.backends.pipeline.Fresco;
/**
 * Created by Kishan on 19-Jun-17.
 */

public class Onesignal_Myapplication extends MultiDexApplication {

    private static Context context;
    String user_id="";



//    firbase chat
private DatabaseReference mUserDatabase;
    private FirebaseAuth mAuth;
    UserSharedPreferences preferences,getPreferences;

    public static Context getContext() {
        return context;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        AccountKit.initialize(getApplicationContext());
        Fabric.with(this, new Crashlytics());
        context = getApplicationContext();
        Paper.init(context);
        Fresco.initialize(getApplicationContext());
        preferences=new UserSharedPreferences(context,"");
     getPreferences=new UserSharedPreferences(context);

        //MyNotificationOpenedHandler : This will be called when a notification is tapped on.
        //MyNotificationReceivedHandler : This will be called when a notification is received while your app is running.
        OneSignal.startInit(this)
                .setNotificationOpenedHandler(new MyNotificationOpenedHandler(this))
                .setNotificationReceivedHandler( new MyNotificationReceivedHandler() )
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .init();

        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                    user_id=userId;
                Common.logg("onsignal_id",userId+"");
                preferences.setplayerid(user_id);

            }
        });
        // Branch logging for debugging
      ;



        if (getPreferences.getlogin()){
            firbase_setuponline();
        }
    }



    private void firbase_setuponline(){
        mAuth = FirebaseAuth.getInstance();

        if(mAuth.getCurrentUser() != null) {

            mUserDatabase = FirebaseDatabase.getInstance()
                    .getReference().child("Users").child("user"+getPreferences.getuserid());

            mUserDatabase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    if (dataSnapshot != null) {

                        mUserDatabase.child("online").onDisconnect().setValue(ServerValue.TIMESTAMP);
                        mUserDatabase.child("image").onDisconnect().setValue(preferences.getimage());
                         mUserDatabase.child("name").onDisconnect().setValue(preferences.getname());

                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }
    }
}
