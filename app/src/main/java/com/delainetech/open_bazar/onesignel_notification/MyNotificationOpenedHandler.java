package com.delainetech.open_bazar.onesignel_notification;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.delainetech.open_bazar.Utils.UserSharedPreferences;
import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Kishan on 19-Jun-17.
 */

public class MyNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {

    UserSharedPreferences userSharedPreferences;
    Context context;

    public MyNotificationOpenedHandler(Context context) {
        this.context = context;
        userSharedPreferences=new UserSharedPreferences(context);
    }

    @Override
    public void notificationOpened(OSNotificationOpenResult result) {
        OSNotificationAction.ActionType actionType = result.action.type;
        JSONObject data = result.notification.payload.additionalData;

        String activityToBeOpened;

        Log.e("onsignaldata1","notification: "+data);
        //While sending a Push notification from OneSignal dashboard
        // you can send an addtional data named "activityToBeOpened" and retrieve the value of it and do necessary operation
        //If key is "activityToBeOpened" and value is "AnotherActivity", then when a user clicks
        //on the notification, AnotherActivity will be opened.
        //Else, if we have not set any additional data MainActivity is opened.
//        try {
        if (data != null) {
//            if (data.has("type") && userSharedPreferences.getIsLoggedIn()){
//
//
//                    if (data.getString("type").equals("diet_request")){
//                        Log.e("onsignaldata11","test");
//                        Intent intent = new Intent(context, My_requests.class);
//                        intent.putExtra("intent","requestactivity");
////                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
////                                Intent.FLAG_ACTIVITY_CLEAR_TASK |
////                                Intent.FLAG_ACTIVITY_NEW_TASK);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//
//                        context.startActivity(intent);
//                    }else {
//                        Log.e("onsignaldata1","test1");
//                        Intent intent = new Intent(context, Activity_Splash_Screen.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
//                                Intent.FLAG_ACTIVITY_CLEAR_TASK |
//                                Intent.FLAG_ACTIVITY_NEW_TASK);
//                        Onesignal_Myapplication.getContext().startActivity(intent);
//                    }
//
//            }else if (data.has("value") && userSharedPreferences.getIsLoggedIn()){
//
//                JSONObject jsonObject=new JSONObject(data.getString("value"));
//                Intent intent = new Intent(context,ChatActivity.class);
//
//                intent.putExtra("user_id",jsonObject.getString("user"));
//                intent.putExtra("user_name",jsonObject.getString("username"));
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//
//               context.startActivity(intent);
//        }
        }
//        } catch (JSONException e) {
//            e.printStackTrace();
//           Log.e("error","",e);
//        }

        //If we send notification with action buttons we need to specidy the button id's and retrieve it to
        //do the necessary operation.
        if (actionType == OSNotificationAction.ActionType.ActionTaken) {
            if (result.action.actionID.equals("ActionOne")) {
                Toast.makeText(Onesignal_Myapplication.getContext(), "ActionOne Button was pressed", Toast.LENGTH_LONG).show();
            } else if (result.action.actionID.equals("ActionTwo")) {
                Toast.makeText(Onesignal_Myapplication.getContext(), "ActionTwo Button was pressed", Toast.LENGTH_LONG).show();
            }
        }
    }

    }
