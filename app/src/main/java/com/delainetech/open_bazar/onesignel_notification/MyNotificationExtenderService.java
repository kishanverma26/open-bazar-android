package com.delainetech.open_bazar.onesignel_notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.delainetech.open_bazar.R;
import com.onesignal.NotificationExtenderService;
import com.onesignal.OSNotificationDisplayedResult;
import com.onesignal.OSNotificationReceivedResult;
import com.onesignal.OneSignal;

import java.math.BigInteger;

import androidx.core.app.NotificationCompat;

/**
 * Created by Kishan on 19-Jun-17.
 */

public class MyNotificationExtenderService extends NotificationExtenderService {
    @Override
    protected boolean onNotificationProcessing(OSNotificationReceivedResult notification) {
        OverrideSettings overrideSettings = new OverrideSettings();
        overrideSettings.extender = new NotificationCompat.Extender() {
            @Override
            public NotificationCompat.Builder extend(NotificationCompat.Builder builder) {
                // Sets the background notification color to Red on Android 5.0+ devices.
                Bitmap icon = BitmapFactory.decodeResource(Onesignal_Myapplication.getContext().getResources(),
                        R.mipmap.ic_launcher);
                builder.setLargeIcon(icon);
                builder.setDefaults(Notification.DEFAULT_ALL);
                builder.setPriority(NotificationManager.IMPORTANCE_HIGH );
                OneSignal.setInFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification);
                return builder.setColor(new BigInteger("7ED321", 16).intValue());
            }
        };

        OSNotificationDisplayedResult displayedResult = displayNotification(overrideSettings);
        Log.d("OneSignalExample", "Notification displayed with id: " + displayedResult.androidNotificationId);

        return true;
    }
}
