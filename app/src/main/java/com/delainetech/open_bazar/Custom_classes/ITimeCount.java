package com.delainetech.open_bazar.Custom_classes;

/**
 * Created by Dell on 23-06-2017.
 */

public interface ITimeCount {

    void OnTickListener(long timetick);
    void OnFinish();
}
