package com.delainetech.open_bazar.Category;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.delainetech.open_bazar.Models.Category;
import com.delainetech.open_bazar.Models.Category_values;
import com.delainetech.open_bazar.Models.Product_values;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

import static com.delainetech.open_bazar.Utils.Common.logg;


/**
 * Created by Kishan on 21-Dec-17.
 */

public class Product_adapter extends RecyclerView.Adapter<Product_adapter.Viewholder> implements ParamKeys {

    Context mcontext;

//    UserSharedPreferences preferences;

    ArrayList<Product_values> categoryArrayList;
    String type;
    public Product_adapter(Context mcontext, ArrayList<Product_values> categoryArrayList, String type) {
        this.mcontext = mcontext;
        this.type = type;
       this.categoryArrayList = categoryArrayList;

    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_item, parent, false);

        return new Viewholder(itemView);
    }

    @Override
    public void onBindViewHolder(Viewholder holder, final int position)
    {
    holder.tv_title.setText(categoryArrayList.get(position).getValue());
        Glide.with(mcontext).load(Common.Image_Loading_Url(categoryArrayList.get(position).getImage())).into(holder.img);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (((Category_Activity) mcontext).category_values.size()>2){
                ((Category_Activity) mcontext).category_values.set(2,new Category_values(categoryArrayList.get(position).getTital(), categoryArrayList.get(position).getValue()
                        ,"",categoryArrayList.get(position).getDataarray()));

                ((Category_Activity) mcontext).category_values.get(2).setImage(categoryArrayList.get(position).getImage());
            }else {
                ((Category_Activity) mcontext).category_values.add(new Category_values(categoryArrayList.get(position).getTital(), categoryArrayList.get(position).getValue()
                        ,"",categoryArrayList.get(position).getDataarray()));
                ((Category_Activity) mcontext).category_values.get(2).setImage(categoryArrayList.get(position).getImage());

            }
            ((Category_Activity) mcontext).getSupportFragmentManager().beginTransaction()
                    .add(R.id.fram_lay,new OtherProductCategory_fragment( categoryArrayList.get(position).getDataarray(),0)).addToBackStack(null).commit();


            //
//            ((Category_Activity) mcontext).close_cat();

        }
    });

    }

    @Override
    public int getItemCount() {
        return categoryArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder{
        TextView tv_title;
        ImageView img;
        public Viewholder(View itemView) {
            super(itemView);
            tv_title=itemView.findViewById(R.id.tv_title);
            img=itemView.findViewById(R.id.img);

          }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
