package com.delainetech.open_bazar.Category;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.delainetech.open_bazar.Models.Category_values;
import com.delainetech.open_bazar.Models.Product_values;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.ParamKeys;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

import static com.delainetech.open_bazar.Utils.Common.logg;


/**
 * Created by Kishan on 21-Dec-17.
 */

public class EditOtherProduct_adapter extends RecyclerView.Adapter<EditOtherProduct_adapter.Viewholder> implements ParamKeys {

    Context mcontext;

//    UserSharedPreferences preferences;

    ArrayList<Product_values> categoryArrayList;
    JSONArray jsonArray;
    String type;
    int pos=0;
    int item_pos=0;

    String value;
    public EditOtherProduct_adapter(Context mcontext, ArrayList<Product_values> categoryArrayList, String type, JSONArray jsonArray,
                                    int pos, String value,int item_pos) {
        this.mcontext = mcontext;
        this.type = type;
        this.jsonArray = jsonArray;
        this.pos = pos;
        this.item_pos = item_pos;
        this.value = value;

       this.categoryArrayList = categoryArrayList;

    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_item, parent, false);

        return new Viewholder(itemView);
    }

    @Override
    public void onBindViewHolder(Viewholder holder, final int position)
    {
        try {
            holder.tv_title.setText(jsonArray.getString(position));

        holder.img.setVisibility(View.GONE);

    holder.itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try {
            if (((Category_Activity) mcontext).category_values.size()>(pos)){
                Log.e("size111",((Category_Activity) mcontext).category_values.size()+"");
                Log.e("size1112",pos+"");

                    ((Category_Activity) mcontext).category_values.set(pos,new Category_values(categoryArrayList.get(item_pos).getTital(),
                            jsonArray.getString(position)
                            ,"",categoryArrayList.get(item_pos).getDataarray()));

            }






                      ((Category_Activity) mcontext).close_cat();





        } catch (JSONException e) {
            e.printStackTrace();
        }
        }
    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
//        return new  categoryArrayList.get(pos).getDataarray()size();

            return jsonArray.length();

    }

    public class Viewholder extends RecyclerView.ViewHolder{
        TextView tv_title;
        ImageView img;
        public Viewholder(View itemView) {
            super(itemView);
            tv_title=itemView.findViewById(R.id.tv_title);
            img=itemView.findViewById(R.id.img);

          }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
