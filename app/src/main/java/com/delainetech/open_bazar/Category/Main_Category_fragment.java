package com.delainetech.open_bazar.Category;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.delainetech.open_bazar.Custom_classes.DelayedProgressDialog;
import com.delainetech.open_bazar.Custom_classes.MySingleton;
import com.delainetech.open_bazar.Models.Category;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.Common;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static com.delainetech.open_bazar.Utils.Common.ParseString;
import static com.delainetech.open_bazar.Utils.Common.logg;
import static com.delainetech.open_bazar.Utils.ParamKeys.baseurl;

@SuppressLint("ValidFragment")
public class Main_Category_fragment extends Fragment {

    Main_Category_adapter adapter;
    RecyclerView recyclerview;;
    LinearLayoutManager linearLayoutManager;
    Context mcoxt;
    Toolbar toolbar;
    ArrayList<Category> categoryArrayList=new ArrayList<Category>();
    String id;
    View v;
    String main_cat_id="0";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.main_category_fragment, container, false);
        mcoxt=getActivity();
        initializ();

        return v;
    }

    public Main_Category_fragment(String main_cat_id) {
        this.main_cat_id = main_cat_id;
    }

    private void initializ() {

        recyclerview=(RecyclerView) v.findViewById(R.id.recyclerview);
        linearLayoutManager=new LinearLayoutManager(mcoxt);
        recyclerview.setLayoutManager(linearLayoutManager);
        adapter=new Main_Category_adapter(mcoxt,categoryArrayList,"Category");
        recyclerview.setAdapter(adapter);
        fatch_category(mcoxt);

    }


    public void  fatch_category(final Context context){

        final DelayedProgressDialog pro=new DelayedProgressDialog();

        pro.show(getFragmentManager(),"");


        logg("urll",baseurl + "get_category");
        StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "get_category", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                pro.cancel();
                logg("get_category11",response);

                add_data_in_list(response);
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),recyclerview);
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("maincategory_id",main_cat_id);

                return params;
            }
        };

        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(context).addToRequestQueue(stringRequest);

    }

    private void add_data_in_list(String response) {
        try {
            JSONObject jsonObject=new JSONObject(response);
            if (jsonObject.getString("status").equals("success")){
                JSONArray jsonArray=jsonObject.getJSONArray("data");
                for (int i=0;i<jsonArray.length();i++){
                    JSONObject object=jsonArray.getJSONObject(i);
                    categoryArrayList.add(new Category(ParseString(object,"id"),ParseString(object,"en_name"),
                            ParseString(object,"ar_name"),ParseString(object,"image"),
                            ParseString(object,"next_status")));
                }
                adapter.notifyDataSetChanged();


            }

        } catch (JSONException e) {
            e.printStackTrace();
            logg("error",e+"");
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        ((Category_Activity)getActivity()).tv_tital.setText("What are you Advertising?");
    }
}
