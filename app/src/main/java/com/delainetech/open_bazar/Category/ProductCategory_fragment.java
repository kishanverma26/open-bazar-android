package com.delainetech.open_bazar.Category;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.delainetech.open_bazar.Custom_classes.DelayedProgressDialog;
import com.delainetech.open_bazar.Custom_classes.MySingleton;
import com.delainetech.open_bazar.Models.Category;
import com.delainetech.open_bazar.Models.Product_values;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static com.delainetech.open_bazar.Utils.Common.ParseString;
import static com.delainetech.open_bazar.Utils.Common.logg;

public class ProductCategory_fragment extends Fragment implements ParamKeys {

    Product_adapter adapter;
    RecyclerView recyclerview;;
    LinearLayoutManager linearLayoutManager;
    Context mcoxt;

    ArrayList<Product_values> productValuesArrayList=new ArrayList<Product_values>();
    View v;

    String lable="";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.main_category_fragment, container, false);
        mcoxt=getActivity();
        initializ();

        return v;
    }


    private void initializ() {

        recyclerview=(RecyclerView) v.findViewById(R.id.recyclerview);
        linearLayoutManager=new LinearLayoutManager(mcoxt);
        recyclerview.setLayoutManager(linearLayoutManager);
        adapter=new Product_adapter(mcoxt,productValuesArrayList,"Sub-Category");
        recyclerview.setAdapter(adapter);
        fatch_category(mcoxt);

    }


    public void  fatch_category(final Context context){

        final DelayedProgressDialog pro=new DelayedProgressDialog();

        pro.show(getFragmentManager(),"");


        StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "get_productcat", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                pro.cancel();
                logg("get_productcat",response);

                add_data_in_list(response);
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),recyclerview);
            }


        }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(Id,((Category_Activity)getActivity()).category_values.get(1).getId());
                return params;
            }
        };

        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(context).addToRequestQueue(stringRequest);

    }

    private void add_data_in_list(String response) {
        try {
            JSONObject jsonObject=new JSONObject(response);
            if (jsonObject.getString("status").equals("success")){
                JSONArray jsonArray=jsonObject.getJSONArray("data");
                for (int i=0;i<jsonArray.length();i++){
                    JSONObject object=jsonArray.getJSONObject(i);
              JSONObject object1=object.getJSONObject("product");
                    lable=   object1.getString("label");

                    Iterator it = object1.keys();

                    boolean arrFlag = false;
                    while (it.hasNext()) {
                        String key = (String) it.next();
                        if (!arrFlag) {
                            String value = object1.getString(key);
                            Log.i("Key:Value1", "" + key + ":" + value);
                            arrFlag = true;
                            if (!key.equals("label")) {
                                productValuesArrayList.add(new Product_values(object1.getString("label"), key, value + "",object1.getString("image")));
                            }
                        } else {
//                            JSONArray value = object1.getJSONArray(key);
//                            Log.i("Key:Value2222", "" + key + ":" + value);
                            arrFlag = false;
//                            productValuesArrayList.add(new Product_values(object1.getString("label"),key,value+""));
                        }

                    }

                }
                ((Category_Activity)getActivity()).tv_tital.setText(lable);

                adapter.notifyDataSetChanged();


            }

        } catch (JSONException e) {
            e.printStackTrace();
            logg("error",e+"");
        }

    }


}
