package com.delainetech.open_bazar.Category;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.delainetech.open_bazar.Models.Category;
import com.delainetech.open_bazar.Models.Category_values;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;

import java.util.ArrayList;

import static com.delainetech.open_bazar.Utils.Common.logg;


/**
 * Created by Kishan on 21-Dec-17.
 */

public class Main_Category_adapter extends RecyclerView.Adapter<Main_Category_adapter.Viewholder> implements ParamKeys {

    Context mcontext;

//    UserSharedPreferences preferences;

    ArrayList<Category> categoryArrayList;
    String type;
    public Main_Category_adapter(Context mcontext, ArrayList<Category> categoryArrayList,String type) {
        this.mcontext = mcontext;
        this.type = type;
       this.categoryArrayList = categoryArrayList;

    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_item, parent, false);

        return new Viewholder(itemView);
    }

    @Override
    public void onBindViewHolder(Viewholder holder, final int position)
    {
        logg("image",Common.Image_Loading_Url(categoryArrayList.get(position).getImage())+"");
        Glide.with(mcontext).load(Common.Image_Loading_Url(categoryArrayList.get(position).getImage())).into(holder.img);
    holder.tv_title.setText(categoryArrayList.get(position).getEn_name());


    holder.itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (type.equals("Category")) {

                if (((Category_Activity) mcontext).category_values.size()>0){
                    ((Category_Activity) mcontext).category_values.set(0,new Category_values("Category", categoryArrayList.get(position).getEn_name(),
                            categoryArrayList.get(position).getId(),""));

                }else {
                    ((Category_Activity) mcontext).category_values.add(new Category_values("Category", categoryArrayList.get(position).getEn_name(),
                            categoryArrayList.get(position).getId(),""));
                }
                if (categoryArrayList.get(position).getNext_status().equals("1")) {

                    ((Category_Activity) mcontext).getSupportFragmentManager().beginTransaction().add(R.id.fram_lay, new SubCategory_fragment()).addToBackStack(null).commit();
                }
                else {
                    ((Category_Activity) mcontext).close_cat();
                }
            }else
                if
            (type.equals("Sub-Category")){
                    if (((Category_Activity) mcontext).category_values.size()>1){
                        ((Category_Activity) mcontext).category_values.set(1,new Category_values("Sub-Category", categoryArrayList.get(position).getEn_name()
                         ,categoryArrayList.get(position).getId(),""));

                    }else {
                        ((Category_Activity) mcontext).category_values.add(new Category_values("Sub-Category", categoryArrayList.get(position).getEn_name(),categoryArrayList.get(position).getId(),""));
                    }
                    if (categoryArrayList.get(position).getNext_status().equals("1")) {
                        ((Category_Activity) mcontext).getSupportFragmentManager().beginTransaction().add(R.id.fram_lay, new ProductCategory_fragment()).addToBackStack(null).commit();
                    }else {
                        if ( ((Category_Activity) mcontext).category_values.size()>1){
                            logg("sizesize1",((Category_Activity) mcontext).category_values.size()+"");
                            int ii=((Category_Activity) mcontext).category_values.size();
                            ii=ii-1;
                            for (int i=ii; i>=2; i--){
                                logg("sizesize2",((Category_Activity) mcontext).category_values.size()+"");

                                ((Category_Activity) mcontext).category_values.remove(i);
                                logg("sizesize:",i+"");


                            }

                            ((Category_Activity) mcontext).close_cat();

                        }else {
                            ((Category_Activity) mcontext).close_cat();
                        }
                    }
            }
            }
    });
    }

    @Override
    public int getItemCount() {
        return categoryArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder{
        TextView tv_title;
        ImageView img;
        public Viewholder(View itemView) {
            super(itemView);
            tv_title=itemView.findViewById(R.id.tv_title);
            img=itemView.findViewById(R.id.img);

          }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
