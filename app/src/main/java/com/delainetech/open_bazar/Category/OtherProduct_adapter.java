package com.delainetech.open_bazar.Category;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.delainetech.open_bazar.Models.Category_values;
import com.delainetech.open_bazar.Models.Product_values;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.ParamKeys;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

import static com.delainetech.open_bazar.Utils.Common.logg;


/**
 * Created by Kishan on 21-Dec-17.
 */

public class OtherProduct_adapter extends RecyclerView.Adapter<OtherProduct_adapter.Viewholder> implements ParamKeys {

    Context mcontext;

//    UserSharedPreferences preferences;

    ArrayList<Product_values> categoryArrayList;
    JSONArray jsonArray;
    String type;
    int pos=0;
    String value;
    public OtherProduct_adapter(Context mcontext, ArrayList<Product_values> categoryArrayList, String type,JSONArray jsonArray,
                                int pos,String value) {
        this.mcontext = mcontext;
        this.type = type;
        this.jsonArray = jsonArray;
        this.pos = pos;
        this.value = value;

       this.categoryArrayList = categoryArrayList;

    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_item, parent, false);

        return new Viewholder(itemView);
    }

    @Override
    public void onBindViewHolder(Viewholder holder, final int position)
    {
        try {
            holder.tv_title.setText(jsonArray.getString(position));

        holder.img.setVisibility(View.GONE);

    holder.itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try {
            if (((Category_Activity) mcontext).category_values.size()>(pos+3)){

                    ((Category_Activity) mcontext).category_values.set((pos+3),new Category_values(categoryArrayList.get(pos).getTital(),
                            jsonArray.getString(position)
                            ,"",categoryArrayList.get(pos).getDataarray()));

            }
            else {
                ((Category_Activity) mcontext).category_values.add(new Category_values(categoryArrayList.get(pos).getTital(),
                        jsonArray.getString(position)
                        ,"",categoryArrayList.get(pos).getDataarray()));
            }

            logg("sizesize4",categoryArrayList.size()+"");
            logg("sizesize5",(pos+1)+"");

              if (categoryArrayList.size()==(pos+1)){

                  if ( ((Category_Activity) mcontext).category_values.size()>(pos+4)){
                      logg("sizesize1",((Category_Activity) mcontext).category_values.size()+"");
                      int ii=((Category_Activity) mcontext).category_values.size();
                      ii=ii-1;
                      for (int i=ii; i>=(pos+4); i--){
                          logg("sizesize2",((Category_Activity) mcontext).category_values.size()+"");

                          ((Category_Activity) mcontext).category_values.remove(i);
                          logg("sizesize:",i+"");


                      }

                      ((Category_Activity) mcontext).close_cat();

                  }else {
                      ((Category_Activity) mcontext).close_cat();
                  }


              }
              else {
                  ((Category_Activity) mcontext).getSupportFragmentManager().beginTransaction()
                          .add(R.id.fram_lay, new OtherProductCategory_fragment(value, pos + 1)).addToBackStack(null)
                          .commit();
              }
//
//            ((Category_Activity) mcontext).close_cat();


        } catch (JSONException e) {
            e.printStackTrace();
        }
        }
    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
//        return new  categoryArrayList.get(pos).getDataarray()size();

            return jsonArray.length();

    }

    public class Viewholder extends RecyclerView.ViewHolder{
        TextView tv_title;
        ImageView img;
        public Viewholder(View itemView) {
            super(itemView);
            tv_title=itemView.findViewById(R.id.tv_title);
            img=itemView.findViewById(R.id.img);

          }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
