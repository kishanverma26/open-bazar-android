package com.delainetech.open_bazar.Category;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.delainetech.open_bazar.Models.ADS;
import com.delainetech.open_bazar.Models.Category_values;
import com.delainetech.open_bazar.R;
import java.util.ArrayList;

public class Category_Activity extends AppCompatActivity {


    Context mcoxt;
    Toolbar toolbar;
    public TextView tv_tital;
   public ArrayList<Category_values> category_values=new ArrayList<Category_values>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_category_activity);
        mcoxt=Category_Activity.this;
        initializ();
    }

    private void initializ() {
        tv_tital=(TextView) findViewById(R.id.tv_tital);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);

        if (getIntent().hasExtra("list")){
            category_values=  (ArrayList<Category_values>) getIntent().getSerializableExtra("list");
        }

        if (getIntent().hasExtra("pos")){
            getSupportFragmentManager().beginTransaction().add(R.id.fram_lay,
                    new Main_Category_fragment(getIntent().getStringExtra("main_cat_id"))).addToBackStack(null).commit();

            getSupportFragmentManager().beginTransaction().add(R.id.fram_lay, new SubCategory_fragment()).addToBackStack(null).commit();

        }else if (getIntent().hasExtra("type")){
            getSupportFragmentManager().beginTransaction().add(R.id.fram_lay, new EditProductCategory_fragment(getIntent()
                    .getIntExtra("position",0),getIntent().getStringExtra("type")
                    ,getIntent().getStringExtra("tital"))).addToBackStack(null).commit();
        }else  {
            getSupportFragmentManager().beginTransaction().add(R.id.fram_lay,
                    new Main_Category_fragment(getIntent().getStringExtra("main_cat_id"))).addToBackStack(null).commit();
        }

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
               onBackPressed();
                break;

        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {


       FragmentManager fm = getSupportFragmentManager();
                if (fm.getBackStackEntryCount() > 1) {
                    Log.e("Fragcount",fm.getBackStackEntryCount()+"")  ;
                    Log.e("Fragcountsize",category_values.size()+"")  ;
                    Log.e("Fragcountvalue",category_values.get(fm.getBackStackEntryCount()-2).getTital()+"")  ;
                  if (!(fm.getBackStackEntryCount()==1)) {
                      if (!(fm.getBackStackEntryCount()==2)) {
                          tv_tital.setText(category_values.get(fm.getBackStackEntryCount()-2).getTital());
                      }
                      else {
                          tv_tital.setText(getResources().getString(R.string.what_are_you_advertising));

                      }
                    }
                    else {
                      tv_tital.setText(getResources().getString(R.string.what_are_you_advertising));

                  }
                    fm.popBackStack();

                } else {

                    finish();

                }
            }


            public void close_cat(){
                if (category_values.size()>0){
                    Intent i=new Intent();
                    i.putExtra("list",category_values);
                    setResult(RESULT_OK,i);
                }
                finish();

            }

    }

