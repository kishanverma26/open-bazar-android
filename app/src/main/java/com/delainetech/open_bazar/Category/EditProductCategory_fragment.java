package com.delainetech.open_bazar.Category;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.delainetech.open_bazar.Custom_classes.DelayedProgressDialog;
import com.delainetech.open_bazar.Custom_classes.MySingleton;
import com.delainetech.open_bazar.Models.Product_values;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static com.delainetech.open_bazar.Utils.Common.logg;

@SuppressLint("ValidFragment")
public class EditProductCategory_fragment extends Fragment implements ParamKeys {

    EditProduct_adapter adapter;
    RecyclerView recyclerview;;
    LinearLayoutManager linearLayoutManager;
    Context mcoxt;
    EditOtherProduct_adapter editOtherProduct_adapter;
    ArrayList<Product_values> categoryArrayList=new ArrayList<>();
    ArrayList<Product_values> productValuesArrayList=new ArrayList<Product_values>();
    View v;

    String lable="";
    String type="";
    String arrayvalue="";
    int pos=0;
    int item_pos=0;
    String tital="";

    public EditProductCategory_fragment(int pos, String type, String tital) {
        this.pos = pos;
        this.type = type;
        this.tital = tital;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.main_category_fragment, container, false);
        mcoxt=getActivity();
        initializ();

        return v;
    }


    private void initializ() {

        recyclerview=(RecyclerView) v.findViewById(R.id.recyclerview);
        linearLayoutManager=new LinearLayoutManager(mcoxt);
        recyclerview.setLayoutManager(linearLayoutManager);


            fatch_category(mcoxt);

    }


    public void  fatch_category(final Context context){

        final DelayedProgressDialog pro=new DelayedProgressDialog();

        pro.show(getFragmentManager(),"");


        StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "get_productcat", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                pro.cancel();
                logg("get_productcat",response);

                add_data_in_list(response);
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),recyclerview);
            }


        }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(Id,((Category_Activity)getActivity()).category_values.get(1).getId());
                return params;
            }
        };

        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(context).addToRequestQueue(stringRequest);

    }

    private void add_data_in_list(String response) {
        try {
            JSONObject jsonObject=new JSONObject(response);
            if (jsonObject.getString("status").equals("success")){
                JSONArray jsonArray=jsonObject.getJSONArray("data");
                for (int i=0;i<jsonArray.length();i++){
                    JSONObject object=jsonArray.getJSONObject(i);
              JSONObject object1=object.getJSONObject("product");
                    lable=   object1.getString("label");

                    Iterator it = object1.keys();

                    boolean arrFlag = false;
                    while (it.hasNext()) {
                        String key = (String) it.next();
                        if (!arrFlag) {
                            String value = object1.getString(key);
                            Log.i("Key:Value1", "" + key + ":" + value);
                            if (key.equals(type)){
                                arrayvalue=value;
                            }
                            arrFlag = true;
                            if (!key.equals("label")) {
                                productValuesArrayList.add(new Product_values(object1.getString("label"), key, value + ""));
                            }
                        } else {
//                            JSONArray value = object1.getJSONArray(key);
//                            Log.i("Key:Value2222", "" + key + ":" + value);
                            arrFlag = false;
//                            productValuesArrayList.add(new Product_values(object1.getString("label"),key,value+""));
                        }

                    }

                }
                if (pos>2){
                    add_data_in_list(arrayvalue,arrayvalue);
                    logg("type",arrayvalue);

                }else {
                    ((Category_Activity) getActivity()).tv_tital.setText(lable);

                    adapter=new EditProduct_adapter(mcoxt,productValuesArrayList,"Sub-Category");

                    recyclerview.setAdapter(adapter);
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
            logg("error",e+"");
        }

    }
    JSONArray jsonArray;

    private void add_data_in_list(String response,String value1 ) {
        try {
//            JSONArray jsonArray=new JSONArray(response);
            JSONObject object= new JSONObject(response);

            Iterator it = object.keys();

            boolean arrFlag = false;
            while (it.hasNext()) {
                String key = (String) it.next();
                if (!arrFlag) {
                    String value = object.getString(key);
                    Log.i("Key:Value1", "" + key + ":" + value);
                    arrFlag = true;
                    productValuesArrayList.add(new Product_values(key,value,value+""));

                } else {
                    JSONArray value = object.getJSONArray(key);
                    Log.i("Key:Value2222", "" + key + ":" + value);
                    arrFlag = false;
                    productValuesArrayList.add(new Product_values(key,value+"",value+""));

                }

            }


//            logg("test",productValuesArrayList.get(pos).getDataarray());
            logg("test1",tital);
            logg("test11",pos+"");
  logg("test111",((Category_Activity) getActivity()).category_values.size()+"");


            for (int i=0;i<productValuesArrayList.size();i++) {
                if (productValuesArrayList.get(i).getTital().equals(tital)) {
                    jsonArray = new JSONArray(productValuesArrayList.get(i).getDataarray());
                    ((Category_Activity)getActivity()).tv_tital.setText(productValuesArrayList.get(i).getTital());
                    item_pos=i;

                }
            }


            editOtherProduct_adapter=new EditOtherProduct_adapter(mcoxt,productValuesArrayList,"Sub-Category",jsonArray,pos,value1,item_pos);
            recyclerview.setAdapter(editOtherProduct_adapter);



        } catch (JSONException e) {
            e.printStackTrace();
            logg("error",e+"");
        }

    }


}
