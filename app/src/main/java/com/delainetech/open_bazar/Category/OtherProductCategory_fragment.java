package com.delainetech.open_bazar.Category;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.delainetech.open_bazar.Custom_classes.DelayedProgressDialog;
import com.delainetech.open_bazar.Custom_classes.MySingleton;
import com.delainetech.open_bazar.Models.Product_values;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static com.delainetech.open_bazar.Utils.Common.logg;

@SuppressLint("ValidFragment")
public class OtherProductCategory_fragment extends Fragment implements ParamKeys {

    OtherProduct_adapter adapter;
    RecyclerView recyclerview;;
    LinearLayoutManager linearLayoutManager;
    Context mcoxt;

    ArrayList<Product_values> productValuesArrayList=new ArrayList<Product_values>();
    View v;

    String lable="";
    String value="";
    JSONArray jsonArray;
    int pos=0;
    public OtherProductCategory_fragment(String value,int pos) {
        this.value = value;
        this.pos = pos;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.main_category_fragment, container, false);
        mcoxt=getActivity();
        initializ();

        return v;
    }


    private void initializ() {
        recyclerview=(RecyclerView) v.findViewById(R.id.recyclerview);
        linearLayoutManager=new LinearLayoutManager(mcoxt);
        recyclerview.setLayoutManager(linearLayoutManager);
        Log.e("value",value+"");
        add_data_in_list(value);
    }




    private void add_data_in_list(String response) {
        try {
//            JSONArray jsonArray=new JSONArray(response);
            JSONObject object= new JSONObject(response);

                    Iterator it = object.keys();

                    boolean arrFlag = false;
                    while (it.hasNext()) {
                        String key = (String) it.next();
                        if (!arrFlag) {
                            String value = object.getString(key);
                            Log.i("Key:Value1", "" + key + ":" + value);
                            arrFlag = true;
                            productValuesArrayList.add(new Product_values(key,value,value+""));

                        } else {
                            JSONArray value = object.getJSONArray(key);
                            Log.i("Key:Value2222", "" + key + ":" + value);
                            arrFlag = false;
                            productValuesArrayList.add(new Product_values(key,value+"",value+""));

                        }

                    }


                ((Category_Activity)getActivity()).tv_tital.setText(productValuesArrayList.get(pos).getTital());
            jsonArray=new JSONArray(productValuesArrayList.get(pos).getDataarray());
            adapter=new OtherProduct_adapter(mcoxt,productValuesArrayList,"Sub-Category",jsonArray,pos,value);
            recyclerview.setAdapter(adapter);



        } catch (JSONException e) {
            e.printStackTrace();
            logg("error",e+"");
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        ((Category_Activity)getActivity()).tv_tital.setText(productValuesArrayList.get(pos).getTital());


    }


}
