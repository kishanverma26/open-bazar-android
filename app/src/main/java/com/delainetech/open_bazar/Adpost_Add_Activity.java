package com.delainetech.open_bazar;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.SimpleMultiPartRequest;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.numberprogressbar.NumberProgressBar;
import com.delainetech.open_bazar.Adapter.Ad_Category_Preview_adapter;
import com.delainetech.open_bazar.Adapter.Custom_Spinner_Adapter;
import com.delainetech.open_bazar.Adapter.Ad_Image_recycler_adapter;
import com.delainetech.open_bazar.Category.Category_Activity;
import com.delainetech.open_bazar.Custom_classes.CEditText;
import com.delainetech.open_bazar.Fragments.Mobile_verification_adpost_dialog_fragment;
import com.delainetech.open_bazar.Fragments.Select_category_dialog_fragment;
import com.delainetech.open_bazar.Fragments.Success_adpost_dialog_fragment;
import com.delainetech.open_bazar.Models.ADS;
import com.delainetech.open_bazar.Models.Category_values;
import com.delainetech.open_bazar.Models.Gallery;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputLayout;
import com.theartofdev.edmodo.cropper.CropImage;
import com.zfdang.multiple_images_selector.ImagesSelectorActivity;
import com.zfdang.multiple_images_selector.SelectorSettings;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static com.delainetech.open_bazar.Utils.Common.Snakbar;
import static com.delainetech.open_bazar.Utils.Common.logg;


public class Adpost_Add_Activity extends AppCompatActivity implements ParamKeys,View.OnClickListener {

    Toolbar toolbar;
    Context mcontext;
    RecyclerView image_recyclerview;
    LinearLayoutManager linearLayoutManager;
    Ad_Image_recycler_adapter adapter;
    HorizontalScrollView scrollView;
    ImageView iv_add;
    TextView tv_category,tv_tital,tv_location;
    RelativeLayout rl_category,rl_cat_recyclerview;
    Button bt_insertad;
    //    Add image
    LinearLayout ll_addimage;
    List<Gallery> galleryMedias_images = new ArrayList<>();

    int Replace_REQUEST_CODE_GALLERY = 2;
    int REQUEST_CODE_GALLERY = 1;
    int Image_limit=30;
    UserSharedPreferences preferences;
    String[] permissionsRequired = new String[]{
            android.Manifest.permission.CAMERA,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    EditText et_title,et_desc,et_mobile;
    EditText et_price;
    ImageView iv_clear_price,iv_clear_mobile,iv_clear_tital,iv_clear_des;
    ProgressDialog dialog;
    Spinner spinner;
    TextInputLayout it_price;
    LinearLayout ll_price;
    String price_type="price",price="0";



    //category array list
    ArrayList<Category_values> category_values=new ArrayList<Category_values>();
    RecyclerView rv_cat;
    LinearLayoutManager catlinearLayoutManager;
    Ad_Category_Preview_adapter ad_category_preview_adapter;
    CardView cv_price,cv_location;

    public String Main_cat_id="0";
//    Sell Your Stuff=1,Rent Your Stuff=2,Offer Services=3,Start Your Shop=4

    String post_id="";
    Switch sw_mobileno;

    //    shop name
    RelativeLayout rl_shopname;
    EditText et_shopname;
    ImageView iv_shopname;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ads_add_activity);
        Common.hideSoftKeyboard(Adpost_Add_Activity.this);
        mcontext= Adpost_Add_Activity.this;
        initialize();
    }

    private void initialize() {
        dialog=new ProgressDialog(this);
        preferences=UserSharedPreferences.getInstance(mcontext);

        rl_shopname=(RelativeLayout)findViewById(R.id.rl_shopname);
        et_shopname=(EditText)findViewById(R.id.et_shopname);
        iv_shopname=(ImageView) findViewById(R.id.iv_shopname);
        iv_shopname.setOnClickListener(this);

        toolbar=(Toolbar)findViewById(R.id.toolbar);
        cv_location=(CardView) findViewById(R.id.cv_location);
        cv_price=(CardView) findViewById(R.id.cv_price);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);


        ll_price=(LinearLayout) findViewById(R.id.ll_price);
        sw_mobileno=(Switch) findViewById(R.id.sw_mobileno);


        it_price=(TextInputLayout) findViewById(R.id.it_price);
        bt_insertad=(Button) findViewById(R.id.bt_insertad);
        et_price=(EditText) findViewById(R.id.et_price);
        iv_clear_price=(ImageView) findViewById(R.id.iv_clear_price);
        iv_clear_price.setOnClickListener(this);
        iv_clear_mobile=(ImageView) findViewById(R.id.iv_clear_mobile);
        iv_clear_tital=(ImageView) findViewById(R.id.iv_clear_tital);
        iv_clear_des=(ImageView) findViewById(R.id.iv_clear_des);
        iv_clear_mobile.setOnClickListener(this);
        iv_clear_tital.setOnClickListener(this);
        iv_clear_des.setOnClickListener(this);

        et_mobile=(EditText) findViewById(R.id.et_mobile);
        et_title=(EditText) findViewById(R.id.et_title);
        et_desc=(EditText) findViewById(R.id.et_desc);
        ll_addimage=(LinearLayout)findViewById(R.id.ll_addimage);
        ll_addimage.setOnClickListener(this);
        bt_insertad.setOnClickListener(this);

        //add category
        tv_tital=(TextView) findViewById(R.id.tv_tital);
        tv_category=(TextView) findViewById(R.id.tv_category);
        tv_category.setOnClickListener(this);
//        tv_insertad=(TextView) findViewById(R.id.tv_insertad);
//        tv_insertad.setOnClickListener(this);

        tv_location=(TextView) findViewById(R.id.tv_location);
        tv_location.setOnClickListener(this);

        //Category
        rl_category=(RelativeLayout) findViewById(R.id.rl_category);
        rl_cat_recyclerview=(RelativeLayout) findViewById(R.id.rl_cat_recyclerview);
        rl_category.setOnClickListener(this);

        //add image
        iv_add=(ImageView) findViewById(R.id.iv_add);
        iv_add.setOnClickListener(this);
        scrollView=(HorizontalScrollView)findViewById(R.id.horizantal_scroll);
        image_recyclerview=(RecyclerView)findViewById(R.id.image_recyclerview);
        linearLayoutManager=new LinearLayoutManager(mcontext,LinearLayoutManager.HORIZONTAL,false);
        image_recyclerview.setLayoutManager(linearLayoutManager);
        adapter=new Ad_Image_recycler_adapter(this,galleryMedias_images);
        image_recyclerview.setAdapter(adapter);
        image_recyclerview.setNestedScrollingEnabled(true);

        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0,iv_add.getBottom());
            }
        });

        //category recycler view
        rv_cat=(RecyclerView)findViewById(R.id.rv_cat);
        catlinearLayoutManager=new LinearLayoutManager(mcontext);
        rv_cat.setLayoutManager(catlinearLayoutManager);
        ad_category_preview_adapter=new Ad_Category_Preview_adapter(this,category_values);
        rv_cat.setAdapter(ad_category_preview_adapter);
        rv_cat.setNestedScrollingEnabled(false);

        et_mobile.setText(preferences.getmobile());
        spinner=(Spinner) findViewById(R.id.spinner);
        String ar[]={"Price Types","Price", "Ask For Price", "Exchange", "Free"};
        Custom_Spinner_Adapter adapter=new Custom_Spinner_Adapter(this,ar,R.color.dark_text_color);
        spinner.setAdapter(adapter);

//        spinner.setItems("Price", "Ask For Price", "Exchange", "Free");

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position>1){
                    ll_price.setVisibility(View.GONE);
                }else {
                    ll_price.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        edittext_change_listner();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.add_ad,menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.item_post:
                checkvalidateion();
                break;


            case R.id.item_reset:
//                 posted_poupup();
                alert(mcontext);

                break;



        }
        return super.onOptionsItemSelected(item);
    }


    public void alert(Context mcontext){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mcontext);
        alertDialog.setTitle(R.string.reset_all_fields);
        alertDialog.setMessage(getResources().getString(R.string.areyoushure_reset));
        alertDialog.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                et_title.setText("");
                et_desc.setText("");
                et_price.setText("");
                et_mobile.setText("");
                ll_addimage.setVisibility(View.VISIBLE);
                scrollView.setVisibility(View.GONE);
                galleryMedias_images.clear();
                adapter.notifyDataSetChanged();
                rl_category.setVisibility(View.VISIBLE);
                rl_cat_recyclerview.setVisibility(View.GONE);
                category_values.clear();
                ad_category_preview_adapter.notifyDataSetChanged();
                Common.Snakbar(getResources().getString(R.string.reset_all_fields),toolbar);


            }
        });
        alertDialog.setNegativeButton(getResources().getString(R.string.No), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog1= alertDialog.create();
        alertDialog1.show();

        alertDialog1.getButton(alertDialog1.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorAccent));
        alertDialog1.getButton(alertDialog1.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorAccent));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.tv_category:
//                 select_category(this);

                Select_category_dialog_fragment dialogfrg = new Select_category_dialog_fragment();
                FragmentManager frm = getSupportFragmentManager();
                dialogfrg.show(frm, "");
                break;

            case R.id.ll_addimage:

                if ( Common.hasPermissions(this,permissionsRequired)){
                    take_picture(mcontext);
                }else {
                    Common.askpermission(this,permissionsRequired);
                }
                break;
            case R.id.iv_add:
                take_picture(mcontext);
                break;
            case R.id.iv_clear_price:
                et_price.setText("");
                iv_clear_price.setVisibility(View.GONE);
                break;
            case R.id.iv_clear_des:
                et_desc.setText("");
                iv_clear_des.setVisibility(View.GONE);
                break;
            case R.id.iv_clear_tital:
                et_title.setText("");
                iv_clear_tital.setVisibility(View.GONE);
                break;
            case R.id.iv_clear_mobile:
                et_mobile.setText("");
                iv_clear_mobile.setVisibility(View.GONE);
                break;
            case R.id.iv_shopname:
                et_shopname.setText("");
                iv_shopname.setVisibility(View.GONE);
                break;


            case R.id.tv_location:
                startActivity(new Intent(mcontext,Location_picker_Activity.class));

                break;
            case  R.id.bt_insertad:
                checkvalidateion();
                break;

        }
    }
    private ArrayList<String> mResults = new ArrayList<>();
    private void openGallery() {
//        take_album_or_gallry(mcontext);

        Intent intent = new Intent(Adpost_Add_Activity.this, ImagesSelectorActivity.class);
        // max number of images to be selected
        intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, Image_limit);
        // min size of image which will be shown; to filter tiny images (mainly icons)
        intent.putExtra(SelectorSettings.SELECTOR_MIN_IMAGE_SIZE, 100000);
        // show camera or not
        intent.putExtra(SelectorSettings.SELECTOR_SHOW_CAMERA, false);
        // pass current selected images as the initial value
        intent.putStringArrayListExtra(SelectorSettings.SELECTOR_INITIAL_SELECTED_LIST, mResults);
        // start the selector
        startActivityForResult(intent, REQUEST_CODE_GALLERY);

//        startActivityForResult(new GalleryHelper().setMultiselection(true)
//                .setMaxSelectedItems(Image_limit)
//                .setShowVideos(false)
//                .getCallingIntent(this), REQUEST_CODE_GALLERY);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        logg("requestcode=",requestCode+", resultecode="+resultCode);

        logg("Main_cat_id",Main_cat_id.length()+"___ b"+Main_cat_id.equals("4"));
        if (Main_cat_id.equals("4")){
            rl_shopname.setVisibility(View.VISIBLE);
            et_shopname.setVisibility(View.VISIBLE);
        }else {
            rl_shopname.setVisibility(View.GONE);
            et_shopname.setVisibility(View.GONE);
        }

        if (requestCode == REQUEST_CODE_GALLERY ) {
            if (data!=null){

                if(resultCode == RESULT_OK) {
                    mResults = data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS);
                    assert mResults != null;

                    // show results in textview
                    StringBuilder sb = new StringBuilder();
                    sb.append(String.format("Totally %d images selected:", mResults.size())).append("\n");
                    for(String result : mResults) {
                        galleryMedias_images.add(new Gallery(result,false));
                        sb.append(result).append("\n");
                    }
                    mResults.clear();

                }

                Image_limit=30;
                Image_limit= Image_limit-galleryMedias_images.size();

                if (galleryMedias_images.size()>0){
                    ll_addimage.setVisibility(View.GONE);
                    scrollView.setVisibility(View.VISIBLE);
                    adapter=new Ad_Image_recycler_adapter(this,galleryMedias_images);
                    image_recyclerview.setAdapter(adapter);
                }else {
                    ll_addimage.setVisibility(View.VISIBLE);
                    scrollView.setVisibility(View.GONE);
                }


            }

        }else if (requestCode==Replace_REQUEST_CODE_GALLERY){
            if (data!=null){
                mResults = data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS);
                assert mResults != null;

                // show results in textview
                StringBuilder sb = new StringBuilder();
                sb.append(String.format("Totally %d images selected:", mResults.size())).append("\n");
                for(String result : mResults) {
                    galleryMedias_images.set(edit_imagepos,new Gallery(result,galleryMedias_images.get(edit_imagepos).isIs_main()));

                }
                mResults.clear();
                adapter.notifyDataSetChanged();

            }
        }

        if (requestCode == REQUEST_CODE_IMAGE_CAPTURE && resultCode== RESULT_OK) {
            if (mOutputFileUri != null) {
                String uri = mOutputFileUri.toString();
                Log.e("uri", uri);
                galleryMedias_images.add(new Gallery(mOutputFileUri.toString(),false));
                adapter.notifyDataSetChanged();

                if (galleryMedias_images.size()>0){
                    ll_addimage.setVisibility(View.GONE);
                    scrollView.setVisibility(View.VISIBLE);
                    adapter=new Ad_Image_recycler_adapter(this,galleryMedias_images);
                    image_recyclerview.setAdapter(adapter);

                }else {
                    ll_addimage.setVisibility(View.VISIBLE);
                    scrollView.setVisibility(View.GONE);
                }


            }
        }else if (requestCode == Replace_REQUEST_CODE_IMAGE_CAPTURE && resultCode== RESULT_OK){
            if (mOutputFileUri != null) {
                String uri = mOutputFileUri.toString();
                Log.e("uri", uri);
                galleryMedias_images.set(edit_imagepos,new Gallery(mOutputFileUri.toString(),false));
                adapter.notifyDataSetChanged();

                if (galleryMedias_images.size()>0){
                    ll_addimage.setVisibility(View.GONE);
                    scrollView.setVisibility(View.VISIBLE);
                    adapter=new Ad_Image_recycler_adapter(this,galleryMedias_images);
                    image_recyclerview.setAdapter(adapter);

                }else {
                    ll_addimage.setVisibility(View.VISIBLE);
                    scrollView.setVisibility(View.GONE);
                }


            }
        }



        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Log.e("error1",resultUri+"");
                Log.e("error2",result.getError()+"");
                Log.e("error2",result.getOriginalUri()+"");

                galleryMedias_images.set(edit_imagepos,new Gallery(resultUri.toString(),false));
                adapter.notifyDataSetChanged();

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.e("error",error.getMessage()+"");
//                galleryMedias_images.set(edit_imagepos,new Gallery(error.toString(),false));
//                adapter.notifyDataSetChanged();

            }
        }

        if (requestCode==ADS_CATEGORY_RESULT_CODE && resultCode==RESULT_OK){
            category_values = (ArrayList<Category_values>) data.getSerializableExtra("list");
            logg("size",category_values.size()+"");
            if (category_values.size()>0){
                rl_category.setVisibility(View.GONE);
                rl_cat_recyclerview.setVisibility(View.VISIBLE);

                ad_category_preview_adapter=new Ad_Category_Preview_adapter(this,category_values);
                rv_cat.setAdapter(ad_category_preview_adapter);

                cv_price.setVisibility(View.VISIBLE);
                cv_location.setVisibility(View.VISIBLE);
                et_title.requestFocus();
                et_title.setFocusable(true);

                InputMethodManager imm = (InputMethodManager)getSystemService(this.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,InputMethodManager.HIDE_IMPLICIT_ONLY);
            }

        }

        if (requestCode == Mobile_RESULT_CODE && resultCode==RESULT_OK) { // confirm that this response matches your request
            AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
            String toastMessage;
            if (loginResult.getError() != null) {
                toastMessage = loginResult.getError().getErrorType().getMessage();
                Toast.makeText(this, toastMessage, Toast.LENGTH_SHORT).show();
            } else if (loginResult.wasCancelled()) {
                toastMessage = "Login Cancelled";
            } else {
                getaccount_info();
            }
        }else if (requestCode == Mobile_RESULT_CODE && resultCode==RESULT_CANCELED){
            finish();
            Toast.makeText(mcontext, "Your ad is pending because mobile number is not verify", Toast.LENGTH_LONG).show();
        }

    }


    private void getaccount_info() {
        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(Account account) {
                Log.e("mobileno",account.getPhoneNumber()+"");
                if (account.getPhoneNumber().toString().contains(et_mobile.getText().toString().trim().replaceFirst("^0+(?!$)", ""))){
                    verify_ad(post_id);
                } else {
                    finish();
                    Toast.makeText(mcontext, "You have enter diffrent phone number", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onError(AccountKitError accountKitError) {

            }
        });
    }



    //    edit_image
    int edit_imagepos=0;
    public void edit_image(Context mcontext, final int position) {

        edit_imagepos=position;
        View view = getLayoutInflater().inflate(R.layout.edit_picture_bottom_sheet, null);

        final BottomSheetDialog dialog = new BottomSheetDialog(mcontext);
        LinearLayout ll=(LinearLayout)view.findViewById(R.id.ll);
        TextView tv_edit=(TextView)view.findViewById(R.id.tv_edit);
        TextView tv_camera=(TextView)view.findViewById(R.id.tv_camera);
        TextView tv_gallery=(TextView)view.findViewById(R.id.tv_gallery);
        TextView tv_delete=(TextView)view.findViewById(R.id.tv_delete);
        TextView tv_main_image=(TextView)view.findViewById(R.id.tv_main_image);
        WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.dimAmount = 0.0F;

        dialog.getWindow().setAttributes(wlmp);
//        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        tv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                Log.e("uri",galleryMedias_images.get(edit_imagepos).getUri()+"");
                Log.e("uri1",Uri.parse(galleryMedias_images.get(edit_imagepos).getUri())+"");

                if (galleryMedias_images.get(edit_imagepos).getUri().contains("file")) {
                    CropImage.activity(Uri.parse(galleryMedias_images.get(edit_imagepos).getUri()))
                            .start(Adpost_Add_Activity.this);
                }else {
                    CropImage.activity(Uri.fromFile(new File(galleryMedias_images.get(edit_imagepos).getUri())))
                            .start(Adpost_Add_Activity.this);

                }
            }
        });
        tv_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                // start multiple photos selector
                Intent intent = new Intent(Adpost_Add_Activity.this, ImagesSelectorActivity.class);
                // max number of images to be selected
                intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 1);
                // min size of image which will be shown; to filter tiny images (mainly icons)
                intent.putExtra(SelectorSettings.SELECTOR_MIN_IMAGE_SIZE, 100000);
                // show camera or not
                intent.putExtra(SelectorSettings.SELECTOR_SHOW_CAMERA, false);
                // pass current selected images as the initial value
                intent.putStringArrayListExtra(SelectorSettings.SELECTOR_INITIAL_SELECTED_LIST, mResults);
                // start the selector
                startActivityForResult(intent, Replace_REQUEST_CODE_GALLERY);


            }
        });
        tv_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File file = new File(Environment.getExternalStorageDirectory(),
                        UUID.randomUUID().toString() + "img.jpg");
                mOutputFileUri = FileProvider.getUriForFile(Adpost_Add_Activity.this, "com.open_bazar.fileprovider", file);
                Log.e("uri", ("file:" + file.getAbsolutePath()) + "");
                intent.putExtra(MediaStore.EXTRA_OUTPUT, mOutputFileUri);
                mOutputFileUri = Uri.parse("file:" + file.getAbsolutePath());
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivityForResult(intent, Replace_REQUEST_CODE_IMAGE_CAPTURE);
            }
        });

        tv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                galleryMedias_images.remove(position);
                adapter.notifyDataSetChanged();

                if (galleryMedias_images.size()==0){
                    ll_addimage.setVisibility(View.VISIBLE);
                    scrollView.setVisibility(View.GONE);
                }

            }
        });
        tv_main_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<galleryMedias_images.size();i++){
                    galleryMedias_images.set(i,new Gallery(galleryMedias_images.get(i).getUri(),false));

                }
                galleryMedias_images.add(0,new Gallery(galleryMedias_images.get(edit_imagepos).getUri(),true));
                galleryMedias_images.remove(edit_imagepos+1);
                adapter.notifyDataSetChanged();
                dialog.dismiss();

            }
        });


        dialog.setContentView(view);
        dialog.show();
    }



    public void take_picture(Context mcontext) {

        View view = getLayoutInflater().inflate(R.layout.take_picture_dialog_bottom_sheet, null);

        final BottomSheetDialog dialog = new BottomSheetDialog(mcontext);
        LinearLayout ll=(LinearLayout)view.findViewById(R.id.ll);
        TextView tv_camera=(TextView)view.findViewById(R.id.tv_camera);
        TextView tv_gallery=(TextView)view.findViewById(R.id.tv_gallery);
        WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.dimAmount = 0.0F;

        dialog.getWindow().setAttributes(wlmp);
//        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        tv_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                open_camera();
                dialog.dismiss();
            }
        });
        tv_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openGallery();
                dialog.dismiss();
            }
        });


        dialog.setContentView(view);
        dialog.show();
    }




    private Uri mOutputFileUri = null;
    private static final int REQUEST_CODE_IMAGE_CAPTURE = 12121;
    private static final int Replace_REQUEST_CODE_IMAGE_CAPTURE = 1211;

    private void open_camera(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStorageDirectory(),
                UUID.randomUUID().toString() + "img.jpg");
        mOutputFileUri = FileProvider.getUriForFile(Adpost_Add_Activity.this, "com.open_bazar.fileprovider", file);
        Log.e("uri", ("file:" + file.getAbsolutePath()) + "");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mOutputFileUri);
        mOutputFileUri = Uri.parse("file:" + file.getAbsolutePath());
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(intent, REQUEST_CODE_IMAGE_CAPTURE);
    }



    int imageuploadcount=0;
    private void upload_image(final int i){
        dialog.setTitle("Uploading image "+i+"/"+galleryMedias_images.size());

        SimpleMultiPartRequest request = new SimpleMultiPartRequest(Request.Method.POST, baseurl+"uploadimage", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                logg("response",response+"");
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){

                        image_object =new JSONObject();
                        image_object.put("image",object.getString("image"));
                        logg("jsonarrayimg",object.getString("image")+"");
                        jsonArray.put(i,image_object);
                        Common.logg("jsonarrayy1", jsonArray + "");
                        Common.logg("jsonarrayysize", i + "");
                        Common.logg("jsonarrayylistsize", galleryMedias_images.size() + "");
                        imageuploadcount++;
                        if (imageuploadcount==(galleryMedias_images.size())) {
                            Common.logg("jsonarrayy2", jsonArray + "");
                            post_ad();

                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    dialog.dismiss();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Common.volleyerror(error);
                dialog.dismiss();
            }
        });
        request.addFile("image", Uri.parse(galleryMedias_images.get(i).getUri().replace("file:",""))+"");

        logg("params",request.getFilesToUpload()+"");
        request.setOnProgressListener(new Response.ProgressListener() {
            @Override
            public void onProgress(long transferredBytes, long totalSize) {
                int percentage = (int) ((transferredBytes / ((float) totalSize)) * 100);
                logg("percentage",percentage+"");
//                progress.setProgress(percentage);
            }
        });
        RequestQueue mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        mRequestQueue.add(request);



    }

    JSONObject image_object;
    JSONArray jsonArray=null;
    JSONArray category_array=null;
    private void checkvalidateion() {
        if ((spinner.getSelectedItemPosition() <= 1) && (et_price.getText().toString().trim().length() == 0)) {
            Snakbar("Please fill all fields", et_price);
        } else {

            if (((spinner.getSelectedItemPosition() == 1) && (Integer.parseInt(et_price.getText().toString().trim()) == 0))) {
                if (spinner.getSelectedItemPosition() == 0) {
                    price_type = "price";
                }
                Snakbar("Please entry price", et_price);


            } else {

                if (category_values.size() > 0) {
                    category_array = new JSONArray();
                    for (int i = 0; i < category_values.size(); i++) {
                        JSONObject object = new JSONObject();
                        try {
                            object.put("id", category_values.get(i).getId());
                            object.put("tital", category_values.get(i).getTital());
                            object.put("value", category_values.get(i).getValue());
                            object.put("image", category_values.get(i).getImage());
                            object.put("seletecvalue", category_values.get(i).getSeletedvalue());
                            category_array.put(object);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                    Log.e("jsonarraycategory", category_array + "");


                    if ((spinner.getSelectedItemPosition() == 1) || (spinner.getSelectedItemPosition() == 0)) {
                        price_type = "price";
                        if (et_price.getText().toString().trim().length() > 0) {
                            price = et_price.getText().toString().trim();
                        } else {
                            price = "";
                        }
                    } else if (spinner.getSelectedItemPosition() == 2) {
                        price_type = "ask for price";

                    } else if (spinner.getSelectedItemPosition() == 3) {
                        price_type = "exchange";
                    } else if (spinner.getSelectedItemPosition() == 4) {
                        price_type = "free";
                    }

                    Common.hideSoftKeyboard(Adpost_Add_Activity.this);

                    if (Main_cat_id.equals("4") && !(et_shopname.getText().toString().length() > 0)) {
                        Common.Snakbar("Please enter shop name", et_shopname);
                    } else {


                        if (et_title.getText().toString().trim().length() > 0 && et_desc.getText().toString().trim().length() > 0 &&
                                et_mobile.getText().toString().trim().length() > 0 && price.trim().length() > 0) {
                            if (preferences.getaddress().length() > 0) {
                                dialog.setMessage("Please wait......");
                                dialog.show();
                                if (galleryMedias_images.size() > 0) {
                                    imageuploadcount = 0;
                                    jsonArray = new JSONArray();
                                    for (int i = 0; i < galleryMedias_images.size(); i++) {
                                        upload_image(i);

                                    }
                                } else {
                                    post_ad();

                                }

                            } else {
                                Common.Snakbar("Please select city", et_title);
                            }
                        } else {
                            Common.Snakbar("Please fill all fields", et_title);

                        }
                    }


                } else {
                    Common.Snakbar("Please select what are you advertising?", et_title);
                }
            }
        }
    }

    public void  post_ad(){


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "insert_ads", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                pro.cancel();
                dialog.dismiss();
                logg("insert_ads",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){
                        post_id=object.getJSONObject("data").getString("id");
                        preferences.setadmobile(et_mobile.getText().toString());
                        if (object.getString("message").equals("Ad posted but number no verify")){
                            mobileno_noverifyed();
                        }else {

                            posted_poupup();
                        }


                    }else if (object.getString("message").equals("Total Ad Limit Is Over")){
                        Toast.makeText(mcontext, "Ad limit has been over please upgrade your account", Toast.LENGTH_SHORT).show();

                    }

                }
                catch (Exception e){
//                    pro.cancel();
                    Log.e("error","e",e);
                    dialog.dismiss();
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),et_desc);
                dialog.dismiss();
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(User_id,preferences.getuserid());
                params.put(City,preferences.getaddress());
                params.put(Ad_title,et_title.getText().toString().trim());
                params.put(Ad_description,et_desc.getText().toString().trim());
                params.put(Category,category_array+"");
                params.put(Mobile,et_mobile.getText().toString().replaceFirst("^0+(?!$)", ""));
                if (category_values.size()>0)
                {
                    params.put("category_id", category_values.get(0).getId());
                }
                params.put(Ttimestamp,System.currentTimeMillis()+"");
                if (jsonArray!=null) {
                    params.put(Images, jsonArray + "");
                }
                params.put(Price,price);
                if (sw_mobileno.isSelected()) {
                    params.put("hide_mobileno", "true");
                }else {
                    params.put("hide_mobileno", "false");
                }
                params.put("price_type",price_type);
                if (et_mobile.getText().toString().trim().equals(preferences.getmobile())) {
                    params.put("mobile_verify", preferences.getmobileverification()+"");
                }else {
                    params.put("mobile_verify", "false");
                }
                params.put("main_category",Main_cat_id);
                if (Main_cat_id.equals("4")){
                    params.put("shop_name", et_shopname.getText().toString().trim());
                }

//                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
//                ClipData clip = ClipData.newPlainText("label", params+"");
//                clipboard.setPrimaryClip(clip);
                Log.e("params",params+"");
                return params;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {

                if (response.headers == null)
                {
                    // cant just set a new empty map because the member is final.
                    response = new NetworkResponse(
                            response.statusCode,
                            response.data,
                            Collections.<String, String>emptyMap(), // this is the important line, set an empty but non-null map.
                            response.notModified,
                            response.networkTimeMs);


                }

                return super.parseNetworkResponse(response);
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        mRequestQueue.add(stringRequest);

    }


    @Override
    protected void onResume() {
        super.onResume();

        if (preferences.getaddress().length()>0){
            tv_location.setText(preferences.getaddress());
        }else {
            tv_location.setText("City");
        }
    }


    private void posted_poupup()
    {

        Success_adpost_dialog_fragment dialogfrg = new Success_adpost_dialog_fragment();
        FragmentManager frm = getSupportFragmentManager();
        dialogfrg.show(frm, "");


    }

    private void mobileno_noverifyed()
    {

        Mobile_verification_adpost_dialog_fragment dialogfrg = new Mobile_verification_adpost_dialog_fragment(et_mobile.getText().toString().trim());
        FragmentManager frm = getSupportFragmentManager();
        dialogfrg.setCancelable(false);
        dialogfrg.show(frm, "");


    }


    private void edittext_change_listner(){

        et_price.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length()>0){
                    iv_clear_price.setVisibility(View.VISIBLE);
                }
                else {
                    iv_clear_price.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et_title.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length()>0){
                    iv_clear_tital.setVisibility(View.VISIBLE);
                }
                else {
                    iv_clear_tital.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et_desc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length()>0){
                    iv_clear_des.setVisibility(View.VISIBLE);
                }
                else {
                    iv_clear_des.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et_mobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length()>0){
                    iv_clear_mobile.setVisibility(View.VISIBLE);
                }
                else {
                    iv_clear_mobile.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et_shopname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length()>0){
                    iv_shopname.setVisibility(View.VISIBLE);
                }
                else {
                    iv_shopname.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });





    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Common.hideSoftKeyboard(Adpost_Add_Activity.this);

    }


    public void  verify_ad(final String postid){


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "update_mobileverification", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                dialog.dismiss();
                logg("update_mobileverification",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){
                        setResult(Activity.RESULT_OK);
                        finish();
                    }

                }
                catch (Exception e){
                    Log.e("error","e",e);
                    dialog.dismiss();
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),et_desc);
                dialog.dismiss();
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put("id",postid);

                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        MySingleton.getInstance(context).addToRequestQueue(stringRequest);
        RequestQueue mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        mRequestQueue.add(stringRequest);

    }


}
