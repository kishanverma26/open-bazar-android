package com.delainetech.open_bazar;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.delainetech.open_bazar.Adapter.Detail_Adapter_recyclerview;
import com.delainetech.open_bazar.Adapter.Image_Viewpage_Adapter;
import com.delainetech.open_bazar.Adapter.Similar_Ads_Adapter;
import com.delainetech.open_bazar.Chat.ChatActivity;
import com.delainetech.open_bazar.Chat.FirbaseCommon;
import com.delainetech.open_bazar.Custom_classes.CTextView;
import com.delainetech.open_bazar.Custom_classes.DelayedProgressDialog;
import com.delainetech.open_bazar.Custom_classes.SpacesItemDecoration;
import com.delainetech.open_bazar.Models.ADS;
import com.delainetech.open_bazar.Models.Category_values;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import de.hdodenhof.circleimageview.CircleImageView;
import static com.delainetech.open_bazar.Utils.Common.ParseString;
import static com.delainetech.open_bazar.Utils.Common.logg;

public class Ads_Detail_Activity extends AppCompatActivity implements View.OnClickListener,ParamKeys {


    Context mcontext;

    ImageView iv_back,iv_call,iv_bid,iv_chat;
    TextView tv_call,tv_bid,tv_chat;

    ViewPager view_pager;
    Image_Viewpage_Adapter image_viewpage_adapter;
    private ArrayList<String> postimages = new ArrayList<>();
    RelativeLayout rl_chat,rl_call,rl_bid;
    EditText et_bidamount;
    ImageView iv_icon;
    NestedScrollView nested_scroll;
    ADS ads;
    TextView tv_tital,tv_time,tv_views,tv_favorite_count,tv_location,tv_price,tv_des,tv_imagecount,tv_requiredprice,
            tv_myofferads_counts,tv_myofferads,tv_report;
    CTextView tv_like,tv_favorite,tv_share;
    UserSharedPreferences preferences;
    LinearLayout ll_empty;

    de.hdodenhof.circleimageview.CircleImageView iv_circle;
    TextView tv_username,tv_usernamecard;
    AppCompatButton bt_send;
    CardView cv_offer,cv_profile;

    String bidtype="",image0="";
    Button bt_follow;

//    category
ArrayList<Category_values> category_values=new ArrayList<Category_values>();
    RecyclerView recyclerview;
    Detail_Adapter_recyclerview detail_adapter_recyclerview;
    LinearLayoutManager linearLayoutManager;
    TextView tv_jointime,tv_adcount;

    //    contact us
    ImageView iv_contactus,iv_boost;
    TextView tv_shopname;

    //    online status
    private DatabaseReference mUsersDatabase;
    ImageView iv_online;

//    similar ads
ArrayList<ADS> adsArrayList=new ArrayList<ADS>();
    RecyclerView rv;
    LinearLayout ll_similar_ads;
    GridLayoutManager gridLayoutManager;
    Similar_Ads_Adapter similar_ads_adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_pager_fragment);
        mcontext= Ads_Detail_Activity.this;
        init();

    }



    private void init() {
        preferences=UserSharedPreferences.getInstance(mcontext);

        iv_online=(ImageView) findViewById(R.id.iv_online);

//        similar ads
        ll_similar_ads=(LinearLayout) findViewById(R.id.ll_similar_ads);
        rv=(RecyclerView) findViewById(R.id.rv);
        gridLayoutManager=new GridLayoutManager(mcontext,3);
        rv.setLayoutManager(gridLayoutManager);
        int spacingInPixels = getResources().getDimensionPixelSize(com.zfdang.multiple_images_selector.R.dimen.d_2dp);
        rv.addItemDecoration(new SpacesItemDecoration(3,spacingInPixels,true));

        iv_boost=(ImageView) findViewById(R.id.iv_boost);


        iv_contactus=(ImageView) findViewById(R.id.iv_contactus);
        iv_contactus.setOnClickListener(this);
        tv_shopname=(TextView) findViewById(R.id.tv_shopname);

        bt_follow=(Button) findViewById(R.id.bt_follow);
        bt_follow.setOnClickListener(this);
        tv_jointime=(TextView) findViewById(R.id.tv_jointime);
        tv_adcount=(TextView) findViewById(R.id.tv_adcount);
        cv_offer=(CardView) findViewById(R.id.cv_offer);
        cv_profile=(CardView)findViewById(R.id.cv_profile);
        cv_profile.setOnClickListener(this);
        bt_send=(AppCompatButton) findViewById(R.id.bt_send);
        bt_send.setOnClickListener(this);
        ll_empty=(LinearLayout) findViewById(R.id.ll_empty);
        ads=new ADS();
//        user image
        iv_circle=(CircleImageView) findViewById(R.id.iv_circle);
        iv_circle.setOnClickListener(this);
        tv_username=(TextView) findViewById(R.id.tv_username);
        tv_usernamecard=(TextView) findViewById(R.id.tv_usernamecard);

        tv_tital=(TextView) findViewById(R.id.tv_tital);
        tv_time=(TextView) findViewById(R.id.tv_time);
        tv_views=(TextView) findViewById(R.id.tv_views);
        tv_like=(CTextView) findViewById(R.id.tv_like);
        tv_like.setOnClickListener(this);

        tv_favorite=(CTextView) findViewById(R.id.tv_favorite);
        tv_favorite.setOnClickListener(this);
        tv_favorite_count=(TextView) findViewById(R.id.tv_favorite_count);
        tv_location=(TextView) findViewById(R.id.tv_location);
        tv_price=(TextView) findViewById(R.id.tv_price);
        tv_des=(TextView) findViewById(R.id.tv_des);
        tv_imagecount=(TextView) findViewById(R.id.tv_imagecount);
        tv_requiredprice=(TextView) findViewById(R.id.tv_requiredprice);
        tv_myofferads_counts=(TextView) findViewById(R.id.tv_myofferads_counts);
        tv_myofferads=(TextView) findViewById(R.id.tv_myofferads);
        tv_myofferads.setOnClickListener(this);
        tv_report=(TextView) findViewById(R.id.tv_report);
        tv_report.setOnClickListener(this);

        nested_scroll=(NestedScrollView) findViewById(R.id.nested_scroll);
        et_bidamount=(EditText) findViewById(R.id.et_bidamount);
        iv_icon=(ImageView) findViewById(R.id.iv_icon);
        iv_call=(ImageView)findViewById(R.id.iv_call);
        iv_bid=(ImageView)findViewById(R.id.iv_bid);
        iv_chat=(ImageView)findViewById(R.id.iv_chat);
        tv_call=(TextView) findViewById(R.id.tv_call);
        tv_bid=(TextView) findViewById(R.id.tv_bid);
        tv_chat=(TextView) findViewById(R.id.tv_chat);

        rl_chat=(RelativeLayout) findViewById(R.id.rl_chat);
        rl_chat.setOnClickListener(this);
        rl_call=(RelativeLayout) findViewById(R.id.rl_call);
        rl_call.setOnClickListener(this);
        rl_bid=(RelativeLayout) findViewById(R.id.rl_bid);
        rl_bid.setOnClickListener(this);

        view_pager=(ViewPager) findViewById(R.id.view_pager);


        recyclerview=(RecyclerView) findViewById(R.id.recyclerview);
        linearLayoutManager=new LinearLayoutManager(mcontext);
        recyclerview.setLayoutManager(linearLayoutManager);
        recyclerview.setNestedScrollingEnabled(false);
        iv_back=(ImageView) findViewById(R.id.iv_back);
        iv_back.setOnClickListener(this);
        tv_share=(CTextView) findViewById(R.id.tv_share);
        tv_share.setOnClickListener(this);
        fetech_ads(getIntent().getStringExtra("id"));


     }

    @Override
    public void onBackPressed() {
        if (getIntent().hasExtra(Type)){
            finish();
        }else {
            startActivity(new Intent(Ads_Detail_Activity.this, Home_Screen_Activity.class));
            finish();
        }
    }

    Intent i;
    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.iv_contactus:
                i=new Intent(Ads_Detail_Activity.this,Drawer_Frame_Activity.class);
                i.putExtra(Pos,2);
                i.putExtra(Title,"Contact Us");
                startActivity(i);
                break;
            case R.id.tv_share:
                create_dynamic_link(ads.getAd_title(),"adpost/"+ads.getId());


                break;
            case R.id.bt_follow:
                if (ads.getFollow_user().equals("true")){
                    alert(this,ads.getUser_name());
                }else {

                    follow_user(ads.getUser_id());
                }
                break;
             case R.id.iv_back:
                 onBackPressed();
                 break;
             case R.id.iv_circle:
                 Intent ii=new Intent(mcontext, Other_User_Profile.class);
                 ii.putExtra("id",ads.getUser_id());
                 startActivity(ii);
                break;
              case R.id.cv_profile:
                 ii=new Intent(mcontext, Other_User_Profile.class);
                 ii.putExtra("id",ads.getUser_id());
                 startActivity(ii);
                break;
            case R.id.bt_send:
                if (et_bidamount.getText().toString().trim().length()>0) {
                    if (ads.getPrice_type().equals("price")) {
                        FirbaseCommon.regiter_adpost(ads.getId(),ads.getAd_title(),image0,ads.getPrice(),ads.getMobile(),ads.getHide_mobileno());

                        FirbaseCommon.sendMessage("€ " + et_bidamount.getText().toString(), "offer", ads.getId(), preferences.getuserid()
                                , ads.getUser_id(), preferences.getname());
                    }
                    offer_bid(ads.getId());
                }else {
                    Common.Snakbar("Place an bid amount",tv_bid);
                }
                break;
            case R.id.rl_chat:
                Intent chatIntent = new Intent(mcontext, ChatActivity.class);
                chatIntent.putExtra("user_id", ads.getUser_id());
                chatIntent.putExtra("user_name", ads.getUser_name());
                chatIntent.putExtra("name", preferences.getname());
                chatIntent.putExtra("id",preferences.getuserid());
                Log.e("ides", ads.getUser_id() + ",,"  + ",," + preferences.getuserid());
                startActivity(chatIntent);

                customnavbar(2);
                break;

            case R.id.rl_call:
                if (ads.getHide_mobileno().equals("false")) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:"+ads.getMobile()));
                    startActivity(intent);
                    customnavbar(0);
                }else {
                    Toast.makeText(mcontext, "Contact the user for phone number", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.rl_bid:

                customnavbar(1);
                break;
          case R.id.tv_like:
              logg("tets",ads.getLike_status());
              if (ads.getLike_status().equals("No")) {
                  view(ads.getId(), "likes");
              }
                break;
      case R.id.tv_favorite:
              view(ads.getId(),"favorite");

                break;
     case R.id.tv_report:
//              view(ads.getId(),"favorite");
         inflateDialogue();
                break;

           case R.id.tv_myofferads:
          Intent  i=new Intent(mcontext, All_offers_Activity.class);
            i.putExtra("count",Integer.parseInt(ads.getOffer_count()));
            i.putExtra("id",ads.getId());
            i.putExtra("bid_type",bidtype);
            startActivityForResult(i,OFFER_COUNT);

                break;


        }
    }



    //custom navigation bar

    private void customnavbar(int position){
        int active_color=R.color.on_click_icon_color;
        int inactive_color=R.color.white;
        tv_call.setTextColor(getResources().getColor(inactive_color));
        tv_bid.setTextColor(getResources().getColor(inactive_color));
        tv_chat.setTextColor(getResources().getColor(inactive_color));
        final float scale = getResources().getDisplayMetrics().density;
        int active_height_ = (int) (26 * scale + 0.5f);
        int active_width_ = (int) (26 * scale + 0.5f);
        int inactive_height_ = (int) (24 * scale + 0.5f);
        int inactive_width_ = (int) (24 * scale + 0.5f);

        iv_call.setColorFilter(ContextCompat.getColor(mcontext, inactive_color), android.graphics.PorterDuff.Mode.SRC_IN);
        iv_bid.setColorFilter(ContextCompat.getColor(mcontext, inactive_color), android.graphics.PorterDuff.Mode.SRC_IN);
        iv_chat.setColorFilter(ContextCompat.getColor(mcontext, inactive_color), android.graphics.PorterDuff.Mode.SRC_IN);

        iv_call.getLayoutParams().height = inactive_height_;
        iv_call.getLayoutParams().width =inactive_width_;
        iv_bid.getLayoutParams().height = inactive_height_;
        iv_bid.getLayoutParams().width =inactive_width_;
        iv_chat.getLayoutParams().height = inactive_height_;
        iv_chat.getLayoutParams().width =inactive_width_;


        switch (position){
            case 0:
            tv_call.setTextColor(getResources().getColor(active_color));
          iv_call.setColorFilter(ContextCompat.getColor(mcontext, active_color), android.graphics.PorterDuff.Mode.SRC_IN);
                iv_call.getLayoutParams().height = active_height_;
               iv_call.getLayoutParams().width =active_width_;

                //Action
                et_bidamount.setFocusable(false);

                break;

            case 1:
                tv_bid.setTextColor(getResources().getColor(active_color));
                iv_bid.setColorFilter(ContextCompat.getColor(mcontext, active_color), android.graphics.PorterDuff.Mode.SRC_IN);

                iv_bid.getLayoutParams().height = active_height_;
                iv_bid.getLayoutParams().width = active_width_;

//              Action
                et_bidamount.setFocusable(true);
                nested_scroll.post(new Runnable() {
                    @Override
                    public void run() {
                        nested_scroll.fullScroll(View.FOCUS_DOWN);
                    }
                });
                break;


            case 2:
             tv_chat.setTextColor(getResources().getColor(active_color));
          iv_chat.setColorFilter(ContextCompat.getColor(mcontext, active_color), android.graphics.PorterDuff.Mode.SRC_IN);

                iv_chat.getLayoutParams().height = active_height_;
                iv_chat.getLayoutParams().width = active_width_;
                et_bidamount.setFocusable(false);

//                action
                break;

        }

    }

    @Override
    public void onResume() {
        super.onResume();
        customnavbar(4);
    }



    public void  view(final String postid, final String type){


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "ad_view", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                pro.cancel();
                logg("insert_ads",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){

                        if (type.equals("likes")){

                            likedislike(Common.ParseString(object.getJSONObject("data"),"likes"));
                            if (Common.ParseString(object.getJSONObject("data"),"likes").equals("0")){
                                tv_like.setText((Integer.parseInt(tv_like.getText().toString())-1)+"");

                            }else {
                                tv_like.setText((Integer.parseInt(tv_like.getText().toString())+1)+"");

                            }
                            ads.setLike_count(tv_like.getText().toString());
                        }else if (type.equals("favorite"))
                        {
                            addfavriout(Common.ParseString(object.getJSONObject("data"),"favorite"));
                        if (Common.ParseString(object.getJSONObject("data"),"favorite").equals("0")){
                            tv_favorite_count.setText((Integer.parseInt(tv_favorite_count.getText().toString())-1)+"");

                        }else {
                            tv_favorite_count.setText((Integer.parseInt(tv_favorite_count.getText().toString())+1)+"");

                        }
                            ads.setFavorite_count(tv_favorite_count.getText().toString());
                        }

                    }

                }
                catch (Exception e){
//                    pro.cancel();
                    Log.e("error","e",e);
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(User_id,preferences.getuserid());
                params.put("post_id",postid);
                params.put("type",type);

                Log.e("params",params+"");
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }


    public void likedislike(String count){

        if (count.equals("0")){
            ads.setLike_status("No");
            tv_like.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_like_white_24dp,0,0,0);
        }else {
            ads.setLike_status("Yes");
            tv_like.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_like_24dp,0,0,0);

        }
    }

    public void addfavriout(String count){
        if (count.equals("0")){
            ads.setFav_status("No");

            tv_favorite.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_favorite_15dp_grey,0,0,0);
        }else {

            ads.setFav_status("Yes");
            tv_favorite.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_favorite_red_15dp,0,0,0);

        }
    }




    public void  offer_bid(final String postid){

        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "offer_bid", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                pro.cancel();
                logg("insert_ads",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){

                        et_bidamount.setText("");
                        if(bidtype.equals("0")) {
                            Common.Snakbar("Your offer has been placed", tv_bid);
                        }
                        else
                        {
                            Common.Snakbar("Your comment has been placed",tv_bid);
                        }
                        tv_myofferads_counts.setText((Integer.parseInt(ads.getOffer_count())+1)+"");
                        ads.setOffer_count((Integer.parseInt(ads.getOffer_count())+1)+"");
                        cv_offer.setVisibility(View.VISIBLE);

                    }

                }
                catch (Exception e){
//                    pro.cancel();
                    Log.e("error","e",e);
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(User_id,preferences.getuserid());
                params.put(Post_id,postid);
                 params.put(Bid_price,et_bidamount.getText().toString().trim());
                 params.put(Ttimestamp,System.currentTimeMillis()+"");
                 params.put(Bid_type,bidtype);

                Log.e("params",params+"");
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }


    public void  report_ad(final String postid, final String des){

        final DelayedProgressDialog pro=new DelayedProgressDialog();

        pro.show(getSupportFragmentManager(),"");

        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "post_report", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                pro.cancel();
                logg("post_report",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){
                        Toast.makeText(mcontext, "Report has be submitted successfully", Toast.LENGTH_SHORT).show();

                    }

                }
                catch (Exception e){
                    pro.cancel();
                    Log.e("error","e",e);
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(User_id,preferences.getuserid());
                params.put(Post_id,postid);
                params.put("description",des);
                Log.e("params",params+"");
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("innn","innn1111");
        logg("innnrequestcode=",requestCode+", resultecode="+resultCode);

        if (requestCode==OFFER_COUNT && resultCode==RESULT_OK){
            Log.e("innn","innn");
            ads.setOffer_count(data.getIntExtra("count",0)+"");
            tv_myofferads_counts.setText(ads.getOffer_count());

        }

    }


    public void inflateDialogue() {
        LayoutInflater layoutInflater=this.getLayoutInflater();
        final View view = layoutInflater.inflate(R.layout.ad_report_dialog_lay, null);
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Ad Report");

        alertDialog.setCancelable(false);

      final EditText  et_des = (EditText) view.findViewById(R.id.et_des);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (et_des.getText().toString().trim().length()>0) {
                    report_ad(ads.getId(),et_des.getText().toString().trim());
                    dialog.dismiss();
                }else {
                    Toast.makeText(Ads_Detail_Activity.this, "Write your report resion", Toast.LENGTH_SHORT).show();

                }


            }
        });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.setView(view);
        alertDialog.show();
    }


    public void  follow_user(final String userid){


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "follow", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                pro.cancel();
                logg("follow",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){
                        if (object.getString("message").equals("Unfollow Successfully")){
                            bt_follow.setText(getString(R.string.follow_now));
                            ads.setFollow_user("false");
                        }else {
                            bt_follow.setText(getString(R.string.following));
                            ads.setFollow_user("true");
                        }
                        }

                }
                catch (Exception e){
//                    pro.cancel();
                    Log.e("error","e",e);
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(User_id,preferences.getuserid());
                params.put("following_id",userid);
                Log.e("params",params+"");
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }


    public void alert(Context mcoxt,String name){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mcoxt);

        alertDialog.setMessage("Are you sure you want to unfollow "+name+"?");
        alertDialog.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                follow_user(ads.getUser_id());
            }
        });
        alertDialog.setNegativeButton(getResources().getString(R.string.No), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog1= alertDialog.create();
        alertDialog1.show();

        alertDialog1.getButton(alertDialog1.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorAccent));
        alertDialog1.getButton(alertDialog1.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorAccent));
    }


    public void  fetech_ads(final String id){

        final DelayedProgressDialog pro=new DelayedProgressDialog();

        pro.show(getSupportFragmentManager(),"");


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "getpost_detail", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                pro.cancel();
                logg("getpost_detail",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){

                        JSONArray jsonArray=object.getJSONArray("data");

                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject object1=jsonArray.getJSONObject(i);
                            ads.setId(ParseString(object1,"id"));
                            ads.setUser_id(ParseString(object1,"user_id"));
                            ads.setAd_title(ParseString(object1,"ad_title"));
                            ads.setAd_description(ParseString(object1,"ad_description"));
                            ads.setMobile(ParseString(object1,"mobile"));
                            ads.setPrice(ParseString(object1,"price"));
                            ads.setImages(ParseString(object1,"images"));
                            ads.setCategory(ParseString(object1,"category"));
                            ads.setCity(ParseString(object1,"city"));
                            ads.setFavorite_count(ParseString(object1,"favorite_count"));
                            ads.setViews_count(ParseString(object1,"views_count"));
                            ads.setLike_count(ParseString(object1,"like_count"));
                            ads.setTimestamp(ParseString(object1,"timestamp"));
                            ads.setFav_status(ParseString(object1,"fav_stattus"));
                            ads.setLike_status(ParseString(object1,"like_stattus"));
                            ads.setUser_name(ParseString(object1,"user_name"));
                            ads.setUser_image(ParseString(object1,"user_image"));
                            ads.setExpired_date(ParseString(object1,"expired_date"));
                            ads.setOffer_count(ParseString(object1,"offer_count"));
                            ads.setPrice_type(ParseString(object1,"price_type"));
                             ads.setMain_category(ParseString(object1,"main_category"));
                             ads.setUser_regiterdate(ParseString(object1,"user_regiterdate"));
                             ads.setAds_count(ParseString(object1,"ads_count"));
                             ads.setFollow_user(ParseString(object1,"follow_user"));
                             ads.setVerify_phone(ParseString(object1,"mobile_verify"));
                             ads.setHide_mobileno(ParseString(object1,"hide_mobileno"));
                            ads.setBoost_type(ParseString(object1,"boost_type"));

                        }

                    }
                    setdata();

                }
                catch (Exception e){
                    pro.cancel();
                    Log.e("error","e",e);

                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),et_bidamount);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put("id",id);
                params.put(User_id,preferences.getuserid()+"");



                logg("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        MySingleton.getInstance(context).addToRequestQueue(stringRequest);
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }


    public void setdata(){

        tv_tital.setText(ads.getAd_title());
        tv_time.setText(Common.getTimeAgo(Long.parseLong(ads.getTimestamp()), Ads_Detail_Activity.this));
        tv_like.setText(ads.getLike_count());
        tv_location.setText(ads.getCity());
        tv_views.setText(ads.getViews_count());
        tv_des.setText(ads.getAd_description());

        tv_favorite_count.setText(ads.getFavorite_count());

        if (ads.getOffer_count().equals("0")){
            cv_offer.setVisibility(View.GONE);
        }

        if (ads.getPrice_type().equals("price"))
        {
            tv_price.setText("€ "+ads.getPrice());
            bidtype="0";
            int maxLength = 10;
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(maxLength);
            et_bidamount.setFilters(FilterArray);
            et_bidamount.setInputType(InputType.TYPE_CLASS_NUMBER);
            tv_myofferads_counts.setText(ads.getOffer_count());
            tv_requiredprice.setText("The required price € "+ads.getPrice());
            et_bidamount.setHint(mcontext.getResources().getString(R.string.enter_the_amount_of_offer));
            iv_icon.setImageDrawable(mcontext.getResources().getDrawable(R.drawable.ic_auction_red));
            iv_bid.setImageDrawable(mcontext.getResources().getDrawable(R.drawable.ic_auction));
            tv_bid.setText(mcontext.getResources().getString(R.string.bid));
            bt_send.setText(mcontext.getResources().getString(R.string.place_your_offer));

        }
        else  if (ads.getPrice_type().equals("ask for price"))
        {
            tv_price.setText(mcontext.getResources().getString(R.string.askforprice));
            bidtype="1";
            et_bidamount.setInputType(InputType.TYPE_CLASS_TEXT);
            tv_myofferads.setText(mcontext.getResources().getString(R.string.seeallcomments));
            tv_requiredprice.setText(mcontext.getResources().getString(R.string.contactuseraboutad));
            et_bidamount.setHint(mcontext.getResources().getString(R.string.enteryourcomment));
            iv_icon.setImageDrawable(mcontext.getResources().getDrawable(R.drawable.ic_chat1));
            iv_bid.setImageDrawable(mcontext.getResources().getDrawable(R.drawable.ic_chat_icon));
            tv_bid.setText(mcontext.getResources().getString(R.string.comment));
            bt_send.setText(mcontext.getResources().getString(R.string.sendyourcomment));

        }
        else  if (ads.getPrice_type().equals("exchange"))
        {
            bidtype="1";
            et_bidamount.setInputType(InputType.TYPE_CLASS_TEXT);
            tv_price.setText(mcontext.getResources().getString(R.string.exchange));
            tv_myofferads.setText(mcontext.getResources().getString(R.string.seeallcomments));
            tv_requiredprice.setText(mcontext.getResources().getString(R.string.contactuseraboutad));
            et_bidamount.setHint(mcontext.getResources().getString(R.string.enteryourcomment));
            iv_icon.setImageDrawable(mcontext.getResources().getDrawable(R.drawable.ic_chat1));
            iv_bid.setImageDrawable(mcontext.getResources().getDrawable(R.drawable.ic_chat_icon));
            tv_bid.setText(mcontext.getResources().getString(R.string.comment));
            bt_send.setText(mcontext.getResources().getString(R.string.sendyourcomment));
        }
        else  if (ads.getPrice_type().equals("free"))
        {
            bidtype="1";
            tv_price.setText("Free");
            et_bidamount.setInputType(InputType.TYPE_CLASS_TEXT);
            tv_myofferads.setText(mcontext.getResources().getString(R.string.seeallcomments));
            tv_requiredprice.setText(mcontext.getResources().getString(R.string.contactuseraboutad));
            et_bidamount.setHint(mcontext.getResources().getString(R.string.enteryourcomment));
            iv_icon.setImageDrawable(mcontext.getResources().getDrawable(R.drawable.ic_chat1));
            iv_bid.setImageDrawable(mcontext.getResources().getDrawable(R.drawable.ic_chat_icon));
            tv_bid.setText(mcontext.getResources().getString(R.string.comment));
            bt_send.setText(mcontext.getResources().getString(R.string.sendyourcomment));
        }


        if (ads.getImages().length()>0) {
            try {
                ll_empty.setVisibility(View.GONE);
                view_pager.setVisibility(View.VISIBLE);
                JSONArray jsonArray = new JSONArray(ads.getImages());
                for (int i=0;i<jsonArray.length();i++) {

                    postimages.add(jsonArray.getJSONObject(i).getString("image"));
                    image0=jsonArray.getJSONObject(0).getString("image");

                }
                logg("image", Common.Image_Loading_Url(jsonArray.getJSONObject(0).getString("image")) + "");
            } catch (JSONException e) {
                e.printStackTrace();
                logg("excep", e + "");
            }
            tv_imagecount.setText("1 / "+postimages.size());
        }else {
            view_pager.setVisibility(View.GONE);
            ll_empty.setVisibility(View.VISIBLE);
            tv_imagecount.setText("0");
        }

        image_viewpage_adapter=new Image_Viewpage_Adapter(mcontext,postimages,ads);
        view_pager.setAdapter(image_viewpage_adapter);
        view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (ads.getImages().length()>0) {

                    tv_imagecount.setText((position + 1) + " / " + postimages.size());
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        RequestOptions options=new RequestOptions().centerCrop().error(R.drawable.userimage);
        Glide.with(mcontext).load(Common.Image_Loading_Url(ads.getUser_image())).apply(options).into(iv_circle);
        tv_username.setText(ads.getUser_name());
        tv_usernamecard.setText(ads.getUser_name());
        view(ads.getId(),"view");

        if (ads.getLike_status().equals("Yes")){
            likedislike("1");
        }else {
            likedislike("0");
        }
        Common.logg("logdata",ads.getFav_status());
        Common.logg("logdata",ads.getLike_status());
        if (ads.getFav_status().equals("Yes")){
            addfavriout("1");
        }else {
            addfavriout("0");
        }


//        category

        category_values.add(new Category_values("Ad id",ads.getId(),"",
                "",""));
        if (ads.getCategory().length()>0){
            try {
                JSONArray jsonArray=new JSONArray(ads.getCategory());
                for (int i=2;i<jsonArray.length();i++){
                    JSONObject jsonObject=jsonArray.getJSONObject(i);
                    category_values.add(new Category_values(ParseString(jsonObject,"tital"),ParseString(jsonObject,"seletecvalue"),ParseString(jsonObject,"id"),
                            ParseString(jsonObject,"value"),ParseString(jsonObject,"image")));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            detail_adapter_recyclerview=new Detail_Adapter_recyclerview(mcontext,category_values);
            recyclerview.setAdapter(detail_adapter_recyclerview);
        }
        tv_adcount.setText(ads.getAds_count()+" Ads");
        tv_jointime.setText(getResources().getString(R.string.member_since)+" "+Common.changeDateFormatFromAnother(ads.getUser_regiterdate()));

        if (ads.getFollow_user().equals("true")){
            bt_follow.setText(getString(R.string.following));
        }else {
            bt_follow.setText(getString(R.string.follow_now));

        }

        similar_ads();
        //        firbase chat online offline status
        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child("user"+ads.getUser_id());
        mUsersDatabase.keepSynced(true);

        online_offline_firbaselistner();
    }

    private void online_offline_firbaselistner() {
        mUsersDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild("online")) {

                    String userOnline = dataSnapshot.child("online").getValue().toString();

                    if(userOnline.equals("true")){

                        iv_online.setVisibility(View.VISIBLE);

                    } else {

                        iv_online.setVisibility(View.INVISIBLE);

                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void create_dynamic_link(final String msg, String str){

        Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse("https://openbazar.page.link?="+str))
                .setDomainUriPrefix("https://openbazar.page.link")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder("com.open_bazar")
                        .setFallbackUrl(Uri.parse("https://www.dropbox.com/s/o1z7e9w94g7twqz/app-debug.apk?dl=0"))
                        .build())
                // Set parameters
                // ...
                .buildShortDynamicLink()

                .addOnCompleteListener(Ads_Detail_Activity.this, new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {
                            // Short link created
                            Uri shortLink = task.getResult().getShortLink();
                            Uri flowchartLink = task.getResult().getPreviewLink();

                            logg("shortLink",shortLink+" flowchartLink"+flowchartLink);


                            String shareBody2 = msg+" "+shortLink;
                            Intent sharingIntent2 = new Intent(android.content.Intent.ACTION_SEND);
                            sharingIntent2.setType("text/plain");
                            sharingIntent2.putExtra(android.content.Intent.EXTRA_SUBJECT, "Open Bazar");
                            sharingIntent2.putExtra(android.content.Intent.EXTRA_TEXT, shareBody2);
                            startActivity(Intent.createChooser(sharingIntent2, ""));

                        } else {
                            // Error
                            // ...
                            Toast.makeText(mcontext, "Somthing went wrong please try again", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    public void  similar_ads(){

//        final DelayedProgressDialog pro=new DelayedProgressDialog();
//
//        pro.show(getSupportFragmentManager(),"");


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "similar_ads", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                pro.cancel();
                logg("similar_ads",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){

                        JSONArray jsonArray=object.getJSONArray("data");

                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject object1=jsonArray.getJSONObject(i);
                            if (!ads.getId().equals(ParseString(object1,"id"))) {
                                adsArrayList.add(new ADS(ParseString(object1, "id"), ParseString(object1, "user_id"), ParseString(object1, "ad_title"),
                                        ParseString(object1, "ad_description"), ParseString(object1, "mobile"), ParseString(object1, "price")
                                        , ParseString(object1, "images"), ParseString(object1, "category"), ParseString(object1, "city")
                                        , ParseString(object1, "favorite_count"), ParseString(object1, "views_count"), ParseString(object1, "like_count")
                                        , ParseString(object1, "timestamp"), ParseString(object1, "fav_stattus"), ParseString(object1, "like_stattus")
                                        , ParseString(object1, "user_name"), ParseString(object1, "user_image"),
                                        ParseString(object1, "expired_date"), ParseString(object1, "offer_count"),
                                        ParseString(object1, "price_type"), ParseString(object1, "main_category"),
                                        ParseString(object1, "user_regiterdate"), ParseString(object1, "ads_count")
                                        , ParseString(object1, "follow_user"), ParseString(object1, "mobile_verify")
                                        , ParseString(object1, "hide_mobileno"), ParseString(object1, "shop_name")
                                        , ParseString(object1, "boost_type"), ParseString(object1, "category_id")));
                            }
                        }
                        similar_ads_adapter=new Similar_Ads_Adapter(Ads_Detail_Activity.this,adsArrayList);

                        rv.setAdapter(similar_ads_adapter);
                    }

                    if (adsArrayList.size()==0){
                        ll_similar_ads.setVisibility(View.GONE);
                    }

                }
                catch (Exception e){
//                    pro.cancel();
                    Log.e("error","e",e);

                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),ll_similar_ads);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put("id",ads.getUser_id());
                params.put(User_id,preferences.getuserid()+"");
                params.put("category_id",ads.getCategory_id());
                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        MySingleton.getInstance(context).addToRequestQueue(stringRequest);
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }


}
