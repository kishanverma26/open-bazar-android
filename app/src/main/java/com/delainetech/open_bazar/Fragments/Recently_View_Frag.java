package com.delainetech.open_bazar.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.delainetech.open_bazar.Adapter.My_Ads_Recentview_adapter_Other_Users;
import com.delainetech.open_bazar.Adapter.My_Ads_adapter_Other_Users;
import com.delainetech.open_bazar.Adapter.My_Ads_adapter_User;
import com.delainetech.open_bazar.Custom_classes.EndlessRecyclerViewScrollListener;
import com.delainetech.open_bazar.Models.ADS;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import static com.delainetech.open_bazar.Utils.Common.ParseString;
import static com.delainetech.open_bazar.Utils.Common.logg;

public class Recently_View_Frag extends Fragment implements ParamKeys,SwipeRefreshLayout.OnRefreshListener,View.OnClickListener{


    View v;
    Context mcontext;
    UserSharedPreferences preferences;

    RecyclerView reclcler;
    LinearLayoutManager linearLayoutManager;
    My_Ads_Recentview_adapter_Other_Users adapter;
    int index=1;
    EndlessRecyclerViewScrollListener recyclerViewScrollListener;
    SwipeRefreshLayout swiperefresh;
    ArrayList<ADS> adsArrayList=new ArrayList<>();
    TextView tvemptyview;
    boolean isViewShown=false;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.recently_view_frag, container, false);
        mcontext=getActivity();
        initial();

        return v;
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        index=1;
        if (getView() != null) {
            isViewShown = true;
            // fetchdata() contains logic to show data when page is selected mostly asynctask to fill the data
            Log.e("view",isVisibleToUser+"");

        } else {
            isViewShown = false;
        }



    }

    private void initial() {
        preferences= UserSharedPreferences.getInstance(mcontext);
        tvemptyview=v.findViewById(R.id.tvemptyview);
        tvemptyview.setOnClickListener(this);
        swiperefresh=v.findViewById(R.id.swiperefresh);
        reclcler=v.findViewById(R.id.reclcler);
        linearLayoutManager=new LinearLayoutManager(mcontext);
        reclcler.setLayoutManager(linearLayoutManager);
        adapter=new My_Ads_Recentview_adapter_Other_Users(mcontext,adsArrayList,Recently_View_Frag.this);
        reclcler.setAdapter(adapter);


        fetech_ads(index);
        recyclerViewScrollListener=new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if ( (adsArrayList.size()%10)==0){
                    index++;
                    fetech_ads(index);
                }

            }
        };
        reclcler.addOnScrollListener(recyclerViewScrollListener);
        swiperefresh.setOnRefreshListener(this);

        swiperefresh.setColorSchemeColors(getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorPrimary),getResources().getColor(R.color.colorPrimary));



    }



    @Override
    public void onRefresh() {
        adsArrayList.clear();
        index=1;
        fetech_ads(index);

    swiperefresh.setRefreshing(false);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.home_notification:

                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==11){

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("test","test");

    }


    public void  fetech_ads(final int index){

//        final DelayedProgressDialog pro=new DelayedProgressDialog();
//
//        pro.show(getSupportFragmentManager(),"");


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "recent_view", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                pro.cancel();
                logg("recent_view",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){

                        JSONArray jsonArray=object.getJSONArray("data");

                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject object1=jsonArray.getJSONObject(i);

                            adsArrayList.add(new ADS(ParseString(object1,"id"),ParseString(object1,"user_id"),ParseString(object1,"ad_title"),
                                    ParseString(object1,"ad_description"),ParseString(object1,"mobile"),ParseString(object1,"price")
                                    ,ParseString(object1,"images"),ParseString(object1,"category"),ParseString(object1,"city")
                                    ,ParseString(object1,"favorite_count"),ParseString(object1,"views_count"),ParseString(object1,"like_count")
                                    ,ParseString(object1,"timestamp"),ParseString(object1,"fav_stattus"),ParseString(object1,"like_stattus")
                                    ,ParseString(object1,"user_name"),ParseString(object1,"user_image"),
                                    ParseString(object1,"expired_date"), ParseString(object1,"offer_count"),
                                    ParseString(object1,"price_type"),ParseString(object1,"main_category"),
                                    ParseString(object1,"user_regiterdate"),ParseString(object1,"ads_count")
                                    ,ParseString(object1,"follow_user"),ParseString(object1,"mobile_verify")
                                    ,ParseString(object1,"hide_mobileno"),ParseString(object1,"shop_name")
                                    , ParseString(object1, "boost_type"), ParseString(object1, "category_id")));
                        }

                        Collections.reverse(adsArrayList);
                        adapter.notifyDataSetChanged();

                    }


                }
                catch (Exception e){
//                    pro.cancel();
                    Log.e("error","e",e);

                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),reclcler);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put("index",index+"");
                params.put(User_id,preferences.getuserid()+"");



                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        MySingleton.getInstance(context).addToRequestQueue(stringRequest);
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }

}
