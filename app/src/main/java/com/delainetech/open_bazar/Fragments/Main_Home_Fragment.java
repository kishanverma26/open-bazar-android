package com.delainetech.open_bazar.Fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.paperdb.Paper;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.delainetech.open_bazar.Adapter.Home_adapter;
import com.delainetech.open_bazar.Adapter.Search_adapter;
import com.delainetech.open_bazar.Adpost_Add_Activity;
import com.delainetech.open_bazar.Category_Activity_filter;
import com.delainetech.open_bazar.Custom_classes.DelayedProgressDialog;
import com.delainetech.open_bazar.Custom_classes.EndlessRecyclerViewScrollListener;
import com.delainetech.open_bazar.Custom_classes.MySingleton;
import com.delainetech.open_bazar.Filter_Activity;
import com.delainetech.open_bazar.Home_Screen_Activity;
import com.delainetech.open_bazar.Location_picker_Activity;
import com.delainetech.open_bazar.Login_Activity;
import com.delainetech.open_bazar.Models.ADS;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Search_Activity;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;
import com.facebook.accountkit.AccountKit;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.pchmn.materialchips.ChipView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.delainetech.open_bazar.Utils.Common.ParseString;
import static com.delainetech.open_bazar.Utils.Common.logg;

@SuppressLint("NewApi")
public class Main_Home_Fragment extends Fragment implements View.OnClickListener,SwipeRefreshLayout.OnRefreshListener,
        View.OnScrollChangeListener ,ParamKeys {
    View view;

    RecyclerView rview;
    GridLayoutManager gridLayoutManager;
    UserSharedPreferences userPrefernces,userSharedPreferences;
    SwipeRefreshLayout swipeContainer;
    TextView EmptyView;
    Home_adapter home_adapter;
    Context mcontext;
    MaterialSpinner spinner;
    ImageView iv_drawer,iv_up;
    EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    ImageView iv_location,iv_notification;
    RelativeLayout rl_search,rl_sell,rl_rent,rl_services,rl_shop;
    ArrayList<ADS> adsArrayList=new ArrayList<ADS>();

    int index=1;
    String type="all",str_city="",str_cat="";
    ImageView iv_sell,iv_rent,iv_service,iv_shops;
    TextView tv_sell,tv_rent,tv_service,tv_shops,tv_location,tv_cat,tv_filter;
    LinearLayout ll_filter;
    Intent i;

    String str_search="",str_location="",str_pricetype="",str_price_from="",str_price_to="",sort_by="",condittion="",price_type="";
    boolean isfilter=false;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.home_fragment, container, false);
        mcontext=getActivity();
        init();

        return view;
    }

    private void init() {
        userPrefernces= UserSharedPreferences.getInstance(getActivity());
        userSharedPreferences=new  UserSharedPreferences(getActivity(),"");
        tv_filter=view.findViewById(R.id.tv_filter);
        tv_filter.setOnClickListener(this);
        tv_location=view.findViewById(R.id.tv_location);
        tv_location.setOnClickListener(this);
        tv_cat=view.findViewById(R.id.tv_cat);
        tv_cat.setOnClickListener(this);
        ll_filter=view.findViewById(R.id.ll_filter);
        ll_filter.setVisibility(View.GONE);
        iv_sell=view.findViewById(R.id.iv_sell);
        iv_rent=view.findViewById(R.id.iv_rent);
        iv_service=view.findViewById(R.id.iv_service);
        iv_shops=view.findViewById(R.id.iv_shops);
        tv_sell=view.findViewById(R.id.tv_sell);
        tv_rent=view.findViewById(R.id.tv_rent);
        tv_service=view.findViewById(R.id.tv_service);
        tv_shops=view.findViewById(R.id.tv_shops);

        rl_search=(RelativeLayout) view.findViewById(R.id.rl_search);
        rl_search.setOnClickListener(this);
        rl_sell=(RelativeLayout) view.findViewById(R.id.rl_sell);
        rl_sell.setOnClickListener(this);
        rl_rent=(RelativeLayout) view.findViewById(R.id.rl_rent);
        rl_rent.setOnClickListener(this);
        rl_services=(RelativeLayout) view.findViewById(R.id.rl_services);
        rl_services.setOnClickListener(this);
        rl_shop=(RelativeLayout) view.findViewById(R.id.rl_shop);
        rl_shop.setOnClickListener(this);

        iv_drawer=(ImageView)view.findViewById(R.id.iv_drawer);
        iv_drawer.setOnClickListener(this);

        // scroll to top
        iv_up=(ImageView) view.findViewById(R.id.iv_up);
        iv_up.setOnClickListener(this);

        //image view location and notification
        iv_location=(ImageView) view.findViewById(R.id.iv_location);
        iv_location.setOnClickListener(this);
        iv_notification=(ImageView) view.findViewById(R.id.iv_notification);
        iv_notification.setOnClickListener(this);

        spinner=(MaterialSpinner) view.findViewById(R.id.spinner);
        spinner.setItems("India", "USA", "UAE", "UK");

        rview=(RecyclerView)view.findViewById(R.id.recyclerview);
        swipeContainer=(SwipeRefreshLayout)view.findViewById(R.id.Swip_refreshlayout);
        swipeContainer.setOnRefreshListener(this);
        gridLayoutManager=new GridLayoutManager(getActivity(),2);
        rview.setLayoutManager(gridLayoutManager);

        home_adapter=new Home_adapter(mcontext,adsArrayList,Main_Home_Fragment.this);
        rview.setAdapter(home_adapter);

//        if (Paper.book("get_adposts1").contains("response")){
//            if (adsArrayList.size()==0) {
////                load_ads(Paper.book("get_adposts1").read("response").toString());
//            }
//            }


        fetech_ads(index);
        endlessRecyclerViewScrollListener=new EndlessRecyclerViewScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if ( (adsArrayList.size()%10)==0){
                    index++;
                    if (isfilter){
                        filter(index);
                    }else {
                        fetech_ads(index);
                    }
                }
            }
        };
        rview.addOnScrollListener(endlessRecyclerViewScrollListener);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorPrimary),getResources().getColor(R.color.colorPrimary));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            rview.setOnScrollChangeListener(this);
        }else {
            iv_up.setVisibility(View.GONE);
        }

        ((Home_Screen_Activity)mcontext).fab_add_ad.setOnClickListener(this);





    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_drawer:
                ((Home_Screen_Activity)getActivity()).drawer.openDrawer(GravityCompat.START);
                break;

            case R.id.iv_up:
                rview.scrollToPosition(0);
                break;


            case R.id.tv_cat:
                i=new Intent(mcontext, Category_Activity_filter.class);
                i.putExtra("id",type);
                startActivityForResult(i,FILTER_CAT);
                break;
            case R.id.tv_filter:
                startActivityForResult(new Intent(mcontext, Filter_Activity.class),FILTER_ACTIVITY);
                break;
            case R.id.iv_location:
                startActivity(new Intent(mcontext,Location_picker_Activity.class));
                break;
            case R.id.rl_search:
                startActivity(new Intent(mcontext, Search_Activity.class));
                break;
            case R.id.rl_shop:
                ll_filter.setVisibility(View.VISIBLE);
                custom_navbar(4);
                type="4";
                index=1;
                adsArrayList.clear();
                fetech_ads(1);
                break;
            case R.id.rl_sell:
                ll_filter.setVisibility(View.VISIBLE);
                custom_navbar(1);
                type="1";
                index=1;
                adsArrayList.clear();
                fetech_ads(1);
                break;
            case R.id.rl_rent:
                ll_filter.setVisibility(View.VISIBLE);
                custom_navbar(2);
                type="2";
                index=1;
                adsArrayList.clear();
                fetech_ads(1);
                break;
            case R.id.rl_services:
                ll_filter.setVisibility(View.VISIBLE);
                custom_navbar(3);
                type="3";
                index=1;
                adsArrayList.clear();
                fetech_ads(1);
                break;

            case R.id.fab_add_ad:
                Intent i=new Intent(mcontext, Adpost_Add_Activity.class);
                startActivityForResult(i,20);
                break;

            case R.id.tv_location:
                i=new Intent(mcontext,Location_picker_Activity.class);
                i.putExtra("filter",true);
                startActivityForResult(i,LOCTAION_CITY);
                break;

        }
    }

    @Override
    public void onRefresh() {
        ll_filter.setVisibility(View.GONE);
        custom_navbar(5);
        str_city="";
        str_cat="";
        isfilter=false;
        type="all";
        adsArrayList.clear();
        index=1;
        fetech_ads(index);
        swipeContainer.setRefreshing(false);
        tv_cat.setText(getResources().getString(R.string.category));
        tv_location.setText("Location");

    }

    int firstVisibleItem=0;
    @Override
    public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        firstVisibleItem = gridLayoutManager.findFirstVisibleItemPosition();

        if (firstVisibleItem > 5) {
            Log.e("scrool","1");
            iv_up.setVisibility(View.VISIBLE);
        }
        else{
            Log.e("scrool","0");
            //Hide FAB
            iv_up.setVisibility(View.GONE);
        }
    }


    public void  fetech_ads(final int index){

//        final DelayedProgressDialog pro=new DelayedProgressDialog();
//
//        pro.show(getSupportFragmentManager(),"");


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "get_all_post", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                pro.cancel();
                logg("get_all_post",response);

                load_ads(response);

            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Toast.makeText(mcontext, Common.volleyerror(volleyError)+"", Toast.LENGTH_SHORT).show();

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+userPrefernces.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put("index",index+"");
                params.put(User_id,userPrefernces.getuserid()+"");
                params.put("type",type);
                if (str_cat.length()>0 ){

                    params.put("cat_id",str_cat);
                }
                if (str_city.length()>0){
                    params.put("location",str_city);
                }


                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        MySingleton.getInstance(context).addToRequestQueue(stringRequest);
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        logg("requestCode1=",requestCode+", resultCode="+resultCode+"---");
        if (requestCode==20 &&  Activity.RESULT_OK==resultCode)
        {

            adsArrayList.clear();
            index=1;
            fetech_ads(index);

        }else if (requestCode==ADS_DELETE_RESULT_CODE &&  Activity.RESULT_OK==resultCode)
        {
            adsArrayList.clear();
            index=1;
            fetech_ads(index);
        }else if (requestCode==ADS_DELETE_RESULT_CODE &&  resultCode==33)
        {
//            adsArrayList.clear();
            logg("requestCode1siz1",adsArrayList.size()+"");
//            adsArrayList=(ArrayList<ADS>) data.getSerializableExtra("mylist");
            logg("requestCode1siz",adsArrayList.size()+"");
//            home_adapter=new Home_adapter(mcontext,adsArrayList,Main_Home_Fragment.this);
//            rview.setAdapter(home_adapter);
//            home_adapter.notifyDataSetChanged();
        }

        else if (requestCode==LOCTAION_CITY &&  Activity.RESULT_OK==resultCode){
            logg("data",data+"");
            logg("data",data.getStringExtra("city")+"");

            if (data.hasExtra("city"))
            {
                logg("alcities",data.getStringExtra("city"));
                if(data.getStringExtra("city").equalsIgnoreCase("All Cities"))
                {
                    str_city="";
                }
                else {
                    str_city = data.getStringExtra("city");
                }
                logg("alcities",str_city+"0000");
                tv_location.setText(data.getStringExtra("city"));
                adsArrayList.clear();
                index=1;
                fetech_ads(index);
            }
        }else if (requestCode==FILTER_CAT &&  Activity.RESULT_OK==resultCode){
            logg("data",data+"");
            logg("data",data.getStringExtra("name")+"");

            if (data.hasExtra("name")) {
                str_cat=data.getStringExtra("id");
                tv_cat.setText(data.getStringExtra("name"));
                cat_id=data.getStringExtra("id");

                adsArrayList.clear();
                index=1;
                fetech_ads(index);
            }
        }else if (requestCode==FILTER_ACTIVITY &&  Activity.RESULT_OK==resultCode){
            str_search="";
            str_location="";
            str_pricetype="";
            str_price_from="";
            str_price_to="";
            sort_by="";
            condittion="";
            isfilter=true;
            index=1;
            if (data.hasExtra("location")){
                str_city=data.getStringExtra("location");
                tv_location.setText(data.getStringExtra("location"));
            }

            if (data.hasExtra("searchquery")){
                str_search=data.getStringExtra("searchquery");
            }
            if (data.hasExtra("main_cat") && data.hasExtra("cat_id")){
                type=data.getStringExtra("main_cat");
                cat_id=data.getStringExtra("cat_id");
            }

            if (data.hasExtra("price_type")){
                price_type=data.getStringExtra("price_type");
            }

            if (data.hasExtra("price_from")){
                str_price_from=data.getStringExtra("price_from");
            }
            if (data.hasExtra("price_to")){
                str_price_to=data.getStringExtra("price_to");
            }
            if (data.hasExtra("sort_by")){
                sort_by=data.getStringExtra("sort_by");
            }
            if (data.hasExtra("condittion")){
                sort_by=data.getStringExtra("condittion");
            }

            adsArrayList.clear();
            filter(index);
        }
    }

    String cat_id="";

    private  void custom_navbar(int pos){

        iv_sell.setImageDrawable(getResources().getDrawable(R.drawable.ic_sell_product_icon));
        tv_sell.setTextColor(getResources().getColor(R.color.white));
        iv_rent.setImageDrawable(getResources().getDrawable(R.drawable.ic_rent_icone));
        tv_rent.setTextColor(getResources().getColor(R.color.white));
        iv_service.setImageDrawable(getResources().getDrawable(R.drawable.ic_service_icon));
        tv_service.setTextColor(getResources().getColor(R.color.white));
        iv_shops.setImageDrawable(getResources().getDrawable(R.drawable.ic_shop_icon));
        tv_shops.setTextColor(getResources().getColor(R.color.white));

        switch (pos){
            case 1:
                iv_sell.setImageDrawable(getResources().getDrawable(R.drawable.ic_sell_product_icon_grey));
                tv_sell.setTextColor(getResources().getColor(R.color.grey));
                break;

            case 2:
                iv_rent.setImageDrawable(getResources().getDrawable(R.drawable.ic_rent_icone_grey));
                tv_rent.setTextColor(getResources().getColor(R.color.grey));
                break;
            case 3:
                iv_service.setImageDrawable(getResources().getDrawable(R.drawable.ic_service_icon_grey));
                tv_service.setTextColor(getResources().getColor(R.color.grey));
                break;

            case 4:
                iv_shops.setImageDrawable(getResources().getDrawable(R.drawable.ic_shop_icon_grey));
                tv_shops.setTextColor(getResources().getColor(R.color.grey));
                break;
        }

    }

    public void  filter(final int index){

//        final DelayedProgressDialog pro=new DelayedProgressDialog();
//
//        pro.show(getSupportFragmentManager(),"");


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "search_ads", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                pro.cancel();
                logg("search_res",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("true")){

                        JSONArray jsonArray=object.getJSONArray("data");

                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject object1=jsonArray.getJSONObject(i);

                            adsArrayList.add(new ADS(ParseString(object1,"id"),ParseString(object1,"user_id"),ParseString(object1,"ad_title"),
                                    ParseString(object1,"ad_description"),ParseString(object1,"mobile"),ParseString(object1,"price")
                                    ,ParseString(object1,"images"),ParseString(object1,"category"),ParseString(object1,"city")
                                    ,ParseString(object1,"favorite_count"),ParseString(object1,"views_count"),ParseString(object1,"like_count")
                                    ,ParseString(object1,"timestamp"),ParseString(object1,"fav_stattus"),ParseString(object1,"like_stattus")
                                    ,ParseString(object1,"user_name"),ParseString(object1,"user_image"),
                                    ParseString(object1,"expired_date"), ParseString(object1,"offer_count"),
                                    ParseString(object1,"price_type"),ParseString(object1,"main_category"),
                                    ParseString(object1,"user_regiterdate"),ParseString(object1,"ads_count")
                                    ,ParseString(object1,"follow_user"),ParseString(object1,"mobile_verify")
                                    ,ParseString(object1,"hide_mobileno"),ParseString(object1,"shop_name")
                                    , ParseString(object1, "boost_type"), ParseString(object1, "category_id")));
                        }
                        logg("size",adsArrayList.size()+"");
                        if (index==1) {
                            home_adapter = new Home_adapter(mcontext, adsArrayList, Main_Home_Fragment.this);
                            rview.setAdapter(home_adapter);
                        }else {
                            home_adapter.notifyDataSetChanged();
                        }
                    }

                }
                catch (Exception e){
//                    pro.cancel();
                    Log.e("error","e",e);

                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),iv_drawer);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+userPrefernces.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put("index",index+"");
                params.put(User_id,userPrefernces.getuserid()+"");
                if (str_search.length()>0) {
                    params.put("search", str_search);
                }
                if (str_city.length()>0){
                    params.put("city", str_city);
                }
                if (price_type.length()>0){
                    params.put("price_type", price_type);
                }
                if (str_price_from.length()>0){
                    params.put("price_min",str_price_from);
                }

                if (str_price_to.length()>0){
                    params.put("price_max",str_price_to);
                }
                if (condittion.length()>0){
                    params.put("condition",condittion);
                }
                if (sort_by.length()>0){
                    params.put("sort",sort_by);
                }


                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MySingleton.getInstance(mcontext).addToRequestQueue(stringRequest);
//        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
//        mRequestQueue.add(stringRequest);

    }

    private void load_ads(String response){
        try {
            JSONObject object=new JSONObject(response);
            if (object.getString("status").equals("success")){
                if (index==1 && type.equals("all")){
                    Paper.book("get_adposts1").write("response",response);
                    adsArrayList.clear();
                }
                userPrefernces.setnotificationcount(object.getString("noty"));
                JSONArray jsonArray=object.getJSONArray("data");

                for (int i=0;i<jsonArray.length();i++){
                    JSONObject object1=jsonArray.getJSONObject(i);

                    adsArrayList.add(new ADS(ParseString(object1,"id"),ParseString(object1,"user_id"),ParseString(object1,"ad_title"),
                            ParseString(object1,"ad_description"),ParseString(object1,"mobile"),ParseString(object1,"price")
                            ,ParseString(object1,"images"),ParseString(object1,"category"),ParseString(object1,"city")
                            ,ParseString(object1,"favorite_count"),ParseString(object1,"views_count"),ParseString(object1,"like_count")
                            ,ParseString(object1,"timestamp"),ParseString(object1,"fav_stattus"),ParseString(object1,"like_stattus")
                            ,ParseString(object1,"user_name"),ParseString(object1,"user_image"),
                            ParseString(object1,"expired_date"), ParseString(object1,"offer_count"),
                            ParseString(object1,"price_type"),ParseString(object1,"main_category"),
                            ParseString(object1,"user_regiterdate"),ParseString(object1,"ads_count")
                            ,ParseString(object1,"follow_user"),ParseString(object1,"mobile_verify")
                            ,ParseString(object1,"hide_mobileno"),ParseString(object1,"shop_name")
                            , ParseString(object1, "boost_type"), ParseString(object1, "category_id")));
                }
                logg("size",adsArrayList.size()+"");
                if (index==1){
                    home_adapter=new Home_adapter(mcontext,adsArrayList,Main_Home_Fragment.this);
                    rview.setAdapter(home_adapter);
                }else {
                    home_adapter.notifyDataSetChanged();
                }
            }else if (object.getString("status").equals("fail") && object.getString("message").equals("You are blocked by admin. Please contact admin.")){
                blocked_alert();

            }

        }
        catch (Exception e){
//                    pro.cancel();
            Log.e("error","e",e);

        }
    }


    public void blocked_alert(){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mcontext);

        alertDialog.setMessage("You are blocked by Open-Bazar.Please contact Open-Bazar support");
        alertDialog.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                logout(getActivity());
                AccountKit.logOut();
                userPrefernces.Clear();
                i=new Intent(mcontext, Login_Activity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                getActivity().  finish();
                Paper.book().destroy();
            }
        });

        AlertDialog alertDialog1= alertDialog.create();
        alertDialog1.show();

        alertDialog1.getButton(alertDialog1.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorAccent));
        alertDialog1.getButton(alertDialog1.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorAccent));
    }

    public void  logout(final Context context){

//        final DelayedProgressDialog pro=new DelayedProgressDialog();
//
//        pro.show(getSupportFragmentManager(),"");


        StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "logout", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                pro.cancel();
                userPrefernces.Clear();
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
//                Common.Snakbar(Common.volleyerror(volleyError),tv_city);
                userPrefernces.Clear();
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(Id,userPrefernces.getuserid());
                params.put("push_id",userSharedPreferences.getplayerid());
                params.put("device_id",userSharedPreferences.getdeviceid());

                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(context).addToRequestQueue(stringRequest);


    }

}
