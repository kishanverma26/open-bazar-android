package com.delainetech.open_bazar.Fragments;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.delainetech.open_bazar.Adapter.Package_buy_adapter;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;

public class Buy_Package_fragment extends Fragment implements ParamKeys,View.OnClickListener{


    View v;
    Context mcontext;
    UserSharedPreferences preferences;

    RecyclerView reclcler;
    LinearLayoutManager linearLayoutManager;
    Package_buy_adapter adapter;

    TextView tv_faqtext;
    String question[]={"Standard","Advanced","Premium","Supreme"};
    String ans[]={buy_credit_1,buy_credit_2,buy_credit_3,buy_credit_4};
    String price[]={buy_credit_1_price,buy_credit_2_price,buy_credit_3_price,buy_credit_4_price};

    ImageView iv_faqimage;;
    CardView cv;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.faq_frag, container, false);
        mcontext=getActivity();
        initial();

        return v;
    }


    private void initial() {
        cv=v.findViewById(R.id.cv);
        cv.setVisibility(View.GONE);
        preferences=new UserSharedPreferences(mcontext);
        iv_faqimage=v.findViewById(R.id.iv_faqimage);
        tv_faqtext=v.findViewById(R.id.tv_faqtext);
        reclcler=v.findViewById(R.id.reclcler);
        linearLayoutManager=new LinearLayoutManager(mcontext);
        reclcler.setLayoutManager(linearLayoutManager);
        adapter=new Package_buy_adapter(question,ans,price);
        reclcler.setAdapter(adapter);

        tv_faqtext.setText(Coin_FAQ);

        iv_faqimage.setImageResource(R.drawable.open_bazar_coins_logo);

    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.home_notification:

                break;
        }
    }

}
