package com.delainetech.open_bazar.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.aurelhubert.ahbottomnavigation.notification.AHNotification;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.delainetech.open_bazar.Adpost_Add_Activity;
import com.delainetech.open_bazar.Custom_classes.MySingleton;
import com.delainetech.open_bazar.Drawer_Frame_Activity;
import com.delainetech.open_bazar.Home_Screen_Activity;
import com.delainetech.open_bazar.Invite_Activity;
import com.delainetech.open_bazar.Login_Activity;
import com.delainetech.open_bazar.My_Coin_Activity;
import com.delainetech.open_bazar.My_Wallet_Activity;
import com.delainetech.open_bazar.Profile_update_Activity;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Setting_Activity;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;
import com.facebook.accountkit.AccountKit;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import io.paperdb.Paper;

import static com.delainetech.open_bazar.Utils.Common.ParseString;
import static com.delainetech.open_bazar.Utils.Common.logg;


public class Profile_Fragment extends Fragment implements ParamKeys,View.OnClickListener{

    View v;
    Context mcoxt;
    UserSharedPreferences preferences,userSharedPreferences;

    TextView tv_editprofile,tv_Recentlyview,tv_favorite,tv_invitefriend,tv_followers,
            tv_following,tv_myadds,tv_followingads,tv_contactus,tv_help,tv_aboutapp,
            tv_share_profile,tv_share_apps,tv_mywallet,tv_mycoin,tv_myaccount_,tv_myofferads;

    TextView tv_username,tv_time;
    RelativeLayout rl_myfollowers,rl_myfollowings,rl_myads,rll_myfollowers,rll_myfollowings,rll_myads;

    ImageView ivsetting;
    CircleImageView userimg;
    ImageView home_notification;
    TextView tv_adcount,tv_following_count,tv_followers_count,tv_view_count,tv_favorite_count,tv_ads_counts,tv_myofferads_counts,
            tv_followingads_count,tv_followerscard_count,tv_following_counts;
    Intent i;

    ImageView iv_emailverify,iv_phoneverify,iv_fbverify,iv_googleverify;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v=inflater.inflate(R.layout.profile_frag_user,container,false);
        mcoxt=getActivity();
        init();
        return v;
    }

    private void init() {
        preferences=UserSharedPreferences.getInstance(getActivity());
        userSharedPreferences=new UserSharedPreferences(getActivity(),"");
        tv_username=v.findViewById(R.id.tv_username);
        userimg=v.findViewById(R.id.userimg);
        tv_time=v.findViewById(R.id.tv_time);

        home_notification=(ImageView)v. findViewById(R.id.home_notification);
        home_notification.setOnClickListener(this);

        iv_emailverify=(ImageView)v. findViewById(R.id.iv_emailverify);
        iv_phoneverify=(ImageView)v. findViewById(R.id.iv_phoneverify);
        iv_fbverify=(ImageView)v. findViewById(R.id.iv_fbverify);
        iv_googleverify=(ImageView)v. findViewById(R.id.iv_googleverify);


        //        edit profile
        tv_editprofile=v.findViewById(R.id.tv_editprofile);
        tv_editprofile.setOnClickListener(this);

//        edit profile
        ivsetting=v.findViewById(R.id.ivsetting);
        ivsetting.setOnClickListener(this);

//        recently views
        tv_Recentlyview=v.findViewById(R.id.tv_Recentlyview);
        tv_Recentlyview.setOnClickListener(this);

//        favorite views
        tv_favorite=v.findViewById(R.id.tv_favorite);
        tv_favorite.setOnClickListener(this);

//        favorite views
        tv_myaccount_=v.findViewById(R.id.tv_myaccount_);
        tv_myaccount_.setOnClickListener(this);

//        invitefriend
        tv_invitefriend=v.findViewById(R.id.tv_invitefriend);
        tv_invitefriend.setOnClickListener(this);
        tv_share_profile=v.findViewById(R.id.tv_share_profile);
        tv_share_profile.setOnClickListener(this);
        tv_share_apps=v.findViewById(R.id.tv_share_apps);
        tv_share_apps.setOnClickListener(this);

//        Followimg
        tv_followers=v.findViewById(R.id.tv_followers);
        tv_followers.setOnClickListener(this);
        tv_following=v.findViewById(R.id.tv_following);
        tv_following.setOnClickListener(this);

//      My ads
        tv_myadds=v.findViewById(R.id.tv_myadds);
        tv_myadds.setOnClickListener(this);

//      My Wallet
        tv_mywallet=v.findViewById(R.id.tv_mywallet);
        tv_mywallet.setOnClickListener(this);
        tv_mycoin=v.findViewById(R.id.tv_mycoin);
        tv_mycoin.setOnClickListener(this);

//     Following ads
        tv_followingads=v.findViewById(R.id.tv_followingads);
        tv_followingads.setOnClickListener(this);
//     tv_contactus ads
        tv_contactus=v.findViewById(R.id.tv_contactus);
        tv_contactus.setOnClickListener(this);
        tv_help=v.findViewById(R.id.tv_help);
        tv_help.setOnClickListener(this);

        tv_aboutapp=v.findViewById(R.id.tv_aboutapp);
        tv_aboutapp.setOnClickListener(this);


        rl_myfollowers=v.findViewById(R.id.rl_myfollowers);
        rl_myfollowers.setOnClickListener(this);
        rl_myfollowings=v.findViewById(R.id.rl_myfollowings);
        rl_myfollowings.setOnClickListener(this);
        rl_myads=v.findViewById(R.id.rl_myads);
        rl_myads.setOnClickListener(this);

        rll_myfollowers=v.findViewById(R.id.rll_myfollowers);
        rll_myfollowers.setOnClickListener(this);
        rll_myfollowings=v.findViewById(R.id.rll_myfollowings);
        rll_myfollowings.setOnClickListener(this);
        rll_myads=v.findViewById(R.id.rll_myads);
        rll_myads.setOnClickListener(this);


        tv_adcount=v.findViewById(R.id.tv_adcount);
        tv_following_count=v.findViewById(R.id.tv_following_count);
        tv_followers_count=v.findViewById(R.id.tv_followers_count);
        tv_view_count=v.findViewById(R.id.tv_view_count);
        tv_favorite_count=v.findViewById(R.id.tv_favorite_count);
        tv_ads_counts=v.findViewById(R.id.tv_ads_counts);
        tv_myofferads=v.findViewById(R.id.tv_myofferads);
        tv_myofferads.setOnClickListener(this);
        tv_myofferads_counts=v.findViewById(R.id.tv_myofferads_counts);
        tv_followingads_count=v.findViewById(R.id.tv_followingads_count);
        tv_followerscard_count=v.findViewById(R.id.tv_followerscard_count);
        tv_following_counts=v.findViewById(R.id.tv_following_counts);

        tv_time.setText(getResources().getString(R.string.member_since)+" "+changeDateFormatFromAnother(preferences.getcreated_at()));

        ((Home_Screen_Activity)getActivity()).fab_add_ad.setOnClickListener(this);


        if (Paper.book("myprofile").contains("response")){

            load_counts(Paper.book("myprofile").read("response").toString());
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.home_notification:
                getFragmentManager().beginTransaction().replace(R.id.framelayout_hs, new Notification_Screen_Fragment()).commit();

                break;
            case R.id.fab_add_ad:
                Intent i=new Intent(getActivity(), Adpost_Add_Activity.class);
                startActivityForResult(i,10);
                break;

            case R.id.tv_editprofile:
                startActivity(new Intent(mcoxt,Profile_update_Activity.class));
                break;
            case R.id.tv_Recentlyview:
                i=new Intent(mcoxt,Drawer_Frame_Activity.class);
                i.putExtra(Pos,8);
                i.putExtra(Title,"Recently Views");

                startActivity(i);
                break;
            case R.id.tv_myofferads:
                i=new Intent(mcoxt,Drawer_Frame_Activity.class);
                i.putExtra(Pos,10);
                i.putExtra(Title,"My Offers");

                startActivity(i);
                break;
            case R.id.tv_myaccount_:
                i=new Intent(mcoxt,Drawer_Frame_Activity.class);
                i.putExtra(Pos,5);
                i.putExtra(Title,"Account");

                startActivity(i);
                break;
            case R.id.tv_myadds:
                i=new Intent(mcoxt,Drawer_Frame_Activity.class);
                i.putExtra(Pos,0);
                i.putExtra(Title,"My Ads");

                startActivity(i);
                break;
            case R.id.rl_myads:
                i=new Intent(mcoxt,Drawer_Frame_Activity.class);
                i.putExtra(Pos,0);
                i.putExtra(Title,"My Ads");

                startActivity(i);
                break;
            case R.id.rll_myads:
                i=new Intent(mcoxt,Drawer_Frame_Activity.class);
                i.putExtra(Pos,0);
                i.putExtra(Title,"My Ads");

                startActivity(i);
                break;
            case R.id.tv_followingads:
                i=new Intent(mcoxt,Drawer_Frame_Activity.class);
                i.putExtra(Pos,11);
                i.putExtra(Title,"Following Ads");

                startActivity(i);
                break;
            case R.id.tv_favorite:
                i=new Intent(mcoxt,Drawer_Frame_Activity.class);
                i.putExtra(Pos,9);
                i.putExtra(Title,"Favorites");
                startActivity(i);
                break;
            case R.id.tv_followers:
                i=new Intent(mcoxt,Drawer_Frame_Activity.class);
                i.putExtra(Pos,12);
                i.putExtra(Title,"Followers");
                startActivity(i);
                break;
            case R.id.rl_myfollowers:
                i=new Intent(mcoxt,Drawer_Frame_Activity.class);
                i.putExtra(Pos,12);
                i.putExtra(Title,"Followers");
                startActivity(i);
                break;
            case R.id.tv_following:
                i=new Intent(mcoxt,Drawer_Frame_Activity.class);
                i.putExtra(Pos,1);
                i.putExtra(Title,"Following");
                startActivity(i);
                break;
            case R.id.rl_myfollowings:
                i=new Intent(mcoxt,Drawer_Frame_Activity.class);
                i.putExtra(Pos,1);
                i.putExtra(Title,"Following");
                startActivity(i);
                break;
            case R.id.rll_myfollowers:
                i=new Intent(mcoxt,Drawer_Frame_Activity.class);
                i.putExtra(Pos,12);
                i.putExtra(Title,"Followers");
                startActivity(i);
                break;

            case R.id.rll_myfollowings:
                i=new Intent(mcoxt,Drawer_Frame_Activity.class);
                i.putExtra(Pos,1);
                i.putExtra(Title,"Following");
                startActivity(i);
                break;
            case R.id.tv_contactus:
                i=new Intent(mcoxt,Drawer_Frame_Activity.class);
                i.putExtra(Pos,2);
                i.putExtra(Title,"Contact Us");
                startActivity(i);
                break;
            case R.id.tv_help:
                i=new Intent(mcoxt,Drawer_Frame_Activity.class);
                i.putExtra(Pos,13);
                i.putExtra(Title,"Help");
                startActivity(i);
                break;
            case R.id.tv_aboutapp:
                i=new Intent(mcoxt,Drawer_Frame_Activity.class);
                i.putExtra(Pos,6);
                i.putExtra(Title,"About App");
                startActivity(i);
                break;
            case R.id.ivsetting:
                i=new Intent(mcoxt,Setting_Activity.class);
                startActivity(i);
                break;
            case R.id.tv_mywallet:
                i=new Intent(mcoxt,My_Wallet_Activity.class);
                startActivity(i);
                break;
            case R.id.tv_mycoin:
                i=new Intent(mcoxt,My_Coin_Activity.class);
                startActivity(i);
                break;


            case R.id.tv_invitefriend:

//                create_dynamic_link("Signup and get reward","reffer/"+preferences.getuserid());
                startActivity(new Intent(mcoxt, Invite_Activity.class));
                break;
            case R.id.tv_share_profile:

                create_dynamic_link("Take a look what i am selling","myprofile/"+preferences.getuserid());

                break;
            case R.id.tv_share_apps:

                create_dynamic_link("Signup and get reward","reffer/"+preferences.getuserid());

                break;


        }
    }

    @Override
    public void onResume() {
        super.onResume();
        get_counts();


        tv_username.setText(preferences.getname());
        RequestOptions requestOptions=new RequestOptions().centerCrop().placeholder(R.drawable.userimage);
        Common.logg("url",Common.Image_Loading_Url(preferences.getimage()));
        Glide.with(this).load(Common.Image_Loading_Url(preferences.getimage())).apply(requestOptions).into(userimg);

    }

    public String changeDateFormatFromAnother(String date){
        @SuppressLint("SimpleDateFormat") DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss",Locale.US);
        @SuppressLint("SimpleDateFormat") DateFormat outputFormat = new SimpleDateFormat("dd MMMM yyyy",Locale.US);
        String resultDate = "";
        try {
            resultDate=outputFormat.format(inputFormat.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return resultDate;
    }


    public void  get_counts(){


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "myprofile", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                pro.cancel();
                logg("myprofile",response);

                load_counts(response);
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                logg("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(User_id,preferences.getuserid());

                logg("params",params+"");
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue mRequestQueue = Volley.newRequestQueue(getActivity());
        mRequestQueue.add(stringRequest);

    }


    private void load_counts(String res){
        try {
            JSONObject jsonObject=new JSONObject(res);
            if (jsonObject.getString("status").equals("success")){
                Paper.book("myprofile").write("response",res);

                preferences.setnotificationcount(ParseString(jsonObject,"noty"));
                if (getActivity()!=null) {
                    if (!preferences.getnotificationcount().equals("0")) {
                        AHNotification notification = new AHNotification.Builder()
                                .setText(preferences.getnotificationcount())
                                .setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.white))
                                .setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent))
                                .build();
                        ((Home_Screen_Activity) getActivity()).bottomNavigation.setNotification(notification, 3);
                    } else {
                        AHNotification notification = new AHNotification.Builder()
                                .setText("")
                                .setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.white))
                                .setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent))
                                .build();
                        ((Home_Screen_Activity) getActivity()).bottomNavigation.setNotification(notification, 3);
                    }
                }
                JSONObject jsonObject1=jsonObject.getJSONObject("data");
                tv_followers_count.setText(ParseString(jsonObject1,"followers"));
                tv_favorite_count.setText(ParseString(jsonObject1,"fav_count"));
                tv_followerscard_count.setText(ParseString(jsonObject1,"followers"));
                tv_view_count.setText(ParseString(jsonObject1,"view_count"));
                tv_following_count.setText(ParseString(jsonObject1,"following"));
                logg("counts_following",tv_following_count.getText().toString());
                tv_following_counts.setText(ParseString(jsonObject1,"following"));
                tv_myofferads_counts.setText(ParseString(jsonObject1,"bidcount"));
                tv_adcount.setText(ParseString(jsonObject1,"user_adcount"));
                tv_ads_counts.setText(ParseString(jsonObject1,"user_adcount"));
                tv_followingads_count.setText(ParseString(jsonObject1,"following_adcount"));


                if (ParseString(jsonObject1,"verify_phone").equals("true")){
                    iv_phoneverify.setVisibility(View.VISIBLE);
                }
                if (ParseString(jsonObject1,"verify_google").equals("true")){
                    iv_emailverify.setVisibility(View.VISIBLE);
                    iv_googleverify.setVisibility(View.VISIBLE);
                }
                if (ParseString(jsonObject1,"verify_facebook").equals("true")){
                    iv_fbverify.setVisibility(View.VISIBLE);
                }
            }else if (jsonObject.getString("status").equals("fail") && jsonObject.getString("message").equals("You are blocked by admin. Please contact admin.")){
                blocked_alert();

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

//--------------------------------------dynamic linkin


    public void create_dynamic_link(final String msg, String str){

        Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse("https://openbazar.page.link?="+str))
                .setDomainUriPrefix("https://openbazar.page.link")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder("com.open_bazar")
                        .setFallbackUrl(Uri.parse("https://www.dropbox.com/s/o1z7e9w94g7twqz/app-debug.apk?dl=0"))
                        .build())
                // Set parameters
                // ...
                .buildShortDynamicLink()

                .addOnCompleteListener(getActivity(), new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {
                            // Short link created
                            Uri shortLink = task.getResult().getShortLink();
                            Uri flowchartLink = task.getResult().getPreviewLink();

                            logg("shortLink",shortLink+" flowchartLink"+flowchartLink);


                            String shareBody2 = msg+" "+shortLink;
                            Intent sharingIntent2 = new Intent(android.content.Intent.ACTION_SEND);
                            sharingIntent2.setType("text/plain");
                            sharingIntent2.putExtra(android.content.Intent.EXTRA_SUBJECT, "Open Bazar");
                            sharingIntent2.putExtra(android.content.Intent.EXTRA_TEXT, shareBody2);
                            startActivity(Intent.createChooser(sharingIntent2, ""));

                        } else {
                            // Error
                            // ...
                            Toast.makeText(mcoxt, "Somthing went wrong please try again", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }



    public void blocked_alert(){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mcoxt);

        alertDialog.setMessage("You are blocked by Open-Bazar.Please contact Open-Bazar support");
        alertDialog.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                logout(getActivity());
                AccountKit.logOut();
                preferences.Clear();
                i=new Intent(mcoxt, Login_Activity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                getActivity().  finish();
                Paper.book().destroy();
            }
        });

        AlertDialog alertDialog1= alertDialog.create();
        alertDialog1.show();

        alertDialog1.getButton(alertDialog1.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorAccent));
        alertDialog1.getButton(alertDialog1.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorAccent));
    }

    public void  logout(final Context context){

//        final DelayedProgressDialog pro=new DelayedProgressDialog();
//
//        pro.show(getSupportFragmentManager(),"");


        StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "logout", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                pro.cancel();
                preferences.Clear();
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
//                Common.Snakbar(Common.volleyerror(volleyError),tv_city);
                preferences.Clear();
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(Id,preferences.getuserid());
                params.put("push_id",preferences.getplayerid());
                params.put("device_id",userSharedPreferences.getdeviceid());

                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(context).addToRequestQueue(stringRequest);


    }

}



