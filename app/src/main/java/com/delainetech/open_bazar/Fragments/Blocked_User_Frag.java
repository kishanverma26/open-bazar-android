package com.delainetech.open_bazar.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.delainetech.open_bazar.Adapter.Blocked_User_adapter;
import com.delainetech.open_bazar.Adapter.Following_adapter;
import com.delainetech.open_bazar.Custom_classes.DelayedProgressDialog;
import com.delainetech.open_bazar.Custom_classes.EndlessRecyclerViewScrollListener;
import com.delainetech.open_bazar.Models.Following;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import static com.delainetech.open_bazar.Utils.Common.ParseString;
import static com.delainetech.open_bazar.Utils.Common.logg;

public class Blocked_User_Frag extends Fragment implements ParamKeys,SwipeRefreshLayout.OnRefreshListener,View.OnClickListener{


    View v;
    Context mcontext;
    UserSharedPreferences preferences;

    RecyclerView reclcler;
    LinearLayoutManager linearLayoutManager;
    Blocked_User_adapter adapter;
    int index=0;
    EndlessRecyclerViewScrollListener recyclerViewScrollListener;
    SwipeRefreshLayout swiperefresh;

    TextView tvemptyview;
    boolean isViewShown=false;
    ArrayList<Following> followingArrayList=new ArrayList<>();
    TextView tv_tital;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.following_frag, container, false);
        mcontext=getActivity();
        initial();

        return v;
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        index=0;
        if (getView() != null) {
            isViewShown = true;
            // fetchdata() contains logic to show data when page is selected mostly asynctask to fill the data
            Log.e("view",isVisibleToUser+"");

        } else {
            isViewShown = false;
        }



    }

    private void initial() {
        preferences=new UserSharedPreferences(mcontext);
        tv_tital=v.findViewById(R.id.tv_tital);
        tv_tital.setVisibility(View.GONE);
        tvemptyview=v.findViewById(R.id.tvemptyview);
        tvemptyview.setOnClickListener(this);
        swiperefresh=v.findViewById(R.id.swiperefresh);
        reclcler=v.findViewById(R.id.reclcler);
        linearLayoutManager=new LinearLayoutManager(mcontext);
        reclcler.setLayoutManager(linearLayoutManager);
        adapter=new Blocked_User_adapter(mcontext,followingArrayList);
        reclcler.setAdapter(adapter);


        following_list(index);
        recyclerViewScrollListener=new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if ( (followingArrayList.size()%10)==0){
                    index++;
                    following_list(index);
                }

            }
        };
        reclcler.addOnScrollListener(recyclerViewScrollListener);
        swiperefresh.setOnRefreshListener(this);

        swiperefresh.setColorSchemeColors(getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorPrimary),getResources().getColor(R.color.colorPrimary));


    }



    @Override
    public void onRefresh() {
        followingArrayList.clear();
        index=0;
        following_list(index);

      swiperefresh.setRefreshing(false);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.home_notification:

                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==11){

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("test","test");

    }

    public void  following_list(final int index){

        final DelayedProgressDialog pro=new DelayedProgressDialog();

        pro.show(getFragmentManager(),"");


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "blocked_list", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                pro.cancel();
                logg("recent_view",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("message").equals("Success")){

                        JSONArray jsonArray=object.getJSONArray("data");

                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject object1=jsonArray.getJSONObject(i);

               followingArrayList.add(new Following(ParseString(object1,"id"),ParseString(object1,"name")
                 ,ParseString(object1,"profile_image"),"",""
                       ,""));
                        }


                        adapter.notifyDataSetChanged();

                    }


                }
                catch (Exception e){
                    pro.cancel();
                    Log.e("error","e",e);

                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),reclcler);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put("index",index+"");
                params.put(User_id,preferences.getuserid()+"");



                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        MySingleton.getInstance(context).addToRequestQueue(stringRequest);
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }

}
