package com.delainetech.open_bazar.Fragments;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.delainetech.open_bazar.Category_Activity_filter;
import com.delainetech.open_bazar.Filter_Activity;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Search_Activity;
import com.delainetech.open_bazar.Utils.ParamKeys;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;


/**
 * Created by Rsss on 2/5/2018.
 */

public class Select_category_dialog_fragment_Search_Activity extends DialogFragment implements ParamKeys {


    View v;

//    Button bt_home,bt_mycoins;
    Context mcontext;

    LinearLayout ll;
    TextView tv_sell_stuff,tv_rent_stuff,tv_service,tv_shop;
    Intent i;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        v=inflater.inflate(R.layout.select_category_dialog,container,false);
        mcontext=getActivity();
        initi();
        return v;
    }

    private void initi()
    {
        ll=(LinearLayout)v.findViewById(R.id.ll);
        tv_sell_stuff=(TextView)v.findViewById(R.id.tv_sell_stuff);
        tv_rent_stuff=(TextView)v.findViewById(R.id.tv_rent_stuff);
        tv_service=(TextView)v.findViewById(R.id.tv_service);
        tv_shop=(TextView)v.findViewById(R.id.tv_shop);
//    Sell Your Stuff=1,Rent Your Stuff=2,Offer Services=3,Start Your Shop=4

        if (getArguments().getString(Type).equals("filter")){
            tv_sell_stuff.setText("Sell Categories");
            tv_rent_stuff.setText("Rent Categories");
            tv_service.setText("Service Categories");
            tv_shop.setText("Shop Categories");

        }


        tv_sell_stuff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dismiss();
                i=new Intent(mcontext, Category_Activity_filter.class);
                i.putExtra("id","1");
              getActivity().  startActivityForResult(i,FILTER_CAT);

            }
        });
        tv_rent_stuff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dismiss();
                i=new Intent(mcontext, Category_Activity_filter.class);
                i.putExtra("id","2");
                getActivity().  startActivityForResult(i,FILTER_CAT);
            }
        });
        tv_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dismiss();
                i=new Intent(mcontext, Category_Activity_filter.class);
                i.putExtra("id","3");
                getActivity().  startActivityForResult(i,FILTER_CAT);
            }
        });
        tv_shop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dismiss();
                i=new Intent(mcontext, Category_Activity_filter.class);
                i.putExtra("id","4");
                getActivity().   startActivityForResult(i,FILTER_CAT);
            }
        });


    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        Dialog dialog = super.onCreateDialog(savedInstanceState);
        WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.gravity = Gravity.CENTER;
        wlmp.dimAmount = 0.0F;
        dialog.getWindow().setAttributes(wlmp);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return dialog;
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 1.00);
        int screenHeight = (int) (metrics.heightPixels * 1.00);

        getDialog().getWindow().setLayout(screenWidth, screenHeight);
    }



}
