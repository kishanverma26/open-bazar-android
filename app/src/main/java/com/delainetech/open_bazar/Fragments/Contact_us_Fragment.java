package com.delainetech.open_bazar.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import io.paperdb.Paper;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.delainetech.open_bazar.Adapter.Notification_adapter;
import com.delainetech.open_bazar.Custom_classes.EndlessRecyclerViewScrollListener;
import com.delainetech.open_bazar.Models.Notifications;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.delainetech.open_bazar.Utils.Common.logg;

public class Contact_us_Fragment extends Fragment implements View.OnClickListener , ParamKeys
{
    Context mcoxt;
    UserSharedPreferences preferences;
    EditText et_name,et_email,et_mobile,et_suggesstion,et_message;
    Button bt_submit;
    Context mcontext;
    View view;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.contact_us_frag, container, false);
        mcontext=getActivity();
        initiliz();

        return view;
    }

    private void initiliz() {
        mcoxt=getActivity();
        preferences=new UserSharedPreferences(mcoxt);
        bt_submit=view.findViewById(R.id.bt_submit);
        bt_submit.setOnClickListener(this);
        et_name=view.findViewById(R.id.et_name);
        et_email=view.findViewById(R.id.et_email);
        et_mobile=view.findViewById(R.id.et_mobile);
        et_suggesstion=view.findViewById(R.id.et_suggesstion);
        et_message=view.findViewById(R.id.et_message);



    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bt_submit:
                checkvalidation();
                break;
        }
    }

    private void checkvalidation() {
        if (et_name.getText().toString().trim().length()>0 && et_email.getText().toString().trim().length()>0 &&
                et_mobile.getText().toString().trim().length()>0  &&
                et_message.getText().toString().trim().length()>0){
            contcat_us();
        }else {
            Toast.makeText(mcoxt, "Please fill all fields", Toast.LENGTH_SHORT).show();
        }
    }

    ProgressDialog progressDialog;
    public void  contcat_us(){
         progressDialog=new ProgressDialog(mcontext);
        progressDialog.setMessage("Please wait......");
        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "contact_us", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                logg("myprofile",response);
                getActivity().finish();
                Toast.makeText(mcoxt, "Your query has been submitted", Toast.LENGTH_SHORT).show();
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
            progressDialog.dismiss();
                Log.e("Volleyerror","e",volleyError);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                logg("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(User_id,preferences.getuserid());
              params.put("name",et_name.getText().toString());
              params.put("email",et_email.getText().toString());
              params.put("mobile",et_mobile.getText().toString());
              params.put("subject",et_message.getText().toString());


                logg("params",params+"");
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue mRequestQueue = Volley.newRequestQueue(getActivity());
        mRequestQueue.add(stringRequest);

    }


}
