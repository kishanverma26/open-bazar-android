package com.delainetech.open_bazar.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.delainetech.open_bazar.Adapter.Faq_adapter;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;

public class My_Coin_Faq_fragment extends Fragment implements ParamKeys,View.OnClickListener{


    View v;
    Context mcontext;
    UserSharedPreferences preferences;

    RecyclerView reclcler;
    LinearLayoutManager linearLayoutManager;
    Faq_adapter adapter;

    TextView tv_faqtext;
    String question[]={Coin_FAQ_q1,Coin_FAQ_q2,Coin_FAQ_q3,Coin_FAQ_q4,Coin_FAQ_q5};
    String ans[]={Coin_FAQ_a1,Coin_FAQ_a2,Coin_FAQ_a3,Coin_FAQ_a4,Coin_FAQ_a5};
    ImageView iv_faqimage;
    NestedScrollView nested_scroll;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.faq_frag, container, false);
        mcontext=getActivity();
        initial();

        return v;
    }


    private void initial() {
        preferences=new UserSharedPreferences(mcontext);
        nested_scroll=v.findViewById(R.id.nested_scroll);
        iv_faqimage=v.findViewById(R.id.iv_faqimage);
        tv_faqtext=v.findViewById(R.id.tv_faqtext);
        reclcler=v.findViewById(R.id.reclcler);
        linearLayoutManager=new LinearLayoutManager(mcontext);
        reclcler.setLayoutManager(linearLayoutManager);
        adapter=new Faq_adapter(question,ans,nested_scroll);
        reclcler.setAdapter(adapter);
        reclcler.setNestedScrollingEnabled(false);
        tv_faqtext.setText(Coin_FAQ);

        iv_faqimage.setImageResource(R.drawable.open_bazar_coins_logo);


    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.home_notification:

                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==11){

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("test","test");

    }
}
