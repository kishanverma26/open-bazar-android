package com.delainetech.open_bazar.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import io.paperdb.Paper;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.aurelhubert.ahbottomnavigation.notification.AHNotification;
import com.delainetech.open_bazar.Adapter.Notification_adapter;
import com.delainetech.open_bazar.Adpost_Add_Activity;
import com.delainetech.open_bazar.Custom_classes.DelayedProgressDialog;
import com.delainetech.open_bazar.Custom_classes.EndlessRecyclerViewScrollListener;
import com.delainetech.open_bazar.Custom_classes.MySingleton;
import com.delainetech.open_bazar.Home_Screen_Activity;
import com.delainetech.open_bazar.Login_Activity;
import com.delainetech.open_bazar.Models.ADS;
import com.delainetech.open_bazar.Models.Notifications;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;
import com.facebook.accountkit.AccountKit;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.delainetech.open_bazar.Utils.Common.ParseString;
import static com.delainetech.open_bazar.Utils.Common.logg;

public class Notification_Screen_Fragment extends Fragment implements View.OnClickListener , ParamKeys ,
        SwipeRefreshLayout.OnRefreshListener {
    Context mcoxt;

    UserSharedPreferences userpref,userSharedPreferences;
    RecyclerView rv_notification;
    LinearLayoutManager manager;
    Notification_adapter notification_adapter;
    ArrayList<Notifications> notifications;
    SwipeRefreshLayout Swip_refreshlayout;
    EndlessRecyclerViewScrollListener scrollListener;
    int index=0;
    Context mcontext;
    View view;
    TextView tv_mark;

    //    Empty View
    LinearLayout ll_emptyview;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.notification_screen, container, false);
        mcontext=getActivity();
        initiliz();

        return view;
    }

    private void initiliz() {
        mcoxt=getActivity();
        userpref=UserSharedPreferences.getInstance(mcoxt);
        userSharedPreferences=new UserSharedPreferences(mcoxt,"");

//        emptyview
        ll_emptyview=view.findViewById(R.id.ll_emptyview);

        notifications=new ArrayList<>();
        tv_mark=view.findViewById(R.id.tv_mark);
        tv_mark.setOnClickListener(this);
        rv_notification=view.findViewById(R.id.rv_notification);
        Swip_refreshlayout=view.findViewById(R.id.Swip_refreshlayout);
        manager=new LinearLayoutManager(mcoxt);
        rv_notification.setLayoutManager(manager);
        notification_adapter=new Notification_adapter(mcoxt,notifications);
        rv_notification.setAdapter(notification_adapter);

        get_notification(index);
        scrollListener=new EndlessRecyclerViewScrollListener(manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                index++;
                get_notification(index);
//                }
            }
        };

        rv_notification.addOnScrollListener(scrollListener);

        ((Home_Screen_Activity)mcontext).fab_add_ad.setOnClickListener(this);

        Swip_refreshlayout.setOnRefreshListener(this);
        Swip_refreshlayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorPrimary),getResources().getColor(R.color.colorPrimary));

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fab_add_ad:
                Intent i=new Intent(mcontext, Adpost_Add_Activity.class);
                startActivityForResult(i,10);
                break;

            case R.id.tv_mark:
                setas_marked();

                break;

        }
    }


    public void  get_notification(final int index){

        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "fetch_notification", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                pro.cancel();
                logg("get_adposts",response);
                Swip_refreshlayout.setRefreshing(false);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success"))
                    {
                        tv_mark.setVisibility(View.VISIBLE);
                        ll_emptyview.setVisibility(View.GONE);
                        rv_notification.setVisibility(View.VISIBLE);
                        userpref.setnotificationcount(ParseString(object,"noty"));
                        if (getActivity()!=null) {
                            if (!userpref.getnotificationcount().equals("0")) {
                                AHNotification notification = new AHNotification.Builder()
                                        .setText(userpref.getnotificationcount())
                                        .setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.white))
                                        .setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent))
                                        .build();
                                ((Home_Screen_Activity) getActivity()).bottomNavigation.setNotification(notification, 3);
                            } else {
                                AHNotification notification = new AHNotification.Builder()
                                        .setText("")
                                        .setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.white))
                                        .setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent))
                                        .build();
                                ((Home_Screen_Activity) getActivity()).bottomNavigation.setNotification(notification, 3);
                            }
                        }
                        JSONArray jsonArray=object.getJSONArray("data");

                        for (int i=0;i<jsonArray.length();i++)
                        {
                            JSONObject object1=jsonArray.getJSONObject(i);
                            notifications.add(new Notifications(ParseString(object1,"id"),ParseString(object1,"user_id"),
                                    ParseString(object1,"refer_id"), ParseString(object1,"callby"),
                                    ParseString(object1,"tital"), ParseString(object1,"type"),
                                    ParseString(object1,"timestamp"),ParseString(object1,"sender_name"),
                                    ParseString(object1,"sender_image"),ParseString(object1,"status")));
                        }
                        notification_adapter.notifyDataSetChanged();

                        if (notifications.size()>0)
                        {
                            tv_mark.setVisibility(View.VISIBLE);
                            ll_emptyview.setVisibility(View.GONE);
                            rv_notification.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            tv_mark.setVisibility(View.GONE);
                            rv_notification.setVisibility(View.GONE);
                            ll_emptyview.setVisibility(View.VISIBLE);
                        }

                    }else if (object.getString("status").equals("fail") && object.getString("message").equals("You are blocked by admin. Please contact admin.")){
                        blocked_alert();

                    }
                    else
                    {
                        if (!(notifications.size()>0)) {
                            tv_mark.setVisibility(View.GONE);
                            rv_notification.setVisibility(View.GONE);
                            ll_emptyview.setVisibility(View.VISIBLE);
                        }
                    }
                }
                catch (Exception e){
//                    pro.cancel();
                    Log.e("error","e",e);
                    rv_notification.setVisibility(View.GONE);
                    ll_emptyview.setVisibility(View.VISIBLE);
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                rv_notification.setVisibility(View.GONE);
                ll_emptyview.setVisibility(View.VISIBLE);
                Common.Snakbar(Common.volleyerror(volleyError),rv_notification);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+userpref.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put("index",index+"");
                params.put(User_id,userpref.getuserid()+"");



                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        MySingleton.getInstance(context).addToRequestQueue(stringRequest);
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }


    public void  setas_marked(){

        final DelayedProgressDialog pro=new DelayedProgressDialog();

        pro.show(getFragmentManager(),"");


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "notification_status", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                pro.cancel();
                logg("get_adposts",response);
                Swip_refreshlayout.setRefreshing(false);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("message").equals("success")){
                        userpref.setnotificationcount("0");

                        for (int i=0;i<notifications.size();i++){
                            notifications.get(i).setStatus("1");
                        }
                        notification_adapter.notifyDataSetChanged();
                        if (!userpref.getnotificationcount().equals("0")) {
                            AHNotification notification = new AHNotification.Builder()
                                    .setText(userpref.getnotificationcount())
                                    .setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.white))
                                    .setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent))
                                    .build();
                            ( (Home_Screen_Activity)getActivity() ).  bottomNavigation.setNotification(notification, 3);
                        }else {
                            AHNotification notification = new AHNotification.Builder()
                                    .setText("")
                                    .setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.white))
                                    .setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent))
                                    .build();
                            ( (Home_Screen_Activity)getActivity() ).  bottomNavigation.setNotification(notification, 3);
                        }

                    }


                }
                catch (Exception e){
                    pro.cancel();
                    Log.e("error","e",e);

                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                pro.cancel();
                Log.e("Volleyerror","e",volleyError);

                Common.Snakbar(Common.volleyerror(volleyError),rv_notification);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+userpref.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put("type","all");
                params.put("noty_id",userpref.getuserid()+"");



                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        MySingleton.getInstance(context).addToRequestQueue(stringRequest);
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }

    @Override
    public void onRefresh() {
        index=0;
        notifications.clear();
        get_notification(index);


    }

    Intent i;
    public void blocked_alert(){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mcontext);

        alertDialog.setMessage("You are blocked by Open-Bazar.Please contact Open-Bazar support");
        alertDialog.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                logout(getActivity());
                AccountKit.logOut();
                userpref.Clear();
                i=new Intent(mcontext, Login_Activity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                getActivity().  finish();
                Paper.book().destroy();
            }
        });

        AlertDialog alertDialog1= alertDialog.create();
        alertDialog1.show();

        alertDialog1.getButton(alertDialog1.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorAccent));
        alertDialog1.getButton(alertDialog1.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorAccent));
    }

    public void  logout(final Context context){

//        final DelayedProgressDialog pro=new DelayedProgressDialog();
//
//        pro.show(getSupportFragmentManager(),"");


        StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "logout", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                pro.cancel();
                userpref.Clear();
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
//                Common.Snakbar(Common.volleyerror(volleyError),tv_city);
                userpref.Clear();
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(Id,userpref.getuserid());
                params.put("push_id",userSharedPreferences.getplayerid());
                params.put("device_id",userSharedPreferences.getdeviceid());

                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(context).addToRequestQueue(stringRequest);


    }

}
