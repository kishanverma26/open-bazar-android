package com.delainetech.open_bazar.Fragments;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.delainetech.open_bazar.My_Coin_Activity;
import com.delainetech.open_bazar.R;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;


/**
 * Created by Rsss on 2/5/2018.
 */

public class Success_adpost_dialog_fragment extends DialogFragment {


    View v;

    Button bt_home,bt_mycoins;
    Context mcontext;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        v=inflater.inflate(R.layout.ad_posted_success_poupup,container,false);
        mcontext=getActivity();
        initi();
        return v;


    }

    private void initi() {
        bt_home=(Button) v.findViewById(R.id.bt_home);
        bt_mycoins=(Button) v.findViewById(R.id.bt_mycoins);

        bt_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                getActivity().setResult(Activity.RESULT_OK);
                getActivity().finish();
            }
        });
        bt_mycoins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i=new Intent(mcontext, My_Coin_Activity.class);
                startActivity(i);

               getActivity().finish();
            }
        });


    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        Dialog dialog = super.onCreateDialog(savedInstanceState);
        WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.gravity = Gravity.CENTER;
        wlmp.dimAmount = 0.0F;
        dialog.getWindow().setAttributes(wlmp);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return dialog;
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 1.00);
        int screenHeight = (int) (metrics.heightPixels * 1.00);

        getDialog().getWindow().setLayout(screenWidth, screenHeight);


    }

}
