package com.delainetech.open_bazar.Fragments;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.delainetech.open_bazar.Category.Category_Activity;
import com.delainetech.open_bazar.My_Wallet_Activity;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import io.paperdb.Paper;

import static com.delainetech.open_bazar.Utils.Common.logg;
import static com.delainetech.open_bazar.Utils.ParamKeys.ADS_CATEGORY_RESULT_CODE;


/**
 * Created by Rsss on 2/5/2018.
 */

public class Redeem_Coin_dialog_fragment extends DialogFragment implements ParamKeys {


    View v;
    UserSharedPreferences preferences;
    Context mcontext;


    EditText et_bidamount;
    Button bt_send;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        v=inflater.inflate(R.layout.redeem_coin_dialog,container,false);
        mcontext=getActivity();
        initi();
        return v;
    }

    private void initi()
    {
        preferences=UserSharedPreferences.getInstance(getActivity());
        et_bidamount=(EditText) v.findViewById(R.id.et_bidamount);
        bt_send=(Button) v.findViewById(R.id.bt_send);



        bt_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                if (et_bidamount.getText().toString().trim().length()>0){
                    if (Integer.parseInt(et_bidamount.getText().toString())>0) {
                        redeem_mycoins();
                        dismiss();
                    }else {
                        Toast.makeText(getActivity(), "Please enter coin amount", Toast.LENGTH_SHORT).show();

                    }


                }else {
                    Toast.makeText(getActivity(), "Please enter coin amount", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        Dialog dialog = super.onCreateDialog(savedInstanceState);
        WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.gravity = Gravity.CENTER;
        wlmp.dimAmount = 0.0F;
        dialog.getWindow().setAttributes(wlmp);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return dialog;
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 1.00);
        int screenHeight = (int) (metrics.heightPixels * 1.00);

        getDialog().getWindow().setLayout(screenWidth, screenHeight);
    }


    public void  redeem_mycoins(){


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "redeem_coin", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                logg("redeem_coin",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("message").equals("No suficent balance")){
                        Toast.makeText(mcontext, "You does't have sufficent coin to redeem this coin amount", Toast.LENGTH_SHORT).show();
                    }else {
                        dismiss();
                        Toast.makeText(mcontext, "Successfully redeem", Toast.LENGTH_SHORT).show();
                        ((My_Wallet_Activity)mcontext).index=0;
                        ((My_Wallet_Activity)mcontext).mycoinsArrayList.clear();
                        ((My_Wallet_Activity)mcontext).fetech_mycoins(((My_Wallet_Activity)mcontext).index);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("error","",e);
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                Common.Snakbar(Common.volleyerror(volleyError),et_bidamount);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();

                params.put("coin",et_bidamount.getText().toString().trim());
                params.put(User_id,preferences.getuserid());

                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }



}
