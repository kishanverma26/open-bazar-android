package com.delainetech.open_bazar.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.delainetech.open_bazar.Adapter.Faq_adapter;
import com.delainetech.open_bazar.Drawer_Frame_Activity;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;

public class My_Wallet_Faq_fragment extends Fragment implements ParamKeys,View.OnClickListener{


    View v;
    Context mcontext;
    UserSharedPreferences preferences;

    RecyclerView reclcler;
    LinearLayoutManager linearLayoutManager;
    Faq_adapter adapter;

    String question[]={FAQ_q1,FAQ_q2,FAQ_q3,FAQ_q4,FAQ_q5};
    String ans[]={FAQ_a1,FAQ_a2,FAQ_a3,FAQ_a4,FAQ_a5};
    ImageView iv_faqimage;
   public NestedScrollView nested_scroll;

    Intent i;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.faq_frag, container, false);
        mcontext=getActivity();
        initial();

        return v;
    }




    private void initial() {

        nested_scroll=v.findViewById(R.id.nested_scroll);
        preferences=new UserSharedPreferences(mcontext);
        reclcler=v.findViewById(R.id.reclcler);
        linearLayoutManager=new LinearLayoutManager(mcontext);
        reclcler.setLayoutManager(linearLayoutManager);
        adapter=new Faq_adapter(question,ans,nested_scroll);
        reclcler.setAdapter(adapter);
        iv_faqimage=v.findViewById(R.id.iv_faqimage);
        iv_faqimage.setImageResource(R.drawable.open_bazar_wallet_logo);
        reclcler.setNestedScrollingEnabled(false);
    }




    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.home_notification:

                break;
            case R.id.cv_buycredit:
                i=new Intent(mcontext,Drawer_Frame_Activity.class);
                //                i.putExtra(Pos,7);
                i.putExtra(Pos,20);
                i.putExtra(Title,"Buy Credits Packages");
                startActivity(i);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==11){

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("test","test");

    }
}
