package com.delainetech.open_bazar.Fragments;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;

public class About_us_fragment extends Fragment implements ParamKeys{


    View v;
    Context mcontext;
    UserSharedPreferences preferences;


    TextView tv_about1,tv_about2,tv_about3;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.about_app_fragment, container, false);
        mcontext=getActivity();
        initial();

        return v;
    }

    private void initial() {
        tv_about1=v.findViewById(R.id.tv_about1);
        tv_about1.setText(about_us_str_1);
        tv_about2=v.findViewById(R.id.tv_about2);
        tv_about2.setText(about_us_str_2);
        tv_about3=v.findViewById(R.id.tv_about3);
        tv_about3.setText(about_us_str_3);


    }




}
