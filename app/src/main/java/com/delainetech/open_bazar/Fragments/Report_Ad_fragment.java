package com.delainetech.open_bazar.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.delainetech.open_bazar.Adapter.My_Ads_adapter_User;
import com.delainetech.open_bazar.Adapter.Report_adapter;
import com.delainetech.open_bazar.Custom_classes.DelayedProgressDialog;
import com.delainetech.open_bazar.Custom_classes.EndlessRecyclerViewScrollListener;
import com.delainetech.open_bazar.Models.ADS;
import com.delainetech.open_bazar.Models.REPORT;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import static com.delainetech.open_bazar.Utils.Common.ParseString;
import static com.delainetech.open_bazar.Utils.Common.logg;

@SuppressLint("ValidFragment")
public class Report_Ad_fragment extends Fragment implements ParamKeys,View.OnClickListener{


    View v;
    Context mcontext;
    UserSharedPreferences preferences;

    RecyclerView reclcler;
    LinearLayoutManager linearLayoutManager;
    Report_adapter adapter;
    EditText et_additional;
    TextView textcount;

    ArrayList<REPORT> adsArrayList=new ArrayList<>();
    boolean reportreson=false;
    String id="",resone="",type;
    Button bt_send;
    @SuppressLint("ValidFragment")
    public Report_Ad_fragment(String id,String type) {
        this.id = id;
        this.type = type;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.report_reason, container, false);
        mcontext=getActivity();
        initial();

        return v;
    }



    private void initial() {
        if (type.equals("ads")) {
            adsArrayList.add(new REPORT("Ad Sold", false));
            adsArrayList.add(new REPORT("Duplicate Ad", false));
            adsArrayList.add(new REPORT("Inappropriate", false));
            adsArrayList.add(new REPORT("Wrong category Ad", false));
            adsArrayList.add(new REPORT("Website Listing", false));
            adsArrayList.add(new REPORT("Against the rules", false));
            adsArrayList.add(new REPORT("Other", false));
        }else if (type.equals("user")){
            adsArrayList.add(new REPORT("Inauthentic", false));
            adsArrayList.add(new REPORT("Inappropriate Communication", false));
            adsArrayList.add(new REPORT("Violating Ads", false));
            adsArrayList.add(new REPORT("Sold Ads", false));
            adsArrayList.add(new REPORT("Inappropriate Ads", false));
            adsArrayList.add(new REPORT("Suspicious Advertiser", false));
            adsArrayList.add(new REPORT("Other", false));

        }
        preferences= UserSharedPreferences.getInstance(mcontext);
        bt_send=v.findViewById(R.id.bt_send);
        bt_send.setOnClickListener(this);
        et_additional=v.findViewById(R.id.et_additional);
        textcount=v.findViewById(R.id.textcount);

        reclcler=v.findViewById(R.id.reclcler);
        linearLayoutManager=new LinearLayoutManager(mcontext);
        reclcler.setLayoutManager(linearLayoutManager);
        reclcler.setNestedScrollingEnabled(false);
        adapter=new Report_adapter(mcontext,adsArrayList,Report_Ad_fragment.this);
        reclcler.setAdapter(adapter);


        et_additional.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textcount.setText(et_additional.getText().length()+" / 120");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });




    }



            public void update(){
                reclcler.post(new Runnable()
                {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                });
            }




    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.home_notification:

                break;
            case R.id.bt_send:
                for (int i=0;i<adsArrayList.size();i++){
                    if (adsArrayList.get(i).getSelect()){
                        resone=adsArrayList.get(i).getTital();
                    }
                }
                if (resone.length()>0){
                    report_ad(id,et_additional.getText().toString(),resone);
                }else {
                    Toast.makeText(mcontext, "Select one reason", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }



    public void  report_ad(final String postid, final String des,final String  resaon){

        final DelayedProgressDialog pro=new DelayedProgressDialog();

        pro.show(getFragmentManager(),"");

        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "post_report", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                pro.cancel();
                logg("post_report",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){
                        getActivity().finish();
                        Toast.makeText(mcontext, "Report has be submitted successfully", Toast.LENGTH_SHORT).show();

                    }

                }
                catch (Exception e){
                    pro.cancel();
                    Log.e("error","e",e);
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(User_id,preferences.getuserid());
                params.put(Post_id,postid);
                params.put("description",des);
                if (type.equals("ads")) {
                    params.put("type", "1");
                }else {
                    params.put("type", "0");
                }
                params.put("reason",resaon);

                Log.e("params",params+"");
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }


}
