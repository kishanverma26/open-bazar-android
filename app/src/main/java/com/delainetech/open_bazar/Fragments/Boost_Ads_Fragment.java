package com.delainetech.open_bazar.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.delainetech.open_bazar.Adapter.Boost_Ads_adapter;
import com.delainetech.open_bazar.Adapter.Upgrade_Account_adapter;
import com.delainetech.open_bazar.Drawer_Frame_Activity;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;
import com.delainetech.open_bazar.WebView_Activity;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class Boost_Ads_Fragment extends Fragment implements ParamKeys,View.OnClickListener{


    View v;
    Context mcontext;
    UserSharedPreferences preferences;

    RecyclerView reclcler;
    LinearLayoutManager linearLayoutManager;
    Boost_Ads_adapter adapter;

    TextView tv_faqtext;
    String question[]={"Rocket","Feature Ad","Repost Ad"};
    String ans[]={"Special Badge \n10 Days at the Top","Special Badge \n 7 Days at the Top","Get more views \n 5 Days at the Top"};
    String price[]={"30","20","10"};

    ImageView iv_faqimage;;
    CardView cv;
    String addid="";
    Intent i;
    @SuppressLint("ValidFragment")
    public Boost_Ads_Fragment(String id) {
        this.addid = id;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.faq_frag, container, false);
        mcontext=getActivity();
        initial();

        return v;
    }


    private void initial() {
        cv=v.findViewById(R.id.cv);
        cv.setVisibility(View.GONE);
        preferences=new UserSharedPreferences(mcontext);


        iv_faqimage=v.findViewById(R.id.iv_faqimage);
        tv_faqtext=v.findViewById(R.id.tv_faqtext);
        reclcler=v.findViewById(R.id.reclcler);
        linearLayoutManager=new LinearLayoutManager(mcontext);
        reclcler.setLayoutManager(linearLayoutManager);
        adapter=new Boost_Ads_adapter(question,ans,price,addid);
        reclcler.setAdapter(adapter);

        tv_faqtext.setText(Coin_FAQ);

        iv_faqimage.setImageResource(R.drawable.open_bazar_coins_logo);

    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }

}
