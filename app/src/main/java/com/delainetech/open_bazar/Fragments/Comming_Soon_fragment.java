package com.delainetech.open_bazar.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class Comming_Soon_fragment extends Fragment implements ParamKeys{


    View v;
    Context mcontext;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.comming_soon, container, false);
        mcontext=getActivity();
        initial();

        return v;
    }

    private void initial() {

    }




}
