package com.delainetech.open_bazar.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.delainetech.open_bazar.Adapter.Faq_adapter;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;

import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class Help_fragment extends Fragment implements ParamKeys,View.OnClickListener{


    View v;
    Context mcontext;
    UserSharedPreferences preferences;

    RecyclerView reclcler;
    LinearLayoutManager linearLayoutManager;
    Faq_adapter adapter;

    TextView tv_faqtext;
    String question[]={Help_q1,Help_q2,Help_q3,Help_q4,Help_q5,Help_q6,Help_q7};
    String ans[]={help_ans1,help_ans2,help_ans3,help_ans4,help_ans5,help_ans6,help_ans7};
    ImageView iv_faqimage;
    NestedScrollView nested_scroll;
    LinearLayout ll_header;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.help_frag, container, false);
        mcontext=getActivity();
        initial();

        return v;
    }


    private void initial() {
        preferences=new UserSharedPreferences(mcontext);
        ll_header=v.findViewById(R.id.ll_header);
       nested_scroll=v.findViewById(R.id.nested_scroll);
        iv_faqimage=v.findViewById(R.id.iv_faqimage);
        tv_faqtext=v.findViewById(R.id.tv_faqtext);
        reclcler=v.findViewById(R.id.reclcler);
        linearLayoutManager=new LinearLayoutManager(mcontext);
        reclcler.setLayoutManager(linearLayoutManager);
        adapter=new Faq_adapter(question,ans,nested_scroll);
        reclcler.setAdapter(adapter);
        reclcler.setNestedScrollingEnabled(false);
        tv_faqtext.setVisibility(View.GONE);

        iv_faqimage.setImageResource(R.drawable.logo);


    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.home_notification:

                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==11){

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("test","test");

    }
}
