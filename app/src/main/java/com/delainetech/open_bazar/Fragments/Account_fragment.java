package com.delainetech.open_bazar.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import io.paperdb.Paper;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.delainetech.open_bazar.Drawer_Frame_Activity;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;
import com.white.progressview.CircleProgressView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static com.delainetech.open_bazar.Utils.Common.ParseString;
import static com.delainetech.open_bazar.Utils.Common.logg;

public class Account_fragment extends Fragment implements ParamKeys,View.OnClickListener{


    View v;
    Context mcontext;
    UserSharedPreferences preferences;
    TextView tv_upgread_acc;
    Button bt_upgread_acc;
    Intent i;
    TextView tv_liveadscounts,tv_remainingcounts,tv_liveads_count,tv_paidadscount,tv_totaladslimit,tv_accounttype,tv_deletedadscount;
    CircleProgressView circle_progress_normal;
    int progress=70;
    Timer timer;
    Button bt_upgread;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.account_fragment, container, false);
        mcontext=getActivity();
        initial();

        return v;
    }

    private void initial() {
        preferences=new UserSharedPreferences(mcontext);
        circle_progress_normal=v.findViewById(R.id.circle_progress_normal);
         circle_progress_normal.setProgress(0);
        circle_progress_normal.setMax(100);
        tv_liveadscounts=v.findViewById(R.id.tv_liveadscounts);
        tv_remainingcounts=v.findViewById(R.id.tv_remainingcounts);
        tv_liveads_count=v.findViewById(R.id.tv_liveads_count);
        tv_paidadscount=v.findViewById(R.id.tv_paidadscount);
        tv_totaladslimit=v.findViewById(R.id.tv_totaladslimit);
        tv_accounttype=v.findViewById(R.id.tv_accounttype);
        tv_deletedadscount=v.findViewById(R.id.tv_deletedadscount);

        bt_upgread_acc=v.findViewById(R.id.bt_upgread_acc);
        bt_upgread_acc.setOnClickListener(this);
        bt_upgread=v.findViewById(R.id.bt_upgread);
        bt_upgread.setOnClickListener(this);



        tv_liveadscounts.setText(Html.fromHtml("<font color=#58697B>Live Ads </font> <font color=#c62828>0</font>"));
        tv_remainingcounts.setText(Html.fromHtml("<font color=#58697B>Remaining </font> <font color=#c62828>0</font>"));


        if (Paper.book("my_account").contains("response")){

            setdata(Paper.book("my_account").read("response").toString());
        }

            fetech_myaccount();


    }

    int live_ad=0,tottal_ad=0,val=0;

    private void setdata(String res) {
        try {
            JSONObject object=new JSONObject(res);
            Paper.book("my_account").write("response",res);
            tv_liveadscounts.setText(Html.fromHtml("<font color=#58697B>Live Ads </font> <font color=#c62828>"+
                    ParseString(object.getJSONObject("data"),"ads_count")+"</font>"));

            tv_remainingcounts.setText(Html.fromHtml("<font color=#58697B>Remaining </font> <font color=#c62828>"+
                    ParseString(object.getJSONObject("data"),"total_ads")+"</font>"));

            tv_liveads_count.setText(ParseString(object.getJSONObject("data"),"ads_count"));

            live_ad=Integer.parseInt(ParseString(object.getJSONObject("data"),"ads_count"));
            tottal_ad=Integer.parseInt(ParseString(object.getJSONObject("data"),"total_ads"));


            tv_remainingcounts.setText(Html.fromHtml("<font color=#58697B>Remaining </font> <font color=#c62828>"+
                    (tottal_ad)+"</font>"));

            tv_paidadscount.setText(ParseString(object.getJSONObject("data"),"total_paid_ads"));
            tv_liveads_count.setText(ParseString(object.getJSONObject("data"),"total_free_ads"));
            tv_deletedadscount.setText(ParseString(object.getJSONObject("data"),"delete_count"));

            if (ParseString(object.getJSONObject("data"),"total_paid_ads").length()>0 && ParseString(object.getJSONObject("data"),"total_free_ads").length()>0) {
                tv_totaladslimit.setText((Integer.parseInt(ParseString(object.getJSONObject("data"), "total_paid_ads")) +
                        Integer.parseInt(ParseString(object.getJSONObject("data"), "total_free_ads"))) + "");

            }
            progress=   live_ad*100/(tottal_ad+live_ad);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                circle_progress_normal.setProgress(progress,true);
            }
            else {
                circle_progress_normal.setProgress(progress);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bt_upgread:
                i=new Intent(mcontext, Drawer_Frame_Activity.class);
//                i.putExtra(Pos,16);
                i.putExtra(Pos,20);
                i.putExtra(Title,"Upgrade Account");
                startActivity(i);
                break;
            case R.id.bt_upgread_acc:
//                i=new Intent(mcontext,Drawer_Frame_Activity.class);
//                i.putExtra(Pos,7);
//                i.putExtra(Title,"Buy Credits Packages");
//                startActivity(i);
                break;

        }
    }


    public void  fetech_myaccount(){


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "my_account", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                pro.cancel();
                logg("my_account",response);

                setdata(response);
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),tv_liveadscounts);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(User_id,preferences.getuserid());




                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }


}
