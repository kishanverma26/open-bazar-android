package com.delainetech.open_bazar.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.viewpager.widget.ViewPager;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.InputFilter;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.delainetech.open_bazar.Adapter.Detail_Adapter_recyclerview;
import com.delainetech.open_bazar.Adapter.Home_adapter;
import com.delainetech.open_bazar.Adapter.Image_Viewpage_Adapter;
import com.delainetech.open_bazar.Adapter.Similar_Ads_Adapter;
import com.delainetech.open_bazar.All_offers_Activity;
import com.delainetech.open_bazar.Chat.ChatActivity;
import com.delainetech.open_bazar.Chat.FirbaseCommon;
import com.delainetech.open_bazar.Custom_classes.CTextView;
import com.delainetech.open_bazar.Custom_classes.SpacesItemDecoration;
import com.delainetech.open_bazar.Drawer_Frame_Activity;
import com.delainetech.open_bazar.Models.ADS;
import com.delainetech.open_bazar.Models.Category_values;
import com.delainetech.open_bazar.Other_User_Profile;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.zfdang.multiple_images_selector.FolderPopupWindow;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.delainetech.open_bazar.Utils.Common.ParseString;
import static com.delainetech.open_bazar.Utils.Common.logg;

@SuppressLint("ValidFragment")
public class Ads_Detail_fragment extends Fragment implements View.OnClickListener,ParamKeys {

    View view;
    Context mcontext;

    ImageView iv_back,iv_call,iv_bid,iv_chat;
    TextView tv_call,tv_bid,tv_chat;

    ViewPager view_pager;
    Image_Viewpage_Adapter image_viewpage_adapter;
    private ArrayList<String> postimages = new ArrayList<>();
    RelativeLayout rl_chat,rl_call,rl_bid;
    EditText et_bidamount;
    ImageView iv_icon;
    NestedScrollView nested_scroll;
    ADS ads;
    TextView tv_tital,tv_time,tv_views,tv_favorite_count,tv_location,tv_price,tv_des,tv_imagecount,tv_requiredprice,
            tv_myofferads_counts,tv_myofferads,tv_report;
    CTextView tv_like,tv_favorite,tv_share;
    UserSharedPreferences preferences;
    LinearLayout ll_empty;

    de.hdodenhof.circleimageview.CircleImageView iv_circle,userimg;
    TextView tv_username,tv_usernamecard;
    AppCompatButton bt_send;
    CardView cv_offer,cv_profile;

    String bidtype="",image0="";
    Button bt_follow;

//    similar ads
    RecyclerView rv;
    LinearLayout ll_similar_ads;
    GridLayoutManager gridLayoutManager;
    Similar_Ads_Adapter similar_ads_adapter;

//    category
ArrayList<Category_values> category_values=new ArrayList<Category_values>();
ArrayList<ADS> adsArrayList=new ArrayList<ADS>();

    RecyclerView recyclerview;
    Detail_Adapter_recyclerview detail_adapter_recyclerview;
    LinearLayoutManager linearLayoutManager;
    TextView tv_jointime,tv_adcount;

//    contact us
    ImageView iv_contactus;
    TextView tv_shopname;


//    online status
private DatabaseReference mUsersDatabase;
ImageView iv_online;


//Boost
    ImageView iv_boost;
    @SuppressLint("ValidFragment")
    public Ads_Detail_fragment(ADS ads) {
        this.ads = ads;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.detail_pager_fragment, container, false);
        mcontext=getActivity();
        init();

        return view;
    }


    private void init() {
        preferences=UserSharedPreferences.getInstance(mcontext);

//        similar ads
        ll_similar_ads=view.findViewById(R.id.ll_similar_ads);
        rv=view.findViewById(R.id.rv);
        gridLayoutManager=new GridLayoutManager(mcontext,3);
        rv.setLayoutManager(gridLayoutManager);
        int spacingInPixels = getResources().getDimensionPixelSize(com.zfdang.multiple_images_selector.R.dimen.d_2dp);
        rv.addItemDecoration(new SpacesItemDecoration(3,spacingInPixels,true));

        tv_shopname=view.findViewById(R.id.tv_shopname);
        iv_contactus=view.findViewById(R.id.iv_contactus);
        iv_contactus.setOnClickListener(this);
        bt_follow=view.findViewById(R.id.bt_follow);
        bt_follow.setOnClickListener(this);
        tv_jointime=view.findViewById(R.id.tv_jointime);
        tv_adcount=view.findViewById(R.id.tv_adcount);
        cv_offer=view.findViewById(R.id.cv_offer);
        cv_profile=view.findViewById(R.id.cv_profile);
        cv_profile.setOnClickListener(this);
        bt_send=view.findViewById(R.id.bt_send);
        bt_send.setOnClickListener(this);
        ll_empty=view.findViewById(R.id.ll_empty);

//        user image
        iv_circle=view.findViewById(R.id.iv_circle);
        userimg=view.findViewById(R.id.userimg);
        iv_circle.setOnClickListener(this);
        tv_username=view.findViewById(R.id.tv_username);
        tv_usernamecard=view.findViewById(R.id.tv_usernamecard);

        tv_tital=view.findViewById(R.id.tv_tital);
        tv_time=view.findViewById(R.id.tv_time);
        tv_views=view.findViewById(R.id.tv_views);
        tv_like=view.findViewById(R.id.tv_like);
        tv_like.setOnClickListener(this);

        tv_favorite=view.findViewById(R.id.tv_favorite);
        tv_favorite.setOnClickListener(this);
        tv_share=view.findViewById(R.id.tv_share);
        tv_share.setOnClickListener(this);
        tv_favorite_count=view.findViewById(R.id.tv_favorite_count);
        tv_location=view.findViewById(R.id.tv_location);
        tv_price=view.findViewById(R.id.tv_price);
        tv_des=view.findViewById(R.id.tv_des);
        tv_imagecount=view.findViewById(R.id.tv_imagecount);
        tv_requiredprice=view.findViewById(R.id.tv_requiredprice);
        tv_myofferads_counts=view.findViewById(R.id.tv_myofferads_counts);
        tv_myofferads=view.findViewById(R.id.tv_myofferads);
        tv_myofferads.setOnClickListener(this);
        tv_report=view.findViewById(R.id.tv_report);
        tv_report.setOnClickListener(this);

        nested_scroll=view.findViewById(R.id.nested_scroll);
        et_bidamount=view.findViewById(R.id.et_bidamount);
        iv_icon=view.findViewById(R.id.iv_icon);
        iv_call=view.findViewById(R.id.iv_call);
        iv_bid=view.findViewById(R.id.iv_bid);
        iv_chat=view.findViewById(R.id.iv_chat);
        tv_call=view.findViewById(R.id.tv_call);
        tv_bid=view.findViewById(R.id.tv_bid);
        tv_chat=view.findViewById(R.id.tv_chat);

        rl_chat=view.findViewById(R.id.rl_chat);
        rl_chat.setOnClickListener(this);
        rl_call=view.findViewById(R.id.rl_call);
        rl_call.setOnClickListener(this);
        rl_bid=view.findViewById(R.id.rl_bid);
        rl_bid.setOnClickListener(this);

        view_pager=view.findViewById(R.id.view_pager);

        iv_boost=view.findViewById(R.id.iv_boost);
        iv_online=view.findViewById(R.id.iv_online);
        recyclerview=view.findViewById(R.id.recyclerview);
        linearLayoutManager=new LinearLayoutManager(mcontext);
        recyclerview.setLayoutManager(linearLayoutManager);
        recyclerview.setNestedScrollingEnabled(false);
        iv_back=view.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(this);

        tv_tital.setText(ads.getAd_title());
        tv_time.setText(Common.getTimeAgo(Long.parseLong(ads.getTimestamp()),getActivity()));
        tv_location.setText(ads.getCity());
        tv_views.setText(ads.getViews_count());
        tv_des.setText(ads.getAd_description());

        if (ads.getOffer_count().equals("0")){
            cv_offer.setVisibility(View.GONE);
        }

        logg("offercount",ads.getOffer_count()+"");
        if (ads.getPrice_type().equals("price"))
        {
            tv_price.setText("€ "+ads.getPrice());
            bidtype="0";
            int maxLength = 10;
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(maxLength);
            et_bidamount.setFilters(FilterArray);
            et_bidamount.setInputType(InputType.TYPE_CLASS_NUMBER);
            tv_myofferads_counts.setText(ads.getOffer_count());
            tv_requiredprice.setText("The required price € "+ads.getPrice());
            et_bidamount.setHint(mcontext.getResources().getString(R.string.enter_the_amount_of_offer));
            iv_icon.setImageDrawable(mcontext.getResources().getDrawable(R.drawable.ic_auction_red));
            iv_bid.setImageDrawable(mcontext.getResources().getDrawable(R.drawable.ic_auction));
            tv_bid.setText(mcontext.getResources().getString(R.string.bid));
            bt_send.setText(mcontext.getResources().getString(R.string.place_your_offer));

        }
        else  if (ads.getPrice_type().equals("ask for price"))
        {
            tv_price.setText(mcontext.getResources().getString(R.string.askforprice));
            bidtype="1";
            et_bidamount.setInputType(InputType.TYPE_CLASS_TEXT);
            tv_myofferads.setText(mcontext.getResources().getString(R.string.seeallcomments));
            tv_requiredprice.setText(mcontext.getResources().getString(R.string.contactuseraboutad));
            et_bidamount.setHint(mcontext.getResources().getString(R.string.enteryourcomment));
            tv_myofferads_counts.setText(ads.getOffer_count());
            iv_icon.setImageDrawable(mcontext.getResources().getDrawable(R.drawable.ic_chat1));
            iv_bid.setImageDrawable(mcontext.getResources().getDrawable(R.drawable.ic_chat_icon));
            tv_bid.setText(mcontext.getResources().getString(R.string.comment));
            bt_send.setText(mcontext.getResources().getString(R.string.sendyourcomment));

        }
        else  if (ads.getPrice_type().equals("exchange"))
        {
            bidtype="1";
            et_bidamount.setInputType(InputType.TYPE_CLASS_TEXT);
            tv_price.setText(mcontext.getResources().getString(R.string.exchange));
            tv_myofferads.setText(mcontext.getResources().getString(R.string.seeallcomments));
            tv_requiredprice.setText(mcontext.getResources().getString(R.string.contactuseraboutad));
            et_bidamount.setHint(mcontext.getResources().getString(R.string.enteryourcomment));
            tv_myofferads_counts.setText(ads.getOffer_count());
            iv_icon.setImageDrawable(mcontext.getResources().getDrawable(R.drawable.ic_chat1));
            iv_bid.setImageDrawable(mcontext.getResources().getDrawable(R.drawable.ic_chat_icon));
            tv_bid.setText(mcontext.getResources().getString(R.string.comment));
            bt_send.setText(mcontext.getResources().getString(R.string.sendyourcomment));
        }
        else  if (ads.getPrice_type().equals("free"))
        {
            bidtype="1";
            tv_price.setText("Free");
            et_bidamount.setInputType(InputType.TYPE_CLASS_TEXT);
            tv_myofferads.setText(mcontext.getResources().getString(R.string.seeallcomments));
            tv_requiredprice.setText(mcontext.getResources().getString(R.string.contactuseraboutad));
            et_bidamount.setHint(mcontext.getResources().getString(R.string.enteryourcomment));
            tv_myofferads_counts.setText(ads.getOffer_count());
            iv_icon.setImageDrawable(mcontext.getResources().getDrawable(R.drawable.ic_chat1));
            iv_bid.setImageDrawable(mcontext.getResources().getDrawable(R.drawable.ic_chat_icon));
            tv_bid.setText(mcontext.getResources().getString(R.string.comment));
            bt_send.setText(mcontext.getResources().getString(R.string.sendyourcomment));
        }


        if (ads.getImages().length()>0)
        {
            try {
                ll_empty.setVisibility(View.GONE);
                view_pager.setVisibility(View.VISIBLE);
                JSONArray jsonArray = new JSONArray(ads.getImages());
                for (int i=0;i<jsonArray.length();i++) {

                    postimages.add(jsonArray.getJSONObject(i).getString("image"));
                }
                image0=jsonArray.getJSONObject(0).getString("image");
                logg("image", Common.Image_Loading_Url(jsonArray.getJSONObject(0).getString("image")) + "");
            } catch (JSONException e) {
                e.printStackTrace();
                logg("excep", e + "");
            }
            tv_imagecount.setText("1 / "+postimages.size());
        }
        else
        {
            view_pager.setVisibility(View.GONE);
            ll_empty.setVisibility(View.VISIBLE);
            tv_imagecount.setText("0");
        }

        image_viewpage_adapter=new Image_Viewpage_Adapter(mcontext,postimages,ads);
        view_pager.setAdapter(image_viewpage_adapter);
        view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (ads.getImages().length()>0) {

                    tv_imagecount.setText((position + 1) + " / " + postimages.size());
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        RequestOptions options=new RequestOptions().centerCrop().error(R.drawable.userimage);
        Glide.with(mcontext).load(Common.Image_Loading_Url(ads.getUser_image())).apply(options).into(iv_circle);
        Glide.with(mcontext).load(Common.Image_Loading_Url(ads.getUser_image())).apply(options).into(userimg);
        tv_username.setText(ads.getUser_name());
        tv_usernamecard.setText(ads.getUser_name());
        view(ads.getId(),"view");

        Common.logg("logdata_likestatus",ads.getLike_status()+"---"+ads.getLike_count());
        if (ads.getLike_status().equals("Yes")){
            likedislike("1");
        }else {
            likedislike("0");
        }
        tv_like.setText(ads.getLike_count().toString());
        logg("likedata",tv_like.getText().toString());

        Common.logg("logdata",ads.getFav_status()+"----"+ads.getFavorite_count());

        if (ads.getFav_status().equals("Yes")){
            addfavriout("1");
        }else {
            addfavriout("0");
        }

        tv_favorite_count.setText(ads.getFavorite_count().toString());

//        category
        category_values.add(new Category_values("Ad id",ads.getId(),"",
                "",""));

        if (ads.getCategory().length()>0){
            try {
                JSONArray jsonArray=new JSONArray(ads.getCategory());
                for (int i=2;i<jsonArray.length();i++){
                    JSONObject jsonObject=jsonArray.getJSONObject(i);
                category_values.add(new Category_values(ParseString(jsonObject,"tital"),ParseString(jsonObject,"seletecvalue"),ParseString(jsonObject,"id"),
                        ParseString(jsonObject,"value"),ParseString(jsonObject,"image")));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            detail_adapter_recyclerview=new Detail_Adapter_recyclerview(mcontext,category_values);
            recyclerview.setAdapter(detail_adapter_recyclerview);
        }
        tv_adcount.setText(ads.getAds_count()+" Ads");
        tv_jointime.setText(getResources().getString(R.string.member_since)+" "+Common.changeDateFormatFromAnother(ads.getUser_regiterdate()));

        if (ads.getFollow_user().equals("true")){
            bt_follow.setText(getString(R.string.following));
        }else {
            bt_follow.setText(getString(R.string.follow_now));

        }

        if (ads.getMain_category().equals("4")){
            tv_shopname.setText(ads.getShop_name());
        }


//        firbase chat online offline status
        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child("user"+ads.getUser_id());
        mUsersDatabase.keepSynced(true);

        if (ads.getBoost_type().equals("0")){
            iv_boost.setVisibility(View.VISIBLE);
            iv_boost.setImageDrawable(getResources().getDrawable(R.drawable.boost_ic_rocket));
        }else if (ads.getBoost_type().equals("1")){
            iv_boost.setVisibility(View.VISIBLE);
            iv_boost.setImageDrawable(getResources().getDrawable(R.drawable.boost_ic_feature));
        }else {
           iv_boost.setVisibility(View.GONE);
       }
        online_offline_firbaselistner();

        similar_ads();
    }

    private void online_offline_firbaselistner() {
        mUsersDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild("online")) {

                    String userOnline = dataSnapshot.child("online").getValue().toString();

                    if(userOnline.equals("true")){

                        iv_online.setVisibility(View.VISIBLE);

                    } else {

                        iv_online.setVisibility(View.INVISIBLE);

                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.bt_follow:
                if (ads.getFollow_user().equals("true")){
                    alert(getActivity(),ads.getUser_name());
                }else {
                    follow_user(ads.getUser_id());
                }
                break;

            case R.id.iv_back:
                getActivity().onBackPressed();
                break;

            case R.id.iv_circle:
                Intent ii=new Intent(mcontext, Other_User_Profile.class);
                ii.putExtra("id",ads.getUser_id());
                startActivity(ii);
                break;

            case R.id.cv_profile:
                ii=new Intent(mcontext, Other_User_Profile.class);
                ii.putExtra("id",ads.getUser_id());
                startActivity(ii);
                break;

            case R.id.bt_send:

                if(ads.getPrice_type().equalsIgnoreCase("price"))
                {
                    if (et_bidamount.getText().toString().trim().length() > 0)
                    {
                        if (ads.getPrice_type().equals("price"))
                        {
                            FirbaseCommon.regiter_adpost(ads.getId(), ads.getAd_title(), image0, ads.getPrice(), ads.getMobile(), ads.getHide_mobileno());

                            FirbaseCommon.sendMessage("€ " + et_bidamount.getText().toString(), "offer", ads.getId(), preferences.getuserid()
                                    , ads.getUser_id(), preferences.getname());
                        }
                        offer_bid(ads.getId());

                    }
                    else
                    {
                        Common.Snakbar("Place a bid amount", tv_bid);
                    }
                }
                else
                {
                    if (et_bidamount.getText().toString().trim().length() > 0)
                    {
                        offer_bid(ads.getId());
                    }
                    else
                    {
                        Common.Snakbar("Place a comment", tv_bid);
                    }
                }
                break;
            case R.id.rl_chat:
                Intent chatIntent = new Intent(getActivity(), ChatActivity.class);
                if (ads.getPrice_type().equals("price"))
                {
                    FirbaseCommon.regiter_adpost(ads.getId(),ads.getAd_title(),image0,ads.getPrice(),ads.getMobile(),ads.getHide_mobileno());
                    chatIntent.putExtra("ad_price","€ "+ads.getPrice());
                }else {
                    FirbaseCommon.regiter_adpost(ads.getId(),ads.getAd_title(),image0,ads.getPrice_type(),ads.getMobile(),ads.getHide_mobileno());
                    chatIntent.putExtra("ad_price",ads.getPrice_type());
                }


                chatIntent.putExtra("user_id", ads.getUser_id());
                chatIntent.putExtra("user_name", ads.getUser_name());
                chatIntent.putExtra("name", preferences.getname());
                chatIntent.putExtra("id",preferences.getuserid());
                chatIntent.putExtra("ad_tital",ads.getAd_title());
                chatIntent.putExtra("ad_image",image0);
                chatIntent.putExtra("ad_id",ads.getId());

                logg("ides", ads.getUser_id() + ",,"  + ",," + preferences.getuserid());
                startActivity(chatIntent);

                customnavbar(2);
                break;

            case R.id.rl_call:
                if (ads.getHide_mobileno().equals("false")) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:"+ads.getMobile()));
                    startActivity(intent);
                    customnavbar(0);
                }else {
                    Toast.makeText(mcontext, "Contact the user for phone number", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.rl_bid:
                customnavbar(1);
                break;
          case R.id.tv_like:
              logg("tets",ads.getLike_status());
              if (ads.getLike_status().equals("No")) {
                  view(ads.getId(), "likes");
              }
              break;

          case R.id.tv_favorite:
              view(ads.getId(),"favorite");
              break;

          case R.id.tv_share:
              create_dynamic_link(ads.getAd_title(),"adpost/"+ads.getId()+","+preferences.getuserid());
              break;

          case R.id.tv_report:
              Intent intent=new Intent(mcontext, Drawer_Frame_Activity.class);
              intent.putExtra(Pos,14);
              intent.putExtra("type","ads");
              intent.putExtra(Title,"Ad Report");
              intent.putExtra("id",ads.getId());
              startActivity(intent);
              break;

          case R.id.tv_myofferads:
              Intent   i=new Intent(mcontext, All_offers_Activity.class);
              i.putExtra("count",Integer.parseInt(ads.getOffer_count()));
              i.putExtra("id",ads.getId());
              i.putExtra("bid_type",bidtype);
              startActivityForResult(i,OFFER_COUNT);
              break;

          case R.id.iv_contactus:
              i=new Intent(getActivity(),Drawer_Frame_Activity.class);
              i.putExtra(Pos,2);
              i.putExtra(Title,"Contact Us");
              startActivity(i);
              break;
        }
    }

    //custom navigation bar
    private void customnavbar(int position)
    {
        int active_color=R.color.on_click_icon_color;
        int inactive_color=R.color.white;
        tv_call.setTextColor(getResources().getColor(inactive_color));
        tv_bid.setTextColor(getResources().getColor(inactive_color));
        tv_chat.setTextColor(getResources().getColor(inactive_color));
        final float scale = getResources().getDisplayMetrics().density;
        int active_height_ = (int) (26 * scale + 0.5f);
        int active_width_ = (int) (26 * scale + 0.5f);
        int inactive_height_ = (int) (24 * scale + 0.5f);
        int inactive_width_ = (int) (24 * scale + 0.5f);

        iv_call.setColorFilter(ContextCompat.getColor(mcontext, inactive_color), android.graphics.PorterDuff.Mode.SRC_IN);
        iv_bid.setColorFilter(ContextCompat.getColor(mcontext, inactive_color), android.graphics.PorterDuff.Mode.SRC_IN);
        iv_chat.setColorFilter(ContextCompat.getColor(mcontext, inactive_color), android.graphics.PorterDuff.Mode.SRC_IN);

        iv_call.getLayoutParams().height = inactive_height_;
        iv_call.getLayoutParams().width =inactive_width_;
        iv_bid.getLayoutParams().height = inactive_height_;
        iv_bid.getLayoutParams().width =inactive_width_;
        iv_chat.getLayoutParams().height = inactive_height_;
        iv_chat.getLayoutParams().width =inactive_width_;


        switch (position){
            case 0:
            tv_call.setTextColor(getResources().getColor(active_color));
          iv_call.setColorFilter(ContextCompat.getColor(mcontext, active_color), android.graphics.PorterDuff.Mode.SRC_IN);
                iv_call.getLayoutParams().height = active_height_;
               iv_call.getLayoutParams().width =active_width_;

                //Action
                et_bidamount.setFocusable(false);

                break;

            case 1:
                tv_bid.setTextColor(getResources().getColor(active_color));
                iv_bid.setColorFilter(ContextCompat.getColor(mcontext, active_color), android.graphics.PorterDuff.Mode.SRC_IN);

                iv_bid.getLayoutParams().height = active_height_;
                iv_bid.getLayoutParams().width = active_width_;

//              Action
                et_bidamount.setFocusable(true);
                nested_scroll.post(new Runnable() {
                    @Override
                    public void run() {
                        nested_scroll.fullScroll(View.FOCUS_DOWN);
                    }
                });
                break;


            case 2:
             tv_chat.setTextColor(getResources().getColor(active_color));
             iv_chat.setColorFilter(ContextCompat.getColor(mcontext, active_color), android.graphics.PorterDuff.Mode.SRC_IN);

             iv_chat.getLayoutParams().height = active_height_;
             iv_chat.getLayoutParams().width = active_width_;
             et_bidamount.setFocusable(false);

             break;

        }

    }

    @Override
    public void onResume() {
        super.onResume();
        customnavbar(4);
    }

    public void  view(final String postid, final String type){

        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "ad_view", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                logg("insert_ads",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){

                        if (type.equals("likes"))
                        {
                            likedislike(Common.ParseString(object.getJSONObject("data"),"likes"));
                            if (Common.ParseString(object.getJSONObject("data"),"likes").equals("0")){
                                if ((Integer.parseInt(tv_like.getText().toString())>0)) {
                                    tv_like.setText((Integer.parseInt(tv_like.getText().toString()) - 1) + "");
                                }
                            }else {
                                tv_like.setText((Integer.parseInt(tv_like.getText().toString())+1)+"");

                            }
                            ads.setLike_count(tv_like.getText().toString());
                        }
                        else if (type.equals("favorite"))
                        {
                            addfavriout(Common.ParseString(object.getJSONObject("data"),"favorite"));
                            if (Common.ParseString(object.getJSONObject("data"),"favorite").equals("0"))
                            {
                                if ((Integer.parseInt(tv_favorite_count.getText().toString())>0))
                                {
                                    tv_favorite_count.setText((Integer.parseInt(tv_favorite_count.getText().toString()) - 1) + "");
                                }
                            }
                            else
                            {
                                tv_favorite_count.setText((Integer.parseInt(tv_favorite_count.getText().toString())+1)+"");
                            }
                            ads.setFavorite_count(tv_favorite_count.getText().toString());
                            logg("favourite_count",ads.getFavorite_count()+"-----"+tv_favorite_count.getText().toString());
                        }

                    }

                }
                catch (Exception e){}
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                Common.Snakbar(Common.volleyerror(volleyError),tv_bid);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                logg("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(User_id,preferences.getuserid());
                params.put("post_id",postid);
                params.put("type",type);

                logg("params",params+"");
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }


    public void likedislike(String count){

        if (count.equals("0")){
            ads.setLike_status("No");
            tv_like.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_like_white_24dp,0,0,0);
        }else {
            ads.setLike_status("Yes");
            tv_like.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_like_24dp,0,0,0);

        }
    }

    public void addfavriout(String count)
    {
        logg("logdata3",count+"");
        if (count.equals("0"))
        {
            ads.setFav_status("No");
            tv_favorite.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_favorite_15dp_grey,0,0,0);
        }
        else
        {
            ads.setFav_status("Yes");
            tv_favorite.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_favorite_red_15dp,0,0,0);
        }
    }




    public void  offer_bid(final String postid){

        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "offer_bid", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                logg("insert_ads",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){

                        et_bidamount.setText("");
                        if(bidtype.equals("0"))
                        {
                            Common.Snakbar("Your offer has been placed", tv_bid);
                        }
                        else
                        {
                            Common.Snakbar("Your comment has been placed",tv_bid);
                        }
                        tv_myofferads_counts.setText((Integer.parseInt(ads.getOffer_count())+1)+"");
                        ads.setOffer_count((Integer.parseInt(ads.getOffer_count())+1)+"");
                        cv_offer.setVisibility(View.VISIBLE);

                    }

                }
                catch (Exception e){}
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                Common.Snakbar(Common.volleyerror(volleyError),tv_bid);
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                logg("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(User_id,preferences.getuserid());
                params.put(Post_id,postid);
                params.put(Bid_price,et_bidamount.getText().toString().trim());
                params.put(Ttimestamp,System.currentTimeMillis()+"");
                params.put(Bid_type,bidtype);

                logg("params",params+"");
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        logg("innn","innn1111");
        logg("innnrequestcode=",requestCode+", resultecode="+resultCode);

        if (requestCode==OFFER_COUNT && resultCode==RESULT_OK){
            logg("innn","innn");
            ads.setOffer_count(data.getIntExtra("count",0)+"");
            tv_myofferads_counts.setText(ads.getOffer_count());

        }

    }


//    public void inflateDialogue() {
//        LayoutInflater layoutInflater=getActivity().getLayoutInflater();
//        final View view = layoutInflater.inflate(R.layout.ad_report_dialog_lay, null);
//        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
//        alertDialog.setTitle("Ad Report");
//
//        alertDialog.setCancelable(false);
//
//      final EditText  et_des = (EditText) view.findViewById(R.id.et_des);
//
//        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Submit", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                if (et_des.getText().toString().trim().length()>0) {
////                    report_ad(ads.getId(),et_des.getText().toString().trim());
//                    dialog.dismiss();
//                }else {
//                    Toast.makeText(getActivity(), "Write your report resion", Toast.LENGTH_SHORT).show();
//
//                }
//
//
//            }
//        });
//        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//        });
//        alertDialog.setView(view);
//        alertDialog.show();
//    }


    public void  follow_user(final String userid){


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "follow", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                logg("follow",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){
                        if (object.getString("message").equals("Unfollow Successfully")){
                            bt_follow.setText(getString(R.string.follow_now));
                            ads.setFollow_user("false");
                        }else {
                            bt_follow.setText(getString(R.string.following));
                            ads.setFollow_user("true");
                        }
                        }

                }
                catch (Exception e){}
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                Common.Snakbar(Common.volleyerror(volleyError),tv_bid);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                logg("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(User_id,preferences.getuserid());
                params.put("following_id",userid);
                logg("params",params+"");
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }


    public void alert(Context mcoxt,String name){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mcoxt);

        alertDialog.setMessage("Are you sure you want to unfollow "+name+"?");
        alertDialog.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                follow_user(ads.getUser_id());
            }
        });
        alertDialog.setNegativeButton(getResources().getString(R.string.No), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog1= alertDialog.create();
        alertDialog1.show();

        alertDialog1.getButton(alertDialog1.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorAccent));
        alertDialog1.getButton(alertDialog1.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorAccent));
    }

    public void create_dynamic_link(final String msg, String str){

        Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse("https://openbazar.page.link?="+str))
                .setDomainUriPrefix("https://openbazar.page.link")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder("com.open_bazar")
                        .setFallbackUrl(Uri.parse("https://www.dropbox.com/s/o1z7e9w94g7twqz/app-debug.apk?dl=0"))
                        .build())
                // Set parameters
                // ...
                .buildShortDynamicLink()

                .addOnCompleteListener(getActivity(), new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {
                            // Short link created
                            Uri shortLink = task.getResult().getShortLink();
                            Uri flowchartLink = task.getResult().getPreviewLink();

                            logg("shortLink",shortLink+" flowchartLink"+flowchartLink);


                            String shareBody2 = msg+" "+shortLink;
                            Intent sharingIntent2 = new Intent(android.content.Intent.ACTION_SEND);
                            sharingIntent2.setType("text/plain");
                            sharingIntent2.putExtra(android.content.Intent.EXTRA_SUBJECT, "Open Bazar");
                            sharingIntent2.putExtra(android.content.Intent.EXTRA_TEXT, shareBody2);
                            startActivity(Intent.createChooser(sharingIntent2, ""));

                        } else {
                            // Error
                            // ...
                            Toast.makeText(mcontext, "Somthing went wrong please try again", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


    public void  similar_ads(){

        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "similar_ads", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                logg("similar_ads",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){

                        JSONArray jsonArray=object.getJSONArray("data");

                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject object1=jsonArray.getJSONObject(i);
                            if (!ads.getId().equals(ParseString(object1,"id"))) {
                                adsArrayList.add(new ADS(ParseString(object1, "id"), ParseString(object1, "user_id"), ParseString(object1, "ad_title"),
                                        ParseString(object1, "ad_description"), ParseString(object1, "mobile"), ParseString(object1, "price")
                                        , ParseString(object1, "images"), ParseString(object1, "category"), ParseString(object1, "city")
                                        , ParseString(object1, "favorite_count"), ParseString(object1, "views_count"), ParseString(object1, "like_count")
                                        , ParseString(object1, "timestamp"), ParseString(object1, "fav_stattus"), ParseString(object1, "like_stattus")
                                        , ParseString(object1, "user_name"), ParseString(object1, "user_image"),
                                        ParseString(object1, "expired_date"), ParseString(object1, "offer_count"),
                                        ParseString(object1, "price_type"), ParseString(object1, "main_category"),
                                        ParseString(object1, "user_regiterdate"), ParseString(object1, "ads_count")
                                        , ParseString(object1, "follow_user"), ParseString(object1, "mobile_verify")
                                        , ParseString(object1, "hide_mobileno"), ParseString(object1, "shop_name")
                                        , ParseString(object1, "boost_type"), ParseString(object1, "category_id")));
                            }
                        }
                        similar_ads_adapter=new Similar_Ads_Adapter(getActivity(),adsArrayList);

                      rv.setAdapter(similar_ads_adapter);
                    }

                    if (adsArrayList.size()==0){
                        ll_similar_ads.setVisibility(View.GONE);
                    }

                }
                catch (Exception e){}
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                Common.Snakbar(Common.volleyerror(volleyError),ll_similar_ads);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                logg("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put("id",ads.getUser_id());
                params.put(User_id,preferences.getuserid()+"");
                 params.put("category_id",ads.getCategory_id());

                logg("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }


}
