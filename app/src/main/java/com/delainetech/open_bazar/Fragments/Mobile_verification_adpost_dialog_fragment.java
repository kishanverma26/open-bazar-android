package com.delainetech.open_bazar.Fragments;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.delainetech.open_bazar.Adpost_Add_Activity;
import com.delainetech.open_bazar.My_Coin_Activity;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.facebook.accountkit.ui.SkinManager;
import com.facebook.accountkit.ui.UIManager;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import static com.delainetech.open_bazar.Utils.Common.logg;


/**
 * Created by Rsss on 2/5/2018.
 */

public class Mobile_verification_adpost_dialog_fragment extends DialogFragment implements ParamKeys {


    View v;

    Button bt_home,bt_mycoins;
    Context mcontext;
    String mobilenum="";

    public Mobile_verification_adpost_dialog_fragment(String mobilenum) {
        this.mobilenum = mobilenum;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        v=inflater.inflate(R.layout.phone_no_verification_poup,container,false);
        mcontext=getActivity();
        initi();
        return v;
    }

    private void initi() {
        bt_home=(Button) v.findViewById(R.id.bt_home);
        bt_mycoins=(Button) v.findViewById(R.id.bt_mycoins);

        bt_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                onSMSLoginFlow();
            }
        });
        bt_mycoins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i=new Intent(mcontext, My_Coin_Activity.class);
                startActivity(i);

               getActivity().finish();
            }
        });


    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        Dialog dialog = super.onCreateDialog(savedInstanceState);
        WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.gravity = Gravity.CENTER;
        wlmp.dimAmount = 0.0F;
        dialog.getWindow().setAttributes(wlmp);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        return dialog;
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 1.00);
        int screenHeight = (int) (metrics.heightPixels * 1.00);

        getDialog().getWindow().setLayout(screenWidth, screenHeight);


    }


    public void onSMSLoginFlow() {
        String locale = getActivity().getResources().getConfiguration().locale.getCountry();
        logg("countrycode",locale+"");
        PhoneNumber number=new PhoneNumber(Country_CODE,mobilenum.replaceFirst("^0+(?!$)", ""));
        AccountKit.logOut();
        final Intent intent = new Intent(getActivity(), AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN); // or .ResponseType.TOKEN
        // ... perform additional configuration ...

        UIManager uiManager;
        uiManager = new SkinManager(
                SkinManager.Skin.CLASSIC,Color.parseColor("#E83957"));

        configurationBuilder.setUIManager(uiManager);
        configurationBuilder.setInitialPhoneNumber(number);
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        ( (Adpost_Add_Activity)getActivity()). startActivityForResult(intent, Mobile_RESULT_CODE);
    }



}
