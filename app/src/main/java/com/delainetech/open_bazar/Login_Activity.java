package com.delainetech.open_bazar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.delainetech.open_bazar.Custom_classes.DelayedProgressDialog;
import com.delainetech.open_bazar.Custom_classes.MySingleton;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static com.delainetech.open_bazar.Utils.Common.logg;

public class Login_Activity extends AppCompatActivity implements View.OnClickListener,ParamKeys {

    Context mcoxt;
    CardView cv_signin;
    TextView tv_register,tv_forgotpass,tv_signin;
    EditText et_pass,et_email;
    ImageView iv_pass_eye;
    UserSharedPreferences preferences,playerprefer;
    RelativeLayout rl_google,rl_facebook;

    //google signin
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 007;
    private GoogleSignInOptions gso;

    //    facebook
    CallbackManager callbackManager=null;
    String fb_name="",fb_email="",fb_id="",fb_image="";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        mcoxt=Login_Activity.this;
        initializ();
    }

    private void initializ() {
        preferences=UserSharedPreferences.getInstance(Login_Activity.this);
        playerprefer=new UserSharedPreferences(this,"");

        rl_facebook=findViewById(R.id.rl_facebook);
        rl_google=findViewById(R.id.rl_google);
        et_pass=findViewById(R.id.et_pass);
        et_email=findViewById(R.id.et_email);
        iv_pass_eye=findViewById(R.id.iv_pass_eye);
        iv_pass_eye.setOnClickListener(this);

        tv_signin=findViewById(R.id.tv_signin);
        tv_signin.setOnClickListener(this);
        tv_forgotpass=findViewById(R.id.tv_forgotpass);
        tv_forgotpass.setOnClickListener(this);
        tv_register=findViewById(R.id.tv_register);
        tv_register.setOnClickListener(this);
        cv_signin=findViewById(R.id.cv_signin);
        cv_signin.setOnClickListener(this);
        rl_google.setOnClickListener(this);
        rl_facebook.setOnClickListener(this);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;


        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_signin:
                Common.hideSoftKeyboard(Login_Activity.this);
                checkvalidation();

                break;

            case R.id.tv_register:
                startActivity(new Intent(mcoxt,Signup_Activity.class));
                break;

            case R.id.tv_forgotpass:
                startActivity(new Intent(mcoxt,Forgot_Password_Activity.class));
                break;
            case R.id.iv_pass_eye:
                Common.enableInputVisiblePassword(et_pass,iv_pass_eye);
                break;
            case R.id.rl_google:
                initialize_google();
                break;
            case R.id.rl_facebook:
                LoginManager.getInstance().logOut();
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile","email"));
                facebooklogin();
                break;
        }
    }


    public void   checkvalidation(){

        if (et_email.getText().toString().trim().matches(emailPattern)){

            if (et_pass.getText().toString().length()>5){
                login(mcoxt);
            }else {
                Common.Snakbar("Please enter 6 digit password", et_email);
            }

        }else {
            Common.Snakbar("Please enter valid email id",et_email);
        }
    }
    public void  login(final Context context){

        final DelayedProgressDialog pro=new DelayedProgressDialog();

        pro.show(getSupportFragmentManager(),"");


        StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "login", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                pro.cancel();
                logg("signupres",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){
                        preferences.setuserid(Common.ParseString(object.getJSONObject("data"),Id));
                        preferences.setname(Common.ParseString(object.getJSONObject("data"),Name));
                        preferences.setemail(Common.ParseString(object.getJSONObject("data"),Email));
                        preferences.setmobile(Common.ParseString(object.getJSONObject("data"),Phone_No));
                        preferences.setmobileverification(Boolean.parseBoolean(Common.ParseString(object.getJSONObject("data"),"verify_phone")));
                        preferences.setimage(Common.ParseString(object.getJSONObject("data"),Profile_image));
                        preferences.setaddress(Common.ParseString(object.getJSONObject("data"),Addresss));
                        preferences.setcreated_at(Common.ParseString(object.getJSONObject("data"),"created_at"));
                        preferences.setaddress(Common.ParseString(object.getJSONObject("data"),"address"));
                        preferences.setlogin(true);
                        preferences.settoken(Common.ParseString(object.getJSONObject("data"),Token));
                        preferences.setgoogle_verification(Boolean.parseBoolean(Common.ParseString(object.getJSONObject("data"),"google_verification")));
                        preferences.setfacebook_verification(Boolean.parseBoolean(Common.ParseString(object.getJSONObject("data"),"facebook_verification")));

                        startActivity(new Intent(Login_Activity.this,Home_Screen_Activity.class));
                        finish();
                    }else if (object.getString("message").equals("invalid email")){
                        Common.Snakbar("Email id not registered",et_email);
                    }else if (object.getString("message").equals("invalid password")){
                        Common.Snakbar("Password is incorrect",et_email);
                    }else if (object.getString("status").equals("fail") && object.getString("message").equals("You are blocked by admin. Please contact admin.")){
                        Common.Snakbar("You are blocked by Open-Bazar.Please contact Open-Bazar support",iv_pass_eye);

                    }

                }
                catch (Exception e){
                    pro.cancel();
                    Log.e("error","e",e);
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),et_email);
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(Email,et_email.getText().toString().trim());
                params.put(Password,et_pass.getText().toString());
                params.put(Player_id,playerprefer.getplayerid());
                params.put("device_id",playerprefer.getdeviceid());
                params.put(App_Type,"Android");

                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(context).addToRequestQueue(stringRequest);


    }


    private void initialize_google(){
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(Login_Activity.this,0,
                        new GoogleApiClient.OnConnectionFailedListener() {
                            @Override
                            public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                                logg("connectionfail",connectionResult+"");
                            }
                        } /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        //google signin
        Intent signInIntent =
                Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);

    }

    private void handlegoogleSignInResult(GoogleSignInResult result)
    {
        logg("googlesignin",result+"");

        if (result.isSuccess())
        {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            logg( "acct:",  acct+"");
            logg("display name: ","" + acct.getDisplayName());

            String personName = acct.getDisplayName();
            String personPhotoUrl="";
            if (acct.getPhotoUrl()!=null) {
                personPhotoUrl = acct.getPhotoUrl().toString();
            }
            String email = acct.getEmail();


            logg( "Name: ","" + personName + ", email: " + email
                    + ", Image: " + personPhotoUrl+ ", token: " + acct.getIdToken()+
                    ", Account:"+acct.getAccount());
            try {
                JSONObject jsonObject=new JSONObject();
                jsonObject.put("name",personName);
                jsonObject.put("email",email);
                jsonObject.put("image",personPhotoUrl);
                social_login(this,"google",personName,email,personPhotoUrl,jsonObject.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else {
            // Signed out, show unauthenticated UI.
            logg("resultfalse",result.getStatus()+"");
        }
    }

//   facebook login

    private void facebooklogin(){
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.e("Success", loginResult+"");
                        getfacebookUserDetails(loginResult);
                        Log.e("onSuccess","onSuccess");
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(mcoxt, "Login Cancel", Toast.LENGTH_LONG).show();
                        Log.e("oncanel","oncancel");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(mcoxt,"Somthing wents wrong", Toast.LENGTH_LONG).show();
                        Log.e("error","e",exception);
                        Log.e("onError","onError");

                    }
                });
    }
    private void getfacebookUserDetails(LoginResult loginResult) {

        GraphRequest data_request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject json_object,
                                            GraphResponse response) {

                        Log.e("JSONObject",json_object+"");
                        try {
                            JSONObject obj=new JSONObject(json_object.toString());
                            fb_id=     obj.getString("id");
                            fb_name=      obj.getString("name");
                            fb_email=Common.ParseString(obj,Email);

                            fb_image=obj.getJSONObject("picture").getJSONObject("data").getString("url");
                            social_login(Login_Activity.this,"facebook",fb_name,fb_email,fb_image,json_object.toString());

                        } catch (JSONException e)
                        {
                            Log.e( "e ",e+"" );
                            e.printStackTrace();
                        }

                        Log.e("GraphResponse",response+"");
//                        Intent intent = new Intent(Main_class.this,
//                                UserProfile.class);
//                        intent.putExtra("userProfile", json_object.toString());
//                        startActivity(intent);
                    }

                });
        Bundle permission_param = new Bundle();
        permission_param.putString("fields", "id,name,email,picture.width(120).height(120)");
        data_request.setParameters(permission_param);
        data_request.executeAsync();

    }


    public void  social_login(final Context context, final String type, final String name, final String email,
                              final String image,final  String social_details){

        final DelayedProgressDialog pro=new DelayedProgressDialog();

        pro.show(getSupportFragmentManager(),"");


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "sociallogin", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                pro.cancel();
                logg("signupres",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){

                        preferences.setuserid(Common.ParseString(object.getJSONObject("data"),Id));
                        preferences.setname(Common.ParseString(object.getJSONObject("data"),Name));
                        preferences.setemail(Common.ParseString(object.getJSONObject("data"),Email));
                        preferences.setmobile(Common.ParseString(object.getJSONObject("data"),Phone_No));
                        preferences.setmobileverification(Boolean.parseBoolean(Common.ParseString(object.getJSONObject("data"),"verify_phone")));
                        preferences.setimage(Common.ParseString(object.getJSONObject("data"),Profile_image));
                        preferences.setaddress(Common.ParseString(object.getJSONObject("data"),Addresss));
                        preferences.setcreated_at(Common.ParseString(object.getJSONObject("data"),"created_at"));
                        preferences.setaddress(Common.ParseString(object.getJSONObject("data"),"address"));
                        preferences.setlogin(true);
                        preferences.settoken(Common.ParseString(object.getJSONObject("data"),Token));
                        preferences.setgoogle_verification(Boolean.parseBoolean(Common.ParseString(object.getJSONObject("data"),"google_verification")));
                        preferences.setfacebook_verification(Boolean.parseBoolean(Common.ParseString(object.getJSONObject("data"),"facebook_verification")));

                        startActivity(new Intent(Login_Activity.this,Home_Screen_Activity.class));
                        finish();
                    }else if (object.getString("status").equals("fail") && object.getString("message").equals("You are blocked by admin. Please contact admin.")){
                        Common.Snakbar("You are blocked by Open-Bazar.Please contact Open-Bazar support",iv_pass_eye);

                    }

                }
                catch (Exception e){
                    pro.cancel();
                    Log.e("error","e",e);
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),et_email);
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                if (type.equals("google")){
                    params.put(Signup_type,"google");
                }else {
                    params.put(Signup_type,"facebook");
                    params.put(SocialToken,fb_id);

                }
                params.put(Social_Details,social_details);
                params.put(Name,name);
                params.put(Email,email);
                params.put(App_Type,"Android");
                params.put("reffer_id",preferences.getrefferuserid());
                params.put(Profile_image,image);
                params.put(Player_id,playerprefer.getplayerid());
                params.put("device_id",playerprefer.getdeviceid());


                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(context).addToRequestQueue(stringRequest);


    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (callbackManager!=null){
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handlegoogleSignInResult(result);
        }

    }


    @Override
    public void onPause() {
        super.onPause();
        if (mGoogleApiClient!=null) {
            mGoogleApiClient.stopAutoManage(Login_Activity.this);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage(Login_Activity.this);
            mGoogleApiClient.disconnect();
        }
    }

}
