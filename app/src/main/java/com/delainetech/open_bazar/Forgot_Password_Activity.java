package com.delainetech.open_bazar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.StringRequest;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.delainetech.open_bazar.Custom_classes.DelayedProgressDialog;
import com.delainetech.open_bazar.Custom_classes.MySingleton;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.delainetech.open_bazar.Utils.Common.logg;

public class Forgot_Password_Activity extends AppCompatActivity implements View.OnClickListener ,ParamKeys {

    Context mcoxt;
    ImageView iv_back;
    EditText et_email;
    TextView tv_submit;
    UserSharedPreferences preferences;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password);
        mcoxt=Forgot_Password_Activity.this;
        initializ();
    }

    private void initializ() {
        preferences=UserSharedPreferences.getInstance(this);
        tv_submit=findViewById(R.id.tv_submit);
        et_email=findViewById(R.id.et_email);
        iv_back=findViewById(R.id.iv_back);
        iv_back.setOnClickListener(this);
        tv_submit.setOnClickListener(this);



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_back:
                   finish();
                break;
            case R.id.tv_submit:

                checkvalidation();
                break;

        }
    }

    public void   checkvalidation(){
        Common.hideSoftKeyboard(Forgot_Password_Activity.this);
        if (et_email.getText().toString().trim().matches(emailPattern)){


            send_otp(mcoxt);
        }else {
            Common.Snakbar("Please enter valid email id",et_email);
        }


    }

    public void  send_otp(final Context context){

        final DelayedProgressDialog pro=new DelayedProgressDialog();

        pro.show(getSupportFragmentManager(),"");


        StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "emailotp", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                pro.cancel();
                logg("signupres",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){
                        preferences.setemail(et_email.getText().toString());
                        Intent i=new Intent(mcoxt,Otp_Verification_Activity.class);
                        i.putExtra(Type,"other");
                        i.putExtra(OTP,object.getString("otp"));
                        startActivity(i);
                        finish();
                    }else if (object.getString("message").equals("User not found")){
                        Common.Snakbar("Email id not registered",et_email);
                    }

                }
                catch (Exception e){
                    pro.cancel();
                    Log.e("error","e",e);
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),et_email);
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(Email,et_email.getText().toString().trim());
                params.put(Type,"other");

                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(context).addToRequestQueue(stringRequest);


    }



}
