package com.delainetech.open_bazar;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.delainetech.open_bazar.Adapter.Location_picker_adapter;
import com.delainetech.open_bazar.Custom_classes.DelayedProgressDialog;
import com.delainetech.open_bazar.Custom_classes.MySingleton;
import com.delainetech.open_bazar.Models.Location_Picker;
import com.delainetech.open_bazar.SQL.MyDatabaseHelper;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.delainetech.open_bazar.Utils.Common.logg;

public class Location_picker_Activity extends AppCompatActivity implements ParamKeys, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, View.OnClickListener, LocationListener {

    RecyclerView recyclerview;

    LinearLayoutManager linearLayoutManager;
    Context mcontext;
    Toolbar toolbar;
    Location_picker_adapter adapter;
    ArrayList<Location_Picker> location_pickerArrayList;
    MyDatabaseHelper db;
    EditText et_search;
    UserSharedPreferences preferences;
    TextView tv_currentlocation;
    String city_str="",City_name="";
    boolean ask_gps=true;
    ProgressDialog progressDialog;
    boolean filter=false;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location_picker_activity);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        mcontext = Location_picker_Activity.this;
        initializ();
    }

    private void initializ() {
        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Please wait getting your current location");
        preferences = UserSharedPreferences.getInstance(this);
        db = new MyDatabaseHelper(this);
        try {
            db.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();

        }

        location_pickerArrayList = new ArrayList<Location_Picker>();
        et_search = (EditText) findViewById(R.id.et_search);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));

        tv_currentlocation = (TextView) findViewById(R.id.tv_currentlocation);
        tv_currentlocation.setOnClickListener(this);
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        linearLayoutManager = new LinearLayoutManager(mcontext);
        recyclerview.setLayoutManager(linearLayoutManager);
        location_pickerArrayList = db.GetCity();
        if (getIntent().hasExtra("filter")){
            filter=true;
        }

        location_pickerArrayList.add(0,new Location_Picker("All Cities",false));
        logg("location2",preferences.getaddress()+"");
//        if (!filter)
//        {
        if (preferences.getaddress().length() > 0) {
            for (int i = 0; i < location_pickerArrayList.size(); i++) {
                if (location_pickerArrayList.get(i).getTital().equals(preferences.getaddress())) {
                    location_pickerArrayList.get(i).setSelect(true);
                    logg("location3",location_pickerArrayList.get(i).getTital()+"");
                    logg("location4",preferences.getaddress()+"");

                } else {
                    location_pickerArrayList.get(i).setSelect(false);
                }
            }
        }
//        }


        adapter = new Location_picker_adapter(this, location_pickerArrayList);
        recyclerview.setAdapter(adapter);

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private GoogleApiClient googleApiClient;

    private void location_picker() {
        buildGoogleApiClient();
        createConnectionsLocation();
        check_Runtime_Permissions();
    }

    //get current location enable api client
    private void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(mcontext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    LocationRequest mLocationRequest;

    private void createConnectionsLocation() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(10000 / 2);
    }

    private void check_Runtime_Permissions() {
        if (Common.hasPermissions(mcontext, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                android.Manifest.permission.ACCESS_COARSE_LOCATION})) {
            checkGPSstatus();
        } else {
            Common.askpermission(Location_picker_Activity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION});
        }
    }

    private void checkGPSstatus() {
        logg("gpsstatus", checkGPSStatus(this) + "");
        if (!(checkGPSStatus(this))) {

            if (ask_gps) {
                ask_gps=false;
                buildAlertMessageNoGps();
            }
        } else if (googleApiClient.isConnected()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    Activity#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for Activity#requestPermissions for more details.
                    return;
                }
            }
            progressDialog.show();
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, mLocationRequest, this);

        }
    }

    public static boolean checkGPSStatus(Context mContext) {
        LocationManager locationManager = null;
        boolean gps_enabled = true;

        if (locationManager == null) {
            locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        }

        if ((locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)))
            gps_enabled = true;
        else
            gps_enabled = false;
        logg("gps_enabled", ":2:" + gps_enabled);
        return gps_enabled;
    }


    Location mLocation;

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    Activity#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                return;
            }
        }
        mLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        logg("connected","connected");
        checkGPSstatus();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    public void onClick(View v) {
        location_picker();
    }

    @Override
    public void onLocationChanged(Location location) {
        progressDialog.dismiss();
        logg("location",location.getLatitude()+","+location.getLongitude());
//        logg("locationcity",getLocationName(52.3724479675293,5.265812873840332));
        city_str=getLocationName(location.getLatitude(),location.getLongitude());

//        city_str=getLocationName(52.3724479675293,5.265812873840332);
//        get_location(this,(location.getLatitude()+","+location.getLongitude())+"");
//        Toast.makeText(mcontext, city_str+"", Toast.LENGTH_SHORT).show();
        for (int i = 0; i < location_pickerArrayList.size(); i++) {
            if (city_str.contains(location_pickerArrayList.get(i).getTital())) {
                location_pickerArrayList.get(i).setSelect(true);
                City_name=location_pickerArrayList.get(i).getTital();
                preferences.setaddress(location_pickerArrayList.get(i).getTital());
                logg("poss",i+"");
                logg("location1",location_pickerArrayList.get(i).getTital()+"");

            } else {
                    logg("poss1",i+"");
                location_pickerArrayList.get(i).setSelect(false);
            }

        }

        update_location(this,City_name);
        adapter.notifyDataSetChanged();
        Intent i=new Intent();
        i.putExtra("city",City_name);
        setResult(Activity.RESULT_OK,i);
        logg("test111","testtest");
        finish();
        stopLocationUpdates();

        progressDialog.dismiss();
    }


    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        ask_gps=true;
                        startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS),10);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        ask_gps=true;
                        Toast.makeText(Location_picker_Activity.this, "GPS need to be enable", Toast.LENGTH_SHORT).show();

                        dialog.cancel();
                    }
                })
                .setNeutralButton("Exit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
        ;
        final AlertDialog alert = builder.create();
        alert.show();
    }


    public String getLocationName(double lattitude, double longitude) {

        String cityName = "Not Found";
        Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());
        try {

            List<Address> addresses = gcd.getFromLocation(lattitude, longitude,
                    10);

            for (Address adrs : addresses) {
                if (adrs != null) {

                    String city = adrs.getLocality();
                    if (city != null && !city.equals("")) {
                        cityName = city;
                        System.out.println("city ::  " + cityName);
                        logg("local",adrs.getAddressLine(0)+"--");
                        logg("local1",adrs.getSubLocality()+"--");
                        logg("local2",adrs.getMaxAddressLineIndex()+"--");


                    } else {

                    }
                    // // you should also try with addresses.get(0).toSring();

                }

                logg("local",adrs.getAddressLine(0)+"--");
                logg("local1",adrs.getSubLocality()+"--");
                logg("local2",adrs.getMaxAddressLineIndex()+"--");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return cityName;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        logg("resultCode=",requestCode+", resultCode="+resultCode);
    }

    @Override
    public void onDestroy() {
        // super.onDestroy();
        if (googleApiClient!=null) {
            if (googleApiClient.isConnected()) {
                googleApiClient.disconnect();

            }
        }


        if (progressDialog!=null){
            progressDialog.dismiss();
        }
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (progressDialog!=null){
            progressDialog.dismiss();
        }
    }

    public void  get_location(final Context context, String search_query){

        final DelayedProgressDialog pro=new DelayedProgressDialog();

        pro.show(getSupportFragmentManager(),"");


        logg("urll","https://maps.googleapis.com/maps/api/geocode/json?latlng=" + search_query + "&sensor=true&key=AIzaSyCX-85JYgWL7ferhL-7LQ7DfYilTnpwhG0");
      String url="https://maps.googleapis.com/maps/api/geocode/json?latlng=" + search_query + "&sensor=true&key=AIzaSyCX-85JYgWL7ferhL-7LQ7DfYilTnpwhG0";
        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                pro.cancel();
                logg("query",response);

            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),recyclerview);
            }
        });

        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(context).addToRequestQueue(stringRequest);

    }


    //Stop Location Update Method
    protected void stopLocationUpdates() {
        try {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    googleApiClient, (LocationListener) this);
        }
        catch  (Exception ex)
        {}
    }



    public void  update_location(final Context context, final String location){



        StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl+"update_city", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                logg("query",response);

            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),recyclerview);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization", "Bearer " + preferences.gettoken());
                Log.e("header", header + "");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(Id, preferences.getuserid());
                params.put(City, location);


                Log.e("params", params + "");
                return params;
            }
        };

        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(context).addToRequestQueue(stringRequest);

    }





}
