package com.delainetech.open_bazar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.delainetech.open_bazar.Custom_classes.DelayedProgressDialog;
import com.delainetech.open_bazar.Custom_classes.MySingleton;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import static com.delainetech.open_bazar.Utils.Common.logg;

public class Signup_Activity extends AppCompatActivity implements View.OnClickListener ,ParamKeys {

    Context mcoxt;
    CardView cv_signin;
    CheckBox ckeck_term;
    TextView tv_term,tv_alreadyhaveacc,tv_login,tv_mobile;
    ImageView iv_newpass_eye,iv_confirmpass_eye;
    EditText et_newpass,et_confirmpass,et_fullname,et_email,et_mobile;
    UserSharedPreferences preferences,playerprefer;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_activity);
        mcoxt=Signup_Activity.this;
        initializ();
    }

    private void initializ() {
        preferences=UserSharedPreferences.getInstance(this);
        playerprefer=new UserSharedPreferences(this,"");
        ckeck_term=findViewById(R.id.ckeck_term);
        et_mobile=findViewById(R.id.et_mobile);
        et_fullname=findViewById(R.id.et_fullname);
        et_email=findViewById(R.id.et_email);
        cv_signin=findViewById(R.id.cv_signin);
        cv_signin.setOnClickListener(this);
        tv_mobile=findViewById(R.id.tv_mobile);
        tv_mobile.setOnClickListener(this);
        tv_alreadyhaveacc=findViewById(R.id.tv_alreadyhaveacc);
        tv_alreadyhaveacc.setOnClickListener(this);
        tv_login=findViewById(R.id.tv_login);
        tv_login.setOnClickListener(this);
        tv_term=(TextView)findViewById(R.id.tv_term);

        et_newpass=findViewById(R.id.et_newpass);
        et_confirmpass=findViewById(R.id.et_confirmpass);

        iv_newpass_eye=findViewById(R.id.iv_newpass_eye);
        iv_newpass_eye.setOnClickListener(this);
        iv_confirmpass_eye=findViewById(R.id.iv_confirmpass_eye);
        iv_confirmpass_eye.setOnClickListener(this);

        SpannableString ss = new SpannableString("I have read and agree with the Privacy Policy &  Terms and Conditions");
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                String url =  weburl+"privacy-policy";
                String title="Privacy Policy";
                Intent intent=new Intent(mcoxt,WebView_Activity.class);
                intent.putExtra("text",title);
                intent.putExtra("url",url);
                startActivity(intent);
            }
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(true);
            }
        };
        ClickableSpan privacy = new ClickableSpan() {
            @Override
            public void onClick(View textView) {

                String url1 = weburl+"terms-of-use";
                String title1="Terms Conditions";
                Intent intent1=new Intent(mcoxt,WebView_Activity.class);
                intent1.putExtra("text",title1);
                intent1.putExtra("url",url1);
                startActivity(intent1);
            }
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(true);
            }
        };

        ss.setSpan(clickableSpan, 31, 46, 0);
        ss.setSpan(privacy, 49, 69, 0);
        //49 69

        tv_term.setMovementMethod(LinkMovementMethod.getInstance());
        tv_term.setText(ss, TextView.BufferType.SPANNABLE);
        tv_term.setSelected(true);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_mobile:

                checkvalidation();

                break;
            case R.id.tv_alreadyhaveacc:
                finish();
                break;
            case R.id.tv_login:
                finish();
                break;
            case R.id.iv_newpass_eye:
//                    enableInputVisiblePassword(et_oldpass,iv_oldpass_eye);

                Common.enableInputVisiblePassword(et_newpass,iv_newpass_eye);
                break;
            case R.id.iv_confirmpass_eye:
//                    enableInputVisiblePassword(et_oldpass,iv_oldpass_eye);

                Common.enableInputVisiblePassword(et_confirmpass,iv_confirmpass_eye);
                break;

        }
    }



    public void   checkvalidation(){
        Common.hideSoftKeyboard(Signup_Activity.this);
        if (et_fullname.getText().toString().trim().length()>0){
            if (et_email.getText().toString().trim().matches(emailPattern)){

                if (et_mobile.getText().toString().trim().length()>9){
                    if (et_newpass.getText().toString().length()>5){
                        if (et_newpass.getText().toString().equals(et_confirmpass.getText().toString())){
                            if (ckeck_term.isChecked()) {
                                send_otp(mcoxt);
                            }else {
                                Common.Snakbar("Before regiter you have to agree with our Privacy Policy & Terms and conditions", et_fullname);

                            }
                        }else {
                            Common.Snakbar("New password and confirm password does not match", et_fullname);

                        }
                    }else {
                        Common.Snakbar("Please enter minimum 6 digit password", et_fullname);

                    }

                }else {
                    Common.Snakbar("Please enter valid mobile number", et_fullname);

                }

            }else {
                Common.Snakbar("Please enter valid email id",et_fullname);
            }
        }else {
            Common.Snakbar("Please enter your full name",et_fullname);
        }

    }

    public void  send_otp(final Context context){

        final DelayedProgressDialog pro=new DelayedProgressDialog();

        pro.show(getSupportFragmentManager(),"");


        StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "emailotp", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                pro.cancel();
                logg("signupres",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){
                        preferences.setname(et_fullname.getText().toString().trim());
                        preferences.setemail(et_email.getText().toString().trim());
                        preferences.setmobile(et_mobile.getText().toString().trim());
//                        preferences.setmobileverification(Boolean.parseBoolean(Common.ParseString(object.getJSONObject("data"),"verify_phone")));
                        preferences.setaudiorate(et_newpass.getText().toString());
                        Intent i=new Intent(mcoxt,Otp_Verification_Activity.class);
                        i.putExtra(Type,"signup");
                        i.putExtra(OTP,object.getString("otp"));
                        startActivity(i);
                    }else if (object.getString("message").equals("User already exist")){
                        Common.Snakbar("Email id already registered",et_confirmpass);
                    }

                }
                catch (Exception e){
                    pro.cancel();
                    Log.e("error","e",e);
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),et_confirmpass);
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(Email,et_email.getText().toString().trim());
                params.put(Type,"signup");

                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(context).addToRequestQueue(stringRequest);


    }

    public void  signup(final Context context){

        final DelayedProgressDialog pro=new DelayedProgressDialog();

        pro.show(getSupportFragmentManager(),"");


        StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "signup", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                pro.cancel();
                logg("signupres",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){
                        preferences.setname(et_fullname.getText().toString().trim());
                        preferences.setemail(et_email.getText().toString().trim());
                        preferences.setmobile(et_mobile.getText().toString().trim());
                        preferences.setaudiorate(et_newpass.getText().toString());
                        preferences.setmobileverification(Boolean.parseBoolean(Common.ParseString(object.getJSONObject("data"),"verify_phone")));
                        preferences.setuserid(Common.ParseString(object.getJSONObject("data"),Id));
                        preferences.setimage(Common.ParseString(object.getJSONObject("data"),Profile_image));
                        preferences.setaddress(Common.ParseString(object.getJSONObject("data"),Addresss));
                        preferences.settoken(Common.ParseString(object,ParamKeys.Token));
                        preferences.setcreated_at(Common.ParseString(object.getJSONObject("data"),"created_at"));
                        preferences.setlogin(true);
                        Intent i=new Intent(mcoxt,Home_Screen_Activity.class);
                        startActivity(i);
                        finish();
                    }else if (object.getString("message").equals("Email already Exists")){
                        Common.Snakbar("Email id already exist",et_email);
                    }

                }
                catch (Exception e){
                    pro.cancel();
                    Log.e("error","e",e);
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {


                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),et_email);
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(Email,et_email.getText().toString().trim());
                params.put(Name,et_fullname.getText().toString().trim());
                params.put(Signup_type,"Email");
                params.put(App_Type,"Android");
                params.put("reffer_id",preferences.getrefferuserid());
                params.put(Phone_No,et_mobile.getText().toString());
                params.put(Player_id,playerprefer.getplayerid());
                params.put("device_id",playerprefer.getdeviceid());
                params.put(Password,et_newpass.getText().toString());


                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(context).addToRequestQueue(stringRequest);


    }


}
