package com.delainetech.open_bazar;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.SimpleMultiPartRequest;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.delainetech.open_bazar.Custom_classes.DelayedProgressDialog;
import com.delainetech.open_bazar.Custom_classes.MySingleton;
import com.delainetech.open_bazar.Custom_classes.VolleyMultipartRequest;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.facebook.accountkit.ui.SkinManager;
import com.facebook.accountkit.ui.UIManager;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.delainetech.open_bazar.Utils.Common.logg;

public class Profile_update_Activity extends AppCompatActivity implements View.OnClickListener, ParamKeys , CompoundButton.OnCheckedChangeListener {

    ImageView ivback,ivcheck;
    UserSharedPreferences prefrences;
    Context mcoxt;
    EditText etname,etemail;
    TextView changeprofile,tv_mobile_verify,tv_change_pass;
    CircleImageView profilePic;
    ProgressDialog pd;

    //volley
    Uri outPutfileUri=null;
    byte[] inputData=null;
    String destPath="";
    String type="";
    RequestOptions requestOptions=new RequestOptions().centerCrop().placeholder(R.drawable.userimage);

    Switch google_switch,facebook_switch;
    EditText et_mobileno;

    //    facebook
    CallbackManager callbackManager=null;
    String fb_name="",fb_email="",fb_id="",fb_image="";
    Intent i;

    ImageView im_mobile_verify;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile_activity);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        mcoxt=Profile_update_Activity.this;
        initi();

    }

    private void initi() {
        prefrences=new UserSharedPreferences(mcoxt);
        et_mobileno=(EditText) findViewById(R.id.et_mobileno);
        et_mobileno.setOnClickListener(this);
        google_switch=(Switch) findViewById(R.id.google_switch);
        facebook_switch=(Switch) findViewById(R.id.facebook_switch);
        profilePic=(CircleImageView) findViewById(R.id.profilePic);
        tv_mobile_verify=(TextView) findViewById(R.id.tv_mobile_verify);
        tv_change_pass=(TextView) findViewById(R.id.tv_change_pass);
        tv_change_pass.setOnClickListener(this);
        changeprofile=(TextView) findViewById(R.id.changeprofile);
        changeprofile.setOnClickListener(this);
        ivback=(ImageView)findViewById(R.id.ivback);
        ivback.setOnClickListener(this);
        im_mobile_verify=(ImageView)findViewById(R.id.im_mobile_verify);
        ivcheck=(ImageView)findViewById(R.id.ivcheck);
        ivcheck.setOnClickListener(this);
        etname=(EditText) findViewById(R.id.etname);
        etemail=(EditText)findViewById(R.id.etemail);
        pd=new ProgressDialog(mcoxt);
        setup_ui_data();
        google_switch.setOnCheckedChangeListener(this);
        facebook_switch.setOnCheckedChangeListener(this);

        profile_detail(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.et_mobileno:
                onSMSLoginFlow();
                break;
            case R.id.ivback:
                finish();
                break;
            case R.id.tv_change_pass:
                i=new Intent(mcoxt,ResetPassword_Activity.class);
                i.putExtra("type","change");

                startActivity(i);
                break;

         case R.id.ivcheck:
                update_profilecheck();
                break;
   case R.id.changeprofile:
       CropImage.activity()
               .setGuidelines(CropImageView.Guidelines.ON).setFixAspectRatio(true)
               .start(this);
                break;



        }
    }

    private void update_profilecheck() {
        if (etname.length()>0 && etemail.length()>0 ) {
            JSONObject jsonObject=new JSONObject();

            update_profile();
        }else {
            Toast.makeText(mcoxt, "Please fill all fields", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent result) {
        super.onActivityResult(requestCode, resultCode, result);
        Log.e("requestCode",requestCode+"");
        Log.e("resultCode",resultCode+"");

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult results = CropImage.getActivityResult(result);
            if (resultCode == RESULT_OK) {
                outPutfileUri = results.getUri();
                profilePic.setImageURI(outPutfileUri);

                String outputdir= Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
                destPath = outputdir  + File.separator;
                if ((null == result) || (result.getData() == null)){
                   //  new ImageCompressionAsyncTask(this).execute(outPutfileUri.toString(),destPath);
                     inputData=set_Image(outPutfileUri);
                }
                else {
                 //   new ImageCompressionAsyncTask(this).execute(result.toString(),destPath);
                    inputData=set_Image(result.getData());
                }



                Log.e("arraybit",inputData+"");
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = results.getError();
            }
        }
        if (callbackManager!=null){
            callbackManager.onActivityResult(requestCode, resultCode, result);
        }
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result1 = Auth.GoogleSignInApi.getSignInResultFromIntent(result);
            handlegoogleSignInResult(result1);
        }

        if (requestCode == Mobile_RESULT_CODE && resultCode==RESULT_OK) { // confirm that this response matches your request
            AccountKitLoginResult loginResult = result.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
            String toastMessage;
            if (loginResult.getError() != null) {
                toastMessage = loginResult.getError().getErrorType().getMessage();
                Toast.makeText(this, toastMessage, Toast.LENGTH_SHORT).show();
            } else if (loginResult.wasCancelled()) {
                toastMessage = "Login Cancelled";
                facebook_switch.setChecked(false);
            } else {
                getaccount_info();
            }
        }else if (requestCode == Mobile_RESULT_CODE && resultCode==RESULT_CANCELED){
            finish();
            Toast.makeText(this, "Your Mobile number is not verified", Toast.LENGTH_LONG).show();
        }

    }

    private void getaccount_info() {
        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(Account account) {
                Log.e("mobileno",account.getPhoneNumber()+"");

                update_mobilenumber(account.getPhoneNumber().toString());
            }

            @Override
            public void onError(AccountKitError accountKitError) {

            }
        });
    }




    private byte[]  set_Image(Uri ImageUri)
    {
        byte[] b = new byte[0];
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver() , ImageUri );

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
            Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
            scaled.compress(Bitmap.CompressFormat.JPEG, 100, baos);
          b = baos.toByteArray();
            Log.e("byteArray",b+"");

        }

        catch (Exception e) {
            Log.e("e","e",e);
            e.printStackTrace();
        }
        return b;
    }




    private void update_profile(){

        SimpleMultiPartRequest request = new SimpleMultiPartRequest(Request.Method.POST, baseurl+"update_profile", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                logg("response",response+"");
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("message").equals("Successfully Updated")){
                        JSONObject userdata=object.getJSONObject("data");

                        if (!userdata.isNull("email")) {
                            prefrences.setemail(userdata.getString("email"));
                        }
                        if (!userdata.isNull("name")) {
                            prefrences.setname(userdata.getString("name"));
                        }

                        if (!userdata.isNull("profile_image")) {
                            prefrences.setimage(userdata.getString("profile_image"));
                        }


                        Toast.makeText(mcoxt, "Profile Updated Successfully", Toast.LENGTH_SHORT).show();
                        finish();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Common.volleyerror(error);

            }
        })  ;
        if (outPutfileUri!=null) {
            request.addFile("profile_image", outPutfileUri.toString().replace("file:", "") + "");
        }
        request.addStringParam(Id,prefrences.getuserid());
        request.addStringParam(Name,etname.getText().toString().trim());
        request.addStringParam("verify_google",google_switch.isChecked()+"");
        request.addStringParam("verify_facebook",facebook_switch.isChecked()+"");
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + prefrences.gettoken());
        request.setHeaders(headers);


        logg("params",request.getFilesToUpload()+"");
        logg("params1",request.getBodyContentType()+"");

        request.setOnProgressListener(new Response.ProgressListener() {
            @Override
            public void onProgress(long transferredBytes, long totalSize) {
                int percentage = (int) ((transferredBytes / ((float) totalSize)) * 100);
                logg("percentage",percentage+"");
//                progress.setProgress(percentage);
            }
        });
        RequestQueue mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        mRequestQueue.add(request);


    }

//    class ImageCompressionAsyncTask extends AsyncTask<String, Void, String> {
//
//        Context mContext;
//
//        public ImageCompressionAsyncTask(Context context){
//            mContext = context;
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//
//            String filePath = SiliCompressor.with(mContext).compress(params[0], new File(params[1]));
//            return filePath;
//
//
//            /*
//            Bitmap compressBitMap = null;
//            try {
//                compressBitMap = SiliCompressor.with(mContext).getCompressBitmap(params[0], true);
//                return compressBitMap;
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            return compressBitMap;
//            */
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            /*
//            if (null != s){
//                imageView.setImageBitmap(s);
//                int compressHieght = s.getHeight();
//                int compressWidth = s.getWidth();
//                float length = s.getByteCount() / 1024f; // Size in KB;
//                String text = String.format("Name: %s\nSize: %fKB\nWidth: %d\nHeight: %d", "ff", length, compressWidth, compressHieght);
//                picDescription.setVisibility(View.VISIBLE);
//                picDescription.setText(text);
//            }
//            */
//            try {
//
//                Log.e("pathname",s);
//                File imageFile = new File(s);
//                outPutfileUri = Uri.fromFile(imageFile);
//
//                InputStream iStream =   getContentResolver().openInputStream(  outPutfileUri);
//
//                inputData = getBytes(iStream);
//                Log.e("arraybit",inputData+"");
//
//
//
//                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), outPutfileUri);
//                profilePic.setImageBitmap(bitmap);
//
//                String name = imageFile.getName();
//                float length = imageFile.length() / 1024f; // Size in KB
//                int compressWidth = bitmap.getWidth();
//                int compressHieght = bitmap.getHeight();
//                String text = String.format(Locale.US, "Name: %s\nSize: %fKB\nWidth: %d\nHeight: %d", name, length, compressWidth, compressHieght);
//
//            }
//            catch (IOException e) {
//                e.printStackTrace();
//                Log.e("error","e",e);
//            }
//
//        }
//    }


    @Override
    public void onBackPressed() {
      if (etname.getText().toString().equals(prefrences.getname())){
          finish();
      }else {
          alert();
      }
    }


    public void alert(){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mcoxt);

        alertDialog.setMessage("Do you want to save changes");
        alertDialog.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                finish();
            }
        });
        alertDialog.setNegativeButton(getResources().getString(R.string.No), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                finish();
            }
        });
        AlertDialog alertDialog1= alertDialog.create();
        alertDialog1.show();

        alertDialog1.getButton(alertDialog1.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorAccent));
        alertDialog1.getButton(alertDialog1.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorAccent));
    }



    public void  profile_detail(final Context context){

        final DelayedProgressDialog pro=new DelayedProgressDialog();

        pro.show(getSupportFragmentManager(),"");


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "user_detail", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                pro.cancel();
                logg("signupres",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){

                        prefrences.setaddress(Common.ParseString(object.getJSONObject("data"),"address"));
                        prefrences.setname(Common.ParseString(object.getJSONObject("data"),Name));
                        prefrences.setemail(Common.ParseString(object.getJSONObject("data"),Email));
                        prefrences.setmobile(Common.ParseString(object.getJSONObject("data"),Phone_No));
                        prefrences.setmobileverification(Boolean.parseBoolean(Common.ParseString(object.getJSONObject("data"),"verify_phone")));
                        prefrences.setimage(Common.ParseString(object.getJSONObject("data"),Profile_image));
                        prefrences.setaddress(Common.ParseString(object.getJSONObject("data"),Addresss));
                        prefrences.setgoogle_verification(Boolean.parseBoolean(Common.ParseString(object.getJSONObject("data"),"google_verification")));
                      prefrences.setfacebook_verification(Boolean.parseBoolean(Common.ParseString(object.getJSONObject("data"),"facebook_verification")));


                        setup_ui_data();

                    }

                }
                catch (Exception e){
                    pro.cancel();
                    Log.e("error","e",e);
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),etemail);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+prefrences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();

                params.put(Id,prefrences.getuserid());
                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        MySingleton.getInstance(context).addToRequestQueue(stringRequest);
        RequestQueue mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        mRequestQueue.add(stringRequest);


    }




    private void setup_ui_data(){
        etname.setText(prefrences.getname());
        etemail.setText(prefrences.getemail());
        facebook_switch.setChecked(prefrences.getfacebook_verification());
        google_switch.setChecked(prefrences.getgoogle_verification());
        Glide.with(Profile_update_Activity.this).load(Common.Image_Loading_Url(prefrences.getimage())).apply(requestOptions).into(profilePic);

        et_mobileno.setText(prefrences.getmobile());
        if (prefrences.getmobileverification()) {
            tv_mobile_verify.setText("Verified");
            tv_mobile_verify.setVisibility(View.GONE);
            im_mobile_verify.setVisibility(View.VISIBLE);
        }else {
            tv_mobile_verify.setVisibility(View.VISIBLE);
            im_mobile_verify.setVisibility(View.GONE);
            tv_mobile_verify.setText("Verify");
        }
    }


    public void  update_social_verification(final String type, final String verify, final String details){

        final DelayedProgressDialog pro=new DelayedProgressDialog();

        pro.show(getSupportFragmentManager(),"");


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "verify_social_account", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                pro.cancel();
                logg("verify_social_account",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){


                    if (type.equals("fb")){
                        if (verify.equals("true")) {
                            prefrences.setfacebook_verification(true);
                        }else {
                            prefrences.setfacebook_verification(false);
                        }
                    }else {
                        if (verify.equals("true")) {
                            prefrences.setgoogle_verification(true);
                        }else {
                            prefrences.setgoogle_verification(false);
                        }
                    }

                    }

                }
                catch (Exception e){
                    pro.cancel();
                    Log.e("error","e",e);
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),etemail);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+prefrences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();

                params.put(User_id,prefrences.getuserid());
                params.put("type",type);
                params.put("verify",verify);
                if (details.length()>0) {
                    params.put("details",details);
                }
                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        MySingleton.getInstance(context).addToRequestQueue(stringRequest);
        RequestQueue mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        mRequestQueue.add(stringRequest);


    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()){
            case R.id.google_switch:
                if (isChecked){
                     initialize_google();
                }else {
                    update_social_verification("google","false","");
//                    Toast.makeText(mcoxt, "G False", Toast.LENGTH_SHORT).show();

                }
                break;

            case R.id.facebook_switch:
                if (isChecked){
                    LoginManager.getInstance().logOut();
                    LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile","email"));
                    facebooklogin();
                }else {
                    update_social_verification("fb","false","");

                }
                break;


        }
    }


    //google signin
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 007;
    private GoogleSignInOptions gso;

    private void initialize_google(){
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(Profile_update_Activity.this /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        logg("connectionfail",connectionResult+"");
                    }
                } /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        //google signin
        Intent signInIntent =
                Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);

    }

    private void handlegoogleSignInResult(GoogleSignInResult result) {

        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            logg( "acct:",  acct+"");
            logg("display name: ","" + acct.getDisplayName());

            String personName = acct.getDisplayName();
            String personPhotoUrl="";
            if (acct.getPhotoUrl()!=null) {
                personPhotoUrl = acct.getPhotoUrl().toString();
            }
            String email = acct.getEmail();


            logg( "Name: ","" + personName + ", email: " + email
                    + ", Image: " + personPhotoUrl+ ", token: " + acct.getIdToken()+
                    ", Account:"+acct.getAccount());
            try {
                JSONObject jsonObject=new JSONObject();
                jsonObject.put("name",personName);
                jsonObject.put("email",email);
                jsonObject.put("image",personPhotoUrl);
                update_social_verification("google","true",jsonObject.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }


//   facebook login

    private void facebooklogin(){
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.e("Success", loginResult+"");
                        getfacebookUserDetails(loginResult);
                        Log.e("onSuccess","onSuccess");
                    }

                    @Override
                    public void onCancel() {
                        facebook_switch.setChecked(false);
                        Toast.makeText(mcoxt, "Login Cancel", Toast.LENGTH_LONG).show();
                        Log.e("oncanel","oncancel");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(mcoxt,"Somthing wents wrong", Toast.LENGTH_LONG).show();
                        Log.e("error","e",exception);
                        Log.e("onError","onError");

                    }
                });
    }
    private void getfacebookUserDetails(LoginResult loginResult) {

        GraphRequest data_request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject json_object,
                                            GraphResponse response) {

                        Log.e("JSONObject",json_object+"");
                        try {
                            JSONObject obj=new JSONObject(json_object.toString());
                            fb_id=     obj.getString("id");
                            fb_name=      obj.getString("name");
                            fb_email=Common.ParseString(obj,Email);

                            fb_image=obj.getJSONObject("picture").getJSONObject("data").getString("url");
                            update_social_verification("fb","true",json_object.toString());

                        } catch (JSONException e)
                        {
                            Log.e( "e ",e+"" );
                            e.printStackTrace();
                        }

                        Log.e("GraphResponse",response+"");
//                        Intent intent = new Intent(Main_class.this,
//                                UserProfile.class);
//                        intent.putExtra("userProfile", json_object.toString());
//                        startActivity(intent);
                    }

                });
        Bundle permission_param = new Bundle();
        permission_param.putString("fields", "id,name,email,picture.width(120).height(120)");
        data_request.setParameters(permission_param);
        data_request.executeAsync();

    }

    public void onSMSLoginFlow() {

        AccountKit.logOut();
        final Intent intent = new Intent(this, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN); // or .ResponseType.TOKEN
        // ... perform additional configuration ...

        UIManager uiManager;
        uiManager = new SkinManager(
                SkinManager.Skin.CLASSIC, Color.parseColor("#E83957"));

        configurationBuilder.setUIManager(uiManager);
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        startActivityForResult(intent, Mobile_RESULT_CODE);
    }



    public void  update_mobilenumber(final String mobile){

        final DelayedProgressDialog pro=new DelayedProgressDialog();

        pro.show(getSupportFragmentManager(),"");


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "update_mobilenumber", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                pro.cancel();
                logg("verify_social_account",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){

                    et_mobileno.setText(mobile);
                        tv_mobile_verify.setVisibility(View.GONE);
                        im_mobile_verify.setVisibility(View.VISIBLE);
                        tv_mobile_verify.setText("Verified");
                        prefrences.setmobileverification(true);
                        prefrences.setmobile(mobile);

                    }

                }
                catch (Exception e){
                    pro.cancel();
                    Log.e("error","e",e);
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),etemail);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+prefrences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();

                params.put(User_id,prefrences.getuserid());
                params.put("phone_no",mobile);

                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        MySingleton.getInstance(context).addToRequestQueue(stringRequest);
        RequestQueue mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        mRequestQueue.add(stringRequest);


    }


}
