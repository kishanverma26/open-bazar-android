package com.delainetech.open_bazar.SQL;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.delainetech.open_bazar.Models.Location_Picker;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;


/**
 * Created by DelianeTech on 8/31/2017.
 */

public class MyDatabaseHelper extends SQLiteOpenHelper{

    private static String DB_PATH = "/data/data/com.open_bazar";
    private static String DB_NAME = "openbazar.db";

    private  String TABLE_NAME = "city";
    private  String TABLE_NAME1 = "skillls";
    private  String TABLE_NAME2 = "countries";
    private  String TABLE_NAME3 = "industries";
    private  String TABLE_NAME4 = "institute";
    private  String TABLE_NAME5 = "languages";
    private  String col0 = "id";
    private  String col1 = "city_name";

    private  String col10 = "id";
    private  String col11 = "skill";

    private  String col20 = "id";
    private  String col21 = "code";
    private  String col22 = "country";

    private  String col30 = "id";
    private  String col31 = "industry";

    private  String col40 = "id";
    private  String col41 = "institute";

    private  String col42 = "id";
    private  String col43 = "field2";


    String Create_TB_CITY = "CREATE TABLE IF NOT EXISTS "+TABLE_NAME+" ("+col0+" INTEGER PRIMARY KEY  NOT NULL  UNIQUE , "+col1+" TEXT)";
    String Create_TB_SKILLS = "CREATE TABLE IF NOT EXISTS "+TABLE_NAME1+" ("+col10+" INTEGER PRIMARY KEY  NOT NULL  UNIQUE , "+col11+" TEXT)";
    String Create_TB_COUNTRIES = "CREATE TABLE IF NOT EXISTS "+TABLE_NAME2+" ("+col20+" INTEGER PRIMARY KEY  NOT NULL  UNIQUE , "+col21+" TEXT, "+col22+" TEXT)";
    String Create_TB_INDUSTRIES = "CREATE TABLE IF NOT EXISTS "+TABLE_NAME3+" ("+col30+" INTEGER PRIMARY KEY  NOT NULL  UNIQUE , "+col31+" TEXT)";
    String Create_TB_INSTITUTES = "CREATE TABLE IF NOT EXISTS "+TABLE_NAME4+" ("+col40+" INTEGER PRIMARY KEY  NOT NULL  UNIQUE , "+col41+" TEXT)";
    String Create_TB_LANGUAGE = "CREATE TABLE IF NOT EXISTS "+TABLE_NAME5+" ("+col42+" INTEGER PRIMARY KEY  NOT NULL  UNIQUE , "+col43+" TEXT)";

    public SQLiteDatabase checkDB;
    public boolean dbExist;
    private final Context myContext;

    public MyDatabaseHelper(Context context) {
        super(context, DB_NAME, null, 1);
        this.myContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Create_TB_CITY);
        db.execSQL(Create_TB_COUNTRIES);
        db.execSQL(Create_TB_SKILLS);
        db.execSQL(Create_TB_INDUSTRIES);
        db.execSQL(Create_TB_INSTITUTES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
    @Override
    public synchronized void close() {
        if(checkDB != null)
            checkDB.close();
        super.close();
    }

    public void createDataBase() throws IOException {
        dbExist =  checkDataBase();
        if(!dbExist)
            copyDataBase();
    }

    public Boolean checkDataBase(){
        checkDB = null;
        try{
            String myPath = DB_PATH +"/"+ DB_NAME;

            File file = new File(myPath);
            if (file.exists() && !file.isDirectory())
                checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
        } catch (SQLiteException e) {
            // database does't exist yet.
            Log.e("e","e",e);
        }

        if (checkDB != null) {
            checkDB.close();
        }
        return checkDB!=null?true:false;
    }

    ///////////////////copy database /////////////////
    private void copyDataBase() throws IOException{
        String myPath = DB_PATH + "/" + DB_NAME;
        OutputStream databaseOutputStream = new
                FileOutputStream(myPath);
        InputStream databaseInputStream;

        byte[] buffer = new byte[1024];

        databaseInputStream=myContext.getAssets().open(DB_NAME);

        while ( (databaseInputStream.read(buffer)) > 0 )
        {
            databaseOutputStream.write(buffer);
        }
        databaseInputStream.close();
        databaseOutputStream.flush();
        databaseOutputStream.close();
    }

    public void openDataBase() throws SQLException {

        String myPath = DB_PATH + "/" +DB_NAME;

            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

        }


    public ArrayList<Location_Picker> GetCity(){

        ArrayList<Location_Picker> hobbies=new ArrayList<Location_Picker>();
        openDataBase();
        Cursor cursor = checkDB.rawQuery("select distinct city_name from city", null);
        try {


            if(cursor!=null && cursor.moveToFirst()){
                do{

                   String hobby=cursor.getString(0);
                    hobbies.add(new Location_Picker(hobby,false));
                } while(cursor.moveToNext());
            }
        }finally {
            if(cursor!=null){
                cursor.close();
            }
        }
        close();
        return hobbies;
    }


    public ArrayList<String> GetInstitute(){

        ArrayList<String> institiutes=new ArrayList<String>();
        openDataBase();
        Cursor cursor = checkDB.rawQuery("select distinct institute from institute", null);
        Log.e("count",cursor.getCount()+"");
        try {


            if(cursor!=null && cursor.moveToFirst()){
                do{

                    String institute=cursor.getString(0);
                    institiutes.add(institute);
                } while(cursor.moveToNext());
            }
        }finally {
            if(cursor!=null){
                cursor.close();
            }
        }
        close();
        return institiutes;
    }

    public ArrayList<String> GetIndustries(){

        ArrayList<String> industries=new ArrayList<String>();
        openDataBase();
        Cursor cursor = checkDB.rawQuery("select distinct industry from industries", null);
        Log.e("count",cursor.getCount()+"");
        try {


            if(cursor!=null && cursor.moveToFirst()){
                do{

                    String industry=cursor.getString(0);
                    industries.add(industry);
                } while(cursor.moveToNext());
            }
        }finally {
            if(cursor!=null){
                cursor.close();
            }
        }
        close();
        return industries;
    }

    public ArrayList<String> GetSkills(){

        ArrayList<String> skills=new ArrayList<String>();
        openDataBase();
        Cursor cursor = checkDB.rawQuery("select distinct skill from skillls", null);
        Log.e("count",cursor.getCount()+"");
        try {


            if(cursor!=null && cursor.moveToFirst()){
                do{
                    String skill=cursor.getString(0);
                    skills.add(skill);
                } while(cursor.moveToNext());
            }
        }finally {
            if(cursor!=null){
                cursor.close();
            }
        }
        close();
        return skills;
    }

    public ArrayList<String> GetLanguage(){

        ArrayList<String> language=new ArrayList<String>();
        openDataBase();
        Cursor cursor = checkDB.rawQuery("select distinct field2 from languages", null);
        Log.e("count",cursor.getCount()+"");
        try {


            if(cursor!=null && cursor.moveToFirst()){
                do{
                    String skill=cursor.getString(0);
                    language.add(skill);
                } while(cursor.moveToNext());
            }
        }finally {
            if(cursor!=null){
                cursor.close();
            }
        }
        close();
        return language;
    }


    public  String getCountryCode(String country)
    {
        openDataBase();
        String  code="";
        Cursor cursor = checkDB.rawQuery("select code from countries where country='"+country+"'", null);
        Log.e("count",cursor.getCount()+"");
        try {


            if(cursor!=null && cursor.moveToFirst()){
               // do{
                     code=cursor.getString(0);
                     Log.e("cntry_code",code);
                    //skills.add(skill);
               // } while(cursor.moveToNext());
            }
        }finally {
            if(cursor!=null){
                cursor.close();
            }
        }
        close();
        return code;
    }

    public ArrayList<String> GetCountry(){

        ArrayList<String> countries=new ArrayList<String>();
        openDataBase();
        Cursor cursor = checkDB.rawQuery("select distinct country from countries", null);
        Log.e("count",cursor.getCount()+"");
        try {


            if(cursor!=null && cursor.moveToFirst()){
                do{
                    String country=cursor.getString(0);
                    countries.add(country);
                } while(cursor.moveToNext());
            }
        }finally {
            if(cursor!=null){
                cursor.close();
            }
        }
        close();
        return countries;
    }



    private boolean exist(){
        File dbFile = myContext.getDatabasePath(DB_NAME);
        return dbFile.exists();
    }

}
