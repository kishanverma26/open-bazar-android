package com.delainetech.open_bazar.SQL;

import java.util.ArrayList;
import java.util.List;

public class ContactDTO {



    // Contact phone list.
    private List<DataDTO> phoneList = new ArrayList<DataDTO>();

    public List<DataDTO> getPhoneList() {
        return phoneList;
    }

    public void setPhoneList(List<DataDTO> phoneList) {
        this.phoneList = phoneList;
    }

}
