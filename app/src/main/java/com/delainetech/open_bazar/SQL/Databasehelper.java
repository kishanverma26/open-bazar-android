package com.delainetech.open_bazar.SQL;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.delainetech.open_bazar.Utils.ParamKeys;

import static com.delainetech.open_bazar.Utils.Common.logg;


/**
 * Created by kishan on 19-02-2017.
 */

public class Databasehelper extends SQLiteOpenHelper implements ParamKeys {

    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "openbazar.db";
    public static final String TABLE_Name = "contact";
    public static final String col1 = "id";
    public static final String col2 = "DISPLAY_NAME";
    public static final String col5 = "PHOTO_URI";
    public static final String col6 = "DATA1";
    public static final String col7 = "Invitation_status";







    public Databasehelper(Context context) {
        super(context,DATABASE_NAME,null,DATABASE_VERSION);

    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(" create table " + TABLE_Name + "(" + col1 + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ," + col2 + " TEXT,"
                + col5 + " TEXT," + col6 + " TEXT ," + col7 + " INTEGER"+")");


    }



    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_Name);

        // Create tables again
        onCreate(db);
    }



    public void insertContactdata(String DISPLAY_NAME, String PHOTO_URI,
                                  String email)
    {


        if (checkifexist(email)) {

            SQLiteDatabase db = this.getWritableDatabase();
            {
                ContentValues c = new ContentValues();
                c.put(col2, DISPLAY_NAME);

                c.put(col5, PHOTO_URI);
                c.put(col6, email);//mobile no
              c.put(col7, "0");//mobile no

      long in=db.insert(TABLE_Name, null, c);
      Log.e("inserr",in+"");
   }

        }
    }

    public void update_value(String status,String id){

        SQLiteDatabase db = this.getWritableDatabase();
        {
            ContentValues c = new ContentValues();

            c.put(col7, status);//mobile no

            long in=db.update(TABLE_Name, c,"id="+id, null);
            Log.e("inserr",in+"");
        }
    }



    public Cursor getalldata(int page) {
        page=100*page;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery(" select * from " + TABLE_Name +" ORDER BY "+col2+" asc"+" limit 100 offset "+page , null);
        return res;
    }







    public boolean checkifexist(String no){
        boolean status=false;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = null;
        String sql ="SELECT * FROM "+TABLE_Name+" WHERE "+col6+"='"+no+"'";
        cursor= db.rawQuery(sql,null);
//        Log.e("cursor",cursor.getCount()+"");
        if(cursor.getCount()>0){
          status=false;
        }else{
          status=true;
        }
        cursor.close();
        logg("status",status+"");
        return status;
    }
  public boolean checkmobileno(String no){
        boolean status=false;
        SQLiteDatabase db = this.getWritableDatabase();

        if (no.length()>5){
            Cursor cursor = null;
        Log.e("mobile_no",no+"");
        String sql ="SELECT * FROM "+TABLE_Name+" WHERE "+col6+"='"+no+"'";
      Log.e("query",sql+"");
        cursor= db.rawQuery(sql,null);
//        Log.e("cursor",cursor.getCount()+"");
        if(cursor.getCount()>0){
          status=true;
        }else{
          status=false;
        }
            cursor.close();
        }

        return status;
    }


    private boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    private void closeDb(SQLiteDatabase db) {
        try {
            db.endTransaction();
        }catch (Exception e){

        }
        try {
            db.close();
        }catch (Exception e){

        }
    }

    public Cursor get_alldata() {

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery(" select * from " + TABLE_Name, null);
        return res;
    }
}
