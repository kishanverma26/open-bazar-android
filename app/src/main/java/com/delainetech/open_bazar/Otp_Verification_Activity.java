package com.delainetech.open_bazar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.delainetech.open_bazar.Custom_classes.DelayedProgressDialog;
import com.delainetech.open_bazar.Custom_classes.MySingleton;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.delainetech.open_bazar.Utils.Common.logg;

public class Otp_Verification_Activity extends AppCompatActivity implements View.OnClickListener,ParamKeys {

    Context mcoxt;
    ImageView iv_back;
    EditText et_otp;
    TextView tv_submit,tv_resend;
    String type="",otp;
    UserSharedPreferences preferences,playerprefer;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_verification);
        mcoxt=Otp_Verification_Activity.this;
        initializ();
    }

    private void initializ() {
        preferences=UserSharedPreferences.getInstance(Otp_Verification_Activity.this);
        playerprefer=new UserSharedPreferences(Otp_Verification_Activity.this,"");
        tv_resend=findViewById(R.id.tv_resend);
        tv_submit=findViewById(R.id.tv_submit);
        et_otp=findViewById(R.id.et_otp);
        iv_back=findViewById(R.id.iv_back);

        iv_back.setOnClickListener(this);
        tv_submit.setOnClickListener(this);
        tv_resend.setOnClickListener(this);

        type=Common.Parsintentvalue(Type,Otp_Verification_Activity.this);
         otp=Common.Parsintentvalue(OTP,Otp_Verification_Activity.this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_back:
                   finish();
                break;
            case R.id.tv_submit:
                Common.hideSoftKeyboard(Otp_Verification_Activity.this);
                if (et_otp.getText().toString().trim().length()==5){
                    if (otp.equals(et_otp.getText().toString())){
                        if (type.equals("signup")) {
                            signup(this);
                        }else {
                            Intent i=new Intent(mcoxt,ResetPassword_Activity.class);
                            startActivity(i);
                            finish();
                        }
                    }else {
                        Common.Snakbar("Please enter valid 5 digit otp",et_otp);
                    }
                }else {
                    Common.Snakbar("Please enter 5 digit otp",et_otp);
                }
                break;
            case R.id.tv_resend:
                resend_otp(this);
                break;

        }
    }


    public void  signup(final Context context){

        final DelayedProgressDialog pro=new DelayedProgressDialog();

        pro.show(getSupportFragmentManager(),"");


        StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "signup", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                pro.cancel();
                logg("signupres",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){

                        preferences.setuserid(Common.ParseString(object.getJSONObject("data"),Id));
                        preferences.setimage(Common.ParseString(object.getJSONObject("data"),Profile_image));
                        preferences.setaddress(Common.ParseString(object.getJSONObject("data"),Addresss));
                        preferences.settoken(Common.ParseString(object,ParamKeys.Token));
                        preferences.setcreated_at(Common.ParseString(object.getJSONObject("data"),"created_at"));
                        preferences.setlogin(true);
                        Intent i=new Intent(mcoxt,Home_Screen_Activity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        finish();
                    }

                }
                catch (Exception e){
                    pro.cancel();
                    Log.e("error","e",e);
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),et_otp);
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(Email,preferences.getemail());
                params.put(Name,preferences.getname());
                params.put(Signup_type,"Email");
                params.put(App_Type,"Android");
                params.put("reffer_id",preferences.getrefferuserid());
                params.put(Phone_No,preferences.getmobile());
                params.put(Player_id,playerprefer.getplayerid());
                 params.put(Password,preferences.getaudiorate());


                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(context).addToRequestQueue(stringRequest);


    }

    public void  resend_otp(final Context context){

        final DelayedProgressDialog pro=new DelayedProgressDialog();

        pro.show(getSupportFragmentManager(),"");


        StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "emailotp", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                pro.cancel();
                logg("signupres",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){
                        Toast.makeText(context, "Otp has been sent", Toast.LENGTH_SHORT).show();

                        otp=object.getString("otp");

                    }

                }
                catch (Exception e){
                    pro.cancel();
                    Log.e("error","e",e);
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),et_otp);
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(Email,preferences.getemail());
                params.put(Type,"other");
                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(context).addToRequestQueue(stringRequest);


    }

}
