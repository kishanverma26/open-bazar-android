package com.delainetech.open_bazar;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.delainetech.open_bazar.Custom_classes.DelayedProgressDialog;
import com.delainetech.open_bazar.Custom_classes.MySingleton;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.delainetech.open_bazar.Utils.Common.logg;

public class ResetPassword_Activity extends AppCompatActivity implements View.OnClickListener ,ParamKeys {

    Context mcoxt;
    ImageView iv_back,iv_oldpass_eye,iv_newpass_eye,iv_confirmpass_eye;
    CardView cv_resetpass;
    EditText et_oldpass,et_newpass,et_confirmpass;
    UserSharedPreferences preferences;
    TextView tv_reset,tv_tital;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reset_password);
        mcoxt=ResetPassword_Activity.this;
        initializ();
    }

    private void initializ() {
        preferences=UserSharedPreferences.getInstance(this);
        et_oldpass=findViewById(R.id.et_oldpass);
        et_newpass=findViewById(R.id.et_newpass);
        tv_reset=findViewById(R.id.tv_reset);
        tv_tital=findViewById(R.id.tv_tital);
        et_confirmpass=findViewById(R.id.et_confirmpass);

        iv_oldpass_eye=findViewById(R.id.iv_oldpass_eye);
        iv_oldpass_eye.setOnClickListener(this);
        iv_newpass_eye=findViewById(R.id.iv_newpass_eye);
        iv_newpass_eye.setOnClickListener(this);
        iv_confirmpass_eye=findViewById(R.id.iv_confirmpass_eye);
        iv_confirmpass_eye.setOnClickListener(this);

        iv_back=findViewById(R.id.iv_back);
        iv_back.setOnClickListener(this);

        cv_resetpass=findViewById(R.id.cv_resetpass);
        cv_resetpass.setOnClickListener(this);

        if (getIntent().hasExtra("type")){
            tv_reset.setText("Change Password");
            tv_tital.setText("Change Password");
        }

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;



        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_back:
                   finish();
                break;

            case R.id.cv_resetpass:

                checkvalidation();
                break;
            case R.id.iv_oldpass_eye:
//                    enableInputVisiblePassword(et_oldpass,iv_oldpass_eye);

               Common.enableInputVisiblePassword(et_oldpass,iv_oldpass_eye);
                break;
             case R.id.iv_newpass_eye:
//                    enableInputVisiblePassword(et_oldpass,iv_oldpass_eye);

               Common.enableInputVisiblePassword(et_newpass,iv_newpass_eye);
                break;
             case R.id.iv_confirmpass_eye:
//                    enableInputVisiblePassword(et_oldpass,iv_oldpass_eye);

               Common.enableInputVisiblePassword(et_confirmpass,iv_confirmpass_eye);
                break;

        }
    }

    public void   checkvalidation(){
        Common.hideSoftKeyboard(ResetPassword_Activity.this);
                   if (et_newpass.getText().toString().length()>5){
                        if (et_newpass.getText().toString().equals(et_confirmpass.getText().toString())){
                            reset_pass(mcoxt);

                        }else {
                            Common.Snakbar("New password and confirm password does not match", et_confirmpass);

                        }
                    }else {
                        Common.Snakbar("Please enter minimum 6 digit password", et_confirmpass);

                    }
                     }




    public void  reset_pass(final Context context){

        final DelayedProgressDialog pro=new DelayedProgressDialog();

        pro.show(getSupportFragmentManager(),"");


        StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "reset_password", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                pro.cancel();
                logg("reset_passwordres",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){
                        Toast.makeText(context, "Password has been changed", Toast.LENGTH_SHORT).show();
                        finish();
                    }

                }
                catch (Exception e){
                    pro.cancel();
                    Log.e("error","e",e);
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),et_confirmpass);
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(Email,preferences.getemail());
                params.put(Password,et_newpass.getText().toString());
                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(context).addToRequestQueue(stringRequest);


    }


}
