package com.delainetech.open_bazar.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.delainetech.open_bazar.Chat.ChatActivity;
import com.delainetech.open_bazar.Chat.FirbaseCommon;
import com.delainetech.open_bazar.Custom_classes.CTextView;
import com.delainetech.open_bazar.Detail_Screen_Activity;
import com.delainetech.open_bazar.Drawer_Frame_Activity;
import com.delainetech.open_bazar.Fragments.MY_ADS_Frag;
import com.delainetech.open_bazar.Home_Screen_Activity;
import com.delainetech.open_bazar.Models.ADS;
import com.delainetech.open_bazar.Other_User_Profile;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import static com.delainetech.open_bazar.Utils.Common.logg;


/**
 * Created by Kishan on 21-Dec-17.
 */

public class My_Ads_adapter_User extends RecyclerView.Adapter<My_Ads_adapter_User.Viewholder> implements ParamKeys {

    Context mcontext;
    String image0="";
    UserSharedPreferences preferences;
    RequestOptions options=new RequestOptions().error(R.drawable.logo_bazar_no_image_up).centerCrop();

    ArrayList<ADS> adsArrayList;
    Fragment fragment;
    public My_Ads_adapter_User(Context mcontext, ArrayList<ADS> adsArrayList,Fragment fragment) {
        this.mcontext = mcontext;
         this.adsArrayList = adsArrayList;
        preferences=UserSharedPreferences.getInstance(mcontext);
        this.fragment=fragment;
    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_ads_item_adapter, parent, false);

        return new Viewholder(itemView);
    }

    @Override
    public void onBindViewHolder(Viewholder holder, final int position)
    {
        holder.iv_user.setVisibility(View.GONE);
         holder.tv_location.setText(adsArrayList.get(position).getCity());
        holder.tv_price.setText("€ "+adsArrayList.get(position).getPrice());
        holder.tv_tital.setText(adsArrayList.get(position).getAd_title());
        holder.tv_viewcount.setText(adsArrayList.get(position).getViews_count());

        if (adsArrayList.get(position).getPrice_type().equals("price")){
            holder.tv_price.setText("€ "+adsArrayList.get(position).getPrice());
        }else  if (adsArrayList.get(position).getPrice_type().equals("ask for price")){
            holder.tv_price.setText("Ask For Price");
        }else  if (adsArrayList.get(position).getPrice_type().equals("exchange")){
            holder.tv_price.setText("Exchange");
        }else  if (adsArrayList.get(position).getPrice_type().equals("free")){
            holder.tv_price.setText("Free");
        }
        holder.tv_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Common. create_dynamic_link(adsArrayList.get(position).getAd_title(),"adpost/"+adsArrayList.get(position).getId()+","+preferences.getuserid(),((Drawer_Frame_Activity)mcontext));

            }
        });
        holder.tv_time.setText( Common.getTimeAgo(Long.parseLong(adsArrayList.get(position).getTimestamp()),mcontext));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(mcontext, Detail_Screen_Activity.class);
                i.putExtra("pos",position);
                i.putExtra("list",adsArrayList);
                 fragment.startActivityForResult(i,ADS_DELETE_RESULT_CODE);

            }
        });


//        load image
        if (adsArrayList.get(position).getImages().length()>0) {
            try {
//                logg("images", adsArrayList.get(position).getImages().length() + "");
                JSONArray jsonArray = new JSONArray(adsArrayList.get(position).getImages());
               holder. tv_imagecount.setText(jsonArray.length()+"");
                Glide.with(mcontext).load(Common.Image_Loading_Url(jsonArray.getJSONObject(0).getString("image"))).apply(options).into(holder.iv_image);
//                logg("image", Common.Image_Loading_Url(jsonArray.getJSONObject(0).getString("image0")) + "");
            } catch (JSONException e) {
                e.printStackTrace();
                logg("excep", e + "");
            }
        }else {
            holder. tv_imagecount.setText("0");
            Glide.with(mcontext).load("ab").apply(options).into(holder.iv_image);

        }



        holder.rl_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i=new Intent(mcontext, Home_Screen_Activity.class);
                i.putExtra("from","chat");
                mcontext.startActivity(i);
            }
        });


        holder.tv_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert(mcontext,position);

            }
        });

//        holder.tv_expired.setText(Html.fromHtml(printDifference(adsArrayList.get(position).getExpired_date())));

        holder.tv_boost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             Intent   i=new Intent(mcontext, Drawer_Frame_Activity.class);
                i.putExtra(Pos,17);
                i.putExtra(Title,"Boost Ad");
                i.putExtra("add_id",adsArrayList.get(position).getId());
             mcontext.   startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return adsArrayList.size();
    }



    public class Viewholder extends RecyclerView.ViewHolder{
        TextView tv_tital,tv_price,tv_location,tv_viewcount,tv_imagecount,tv_time,tv_call,tv_share,tv_expired,tv_boost;
        CTextView tv_favorite;
        ImageView iv_user,iv_image;
        RelativeLayout rl_chat;
        public Viewholder(View itemView) {
            super(itemView);
            rl_chat=itemView.findViewById(R.id.rl_chat);
           tv_boost=itemView.findViewById(R.id.tv_boost);
            tv_expired=itemView.findViewById(R.id.tv_expired);
            tv_share=itemView.findViewById(R.id.tv_share);
            tv_call=itemView.findViewById(R.id.tv_call);
            tv_favorite=itemView.findViewById(R.id.tv_favorite);
            tv_time=itemView.findViewById(R.id.tv_time);
            tv_imagecount=itemView.findViewById(R.id.tv_imagecount);
            tv_viewcount=itemView.findViewById(R.id.tv_viewcount);
            iv_image=itemView.findViewById(R.id.iv_image);
            tv_location=itemView.findViewById(R.id.tv_location);
            iv_user=itemView.findViewById(R.id.iv_user);
            tv_tital=itemView.findViewById(R.id.tv_tital);
            tv_price=itemView.findViewById(R.id.tv_price);

        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }




    public void  delete_ad(final String id){


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "delete_ads", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                pro.cancel();
                logg("insert_ads",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){



                    }

                }
                catch (Exception e){
//                    pro.cancel();
                    Log.e("error","e",e);
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(Id,id);
                params.put(User_id,preferences.getuserid());

                Log.e("params",params+"");
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }


    public void alert(final Context mcoxt, final int pos){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mcoxt);

        alertDialog.setMessage("Are your sure you want to delete?");
        alertDialog.setPositiveButton(mcoxt.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                delete_ad(adsArrayList.get(pos).getId());
                adsArrayList.remove(pos);
                notifyDataSetChanged();
            }
        });
        alertDialog.setNegativeButton(mcoxt.getResources().getString(R.string.No), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog1= alertDialog.create();
        alertDialog1.show();

        alertDialog1.getButton(alertDialog1.BUTTON_NEGATIVE).setTextColor(mcoxt.getResources().getColor(R.color.colorAccent));
        alertDialog1.getButton(alertDialog1.BUTTON_POSITIVE).setTextColor(mcoxt.getResources().getColor(R.color.colorAccent));
    }


    public String printDifference(String timestamp) {

        Date startDate=Common.getDate(System.currentTimeMillis());
        Date endDate=Common.getDate(Long.parseLong(timestamp)*1000);
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : "+ endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);
        Log.e("date= ","days:"+elapsedDays);
        return "<font color=#58697B>Expired In: </font> <font color=#c62828>"+elapsedDays+"</font> <font color=#58697B>Days</font>";


    }


}
