package com.delainetech.open_bazar.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.ViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.Common;
import com.github.chrisbanes.photoview.PhotoView;

import java.security.MessageDigest;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;

import static com.delainetech.open_bazar.Utils.Common.logg;
import static com.delainetech.open_bazar.onesignel_notification.Onesignal_Myapplication.getContext;


/**
 * Created by Swt on 9/25/2017.
 */

public class Image_Viewpage_Adapter_full_screen extends PagerAdapter {
    private Context context;
   // int images[];
   private ArrayList<String> postimages = new ArrayList<>();
    private LayoutInflater layoutInflater;
    RequestOptions requestOptions=new RequestOptions().fitCenter();


    public  Image_Viewpage_Adapter_full_screen(Context context, ArrayList<String> postimages) {
        this.context = context;
      //  this.images = image;
        this.postimages = postimages;
        layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return postimages.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
       View itemView = layoutInflater.inflate(R.layout.dialog_mediadialogadapter, container, false);
        PhotoView imageView = (PhotoView) itemView.findViewById(R.id.image);



        Glide.with(context).load(Common.Image_Loading_Url(postimages.get(position))).into(imageView);



         container.addView(itemView);
        return itemView;


    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }



    @Override
    public int getItemPosition(@NonNull Object object) {
        return super.getItemPosition(object);
    }




}
