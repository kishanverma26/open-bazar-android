package com.delainetech.open_bazar.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.delainetech.open_bazar.Custom_classes.CTextView;
import com.delainetech.open_bazar.Detail_Screen_Activity;
import com.delainetech.open_bazar.Drawer_Frame_Activity;
import com.delainetech.open_bazar.Fragments.Report_Ad_fragment;
import com.delainetech.open_bazar.Models.ADS;
import com.delainetech.open_bazar.Models.REPORT;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import static com.delainetech.open_bazar.Utils.Common.logg;


/**
 * Created by Kishan on 21-Dec-17.
 */

public class Report_adapter extends RecyclerView.Adapter<Report_adapter.Viewholder> implements ParamKeys {

    Context mcontext;

    UserSharedPreferences preferences;
    ArrayList<REPORT> adsArrayList;
    Report_Ad_fragment fragment;
    public Report_adapter(Context mcontext, ArrayList<REPORT> adsArrayList,Report_Ad_fragment fragment){
        this.mcontext = mcontext;
         this.adsArrayList = adsArrayList;
         this.fragment = fragment;
        preferences=UserSharedPreferences.getInstance(mcontext);

    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.report_ads_item, parent, false);

        return new Viewholder(itemView);
    }

    @Override
    public void onBindViewHolder(Viewholder holder, final int position)
    {
        holder.sw_switch.setText(adsArrayList.get(position).getTital());
        holder.sw_switch.setChecked(adsArrayList.get(position).getSelect());


        holder.sw_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logg("change1", "change1");
                for (int i = 0; i < adsArrayList.size(); i++) {
                    adsArrayList.get(i).setSelect(false);
                }
                adsArrayList.get(position).setSelect(true);

                logg("change", "change");
                fragment.update();
            }
        });


    }

    @Override
    public int getItemCount() {
        return adsArrayList.size();
    }




    public class Viewholder extends RecyclerView.ViewHolder{
        SwitchCompat sw_switch;
        public Viewholder(View itemView) {
            super(itemView);
            sw_switch=itemView.findViewById(R.id.sw_switch);

        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }




}
