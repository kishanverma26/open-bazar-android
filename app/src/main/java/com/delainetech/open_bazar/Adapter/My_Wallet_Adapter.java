package com.delainetech.open_bazar.Adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.delainetech.open_bazar.Custom_classes.CTextView;
import com.delainetech.open_bazar.Models.MYCOINS;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;

import java.util.ArrayList;


/**
 * Created by Kishan on 21-Dec-17.
 */

public class My_Wallet_Adapter extends RecyclerView.Adapter<My_Wallet_Adapter.Viewholder> implements ParamKeys {

    Context mcontext;

//    UserSharedPreferences preferences;

    ArrayList<MYCOINS> mycoinsArrayList;
    public My_Wallet_Adapter(Context mcontext,ArrayList<MYCOINS> mycoinsArrayList) {
        this.mcontext = mcontext;
        this.mycoinsArrayList=mycoinsArrayList;

    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_wallet_item_adapter, parent, false);

        return new Viewholder(itemView);
    }

    @Override
    public void onBindViewHolder(Viewholder holder, final int position)
    {



//            if (position%2==0){
//                holder.tv_title.setText(mcontext.getResources().getString(R.string.credits));
//                holder.tv_credit.setText("+ 20 Credit");
//                holder.tv_credit.setTextColor(mcontext.getResources().getColor(R.color.greencolor));
//                holder.tv_title.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_add_coin,0,0,0);
//            }else {
//                holder.tv_title.setText(mcontext.getResources().getString(R.string.redeem));
//                holder.tv_credit.setText("- 20 Credit");
//                holder.tv_credit.setTextColor(mcontext.getResources().getColor(R.color.red));
//                holder.tv_title.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_redeem_coin,0,0,0);
//            }

            if (mycoinsArrayList.get(position).getType().equals("credit")) {

                holder.tv_credit.setText("+ " + mycoinsArrayList.get(position).getCoins() + " Credits");
                holder.tv_title.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_add_coin, 0, 0, 0);
                holder.tv_credit.setTextColor(mcontext.getResources().getColor(R.color.greencolor));

            }else {
        holder.tv_credit.setText(mycoinsArrayList.get(position).getCoins().replace("-","- ") + " Debit");
        holder.tv_credit.setTextColor(mcontext.getResources().getColor(R.color.red));
        holder.tv_title.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_redeem_coin,0,0,0);
            }
        holder.tv_title.setText(mycoinsArrayList.get(position).getTital());
        holder.tv_time.setText(Common.getTimeAgo(Long.parseLong(mycoinsArrayList.get(position).getTimestamp()), mcontext));
    }

    @Override
    public int getItemCount() {
        return mycoinsArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder{
        TextView tv_credit,tv_time;
        CTextView tv_title;
        public Viewholder(View itemView) {
            super(itemView);
            tv_title=itemView.findViewById(R.id.tv_title);
            tv_credit=itemView.findViewById(R.id.tv_credit);
            tv_time=itemView.findViewById(R.id.tv_time);

        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
