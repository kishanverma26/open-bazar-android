package com.delainetech.open_bazar.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.delainetech.open_bazar.Chat.ChatActivity;
import com.delainetech.open_bazar.Chat.FirbaseCommon;
import com.delainetech.open_bazar.Custom_classes.CTextView;
import com.delainetech.open_bazar.Detail_Screen_Activity;
import com.delainetech.open_bazar.Models.ADS;
import com.delainetech.open_bazar.Other_User_Profile;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import static com.delainetech.open_bazar.Utils.Common.logg;


/**
 * Created by Kishan on 21-Dec-17.
 */

public class My_Ads_Recentview_adapter_Other_Users extends RecyclerView.Adapter<My_Ads_Recentview_adapter_Other_Users.Viewholder> implements ParamKeys {

    Context mcontext;
    String image0="";
    UserSharedPreferences preferences;
RequestOptions options=new RequestOptions().error(R.drawable.logo_bazar_no_image_up).centerCrop();

    ArrayList<ADS> adsArrayList;
    Fragment fragment;
    public My_Ads_Recentview_adapter_Other_Users(Context mcontext, ArrayList<ADS> adsArrayList, Fragment fragment) {
        this.mcontext = mcontext;
        this.fragment = fragment;
         this.adsArrayList = adsArrayList;
        preferences=UserSharedPreferences.getInstance(mcontext);
    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recently_view_item_adapter, parent, false);

        return new Viewholder(itemView);
    }

    @Override
    public void onBindViewHolder(Viewholder holder, final int position)
    {
        holder.iv_user.setVisibility(View.GONE);
         holder.tv_location.setText(adsArrayList.get(position).getCity());
        holder.tv_price.setText("€ "+adsArrayList.get(position).getPrice());
        holder.tv_tital.setText(adsArrayList.get(position).getAd_title());
        holder.tv_viewcount.setText(adsArrayList.get(position).getViews_count());

        if (adsArrayList.get(position).getPrice_type().equals("price")){
            holder.tv_price.setText("€ "+adsArrayList.get(position).getPrice());
        }else  if (adsArrayList.get(position).getPrice_type().equals("ask for price")){
            holder.tv_price.setText("Ask For Price");
        }else  if (adsArrayList.get(position).getPrice_type().equals("exchange")){
            holder.tv_price.setText("Exchange");
        }else  if (adsArrayList.get(position).getPrice_type().equals("free")){
            holder.tv_price.setText("Free");
        }

        holder.tv_time.setText( Common.getTimeAgo(Long.parseLong(adsArrayList.get(position).getTimestamp()),mcontext));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(mcontext, Detail_Screen_Activity.class);
                i.putExtra("pos",position);
                i.putExtra("list",adsArrayList);
                fragment.startActivityForResult(i,ADS_DELETE_RESULT_CODE);

            }
        });



//        load image
        if (adsArrayList.get(position).getImages().length()>0) {
            try {
//                logg("images", adsArrayList.get(position).getImages().length() + "");
                JSONArray jsonArray = new JSONArray(adsArrayList.get(position).getImages());
                holder. tv_imagecount.setText(jsonArray.length()+"");
                Glide.with(mcontext).load(Common.Image_Loading_Url(jsonArray.getJSONObject(0).getString("image"))).apply(options).into(holder.iv_image);
//                logg("image", Common.Image_Loading_Url(jsonArray.getJSONObject(0).getString("image0")) + "");
            } catch (JSONException e) {
                e.printStackTrace();
                logg("excep", e + "");
            }
        }else {
            holder. tv_imagecount.setText("0");
            Glide.with(mcontext).load("ab").apply(options).into(holder.iv_image);

        }

//        chat
        holder.rl_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


//        load image
                if (adsArrayList.get(position).getImages().length()>0) {
                    try {
//                logg("images", adsArrayList.get(position).getImages().length() + "");
                        JSONArray jsonArray = new JSONArray(adsArrayList.get(position).getImages());

                       image0=jsonArray.getJSONObject(0).getString("image");
//                logg("image", Common.Image_Loading_Url(jsonArray.getJSONObject(0).getString("image0")) + "");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        logg("excep", e + "");
                    }
                }else {
                    image0="";

                }

                Intent chatIntent = new Intent(mcontext, ChatActivity.class);
                if (adsArrayList.get(position).getPrice_type().equals("price"))
                {
                    FirbaseCommon.regiter_adpost(adsArrayList.get(position).getId(),adsArrayList.get(position).getAd_title()
                            ,image0,adsArrayList.get(position).getPrice(),adsArrayList.get(position).getMobile(),
                            adsArrayList.get(position).getHide_mobileno());
                    chatIntent.putExtra("ad_price","€ "+adsArrayList.get(position).getPrice());
                }else {
                    FirbaseCommon.regiter_adpost(adsArrayList.get(position).getId(),adsArrayList.get(position).getAd_title()
                            ,image0,adsArrayList.get(position).getPrice_type(),adsArrayList.get(position).getMobile(),
                            adsArrayList.get(position).getHide_mobileno());
                    chatIntent.putExtra("ad_price",adsArrayList.get(position).getPrice_type());
                }


                chatIntent.putExtra("user_id", adsArrayList.get(position).getUser_id());
                chatIntent.putExtra("user_name", adsArrayList.get(position).getUser_name());
                chatIntent.putExtra("name", preferences.getname());
                chatIntent.putExtra("id",preferences.getuserid());
                chatIntent.putExtra("ad_tital",adsArrayList.get(position).getAd_title());
                chatIntent.putExtra("ad_image",image0);
                chatIntent.putExtra("ad_id",adsArrayList.get(position).getId());

                logg("ides", adsArrayList.get(position).getUser_id() + ",,"  + ",," + preferences.getuserid());
               mcontext. startActivity(chatIntent);

            }
        });



        if (adsArrayList.get(position).getFav_status().equals("Yes")) {
            holder.tv_favorite.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_favorite_red_15dp,0,0,0);

        }else {
            holder.tv_favorite.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_favorite_15dp_grey,0,0,0);


        }


        holder.tv_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view(adsArrayList.get(position).getId(),"favorite",position);
            }
        });

        holder.tv_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+adsArrayList.get(position).getMobile()));
               mcontext. startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return adsArrayList.size();
    }



    public class Viewholder extends RecyclerView.ViewHolder{
        TextView tv_tital,tv_price,tv_location,tv_viewcount,tv_imagecount,tv_time,tv_call;
        CTextView tv_favorite;
        ImageView iv_user,iv_image;
        RelativeLayout rl_chat;
        public Viewholder(View itemView) {
            super(itemView);
            rl_chat=itemView.findViewById(R.id.rl_chat);
            tv_call=itemView.findViewById(R.id.tv_call);
            tv_favorite=itemView.findViewById(R.id.tv_favorite);
            tv_time=itemView.findViewById(R.id.tv_time);
            tv_imagecount=itemView.findViewById(R.id.tv_imagecount);
            tv_viewcount=itemView.findViewById(R.id.tv_viewcount);
            iv_image=itemView.findViewById(R.id.iv_image);
            tv_location=itemView.findViewById(R.id.tv_location);
            iv_user=itemView.findViewById(R.id.iv_user);
            tv_tital=itemView.findViewById(R.id.tv_tital);
            tv_price=itemView.findViewById(R.id.tv_price);

        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }





    public void  view(final String postid, final String type, final int pos){


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "ad_view", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                pro.cancel();
                logg("insert_ads",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){

                        if (type.equals("favorite"))
                        {
                            if (Common.ParseString(object.getJSONObject("data"),"favorite").equals("0")){
                                adsArrayList.get(pos).setFav_status("No");
                                adsArrayList.get(pos).setFavorite_count((Integer.parseInt(adsArrayList.get(pos).getFavorite_count())-1)+"");
                            }else {
                                adsArrayList.get(pos).setFavorite_count((Integer.parseInt(adsArrayList.get(pos).getFavorite_count())+1)+"");

                                adsArrayList.get(pos).setFav_status("Yes");
                            }

                            notifyItemChanged(pos);


                        }

                    }

                }
                catch (Exception e){
//                    pro.cancel();
                    Log.e("error","e",e);
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(User_id,preferences.getuserid());
                params.put("post_id",postid);
                params.put("type",type);

                Log.e("params",params+"");
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }

}
