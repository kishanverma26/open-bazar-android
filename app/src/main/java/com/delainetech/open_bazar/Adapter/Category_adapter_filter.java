package com.delainetech.open_bazar.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.delainetech.open_bazar.Category_Activity_filter;
import com.delainetech.open_bazar.Custom_classes.CTextView;
import com.delainetech.open_bazar.Location_picker_Activity;
import com.delainetech.open_bazar.Models.Category_Filter;
import com.delainetech.open_bazar.Models.Location_Picker;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

import static com.delainetech.open_bazar.Utils.Common.logg;


/**
 * Created by Kishan on 21-Dec-17.
 */

public class Category_adapter_filter extends RecyclerView.Adapter<Category_adapter_filter.Viewholder> implements ParamKeys  {

    Context mcontext;

    UserSharedPreferences preferences;
ArrayList<Category_Filter> location_pickerArrayList;

    String str="";

    public Category_adapter_filter(Context mcontext , ArrayList<Category_Filter> location_pickerArrayList) {
        this.mcontext = mcontext;
        this.location_pickerArrayList=location_pickerArrayList;
        preferences=UserSharedPreferences.getInstance(mcontext);
    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_adapter_filter_item, parent, false);

        return new Viewholder(itemView);
    }

    @Override
    public void onBindViewHolder(final Viewholder holder, final int position)
    {

        Glide.with(mcontext).load(Common.Image_Loading_Url(location_pickerArrayList.get(position).getImage())).into(holder.iv);
        holder.tv.setText(location_pickerArrayList.get(position).getTital()+" ("+location_pickerArrayList.get(position).getCount()+")");

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent();
                i.putExtra("name",location_pickerArrayList.get(position).getTital());
                i.putExtra("id",location_pickerArrayList.get(position).getId());
                ((Category_Activity_filter)mcontext).setResult(Activity.RESULT_OK,i);
                ((Category_Activity_filter)mcontext).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return location_pickerArrayList.size();
    }



    public class Viewholder extends RecyclerView.ViewHolder{
        TextView tv;
        ImageView iv;
        public Viewholder(View itemView) {
            super(itemView);
            iv=itemView.findViewById(R.id.iv);
            tv=itemView.findViewById(R.id.tv);

          }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


}


