package com.delainetech.open_bazar.Adapter;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.delainetech.open_bazar.Models.Upgrade;
import com.delainetech.open_bazar.Payment_Method_Activity;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.github.aakira.expandablelayout.ExpandableLayout;
import com.github.aakira.expandablelayout.ExpandableLayoutListenerAdapter;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.github.aakira.expandablelayout.Utils;

import java.text.DecimalFormat;
import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

import static com.delainetech.open_bazar.Utils.Common.hasPermissions;
import static com.delainetech.open_bazar.Utils.Common.logg;

public class Boost_Ads_adapter extends RecyclerView.Adapter<Boost_Ads_adapter.ViewHolder> implements ParamKeys {

    String question[]={};
    String ans[]={};
    String price[]={};
    String credites="",addid="",type="";

    int background[]={R.drawable.boost_rocket_back,R.drawable.boost_feature_drawable
            ,R.drawable.boost_repost_drawable};
    int icon[]={R.drawable.boost_ic_rocket,R.drawable.boost_ic_feature
            ,R.drawable.boost_ic_repost};


     Context context;
    private SparseBooleanArray expandState = new SparseBooleanArray();



    public Boost_Ads_adapter(String [] question, String [] ans, String [] price,String addid) {
        this.ans=ans;
        this.question=question;
        this.price=price;
        this.addid=addid;

        for (int i = 0; i < question.length; i++) {
            expandState.append(i, false);
        }
        setupd_listdata();
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        this.context = parent.getContext();
        return new ViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.boost_ads_expandable_itme_adapter, parent, false),context);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position)
    {
        holder.setIsRecyclable(false);
        holder.expandableLayout.setInRecyclerView(true);
        holder.expandableLayout.setInterpolator( Utils.createInterpolator(Utils.ACCELERATE_DECELERATE_INTERPOLATOR));
        holder.expandableLayout.setExpanded(expandState.get(position));
        holder.tv_adcount.setText(price[position]+" Views");


        holder.tv_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
               Intent i=new Intent(context, Payment_Method_Activity.class);

               i.putExtra("addid",addid);

               i.putExtra("days",holder.tv_count.getText().toString());
                i.putExtra("type",position+"");

//                if (!holder.tv_count.getText().toString().equals("30")) {

                    if (holder.tv_count.getText().toString().equals("7"))
                    {

                        credites=list.get(0).getCredits().get(position);

                        i.putExtra("credit",credites);

                    }else  if (holder.tv_count.getText().toString().equals("14"))
                    {
                        credites=list.get(1).getCredits().get(position);


                        i.putExtra("credit",credites);

                    } else if (holder.tv_count.getText().toString().equals("30")) {
                        credites=list.get(2).getCredits().get(position);

                        i.putExtra("credit",credites);
                    }
//                }
                context.startActivity(i);
            }
        });

        holder.expandableLayout.setListener(new ExpandableLayoutListenerAdapter() {
            @Override
            public void onPreOpen() {
                createRotateAnimator(holder.buttonLayout, 0f, 180f).start();
                expandState.put(position, true);
            }

            @Override
            public void onPreClose() {
                createRotateAnimator(holder.buttonLayout, 180f, 0f).start();
                expandState.put(position, false);
            }
        });

        holder.iv_img.setImageResource(icon[position]);
        holder.buttonLayout.setRotation(expandState.get(position) ? 180f : 0f);
        holder.buttonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                logg("clicked","buttonLayout");
                onClickButton(holder.expandableLayout);

            }
        });
        holder.textView.setText(question[position]);

        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                logg("clicked","textView");

                onClickButton(holder.expandableLayout);

            }
        });

        holder.ll.setBackground(context.getResources().getDrawable(background[position]));

        holder.iv_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                logg("clicked","iv_plus"+holder.tv_count.getText().toString().equals("30"));
                if (!holder.tv_count.getText().toString().equals("30")) {
                    if (holder.tv_count.getText().toString().equals("7")) {
                        holder.tv_count.setText("14");
                    } else if (holder.tv_count.getText().toString().equals("14")) {
                        holder.tv_count.setText("30");
                    }
//                    notifyDataSetChanged();

                    if (holder.tv_count.getText().toString().equals("7"))
                    {
                        String price=list.get(0).getPrice().get(position);
                        credites=list.get(0).getCredits().get(position);
                        holder.tv_price.setText("€ "+price+" Credits "+credites);
                        setperday_price(price,holder.tv_price_perday);

                    }else  if (holder.tv_count.getText().toString().equals("14"))
                    {
                        String price=list.get(1).getPrice().get(position);
                        credites=list.get(1).getCredits().get(position);

                        holder.tv_price.setText("€ "+price+" Credits "+credites);
                        setperday_price(price,holder.tv_price_perday);


                    } else if (holder.tv_count.getText().toString().equals("30")) {
                        String price=list.get(2).getPrice().get(position);
                        credites=list.get(2).getCredits().get(position);

                        holder.tv_price.setText("€ "+price+" Credits "+credites);
                        setperday_price(price,holder.tv_price_perday);

                    }
                }

            }
        });

         holder.iv_mins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                if (!holder.tv_count.getText().toString().equals("7")) {
                    if (holder.tv_count.getText().toString().equals("30")) {
                        holder.tv_count.setText("14");
                    } else if (holder.tv_count.getText().toString().equals("14")) {
                        holder.tv_count.setText("7");
                    }

                    if (holder.tv_count.getText().toString().equals("7")) {
                        String price=list.get(0).getPrice().get(position);
                        credites=list.get(0).getCredits().get(position);

                        holder.tv_price.setText("€ "+price+" Credits "+credites);
                        setperday_price(price,holder.tv_price_perday);


                    }else  if (holder.tv_count.getText().toString().equals("14")) {
                        String price=list.get(1).getPrice().get(position);
                        credites=list.get(1).getCredits().get(position);

                        holder.tv_price.setText("€ "+price+" Credits "+credites);
                        setperday_price(price,holder.tv_price_perday);


                    } else if (holder.tv_count.getText().toString().equals("30")) {
                        String price=list.get(2).getPrice().get(position);
                        credites=list.get(2).getCredits().get(position);

                        holder.tv_price.setText("€ "+price+" Credits "+credites);
                        setperday_price(price,holder.tv_price_perday);

                    }


                }

            }
        });

         holder.tv_activeads.setText(ans[position]);



        if (holder.tv_count.getText().toString().equals("7")) {
            String price=list.get(0).getPrice().get(position);
            credites=list.get(0).getCredits().get(position);
            holder.tv_price.setText("€ "+price+" Credits "+credites);
            Double perday= (Double.parseDouble(price.replace(",","."))/7);
            String p=  new DecimalFormat("##.##").format(perday);
            holder.tv_price_perday.setText("€ "+p.replace(".",",")+" Per Day");

        }else  if (holder.tv_count.getText().toString().equals("14"))
        {
            String price=list.get(1).getPrice().get(position);
            credites=list.get(1).getCredits().get(position);
            holder.tv_price.setText("€ "+price+" Credits "+credites);
            setperday_price(price,holder.tv_price_perday);


        } else if (holder.tv_count.getText().toString().equals("30"))
        {
            String price=list.get(2).getPrice().get(position);
            credites=list.get(2).getCredits().get(position);
            holder.tv_price.setText("€ "+price+" Credits "+credites);
            setperday_price(price,holder.tv_price_perday);
        }

    }

    private void onClickButton(final ExpandableLayout expandableLayout) {
        expandableLayout.toggle();
    }

    @Override
    public int getItemCount() {
        return background.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView,tv_count,tv_activeads,tv_price_perday;
        public RelativeLayout buttonLayout;
        ImageView iv_img,iv_plus,iv_mins;
        LinearLayout ll;
        TextView tv_adcount,tv_total,tv_price,tv_signin;


//        Custom_Spinner_Adapter adapter=new Custom_Spinner_Adapter(this,ar);

        /**
         * You must use the ExpandableLinearLayout in the recycler view.
         * The ExpandableRelativeLayout doesn't work.
         */

        public ExpandableLinearLayout expandableLayout;
        public ViewHolder(View v,Context context) {
            super(v);
            tv_signin = (TextView) v.findViewById(R.id.tv_signin);
             ll = (LinearLayout) v.findViewById(R.id.ll);
            iv_img = (ImageView) v.findViewById(R.id.iv_img);
            iv_mins = (ImageView) v.findViewById(R.id.iv_mins);
            iv_plus = (ImageView) v.findViewById(R.id.iv_plus);
            tv_price_perday = (TextView) v.findViewById(R.id.tv_price_perday);
            tv_price = (TextView) v.findViewById(R.id.tv_price);
            tv_total = (TextView) v.findViewById(R.id.tv_total);
            tv_activeads = (TextView) v.findViewById(R.id.tv_activeads);
            tv_count = (TextView) v.findViewById(R.id.tv_count);
            textView = (TextView) v.findViewById(R.id.textView);
            tv_adcount = (TextView) v.findViewById(R.id.tv_adcount);
            buttonLayout = (RelativeLayout) v.findViewById(R.id.button);
            expandableLayout = (ExpandableLinearLayout) v.findViewById(R.id.expandableLayout);
        }
    }

    public ObjectAnimator createRotateAnimator(final View target, final float from, final float to) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(target, "rotation", from, to);
        animator.setDuration(300);
        animator.setInterpolator(Utils.createInterpolator(Utils.LINEAR_INTERPOLATOR));
        return animator;
    }

    ArrayList<String> bronz_list_price=new ArrayList<>();
    ArrayList<String> bronz_list_credit=new ArrayList<>();

    ArrayList<Upgrade> list=new ArrayList<>();

    public  void setupd_listdata(){
        bronz_list_price.add("2,80");
        bronz_list_price.add("1,12");
        bronz_list_price.add("0,56");

        bronz_list_credit.add("2520");
        bronz_list_credit.add("1008");
        bronz_list_credit.add("504");

        list.add(new Upgrade( bronz_list_price,bronz_list_credit,"50"));

        bronz_list_price=new ArrayList<>();
        bronz_list_credit=new ArrayList<>();

        bronz_list_price.add("7,42");
        bronz_list_price.add("5,60");
        bronz_list_price.add("2,80");

        bronz_list_credit.add("6678");
        bronz_list_credit.add("5040");
        bronz_list_credit.add("2520");

        list.add(new Upgrade( bronz_list_price,bronz_list_credit,"100"));

        bronz_list_price=new ArrayList<>();
        bronz_list_credit=new ArrayList<>();

        bronz_list_price.add("15,90");
        bronz_list_price.add("9,30");
        bronz_list_price.add("6,60");

        bronz_list_credit.add("14310");
        bronz_list_credit.add("8370");
        bronz_list_credit.add("5940");

        list.add(new Upgrade( bronz_list_price,bronz_list_credit,"200"));

    }



    public void setperday_price(String price ,TextView textView){
        Double perday= (Double.parseDouble(price.replace(",","."))/7);
        String p=  new DecimalFormat("##.##").format(perday);
        textView.setText("€ "+p.replace(".",",")+" Per Day");

    }
}