package com.delainetech.open_bazar.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.delainetech.open_bazar.Adpost_Add_Activity;
import com.delainetech.open_bazar.Adpost_Edit_Activity;
import com.delainetech.open_bazar.Category.Category_Activity;
import com.delainetech.open_bazar.Models.Category_values;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.ParamKeys;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

import static com.delainetech.open_bazar.Utils.Common.logg;


/**
 * Created by Kishan on 21-Dec-17.
 */

public class Edit_Ad_Category_Preview_adapter extends RecyclerView.Adapter<Edit_Ad_Category_Preview_adapter.Viewholder> implements ParamKeys {

    Context mcontext;

//    UserSharedPreferences preferences;

    ArrayList<Category_values> categoryArrayList;
    String type;
    public Edit_Ad_Category_Preview_adapter(Context mcontext, ArrayList<Category_values> categoryArrayList) {
        this.mcontext = mcontext;
       this.categoryArrayList = categoryArrayList;

    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ad_category_preview_itemadapter, parent, false);

        return new Viewholder(itemView);
    }

    @Override
    public void onBindViewHolder(Viewholder holder, final int position)
    {
            holder.tv_catname.setText(categoryArrayList.get(position).getValue());
            holder.tv_cattype.setText(categoryArrayList.get(position).getTital());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i= new Intent(mcontext,Category_Activity.class);
                    if (position==0){
                        i.putExtra("list",categoryArrayList);
                        i.putExtra("main_cat_id", ((Adpost_Edit_Activity)mcontext).Main_cat_id);
                        ((Adpost_Edit_Activity)mcontext). startActivityForResult(i,ADS_CATEGORY_RESULT_CODE);

                    }else if (position==1){
                        i.putExtra("pos",1);
                        i.putExtra("main_cat_id", ((Adpost_Edit_Activity)mcontext).Main_cat_id);
                        i.putExtra("list",categoryArrayList);
                        ((Adpost_Edit_Activity)mcontext). startActivityForResult(i,ADS_CATEGORY_RESULT_CODE);

                    }else if (position==2){
                        i.putExtra("position",position);
                        i.putExtra("type",categoryArrayList.get(position).getTital());
                        i.putExtra("list",categoryArrayList);
                        ((Adpost_Edit_Activity)mcontext). startActivityForResult(i,ADS_CATEGORY_RESULT_CODE);

                    }else if (position>2){
                        i.putExtra("position",position);
                        i.putExtra("type",categoryArrayList.get(2).getValue());
                        i.putExtra("tital",categoryArrayList.get(position).getTital());
                        i.putExtra("list",categoryArrayList);
                        ((Adpost_Edit_Activity)mcontext). startActivityForResult(i,ADS_CATEGORY_RESULT_CODE);

                    }
                    logg("category",categoryArrayList.get(position).getDatavalue());
                }
            });

    }

    @Override
    public int getItemCount() {
        return categoryArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder{
        TextView tv_cattype,tv_catname;

        public Viewholder(View itemView) {
            super(itemView);
            tv_cattype=itemView.findViewById(R.id.tv_cattype);
            tv_catname=itemView.findViewById(R.id.tv_catname);

          }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
