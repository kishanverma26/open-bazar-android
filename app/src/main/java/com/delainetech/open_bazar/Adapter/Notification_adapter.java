package com.delainetech.open_bazar.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.aurelhubert.ahbottomnavigation.notification.AHNotification;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.delainetech.open_bazar.Ads_Detail_Activity;
import com.delainetech.open_bazar.Ads_Detail_Activity_User;
import com.delainetech.open_bazar.Home_Screen_Activity;
import com.delainetech.open_bazar.Models.Notifications;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.delainetech.open_bazar.Utils.Common.logg;
import static com.delainetech.open_bazar.Utils.ParamKeys.Type;
import static com.delainetech.open_bazar.Utils.ParamKeys.baseurl;


public class Notification_adapter extends RecyclerView.Adapter<Notification_adapter.Viewholder> {

    Context mcoxt;
    ArrayList<Notifications> list;
    RequestOptions requestOptions=new RequestOptions().error(R.drawable.userimage).centerCrop();
    Fragment fragment;
    UserSharedPreferences preferences;
    public Notification_adapter(Context mcoxt, ArrayList<Notifications> list) {
        this.mcoxt = mcoxt;
        this.list = list;
        preferences=UserSharedPreferences.getInstance(mcoxt);
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(mcoxt).inflate(R.layout.notification_item,parent,false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(Viewholder holder, final int position) {

        holder.tv_time.setText(Common.getTimeAgo(Long.parseLong(list.get(position).getTimestamp()),mcoxt));
        holder.textView.setText(list.get(position).getTital());

        Glide.with(mcoxt).load(Common.Image_Loading_Url(list.get(position).getSender_image()))
                .apply(requestOptions).into(holder.iv_userimage);

        if (list.get(position).getStatus().equals("0")){
            holder.tv_unseen.setVisibility(View.VISIBLE);
        }else {
            holder.tv_unseen.setVisibility(View.GONE);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setas_marked(list.get(position).getId());
                list.get(position).setStatus("1");
                notifyItemChanged(position);
                Intent i=new Intent(mcoxt, Ads_Detail_Activity_User.class);
                i.putExtra("id",list.get(position).getRefer_id());
                i.putExtra(Type,"notification");
                logg("values",i.getExtras().toString());
                mcoxt.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        Log.e("size",list.size()+"");
        return list.size();

    }

    public class Viewholder extends RecyclerView.ViewHolder
    {

        TextView textView,tv_time;
        ImageView iv_userimage,tv_unseen;
        public Viewholder(View itemView) {
            super(itemView);
            tv_unseen=itemView.findViewById(R.id.tv_unseen);
            textView=itemView.findViewById(R.id.tv_des);
            tv_time=itemView.findViewById(R.id.tv_time);
            iv_userimage=itemView.findViewById(R.id.iv_userimage);


        }
    }

    public void  setas_marked(final String id){

//        final DelayedProgressDialog pro=new DelayedProgressDialog();
//
//        pro.show(( (Home_Screen_Activity)mcoxt ).getSupportFragmentManager(),"");


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "notification_status", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                pro.cancel();
                logg("get_adposts",response);

                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("message").equals("success")) {
                        if (mcoxt != null) {
                            if (!preferences.getnotificationcount().equals("0")) {
                                preferences.setnotificationcount((Integer.parseInt(preferences.getnotificationcount()) - 1) + "");
                            }

                            if (!preferences.getnotificationcount().equals("0")) {
                                AHNotification notification = new AHNotification.Builder()
                                        .setText(preferences.getnotificationcount())
                                        .setBackgroundColor(ContextCompat.getColor(mcoxt, R.color.white))
                                        .setTextColor(ContextCompat.getColor(mcoxt, R.color.colorAccent))
                                        .build();
                                ((Home_Screen_Activity) mcoxt).bottomNavigation.setNotification(notification, 3);
                            } else {
                                AHNotification notification = new AHNotification.Builder()
                                        .setText("")
                                        .setBackgroundColor(ContextCompat.getColor(mcoxt, R.color.white))
                                        .setTextColor(ContextCompat.getColor(mcoxt, R.color.colorAccent))
                                        .build();
                                ((Home_Screen_Activity) mcoxt).bottomNavigation.setNotification(notification, 3);

                            }

                        }
                    }


                }
                catch (Exception e){
//                    pro.cancel();
                    Log.e("error","e",e);

                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);


            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put("type","single");
                params.put("noty_id",id);



                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        MySingleton.getInstance(context).addToRequestQueue(stringRequest);
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcoxt);
        mRequestQueue.add(stringRequest);

    }

}
