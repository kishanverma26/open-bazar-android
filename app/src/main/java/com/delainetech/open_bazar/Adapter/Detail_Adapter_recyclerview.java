package com.delainetech.open_bazar.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.delainetech.open_bazar.Models.Category_values;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.ParamKeys;

import java.util.ArrayList;

public class Detail_Adapter_recyclerview extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ParamKeys {

    Context context;
    ArrayList<Category_values> category_values;
    public Detail_Adapter_recyclerview(Context context,ArrayList<Category_values> category_values) {
        this.context = context;
        this.category_values = category_values;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.detail_item_adapter, parent, false);
        return new Viewholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        ((Viewholder)holder).tv_catname.setText(category_values.get(position).getTital());
        ((Viewholder)holder).tv_catval.setText(category_values.get(position).getValue());

    }

    @Override
    public int getItemCount() {
        return category_values.size();
    }


    public class Viewholder extends RecyclerView.ViewHolder
    {
        TextView tv_catname,tv_catval;

        public Viewholder(View itemView) {
            super(itemView);
            tv_catname=itemView.findViewById(R.id.tv_catname);
            tv_catval=itemView.findViewById(R.id.tv_catval);


        }
    }
}
