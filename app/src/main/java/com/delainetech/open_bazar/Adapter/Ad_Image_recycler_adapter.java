package com.delainetech.open_bazar.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.delainetech.open_bazar.Adpost_Add_Activity;
import com.delainetech.open_bazar.Models.Gallery;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;

import java.util.List;

public class Ad_Image_recycler_adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ParamKeys {

    Context context;
    List<Gallery> galleryMedias;
    RequestOptions requestOptions=new RequestOptions().centerCrop();
    public Ad_Image_recycler_adapter(Context context, List<Gallery> galleryMedias) {
        this.context = context;
        this.galleryMedias=galleryMedias;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_adapter_item, parent, false);
        return new Viewholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {


        Glide.with(context).load(Common.Image_Loading_Url(galleryMedias.get(position).getUri())).apply(requestOptions).into( ((Viewholder)holder).image);

        ((Viewholder) holder).iv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Adpost_Add_Activity)context).edit_image(context,position);
            }
        });

        Common.logg("boolen",galleryMedias.get(position).isIs_main()+"");
        if (galleryMedias.get(position).isIs_main()){
            ((Viewholder) holder).relative_lay.setBackground(context.getResources().getDrawable(R.drawable.red_round_border));
        }else {
            ((Viewholder) holder).relative_lay.setBackgroundResource(0);

        }

    }

    @Override
    public int getItemCount() {
        return galleryMedias.size();
    }


    public class Viewholder extends RecyclerView.ViewHolder
    {
        ImageView image,iv_edit;
        RelativeLayout relative_lay;
        public Viewholder(View itemView) {
            super(itemView);
            image=itemView.findViewById(R.id.image);
            iv_edit=itemView.findViewById(R.id.iv_edit);
            relative_lay=itemView.findViewById(R.id.relative_lay);
        }
    }
}
