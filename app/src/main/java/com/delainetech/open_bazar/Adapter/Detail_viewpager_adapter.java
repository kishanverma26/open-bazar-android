package com.delainetech.open_bazar.Adapter;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;

import com.delainetech.open_bazar.Fragments.Ads_Detail_fragment;
import com.delainetech.open_bazar.Fragments.Ads_Detail_fragment_User;
import com.delainetech.open_bazar.Models.ADS;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;

import java.util.ArrayList;

public class Detail_viewpager_adapter extends FragmentStatePagerAdapter {


    ArrayList<ADS> myList;
    UserSharedPreferences preferences;
    Context context;
    public Detail_viewpager_adapter(FragmentManager fm, ArrayList<ADS> myList, Context context)
    {
        super(fm);
       this. myList=myList;
       this. context=context;
       preferences=UserSharedPreferences.getInstance(context);
    }

    @Override
    public Fragment getItem(int position) {
        if (preferences.getuserid().equals(myList.get(position).getUser_id())){
            return new Ads_Detail_fragment_User(myList.get(position));
        }
        else {

            return new Ads_Detail_fragment(myList.get(position));
        }
        }

    @Override
    public int getCount() {
        return myList.size();
    }

    //this is called when notifyDataSetChanged() is called
    @Override
    public int getItemPosition(Object object) {
        // refresh all fragments when data set changed
        return PagerAdapter.POSITION_NONE;
    }
}
