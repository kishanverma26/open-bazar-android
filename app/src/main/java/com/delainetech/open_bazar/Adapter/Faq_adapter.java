package com.delainetech.open_bazar.Adapter;

import android.animation.ObjectAnimator;
import android.content.Context;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;

import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.github.aakira.expandablelayout.ExpandableLayout;
import com.github.aakira.expandablelayout.ExpandableLayoutListenerAdapter;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.github.aakira.expandablelayout.Utils;

import static com.delainetech.open_bazar.Utils.Common.logg;

public class Faq_adapter extends RecyclerView.Adapter<Faq_adapter.ViewHolder> implements ParamKeys {

    String question[]={};
    String ans[]={};

     Context context;
    private SparseBooleanArray expandState = new SparseBooleanArray();
    NestedScrollView nested_scroll;
    public Faq_adapter(String [] question,String [] ans,NestedScrollView nested_scroll) {
    this.ans=ans;
    this.question=question;
    this.nested_scroll=nested_scroll;

        for (int i = 0; i < question.length; i++) {
            expandState.append(i, false);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        this.context = parent.getContext();
        return new ViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.faq_expendable_list_adapter, parent, false),context);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position)
    {
        holder.setIsRecyclable(false);
        holder.expandableLayout.setInRecyclerView(true);
        holder.expandableLayout.setInterpolator( Utils.createInterpolator(Utils.ACCELERATE_DECELERATE_INTERPOLATOR));
        holder.expandableLayout.setExpanded(expandState.get(position));
//        holder.expandableLayout.setClosePosition(3);

      logg("isexpended",holder.expandableLayout.isExpanded()+"");

        holder.expandableLayout.setListener(new ExpandableLayoutListenerAdapter() {
            @Override
            public void onPreOpen() {
                createRotateAnimator(holder.buttonLayout, 0f, 180f).start();
                expandState.put(position, true);
            }

            @Override
            public void onPreClose() {
                createRotateAnimator(holder.buttonLayout, 180f, 0f).start();
                expandState.put(position, false);
            }
        });


        holder.buttonLayout.setRotation(expandState.get(position) ? 180f : 0f);
        holder.buttonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                onClickButton(holder.expandableLayout,position);
                if (holder.expandableLayout.isExpanded()){
                    holder.textView.setTextColor(context.getResources().getColor(R.color.dark_text_color));
                    holder.view.setBackground(context.getResources().getDrawable(R.drawable.triangle));
                }else {
                    holder.textView.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    holder.view.setBackground(context.getResources().getDrawable(R.drawable.triangle_red));
                }
            }
        });
        holder.textView.setText(question[position]);
        holder.tv_ans.setText(ans[position]);

        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                onClickButton(holder.expandableLayout,position);
                if (holder.expandableLayout.isExpanded()){
                    holder.textView.setTextColor(context.getResources().getColor(R.color.dark_text_color));
                    holder.view.setBackground(context.getResources().getDrawable(R.drawable.triangle));
                }else {
                    holder.textView.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    holder.view.setBackground(context.getResources().getDrawable(R.drawable.triangle_red));
                }
            }
        });

        if (holder.expandableLayout.isExpanded()){
            holder.textView.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            holder.view.setBackground(context.getResources().getDrawable(R.drawable.triangle_red));
        }else {
            holder.textView.setTextColor(context.getResources().getColor(R.color.dark_text_color));
            holder.view.setBackground(context.getResources().getDrawable(R.drawable.triangle));

        }

    }

    private void onClickButton(final ExpandableLayout expandableLayout ,int pos) {

        expandableLayout.toggle();

        if (pos==4){
            nested_scroll.post(new Runnable() {
                @Override
                public void run() {
                    nested_scroll.fullScroll(View.FOCUS_DOWN);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return question.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView,tv_ans;
        public RelativeLayout buttonLayout;
        View view;
        /**
         * You must use the ExpandableLinearLayout in the recycler view.
         * The ExpandableRelativeLayout doesn't work.
         */

        public ExpandableLinearLayout expandableLayout;
        public ViewHolder(View v,Context context) {
            super(v);
            view = (View) v.findViewById(R.id.view);
            textView = (TextView) v.findViewById(R.id.textView);
            tv_ans = (TextView) v.findViewById(R.id.tv_ans);
            buttonLayout = (RelativeLayout) v.findViewById(R.id.button);
            expandableLayout = (ExpandableLinearLayout) v.findViewById(R.id.expandableLayout);
        }
    }

    public ObjectAnimator createRotateAnimator(final View target, final float from, final float to) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(target, "rotation", from, to);
        animator.setDuration(300);
        animator.setInterpolator(Utils.createInterpolator(Utils.LINEAR_INTERPOLATOR));
        return animator;
    }
}