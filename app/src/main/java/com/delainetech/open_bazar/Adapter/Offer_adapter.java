package com.delainetech.open_bazar.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.delainetech.open_bazar.All_offers_Activity;
import com.delainetech.open_bazar.Models.Notifications;
import com.delainetech.open_bazar.Models.Offers;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import static com.delainetech.open_bazar.Utils.Common.logg;


public class Offer_adapter extends RecyclerView.Adapter<Offer_adapter.Viewholder> implements ParamKeys {

    Context mcoxt;
    ArrayList<Offers> list;
    UserSharedPreferences sharedPreferences;
    String bid_type;
    RequestOptions requestOptions=new RequestOptions().centerCrop();
    public Offer_adapter(Context mcoxt, ArrayList<Offers> list,String bid_type) {
        this.mcoxt = mcoxt;
        this.list = list;
        this.bid_type = bid_type;
        sharedPreferences=UserSharedPreferences.getInstance(mcoxt);
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(mcoxt).inflate(R.layout.offer_item,parent,false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(Viewholder holder, final int position) {
        if (bid_type.equals("0")) {
            holder.tv_price.setText("€ " + list.get(position).getBid_price());
            holder.  lastmsg.setText("Offer:");
        }else {
            holder.tv_price.setText(list.get(position).getBid_price());
          holder.  lastmsg.setText("Comment:");
        }
        holder.tv_time.setText(Common.getTimeAgo(Long.parseLong(list.get(position).getTimestamp()),mcoxt));

        if (list.get(position).getUser_id().equals(sharedPreferences.getuserid())){
            holder.iv_delete.setVisibility(View.VISIBLE);
        }else {
            holder.iv_delete.setVisibility(View.GONE);
        }

        holder.iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert(position);
            }
        });

        holder.tv_username.setText(list.get(position).getUser_name());
        Glide.with(mcoxt).load(Common.Image_Loading_Url(list.get(position).getUser_image())).apply(requestOptions).into( holder.iv_circle);

   }

    @Override
    public int getItemCount() {
        Log.e("size",list.size()+"");
        return list.size();

    }

    public class Viewholder extends RecyclerView.ViewHolder
    {

        ImageView iv_circle,iv_delete;
        TextView tv_username,tv_price,tv_time,lastmsg;
        public Viewholder(View itemView) {
            super(itemView);
            iv_circle=itemView.findViewById(R.id.iv_circle);
            tv_username=itemView.findViewById(R.id.tv_username);
            tv_price=itemView.findViewById(R.id.tv_price);
            tv_time=itemView.findViewById(R.id.tv_time);
            iv_delete=itemView.findViewById(R.id.iv_delete);
            lastmsg=itemView.findViewById(R.id.lastmsg);



        }
    }


    public void alert(final int pos){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mcoxt);

        alertDialog.setMessage("Are your sure you want to delete?");
        alertDialog.setPositiveButton(mcoxt.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                offer_bid(list.get(pos).getId(),((All_offers_Activity)mcoxt).id);
               list.remove(pos);
                ((All_offers_Activity)mcoxt).delete();
               notifyDataSetChanged();
            }
        });
        alertDialog.setNegativeButton(mcoxt.getResources().getString(R.string.No), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog1= alertDialog.create();
        alertDialog1.show();

        alertDialog1.getButton(alertDialog1.BUTTON_NEGATIVE).setTextColor(mcoxt.getResources().getColor(R.color.colorAccent));
        alertDialog1.getButton(alertDialog1.BUTTON_POSITIVE).setTextColor(mcoxt.getResources().getColor(R.color.colorAccent));
    }



    public void  offer_bid(final String id,final String postid){


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "delete_offers", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                pro.cancel();
                logg("insert_ads",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){



                    }

                }
                catch (Exception e){
//                    pro.cancel();
                    Log.e("error","e",e);
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+sharedPreferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(Id,id);
                params.put(Post_id,postid);

                Log.e("params",params+"");
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcoxt);
        mRequestQueue.add(stringRequest);

    }


}
