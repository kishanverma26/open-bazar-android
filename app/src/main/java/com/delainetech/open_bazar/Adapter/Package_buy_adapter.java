package com.delainetech.open_bazar.Adapter;

import android.animation.ObjectAnimator;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.github.aakira.expandablelayout.ExpandableLayout;
import com.github.aakira.expandablelayout.ExpandableLayoutListenerAdapter;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.github.aakira.expandablelayout.Utils;

import static com.delainetech.open_bazar.Utils.Common.logg;

public class Package_buy_adapter extends RecyclerView.Adapter<Package_buy_adapter.ViewHolder> implements ParamKeys {

    String question[]={};
    String ans[]={};
    String price[]={};

    int background[]={R.drawable.buy_standard_background_color,R.drawable.buy_advance_pack
            ,R.drawable.buy_premium_pack,R.drawable.buy_suprim_pack};
     Context context;
    private SparseBooleanArray expandState = new SparseBooleanArray();

    public Package_buy_adapter(String [] question, String [] ans, String [] price) {
    this.ans=ans;
    this.question=question;
    this.price=price;

        for (int i = 0; i < question.length; i++) {
            expandState.append(i, false);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        this.context = parent.getContext();
        return new ViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.pakage_expandable_itme_adapter, parent, false),context);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position)
    {
        holder.setIsRecyclable(false);
        holder.expandableLayout.setInRecyclerView(true);
        holder.expandableLayout.setInterpolator( Utils.createInterpolator(Utils.ACCELERATE_DECELERATE_INTERPOLATOR));
        holder.expandableLayout.setExpanded(expandState.get(position));
//        holder.expandableLayout.setClosePosition(3);
        holder.tv_price.setText(price[position]);
      logg("isexpended",holder.expandableLayout.isExpanded()+"");

        holder.expandableLayout.setListener(new ExpandableLayoutListenerAdapter() {
            @Override
            public void onPreOpen() {
                createRotateAnimator(holder.buttonLayout, 0f, 180f).start();
                expandState.put(position, true);
            }

            @Override
            public void onPreClose() {
                createRotateAnimator(holder.buttonLayout, 180f, 0f).start();
                expandState.put(position, false);
            }
        });


        holder.buttonLayout.setRotation(expandState.get(position) ? 180f : 0f);
        holder.buttonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                onClickButton(holder.expandableLayout);

            }
        });
        holder.textView.setText(question[position]);
        holder.tv_ans.setText(ans[position]);

        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                onClickButton(holder.expandableLayout);

            }
        });

        holder.ll.setBackground(context.getResources().getDrawable(background[position]));

    }

    private void onClickButton(final ExpandableLayout expandableLayout) {

        expandableLayout.toggle();


    }

    @Override
    public int getItemCount() {
        return background.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView,tv_ans,tv_price;
        public RelativeLayout buttonLayout;
        LinearLayout ll;
        /**
         * You must use the ExpandableLinearLayout in the recycler view.
         * The ExpandableRelativeLayout doesn't work.
         */

        public ExpandableLinearLayout expandableLayout;
        public ViewHolder(View v,Context context) {
            super(v);
            ll = (LinearLayout) v.findViewById(R.id.ll);
            textView = (TextView) v.findViewById(R.id.textView);
            tv_ans = (TextView) v.findViewById(R.id.tv_ans);
            tv_price = (TextView) v.findViewById(R.id.tv_price);
            buttonLayout = (RelativeLayout) v.findViewById(R.id.button);
            expandableLayout = (ExpandableLinearLayout) v.findViewById(R.id.expandableLayout);
        }
    }

    public ObjectAnimator createRotateAnimator(final View target, final float from, final float to) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(target, "rotation", from, to);
        animator.setDuration(300);
        animator.setInterpolator(Utils.createInterpolator(Utils.LINEAR_INTERPOLATOR));
        return animator;
    }
}