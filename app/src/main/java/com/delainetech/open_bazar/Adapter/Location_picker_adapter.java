package com.delainetech.open_bazar.Adapter;

import android.app.Activity;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.delainetech.open_bazar.Custom_classes.CTextView;
import com.delainetech.open_bazar.Location_picker_Activity;
import com.delainetech.open_bazar.Models.Location_Picker;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;

import java.util.ArrayList;

import static com.delainetech.open_bazar.Utils.Common.logg;


/**
 * Created by Kishan on 21-Dec-17.
 */

public class Location_picker_adapter extends RecyclerView.Adapter<Location_picker_adapter.Viewholder> implements ParamKeys ,Filterable {

    Context mcontext;

    UserSharedPreferences preferences;
ArrayList<Location_Picker> location_pickerArrayList;
    ArrayList<Location_Picker> mStringFilterList;

    ValueFilter valueFilter;
    String str="";

    public Location_picker_adapter(Context mcontext ,ArrayList<Location_Picker> location_pickerArrayList) {
        this.mcontext = mcontext;
        this.location_pickerArrayList=location_pickerArrayList;
        this.mStringFilterList=location_pickerArrayList;
        preferences=UserSharedPreferences.getInstance(mcontext);
    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.location_picker_item_adapter, parent, false);

        return new Viewholder(itemView);
    }

    @Override
    public void onBindViewHolder(final Viewholder holder, final int position)
    {
        if (location_pickerArrayList.get(position).isSelect()){
            holder.tv_city.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_location_seleted,0);
            logg("location",position+"");
            logg("location",location_pickerArrayList.get(position).getTital()+"");

            preferences.setaddress(location_pickerArrayList.get(position).getTital());

        }else {
            holder.tv_city.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);

        }

        holder.tv_city.setText(location_pickerArrayList.get(position).getTital());
        holder.tv_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i=0;i<location_pickerArrayList.size();i++){
                    location_pickerArrayList.get(i).setSelect(false);

                }
                location_pickerArrayList.get(position).setSelect(true);
                str=location_pickerArrayList.get(position).getTital();
                notifyDataSetChanged();
                   preferences.setaddress(location_pickerArrayList.get(position).getTital());
                   Intent i=new Intent();
                   i.putExtra("city",str);
                ((Location_picker_Activity)mcontext).setResult(Activity.RESULT_OK,i);
                ((Location_picker_Activity)mcontext).finish();
           }
        });

    }

    @Override
    public int getItemCount() {
        return location_pickerArrayList.size();
    }



    public class Viewholder extends RecyclerView.ViewHolder{
        CTextView tv_city;
        public Viewholder(View itemView) {
            super(itemView);
            tv_city=itemView.findViewById(R.id.tv_city);
          }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


// Filter
    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }



    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint)
        {
            Log.e("constraint",constraint+"");
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0)
            {
                ArrayList<Location_Picker> filterList = new ArrayList<>();
                for (int i = 0; i < mStringFilterList.size(); i++)
                {
                    if(mStringFilterList.get(i).getTital().toUpperCase().contains(constraint.toString().toUpperCase())) {
                        filterList.add(new Location_Picker( mStringFilterList.get(i).getTital(),mStringFilterList.get(i).isSelect()));
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            }
            else
            {
                results.count = mStringFilterList.size();
                results.values = mStringFilterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results)
        {
            Log.e("resultfiltered",results.count+""+results.values);
            if(results.count>0) {
                location_pickerArrayList = (ArrayList<Location_Picker>) results.values;
            }
            else
            {

            }
            notifyDataSetChanged();

        }

    }


}


