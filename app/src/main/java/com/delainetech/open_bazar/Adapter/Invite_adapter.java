package com.delainetech.open_bazar.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.delainetech.open_bazar.Invite_Activity;
import com.delainetech.open_bazar.Models.Contracts;
import com.delainetech.open_bazar.Models.Following;
import com.delainetech.open_bazar.Other_User_Profile;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.SQL.Databasehelper;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.delainetech.open_bazar.Utils.Common.logg;


/**
 * Created by Kishan on 21-Dec-17.
 */

public class Invite_adapter extends RecyclerView.Adapter<Invite_adapter.Viewholder> implements ParamKeys {

    Context mcontext;

    UserSharedPreferences preferences;
    ArrayList<Contracts> followingArrayList;
    RequestOptions requestOptions=new RequestOptions().centerCrop().error(R.drawable.userimage);
    Databasehelper databasehelper;

    public Invite_adapter(Context mcontext, ArrayList<Contracts> followingArrayList) {
        this.mcontext = mcontext;
        this.followingArrayList = followingArrayList;
        preferences=UserSharedPreferences.getInstance(mcontext);
        databasehelper=new Databasehelper(mcontext);
    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.invitefriend_item_adapter, parent, false);

        return new Viewholder(itemView);
    }

    @Override
    public void onBindViewHolder(final Viewholder holder, final int position)
    {
        holder.tv_name.setText(followingArrayList.get(position).getName());
        Glide.with(mcontext).load(Common.Image_Loading_Url(followingArrayList.get(position).getImage()))
                .apply(requestOptions).into( holder.userimage);

            holder.follow_bt.setWidth(350);
            holder.follow_bt.setText("Invite & Follow");

            if (followingArrayList.get(position).getStatus().equals("1")){
                holder.follow_bt.setText("Following");

                holder.follow_bt.setTextColor(mcontext.getResources().getColor(R.color.colorAccent));
                holder.follow_bt.setBackground(mcontext.getResources().getDrawable(R.drawable.follow_bt_border));

            }else {
                holder.follow_bt.setText("Invite & Follow");

            }
        holder.tv_post.setVisibility(View.GONE);
        holder.follow_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            databasehelper.update_value("1",followingArrayList.get(position).getId());
            followingArrayList.get(position).setStatus("1");
            holder.follow_bt.setText("Following");
                holder.follow_bt.setTextColor(mcontext.getResources().getColor(R.color.colorAccent));
                holder.follow_bt.setBackground(mcontext.getResources().getDrawable(R.drawable.follow_bt_border));

                create_dynamic_link("reffer/"+preferences.getuserid(),followingArrayList.get(position).getEmail(),followingArrayList.get(position).getName());
            }
        });


    }

    @Override
    public int getItemCount() {
        logg("size11",followingArrayList.size()+"00");
        return followingArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder{
        TextView tv_name,tv_post;
        RelativeLayout rv;
        Button follow_bt;
        CircleImageView userimage;
        public Viewholder(View itemView) {
            super(itemView);
            tv_post=itemView.findViewById(R.id.tv_post);
            follow_bt=itemView.findViewById(R.id.follow_bt);
            tv_name=itemView.findViewById(R.id.tv_name);
            rv=itemView.findViewById(R.id.rv);
            userimage=itemView.findViewById(R.id.userimage);

        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void  follow_user(final String email, final String name, final String link){


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "invitation_email", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                pro.cancel();
                logg("follow",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){


                    }

                }
                catch (Exception e){
//                    pro.cancel();
                    Log.e("error","e",e);
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(Email,email);
                params.put("sender",preferences.getname());
                params.put("receiver",name);
                params.put("link",link);
                Log.e("params",params+"");
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }

    //--------------------------------------dynamic linkin


    public void create_dynamic_link(String str,final String email,final String name){

        Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse("https://openbazar.page.link?="+str))
                .setDomainUriPrefix("https://openbazar.page.link")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder("com.open_bazar")
                        .setFallbackUrl(Uri.parse("https://www.dropbox.com/s/o1z7e9w94g7twqz/app-debug.apk?dl=0"))
                        .build())
                // Set parameters
                // ...
                .buildShortDynamicLink()

                .addOnCompleteListener((Invite_Activity)mcontext, new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {
                            // Short link created
                            Uri shortLink = task.getResult().getShortLink();
                            Uri flowchartLink = task.getResult().getPreviewLink();

                            logg("shortLink",shortLink+" flowchartLink"+flowchartLink);


                           follow_user(email,name,shortLink.toString());

                        } else {
                            // Error
                            // ...
                            Toast.makeText(mcontext, "Somthing went wrong please try again", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


}
