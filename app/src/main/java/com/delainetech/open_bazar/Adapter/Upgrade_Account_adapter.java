package com.delainetech.open_bazar.Adapter;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.delainetech.open_bazar.Models.Upgrade;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.github.aakira.expandablelayout.ExpandableLayout;
import com.github.aakira.expandablelayout.ExpandableLayoutListenerAdapter;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.github.aakira.expandablelayout.Utils;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

import static com.delainetech.open_bazar.Utils.Common.logg;

public class Upgrade_Account_adapter extends RecyclerView.Adapter<Upgrade_Account_adapter.ViewHolder> implements ParamKeys {

    String question[]={};
    String ans[]={};
    String price[]={};

    int background[]={R.drawable.upgread_account_bronze,R.drawable.upgread_account_silver
            ,R.drawable.upgread_account_gold,R.drawable.upgread_account_diamond};
    int icon[]={R.drawable.upgrade_account_bronze,R.drawable.upgrade_account_silver
            ,R.drawable.upgrade_account_gold,R.drawable.upgrade_account_diamond};

    String getmorestrs[]={"Get More Active Ads","Get More Views and Likes","Shape your financial future","Create a solid network"};
     Context context;
    private SparseBooleanArray expandState = new SparseBooleanArray();
    int credit_pos=0,price_pos=0;

    public Upgrade_Account_adapter(String [] question, String [] ans, String [] price) {
        this.ans=ans;
        this.question=question;
        this.price=price;

        for (int i = 0; i < question.length; i++) {
            expandState.append(i, false);
        }
        setupd_listdata();
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        this.context = parent.getContext();
        return new ViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.upgrade_account_expandable_itme_adapter, parent, false),context);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position)
    {
        holder.setIsRecyclable(false);
        holder.expandableLayout.setInRecyclerView(true);
        holder.expandableLayout.setInterpolator( Utils.createInterpolator(Utils.ACCELERATE_DECELERATE_INTERPOLATOR));
        holder.expandableLayout.setExpanded(expandState.get(position));
//        holder.expandableLayout.setClosePosition(3);
        logg("price_pos"+position,list.get(position).getPrice().get(price_pos));
        holder.tv_price.setText("€ "+ list.get(position).getPrice().get(price_pos)+" Credits "+ list.get(position).getCredits().get(credit_pos));
      logg("isexpended",holder.expandableLayout.isExpanded()+"");

        holder.expandableLayout.setListener(new ExpandableLayoutListenerAdapter() {
            @Override
            public void onPreOpen() {
                createRotateAnimator(holder.buttonLayout, 0f, 180f).start();
                expandState.put(position, true);
            }

            @Override
            public void onPreClose() {
                createRotateAnimator(holder.buttonLayout, 180f, 0f).start();
                expandState.put(position, false);
            }
        });

        holder.iv_img.setImageResource(icon[position]);
        holder.buttonLayout.setRotation(expandState.get(position) ? 180f : 0f);
        holder.buttonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                onClickButton(holder.expandableLayout);

            }
        });
        holder.tv_getmore.setText(getmorestrs[position]);

        holder.textView.setText(question[position]);

        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                onClickButton(holder.expandableLayout);

            }
        });

        holder.ll.setBackground(context.getResources().getDrawable(background[position]));

        holder.spinner.setAdapter(new Custom_Spinner_Adapter_upgrade_account(context,ans,R.color.white));

            holder.tv_adcount.setText(list.get(position).getLive_ads()+" Ads");
        holder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int in, long id) {
                holder.tv_price.setText("€ "+ list.get(position).getPrice().get(in)+" Credits "+ list.get(position).getCredits().get(in));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void onClickButton(final ExpandableLayout expandableLayout) {

        expandableLayout.toggle();


    }

    @Override
    public int getItemCount() {
        return background.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView,tv_price;
        public RelativeLayout buttonLayout;
        ImageView iv_img;
        LinearLayout ll;
        TextView tv_adcount;
        Spinner spinner;
        String ar[]={"Price Types","Price", "Ask For Price", "Exchange", "Free"};
        TextView tv_getmore;
//        Custom_Spinner_Adapter adapter=new Custom_Spinner_Adapter(this,ar);

        /**
         * You must use the ExpandableLinearLayout in the recycler view.
         * The ExpandableRelativeLayout doesn't work.
         */

        public ExpandableLinearLayout expandableLayout;
        public ViewHolder(View v,Context context) {
            super(v);
            spinner = (Spinner) v.findViewById(R.id.spinner);
            tv_getmore = (TextView) v.findViewById(R.id.tv_getmore);
            ll = (LinearLayout) v.findViewById(R.id.ll);
            iv_img = (ImageView) v.findViewById(R.id.iv_img);
            textView = (TextView) v.findViewById(R.id.textView);
            tv_adcount = (TextView) v.findViewById(R.id.tv_adcount);
            tv_price = (TextView) v.findViewById(R.id.tv_price);
            buttonLayout = (RelativeLayout) v.findViewById(R.id.button);
            expandableLayout = (ExpandableLinearLayout) v.findViewById(R.id.expandableLayout);
        }
    }

    public ObjectAnimator createRotateAnimator(final View target, final float from, final float to) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(target, "rotation", from, to);
        animator.setDuration(300);
        animator.setInterpolator(Utils.createInterpolator(Utils.LINEAR_INTERPOLATOR));
        return animator;
    }

    ArrayList<String> bronz_list_price=new ArrayList<>();
      ArrayList<String> bronz_list_credit=new ArrayList<>();

    ArrayList<Upgrade> list=new ArrayList<>();

    public  void setupd_listdata(){
        bronz_list_price.add("6,49");
        bronz_list_price.add("15,49");
        bronz_list_price.add("21,30");
        bronz_list_price.add("33,49");

        bronz_list_credit.add("5841");
        bronz_list_credit.add("13941");
        bronz_list_credit.add("19170");
        bronz_list_credit.add("30141");

        list.add(new Upgrade( bronz_list_price,bronz_list_credit,"50"));

        bronz_list_price=new ArrayList<>();
        bronz_list_credit=new ArrayList<>();

        bronz_list_price.add("9,25");
        bronz_list_price.add("19,75");
        bronz_list_price.add("29,30");
        bronz_list_price.add("41,49");

        bronz_list_credit.add("8325");
        bronz_list_credit.add("17775");
        bronz_list_credit.add("26370");
        bronz_list_credit.add("37341");

        list.add(new Upgrade( bronz_list_price,bronz_list_credit,"100"));

        bronz_list_price=new ArrayList<>();
        bronz_list_credit=new ArrayList<>();

        bronz_list_price.add("11,10");
        bronz_list_price.add("25,90");
        bronz_list_price.add("35,49");
        bronz_list_price.add("55,60");

        bronz_list_credit.add("9990");
        bronz_list_credit.add("23310");
        bronz_list_credit.add("31941");
        bronz_list_credit.add("50040");

        list.add(new Upgrade( bronz_list_price,bronz_list_credit,"200"));

        bronz_list_price=new ArrayList<>();
        bronz_list_credit=new ArrayList<>();

        bronz_list_price.add("14,99");
        bronz_list_price.add("29,90");
        bronz_list_price.add("43,49");
        bronz_list_price.add("92,49");

        bronz_list_credit.add("13491");
        bronz_list_credit.add("26910");
        bronz_list_credit.add("39141");
        bronz_list_credit.add("83241");

        list.add(new Upgrade( bronz_list_price,bronz_list_credit,"500"));

    }
}