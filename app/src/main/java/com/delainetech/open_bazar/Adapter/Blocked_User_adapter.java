package com.delainetech.open_bazar.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.delainetech.open_bazar.Models.Following;
import com.delainetech.open_bazar.Other_User_Profile;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.delainetech.open_bazar.Utils.Common.logg;


/**
 * Created by Kishan on 21-Dec-17.
 */

public class Blocked_User_adapter extends RecyclerView.Adapter<Blocked_User_adapter.Viewholder> implements ParamKeys {

    Context mcontext;

    UserSharedPreferences preferences;
    ArrayList<Following> followingArrayList;
    RequestOptions requestOptions=new RequestOptions().centerCrop().error(R.drawable.userimage);

    public Blocked_User_adapter(Context mcontext, ArrayList<Following> followingArrayList) {
        this.mcontext = mcontext;
        this.followingArrayList = followingArrayList;
        preferences=UserSharedPreferences.getInstance(mcontext);
        mRootRef = FirebaseDatabase.getInstance().getReference();


    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.following_item_adapter, parent, false);

        return new Viewholder(itemView);
    }

    @Override
    public void onBindViewHolder(final Viewholder holder, final int position)
    {
        holder.tv_name.setText(followingArrayList.get(position).getName());
        Glide.with(mcontext).load(Common.Image_Loading_Url(followingArrayList.get(position).getProfile_image()))
                .apply(requestOptions).into( holder.userimage);
        if ((followingArrayList.get(position).getFollowstatus().length()>0)) {
            if (followingArrayList.get(position).getFollowstatus().equals("YES")) {
                holder.follow_bt.setText("Blocked");
                holder.follow_bt.setBackground(mcontext.getResources().getDrawable(R.drawable.follow_bt_border));
                holder.follow_bt.setTextColor(mcontext.getResources().getColor(R.color.colorAccent));
            } else if (followingArrayList.get(position).getFollowstatus().equals("NO")) {
                holder.follow_bt.setText("Blocked");
                holder.follow_bt.setBackground(mcontext.getResources().getDrawable(R.drawable.button_red_border));
            }
        }else {
            holder.follow_bt.setText("Blocked");
            holder.follow_bt.setBackground(mcontext.getResources().getDrawable(R.drawable.follow_bt_border));
            holder.follow_bt.setTextColor(mcontext.getResources().getColor(R.color.colorAccent));

        }
        holder.tv_post.setText(followingArrayList.get(position).getAds_count()+" Post");
        holder.tv_post.setVisibility(View.GONE);
        holder.follow_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                follow_user(followingArrayList.get(position).getFollowing_id());
                followingArrayList.remove(position);
                notifyDataSetChanged();
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii=new Intent(mcontext, Other_User_Profile.class);
                ii.putExtra("id",followingArrayList.get(position).getFollowing_id());
               mcontext. startActivity(ii);
            }
        });

    }

    @Override
    public int getItemCount() {
        return followingArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder{
        TextView tv_name,tv_post;
        RelativeLayout rv;
        Button follow_bt;
        CircleImageView userimage;
        public Viewholder(View itemView) {
            super(itemView);
            tv_post=itemView.findViewById(R.id.tv_post);
            follow_bt=itemView.findViewById(R.id.follow_bt);
            tv_name=itemView.findViewById(R.id.tv_name);
            rv=itemView.findViewById(R.id.rv);
            userimage=itemView.findViewById(R.id.userimage);

        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private DatabaseReference mRootRef;
    public void  follow_user(final String id){

        final String mChatUser="user"+id;
        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "block_user", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                pro.cancel();
                logg("follow",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("true")){

                        mRootRef.child("Block").child("user"+preferences.getuserid()).child(mChatUser).child("blocked"+mChatUser).setValue(false);
                        mRootRef.child("Block").child(mChatUser).child("user"+preferences.getuserid()).child("blocked"+mChatUser).setValue(false);

                    }

                }
                catch (Exception e){
//                    pro.cancel();
                    Log.e("error","e",e);
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(User_id,preferences.getuserid());
                params.put("block_id",id);
                params.put("type","unblock");

                Log.e("params",params+"");
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }

}
