package com.delainetech.open_bazar.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.delainetech.open_bazar.Detail_Screen_Activity;
import com.delainetech.open_bazar.Fragments.Main_Home_Fragment;
import com.delainetech.open_bazar.Models.ADS;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;
import com.facebook.drawee.view.SimpleDraweeView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import static com.delainetech.open_bazar.Utils.Common.logg;

public class Similar_Ads_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ParamKeys {

    Context context;
    ArrayList<ADS> adsArrayList;
    RequestOptions options=new RequestOptions().error(R.drawable.home_empty_view).centerCrop();
    UserSharedPreferences preferences;
    public Similar_Ads_Adapter(Context context, ArrayList<ADS> adsArrayList) {
        this.context = context;
        this.adsArrayList = adsArrayList;
        preferences=UserSharedPreferences.getInstance(context);
    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.similar_ads_item_grid, parent, false);
        return new Viewholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

         ((Viewholder) holder).tv_price.setText("€ "+adsArrayList.get(position).getPrice());


        if (adsArrayList.get(position).getPrice_type().equals("price")){
            ((Viewholder) holder).tv_price.setText("€ "+adsArrayList.get(position).getPrice());
        }else  if (adsArrayList.get(position).getPrice_type().equals("ask for price")){
            ((Viewholder) holder).tv_price.setText("Ask For Price");
        }else  if (adsArrayList.get(position).getPrice_type().equals("exchange")){
            ((Viewholder) holder).tv_price.setText("Exchange");
        }else  if (adsArrayList.get(position).getPrice_type().equals("free")){
            ((Viewholder) holder).tv_price.setText("Free");
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(context, Detail_Screen_Activity.class);
                i.putExtra("pos",position);
                i.putExtra("list",adsArrayList);
                context.startActivity(i);

            }
        });


//        load image
        if (adsArrayList.get(position).getImages().length()>0) {
            try {
//                logg("images", adsArrayList.get(position).getImages().length() + "");
                JSONArray jsonArray = new JSONArray(adsArrayList.get(position).getImages());
                Glide.with(context).load(Common.Image_Loading_Url(jsonArray.getJSONObject(0).getString("image"))).apply(options).into(((Viewholder) holder).image);
//                logg("image", Common.Image_Loading_Url(jsonArray.getJSONObject(0).getString("image0")) + "");
            } catch (JSONException e) {
                e.printStackTrace();
                logg("excep", e + "");
            }
        }else {
            Glide.with(context).load("ab").apply(options).into(((Viewholder) holder).image);

        }
    }

    @Override
    public int getItemCount() {
        return adsArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Viewholder extends RecyclerView.ViewHolder
    {
        TextView tv_price;

        SimpleDraweeView image;
        public Viewholder(View itemView) {
            super(itemView);
            image=itemView.findViewById(R.id.folder_cover_image);
            tv_price=itemView.findViewById(R.id.tv_price);


        }
    }


}
