package com.delainetech.open_bazar.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.delainetech.open_bazar.R;

import java.util.List;

import androidx.annotation.NonNull;

public class Custom_Spinner_Adapter extends BaseAdapter {

    Context context;
    String name[];
    int color=0;


    public Custom_Spinner_Adapter(Context context, String[] name,int color) {
        this.context = context;
        this.name = name;
        this.color = color;


    }

    @Override
    public int getCount() {
        return name.length;
    }

    @Override
    public Object getItem(int position) {
        return name[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater =(LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        convertView = inflater.inflate(R.layout.spinner_item_adapter, null);
        TextView names = (TextView) convertView.findViewById(R.id.text);
        names.setText(name[position]);
        names.setTextColor(context.getResources().getColor(color));

        return convertView;
    }
}
