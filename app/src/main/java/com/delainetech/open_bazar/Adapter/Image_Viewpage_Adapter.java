package com.delainetech.open_bazar.Adapter;

import android.app.Dialog;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.bumptech.glide.request.RequestOptions;
import com.delainetech.open_bazar.Custom_classes.HackyViewPager;
import com.delainetech.open_bazar.Drawer_Frame_Activity;
import com.delainetech.open_bazar.Models.ADS;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;

import java.security.MessageDigest;
import java.util.ArrayList;

import static com.delainetech.open_bazar.Utils.Common.logg;


/**
 * Created by Swt on 9/25/2017.
 */

public class Image_Viewpage_Adapter extends PagerAdapter implements ParamKeys {
    private Context context;
   // int images[];
   private ArrayList<String> postimages = new ArrayList<>();
    private LayoutInflater layoutInflater;
    ADS ads;
    UserSharedPreferences preferences;

    RequestOptions requestOptions=new RequestOptions().transform(new BitmapTransformation() {
        @Override
        public void updateDiskCacheKey(@NonNull MessageDigest messageDigest) {

        }

        @Override
        protected Bitmap transform(BitmapPool pool,
                                   Bitmap toTransform, int outWidth,
                                   int outHeight) {
            logg("outWidth=",outWidth+" ,toTransform.getWidth()="+toTransform.getWidth());
            return Bitmap.createBitmap(toTransform, 0, 0,
                    toTransform.getWidth(), toTransform.getWidth());

        }
    }).fitCenter();
    public Image_Viewpage_Adapter(Context context, ArrayList<String> postimages ,ADS ads) {
        this.context = context;
        this.ads = ads;
        this.postimages = postimages;
        layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        preferences=UserSharedPreferences.getInstance(context);
    }

    @Override
    public int getCount() {
        return postimages.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
       View itemView = layoutInflater.inflate(R.layout.mediadialogadapter, container, false);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.image);
        Glide.with(context).load(Common.Image_Loading_Url(postimages.get(position))).apply(requestOptions).into(imageView);

         container.addView(itemView);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaDialog(postimages,context);
            }
        });
        return itemView;


    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

    private void MediaDialog(ArrayList<String> media, final Context c) {
        final Dialog dialog = new Dialog(c, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.setContentView(R.layout.dialog_mediadialoglayout);
        HackyViewPager rview = (HackyViewPager) dialog.findViewById(R.id.media_rv);
        TextView tv_like=(TextView) dialog.findViewById(R.id.tv_like);
        ImageView iv_close=(ImageView)dialog.findViewById(R.id.iv_close);
        ImageView iv_report=(ImageView)dialog.findViewById(R.id.iv_report);
        final TextView tv_imagecount=(TextView)dialog.findViewById(R.id.tv_imagecount);
        Image_Viewpage_Adapter_full_screen adapter = new Image_Viewpage_Adapter_full_screen(c, media);
        rview.setAdapter(adapter);
        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
        tv_imagecount.setText("1 / "+postimages.size());
        rview.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    tv_imagecount.setText((position + 1) + " / " + postimages.size());

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

            if (ads.getUser_id().equals(preferences.getuserid())){
                iv_report.setVisibility(View.GONE);
            }
        iv_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(c, Drawer_Frame_Activity.class);
                intent.putExtra(Pos,14);
                intent.putExtra("type","ads");
                intent.putExtra(Title,"Ad Report");
                intent.putExtra("id",ads.getId());

               c. startActivity(intent);
            }
        });

        tv_like.setText(ads.getLike_count());

        dialog.show();
    }


    @Override
    public int getItemPosition(@NonNull Object object) {
        return super.getItemPosition(object);
    }
}
