package com.delainetech.open_bazar;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.aurelhubert.ahbottomnavigation.notification.AHNotification;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.delainetech.open_bazar.Chat.Chat_Inbox_Frag_new;
import com.delainetech.open_bazar.Custom_classes.DelayedProgressDialog;
import com.delainetech.open_bazar.Custom_classes.MySingleton;
import com.delainetech.open_bazar.Utils.PushService;
import com.facebook.accountkit.AccountKit;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;
import io.paperdb.Paper;

import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.delainetech.open_bazar.Fragments.Main_Home_Fragment;
import com.delainetech.open_bazar.Fragments.Notification_Screen_Fragment;
import com.delainetech.open_bazar.Fragments.Profile_Fragment;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

import java.util.HashMap;
import java.util.Map;

public class Home_Screen_Activity extends AppCompatActivity implements ParamKeys,NavigationView.OnNavigationItemSelectedListener
        ,View.OnClickListener,AHBottomNavigation.OnTabSelectedListener , LifecycleObserver
{
    String TAG="Home_Screen_Activity";
    Context mcoxt;
    FrameLayout frameLayout;
    UserSharedPreferences preferences,userSharedPreferences;
    String numbers="",dataValue="";

    //navigation drawer
    NavigationView navigationView;
    public   DrawerLayout drawer;

    //header image and text
    ImageView profile;
    TextView tvname;

    //bottomnavigation
    ImageView iv_home,iv_chat,iv_notification,iv_profile;
    ImageView tv_postad;
    TextView tv_home,tv_chat,tv_notice,tv_profile,tv_username;
    public FloatingActionButton fab_add_ad;
    RelativeLayout rl_home,rl_chat,rl_notice,rl_profile;

    BottomNavigationView bottom_navigation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);
        mcoxt=Home_Screen_Activity.this;
        mRootRef = FirebaseDatabase.getInstance().getReference();
        initializ();

    }

    private void initializ() {
        //bottom navigation
        preferences=new UserSharedPreferences(mcoxt);
        userSharedPreferences=new UserSharedPreferences(mcoxt,"");
        rl_home=(RelativeLayout)findViewById(R.id.rl_home);
        rl_chat=(RelativeLayout)findViewById(R.id.rl_chat);
        rl_notice=(RelativeLayout)findViewById(R.id.rl_notice);
        rl_profile=(RelativeLayout)findViewById(R.id.rl_profile);
        fab_add_ad=(FloatingActionButton) findViewById(R.id.fab_add_ad);



        tv_home=(TextView)findViewById(R.id.tv_home);
        tv_notice=(TextView)findViewById(R.id.tv_notice);
        tv_profile=(TextView)findViewById(R.id.tv_profile);
        tv_chat=(TextView)findViewById(R.id.tv_chat);
        iv_home=(ImageView)findViewById(R.id.iv_home);
        iv_chat=(ImageView)findViewById(R.id.iv_chat);
        iv_notification=(ImageView)findViewById(R.id.iv_notification);
        iv_profile=(ImageView)findViewById(R.id.iv_profile);
        tv_postad=(ImageView) findViewById(R.id.tv_postad);
        tv_home.setOnClickListener(this);
        tv_chat.setOnClickListener(this);
        tv_notice.setOnClickListener(this);
        tv_profile.setOnClickListener(this);
        iv_home.setOnClickListener(this);
        iv_chat.setOnClickListener(this);
        iv_notification.setOnClickListener(this);
        iv_profile.setOnClickListener(this);

        tv_postad.setOnClickListener(this);
        rl_home.setOnClickListener(this);
        rl_chat.setOnClickListener(this);
        rl_notice.setOnClickListener(this);
        rl_profile.setOnClickListener(this);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
        //initialize navigation header
        View hView = navigationView.getHeaderView(0);
        profile = (ImageView) hView.findViewById(R.id.profileImage);
        tvname = (TextView) hView.findViewById(R.id.tv_username);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        tvname.setText(preferences.getname());
        Glide.with(mcoxt).load(Common.Image_Loading_Url(preferences.getimage())).apply(new RequestOptions().centerCrop().error(R.drawable.userimage)).into(profile);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, null, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        frameLayout=findViewById(R.id.framelayout_hs);

//        customnavbar(0);



        bottomnavigationbar_init();
        register_user("user"+preferences.getuserid());


    }
    public AHBottomNavigation bottomNavigation;
    private void bottomnavigationbar_init() {
        bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);
        // Create items
        AHBottomNavigationItem item1 = new AHBottomNavigationItem(getString(R.string.home), R.drawable.ic_home_icon, R.color.black);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(getString(R.string.chats), R.drawable.ic_chat, R.color.black);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem("", R.drawable.ic_chat, R.color.black);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem(getString(R.string.notice), R.drawable.ic_notifications_white_24dp, R.color.black);
        AHBottomNavigationItem item5 = new AHBottomNavigationItem(getString(R.string.profile), R.drawable.profile_user_icon_ic, R.color.black);

        // Add items
        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item3);
        bottomNavigation.addItem(item4);
        bottomNavigation.addItem(item5);

        // Set background color
        bottomNavigation.setDefaultBackgroundColor(getResources().getColor(R.color.colorPrimary));

        // Change colors
        bottomNavigation.setAccentColor(getResources().getColor(R.color.on_click_icon_color));
        bottomNavigation.setInactiveColor(getResources().getColor(R.color.white));

        // Manage titles
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);

        // Set current item programmatically

        if (getIntent().hasExtra("from")){
            bottomNavigation.setCurrentItem(1);
            getSupportFragmentManager().beginTransaction().replace(R.id.framelayout_hs, new Chat_Inbox_Frag_new()).commit();

        }else {
            bottomNavigation.setCurrentItem(0);
            getSupportFragmentManager().beginTransaction().replace(R.id.framelayout_hs, new Main_Home_Fragment()).commit();
        }
        // Customize notification (title, background, typeface)
        bottomNavigation.setNotificationBackgroundColor(getResources().getColor(R.color.colorAccent));

        bottomNavigation.setOnTabSelectedListener(this);

        if (!preferences.getnotificationcount().equals("0")) {
            AHNotification notification = new AHNotification.Builder()
                    .setText(preferences.getnotificationcount())
                    .setBackgroundColor(ContextCompat.getColor(Home_Screen_Activity.this, R.color.white))
                    .setTextColor(ContextCompat.getColor(Home_Screen_Activity.this, R.color.colorAccent))
                    .build();
            bottomNavigation.setNotification(notification, 3);
        }else {
            AHNotification notification = new AHNotification.Builder()
                    .setText("")
                    .setBackgroundColor(ContextCompat.getColor(Home_Screen_Activity.this, R.color.white))
                    .setTextColor(ContextCompat.getColor(Home_Screen_Activity.this, R.color.colorAccent))
                    .build();
            bottomNavigation.setNotification(notification, 3);
        }

        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
    }

    public void alert(){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mcoxt);

        alertDialog.setMessage(getResources().getString(R.string.areyoushure));
        alertDialog.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertDialog.setNegativeButton(getResources().getString(R.string.No), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog1= alertDialog.create();
        alertDialog1.show();

        alertDialog1.getButton(alertDialog1.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorAccent));
        alertDialog1.getButton(alertDialog1.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorAccent));
    }

    @Override
    public void onBackPressed() {
        alert();
    }

    Intent i;
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.item_home:
                //change fragment
                bottomNavigation.setCurrentItem(0);
                getSupportFragmentManager().beginTransaction().replace(R.id.framelayout_hs, new Main_Home_Fragment()).commit();

                break;

            case R.id.item_categories:

                break;
            case R.id.item_notification:
                //change fragment
                bottomNavigation.setCurrentItem(2);

                getSupportFragmentManager().beginTransaction().replace(R.id.framelayout_hs, new Notification_Screen_Fragment()).commit();

                break;
            case R.id.item_chat:
                //change fragment
                bottomNavigation.setCurrentItem(1);
                getSupportFragmentManager().beginTransaction().replace(R.id.framelayout_hs, new Chat_Inbox_Frag_new()).commit();

                break;
            case R.id.item_myaccount:
                i=new Intent(mcoxt,Drawer_Frame_Activity.class);
                i.putExtra(Pos,5);
                i.putExtra(Title,"Account");

                startActivity(i);
                break;
            case R.id.item_myads:
                i=new Intent(mcoxt,Drawer_Frame_Activity.class);
                i.putExtra(Pos,0);
                i.putExtra(Title,"My Ads");

                startActivity(i);
                break;
            case R.id.item_ads:
                Intent i=new Intent(mcoxt, Adpost_Add_Activity.class);
                startActivityForResult(i,10);
                break;

            case R.id.item_recentview:
                i=new Intent(mcoxt,Drawer_Frame_Activity.class);
                i.putExtra(Pos,8);
                i.putExtra(Title,"Recently Views");

                startActivity(i);
                break;
            case R.id.item_myoffer:
                i=new Intent(mcoxt,Drawer_Frame_Activity.class);
                i.putExtra(Pos,10);
                i.putExtra(Title,"My Offers");

                startActivity(i);
                break;
            case R.id.item_mywallet:
                i=new Intent(mcoxt,My_Wallet_Activity.class);
                startActivity(i);
                break;
            case R.id.item_mycoin:
                i=new Intent(mcoxt,My_Coin_Activity.class);
                startActivity(i);
                break;
            case R.id.item_following:
                i=new Intent(mcoxt,Drawer_Frame_Activity.class);
                i.putExtra(Pos,1);
                i.putExtra(Title,"Following");
                startActivity(i);
                break;
            case R.id.item_invite:
                startActivity(new Intent(mcoxt, Invite_Activity.class));
                break;
            case R.id.item_setting:
                i=new Intent(mcoxt,Setting_Activity.class);

                startActivity(i);

                break;
            case R.id.item_help:
                i=new Intent(mcoxt,Drawer_Frame_Activity.class);
                i.putExtra(Pos,13);
                i.putExtra(Title,"Help");
                startActivity(i);

                break;
            case R.id.item_logout:
                online_status(ServerValue.TIMESTAMP);
                logout(this);
                AccountKit.logOut();
                preferences.Clear();
                i=new Intent(mcoxt,Login_Activity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
                Paper.book().destroy();
                break;

        }
        drawer.closeDrawer(Gravity.LEFT);
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {
        switch (v.getId()){
//            case R.id.iv_home:
//                customnavbar(0);
//                break;
//             case R.id.rl_home:
//                customnavbar(0);
//                break;
//            case R.id.iv_chat:
//                customnavbar(1);
//                break;
//            case R.id.rl_chat:
//                customnavbar(1);
//                break;
//            case R.id.iv_notification:
//                customnavbar(3);
//                break;
//            case R.id.rl_notice:
//                customnavbar(3);
//                break;
//            case R.id.rl_profile:
//                customnavbar(4);
//                break;
//             case R.id.iv_profile:
//                customnavbar(4);
//                break;

            case R.id.fab_add_ad:
                Intent i=new Intent(mcoxt, Adpost_Add_Activity.class);
                startActivityForResult(i,20);
                break;


        }
    }


    @Override
    public boolean onTabSelected(int position, boolean wasSelected) {
        switch (position){
            case 0:

                //change fragment
                getSupportFragmentManager().beginTransaction().replace(R.id.framelayout_hs, new Main_Home_Fragment()).commit();

                break;

            case 1:
                //change fragment
                getSupportFragmentManager().beginTransaction().replace(R.id.framelayout_hs, new Chat_Inbox_Frag_new()).commit();

                break;

            case 3:
                //change fragment
                getSupportFragmentManager().beginTransaction().replace(R.id.framelayout_hs, new Notification_Screen_Fragment()).commit();

                break;

            case 4:

                //change fragment
                getSupportFragmentManager().beginTransaction().replace(R.id.framelayout_hs, new Profile_Fragment()).commit();

                break;


        }
        return true;
    }


    ////custom navigation bar
//
//    @RequiresApi(api = Build.VERSION_CODES.M)
//    private void customnavbar(int position){
//        int active_color=R.color.on_click_icon_color;
//        int inactive_color=R.color.white;
//        tv_home.setTextColor(getResources().getColor(inactive_color));
//        tv_chat.setTextColor(getResources().getColor(inactive_color));
//        tv_notice.setTextColor(getResources().getColor(inactive_color));
//        tv_profile.setTextColor(getResources().getColor(inactive_color));
//        final float scale = getResources().getDisplayMetrics().density;
//        int active_height_ = (int) (26 * scale + 0.5f);
//        int active_width_ = (int) (26 * scale + 0.5f);
//        int inactive_height_ = (int) (24 * scale + 0.5f);
//        int inactive_width_ = (int) (24 * scale + 0.5f);
//
//        iv_home.setColorFilter(ContextCompat.getColor(this, inactive_color), android.graphics.PorterDuff.Mode.SRC_IN);
//        iv_chat.setColorFilter(ContextCompat.getColor(this, inactive_color), android.graphics.PorterDuff.Mode.SRC_IN);
//        iv_notification.setColorFilter(ContextCompat.getColor(this, inactive_color), android.graphics.PorterDuff.Mode.SRC_IN);
//        iv_profile.setColorFilter(ContextCompat.getColor(this, inactive_color), android.graphics.PorterDuff.Mode.SRC_IN);
//        iv_home.getLayoutParams().height = inactive_height_;
//        iv_home.getLayoutParams().width =inactive_width_;
//        iv_chat.getLayoutParams().height = inactive_height_;
//        iv_chat.getLayoutParams().width =inactive_width_;
//        iv_notification.getLayoutParams().height = inactive_height_;
//        iv_notification.getLayoutParams().width =inactive_width_;
//        iv_profile.getLayoutParams().height = inactive_height_;
//        iv_profile.getLayoutParams().width =inactive_width_;
//
//
//        switch (position){
//            case 0:
//                callripple_effect_android(rl_home);
//            tv_home.setTextColor(getResources().getColor(active_color));
//          iv_home.setColorFilter(ContextCompat.getColor(this, active_color), android.graphics.PorterDuff.Mode.SRC_IN);
//                iv_home.getLayoutParams().height = active_height_;
//               iv_home.getLayoutParams().width =active_width_;
//
//                //change fragment
//                getSupportFragmentManager().beginTransaction().replace(R.id.framelayout_hs, new Main_Home_Fragment()).commit();
//
//                break;
//
//            case 1:
//                callripple_effect_android(rl_chat);
//            tv_chat.setTextColor(getResources().getColor(active_color));
//          iv_chat.setColorFilter(ContextCompat.getColor(this, active_color), android.graphics.PorterDuff.Mode.SRC_IN);
//
//                iv_chat.getLayoutParams().height = active_height_;
//                iv_chat.getLayoutParams().width = active_width_;
//          //change fragment
//                getSupportFragmentManager().beginTransaction().replace(R.id.framelayout_hs, new Chat_Inbox_Frag()).commit();
//
//                break;
//
//            case 3:
//            tv_notice.setTextColor(getResources().getColor(active_color));
//          iv_notification.setColorFilter(ContextCompat.getColor(this, active_color), android.graphics.PorterDuff.Mode.SRC_IN);
//
//                iv_notification.getLayoutParams().height = active_height_;
//                iv_notification.getLayoutParams().width = active_width_;
//
//                //change fragment
//                getSupportFragmentManager().beginTransaction().replace(R.id.framelayout_hs, new Notification_Screen_Fragment()).commit();
//
//                break;
//
//            case 4:
//            tv_profile.setTextColor(getResources().getColor(active_color));
//          iv_profile.setColorFilter(ContextCompat.getColor(this, active_color), android.graphics.PorterDuff.Mode.SRC_IN);
//                tv_profile.getLayoutParams().height = active_height_;
//                iv_profile.getLayoutParams().width =active_width_;
//
//          //change fragment
//                getSupportFragmentManager().beginTransaction().replace(R.id.framelayout_hs, new Profile_Fragment()).commit();
//
//                break;
//
//
//
//        }
//
//
//
//    }


    DatabaseReference mDatabase;

    public void register_user(String name){
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(name);

        HashMap<Object, Object> userMap = new HashMap<>();
        userMap.put("name", preferences.getname());
        userMap.put("image", preferences.getimage());
        userMap.put("online", true);

        mDatabase.setValue(userMap);
    }

    private DatabaseReference mRootRef;

    @Override
    protected void onResume() {
        super.onResume();
        if (mRootRef!=null) {
            online_status("true");
        }
        PushService. chatactivitystr="in";

        if (!Common.isNetworkConnected(this)){
            Toast.makeText(this, No_Internet_Connection, Toast.LENGTH_SHORT).show();
        }
        tvname.setText(preferences.getname());
        Glide.with(mcoxt).load(Common.Image_Loading_Url(preferences.getimage())).apply(new RequestOptions().centerCrop().error(R.drawable.userimage)).into(profile);

    }



    @Override
    protected void onPause() {
        super.onPause();
        if (mRootRef!=null) {
            online_status(ServerValue.TIMESTAMP);
        }
    }

    private void online_status(Object status){
        mRootRef.child("Users").child("user"+preferences.getuserid()).child("online").setValue(status);

    }

    public void  logout(final Context context){

//        final DelayedProgressDialog pro=new DelayedProgressDialog();
//
//        pro.show(getSupportFragmentManager(),"");


        StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "logout", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                pro.cancel();
                preferences.Clear();
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);
//                Common.Snakbar(Common.volleyerror(volleyError),tv_city);
                preferences.Clear();
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(Id,preferences.getuserid());
                params.put("push_id",userSharedPreferences.getplayerid());
                params.put("device_id",userSharedPreferences.getdeviceid());
                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(context).addToRequestQueue(stringRequest);


    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onAppBackgrounded() {
        online_status(ServerValue.TIMESTAMP);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void onAppForegrounded() {
        // App in foreground
        online_status("true");
    }

}
