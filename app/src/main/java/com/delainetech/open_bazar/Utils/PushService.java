package com.delainetech.open_bazar.Utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import android.util.Log;

import com.delainetech.open_bazar.Chat.ChatActivity;
import com.delainetech.open_bazar.R;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;


public class PushService extends Service {

     UserSharedPreferences user;
   // SharedPreferences user,inst,learner;
    String user_id="",status;
    DatabaseReference mref;
//    Firebase mref,mref1,mref2,mrefupdateSatusref;
    private JSONObject jsonObject;
    public static String chatactivitystr="";
    int UserStatus;
    public Map<String, Query> mapQuery;
    public Map<String, ChildEventListener> mapChildEventListenerMap;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mapQuery = new HashMap<>();
        mapChildEventListenerMap = new HashMap<>();
        user=new UserSharedPreferences(this);
        user_id=user.getuserid();


        if(user.getlogin()) {
            mref = FirebaseDatabase.getInstance().getReference();
            DatabaseReference messageRef = mref.child("messages").child(user_id);

            Query messageQuery = messageRef.limitToLast(1);



            Log.e("service_url",  messageRef.toString()+"");

            messageQuery.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                    String message = dataSnapshot.getValue().toString();
                    Log.e("last_msg1111",message);
                    String msg = "";
                    String username = "";
                    String user = "";
                    int i = 0;

                    for (DataSnapshot userAnswerSnapshot : dataSnapshot.getChildren()) {

                        msg = userAnswerSnapshot.child("message").getValue().toString();
                        username = userAnswerSnapshot.child("user").getValue().toString();
                        user = userAnswerSnapshot.child("user").getValue().toString();

                        i++;
                    }

                    if (i == 1) {

                        if (!user.equals(user_id)  && chatactivitystr.equals("")) {
                            Intent in = new Intent("push");
                            in.putExtra("msg", true);
                            sendNotification(msg, username, user);
                            sendBroadcast(in);


                        }

                    }

                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    String message = dataSnapshot.getValue().toString();
                    // Log.e("last_msg1",message);
                    String msg = "";
                    String username = "";
                    String user = "";

                    for (DataSnapshot userAnswerSnapshot : dataSnapshot.getChildren()) {

                        msg = userAnswerSnapshot.child("message").getValue().toString();
                        username = userAnswerSnapshot.child("username").getValue().toString();
                        user = userAnswerSnapshot.child("user").getValue().toString();

                    }

                    Log.e("compare_ids", user + "," + user_id);
                    if (!user.equals(user_id)  && chatactivitystr.equals("")) {

                        SharedPreferences user1 = getSharedPreferences("user", Context.MODE_PRIVATE);

                        String notification = getSharedPreferences("user", Context.MODE_PRIVATE)
                                .getString("notification", "");
                        Log.e("notification_data", notification);

                        if (notification.length() <= 0)

                        {
                            jsonObject = new JSONObject();
                        } else {

                            try {
                                jsonObject = new JSONObject(notification);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.e("e", "excptn", e);
                            }
                        }
                        HashMap hash = null;
                        try {
                            hash = (HashMap) jsonToMap(jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("e", "ex", e);
                        }


                        Intent in = new Intent("push");
                        in.putExtra("msg", true);

                        sendNotification(msg, username, user);
                        sendBroadcast(in);

                    }
                }


                public Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
                    Map<String, Object> retMap = new HashMap<String, Object>();

                    if (json != JSONObject.NULL) {
                        retMap = toMap(json);
                    }
                    return retMap;
                }

                public List<Object> toList(JSONArray array) throws JSONException {
                    List<Object> list = new ArrayList<Object>();
                    for (int i = 0; i < array.length(); i++) {
                        Object value = array.get(i);
                        if (value instanceof JSONArray) {
                            value = toList((JSONArray) value);
                        } else if (value instanceof JSONObject) {
                            value = toMap((JSONObject) value);
                        }
                        list.add(value);
                    }
                    return list;
                }


                public Map<String, Object> toMap(JSONObject object) throws JSONException {
                    Map<String, Object> map = new HashMap<String, Object>();
                    Log.e("objectarray", object + "");
                    if ((object != null)) {
                        Iterator<String> keysItr = object.keys();
                        while (keysItr.hasNext()) {
                            String key = keysItr.next();
                            Object value = object.get(key);

                            if (value instanceof JSONArray) {
                                value = toList((JSONArray) value);
                            } else if (value instanceof JSONObject) {
                                value = toMap((JSONObject) value);
                            }
                            map.put(key, value);
                        }
                    }
                    return map;
                }


                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    String message = dataSnapshot.getValue().toString();

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }


            });




        }


    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.e("check","check user status");
     //   updateUserStatus();
    }

    private void sendNotification(String messageBody, String username, String user_id) {
        final int NOTIFICATION_ID = 1;
        final String NOTIFICATION_CHANNEL_ID = "my_yognirog_notification_channel";

        Random rand=new Random();
        Log.e("msgBody",messageBody);
        Intent intent = new Intent(this,ChatActivity.class);
        intent.putExtra("message","message");
        intent.putExtra("receiver_id",user_id);
        intent.putExtra("name",username);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this,NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("New message from "+username)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody)).setPriority(Notification.PRIORITY_MAX)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setContentIntent(pendingIntent);


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_DEFAULT);

            // Configure the notification channel.
            notificationChannel.setDescription("Channel description");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.GREEN);

            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
            notificationBuilder.setPriority(NotificationManager.IMPORTANCE_HIGH);
        }

        notificationManager.notify(NOTIFICATION_ID/* ID of notification */, notificationBuilder.build());
    }


    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }
}
