package com.delainetech.open_bazar.Utils;

/**
 * Created by Kishan on 27-Dec-17.
 */

public interface ParamKeys {

    String Country_CODE="+31";
    int LOCTAION_CITY=101;
    int OFFER_COUNT=102;
    int ADS_DELETE_RESULT_CODE=103;
    int ADS_CATEGORY_RESULT_CODE=104;
    int Mobile_RESULT_CODE=105;
    int FILTER_LOCTAION_CITY=106;
    int FILTER_CAT=107;
    int FILTER_ACTIVITY=108;

    String Pos="pos";
    String SocialToken="social_token";

//    testing server
//    String baseurl="http://139.59.75.219/OpenBazar/public/api/";
//    String imageurl="http://139.59.75.219/OpenBazar/public/images/";
//    String weburl="http://139.59.75.219/OpenBazar/public/";

//    Live server
    String baseurl="http://134.209.86.177/app-admin/api/";
    String imageurl="http://134.209.86.177/app-admin/public/images/";
    String weburl="http://134.209.86.177/app-admin/public/";


    String Googlemap_imgurl="http://maps.google.com/maps/api/staticmap?key=AIzaSyBXAdCe4nJuapECudMeh4q-gGlU-yAMQX0&zoom=13&size=500x500&sensor=false&markers=color:red|";
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String Password="password";
    String Name="name";
    String Profile_image="profile_image";
    String Addresss="address";
    String Token="token";
    String Social_Details="social_details";
    String DOB="DOB";
    String About="about";
    String Player_id="player_id";

    String Ad_title="ad_title";
    String Id="id";
    String Email="email";
    String Type="type";
   String OTP="otp";
    String Mobile="mobile";
    String Ad_description="ad_description";
    String Price="price";
    String Images="images";
    String Category="category";
    String City="city";
    String Ttimestamp="timestamp";
    String Bid_type="bid_type";
    String Company_Info="company_info";
    String Bid_price="bid_price";
    String User_id="user_id";
    String Other_id="other_id";
    String Post_id="post_id";

    String Title="title";

    String Phone_No="phone_no";
    String Signup_type="signup_type";
     String App_Type="app_type";




    int Share_Intent_val=1000;
    String Comming_Soon_text="Coming soon....";
    String No_Internet_Connection="Check your internet connection";


    String FAQ_q1="Where I can use Credit/wallet?";
    String FAQ_a1="You can use the \"Credit/Wallet\" to purchase the services on the site such as: featured Ad, bump up Ad, Rocket Ad and memberships.";
    String FAQ_q2="How I can buy Credit for My Wallet?";
    String FAQ_a2="From Home page, go to “My Account” and then choose “My Wallet”/ Click on “Buy Credit/ Select the package you need and complete the payment process.";
    String FAQ_q3="Does My Credit have expiration date?";
    String FAQ_a3="Yes, it has an expiration date, you can use it during one year from the purchase date.";
    String FAQ_q4="Is My Credit refundable?";
    String FAQ_a4="No, because this credit is not withdrawable.";
  String FAQ_q5="How do I check my Credit balance and transactions?";
    String FAQ_a5="By clicking on \"My Account\" button and then \"My Wallet\" you can review the page of all information from the current balance and purchase of credit.";

    String Coin_FAQ="Open Bazar Coins is an Affiliate system which allows you to collect Coins by posting Ads, inviting friends or like Ads and it helps you collect rewards, store credits and get various offerings and redeem Coins multiple times as and when required";
    String Coin_FAQ_q1="Where I can use My Coins?";
    String Coin_FAQ_a1="You can use the \"Credit/Coins\" to purchase the services on the site such as: featured Ad, bump up Ad, Rocket Ad and memberships.";
    String Coin_FAQ_q2="How I can exchange Coins?";
    String Coin_FAQ_a2="From Home page, go to “My Account” and then choose “My Wallet”/Click on “Redeem/Select the coins amount you need and complete the Redeem process, once the process completed then you will see the Credit on “My Wallet”";
    String Coin_FAQ_q3="Does My Coins have expiration date?";
    String Coin_FAQ_a3="Yes, it has an expiration date, you can use it during one year from the collected date.";
    String Coin_FAQ_q4="Is My Coins refundable?";
    String Coin_FAQ_a4="No, because this credit is not withdrawable.";
    String Coin_FAQ_q5="How do I check my Coins balance?";
    String Coin_FAQ_a5="By clicking on \"My Account\" button and then \"My Coins\" you can review the page of all information from the current balance and Redeem transactions.";


    String buy_credit_1="Add credit to your wallet by making one-time payment to buy our various offerings or upgrade your package of membership.";
    String buy_credit_2="The easiest and fastest way to repost Ad, Feature Ad or Rocket Ad to get more views, no need to use credit card or transfer money just use your credits from your Wallet and save your time.";
    String buy_credit_3="We Offer you the smartest and safest way to make a benefit from our large variety and help you to shape your financial future.";
    String buy_credit_4="Start your own business and sell your goods or open your shop and create online a solid network and get more buyers without central institutions or intermediaries";
    String buy_credit_1_price="€ 9,49 Credits 8541";
    String buy_credit_2_price="€ 46,41 Credits 41769";
    String buy_credit_3_price="€ 130,25 Credits 117225";
    String buy_credit_4_price="€ 249,73 Credits 224757";


    String Help_q1="What is the Open Bazar?";
    String Help_q2="How does the site work?";
    String Help_q3="Is the Open Bazar subscription free?";
    String Help_q4="How does the Open Bazar protect its users?";
    String Help_q5="How does the Open Bazar gain money?";
    String Help_q6="Can you market to a private company by advertising it on the Open Bazar?";
    String Help_q7="What is the next step after completing the sale?";

    String help_ans1="The Open Bazar is an online market specialized in classified ads that allow the user to add special ads or browse the existing ads.That facilitates and accelerates the business processes of selling and buying and renting of many new and used things. As well as services according to the classification on the lists on the site to be connected between the advertiser and browser from the same country or the city is direct and fast.";
    String help_ans2="The Open Bazar is a safe and easy to use platform for the sale and purchase of real estate, vehicles, electronics, furniture, telephones, clothing and many other supplies, in addition to services.";
    String help_ans3="Yes, the Open Bazar is a free site with the possibility of subscription paid services that provide the user with many advantages for a small fee.";
    String help_ans4="We have a public follow-up team that follows up ads and agrees to post them on the site after filtering them to ensure user-friendly content, needs, market requirements, advertiser credibility, and direct communication.";
    String help_ans5="As we explained earlier, the Open Bazar mechanism depends on advertising, but it provides features and services to its users to enhance the opportunity to profit faster through advertising and \"stores\", which in turn facilitates the sales of advertised goods, giving companies a greater presence online, Features are driven.";
    String help_ans6="No, because this is contrary to terms of use, and marketing depends on showing the same product or service that the company delivers through advertising. Instead, the open market allows sellers to \"shop\"\n" +
            "To market their products and services under the slogan of their companies; to increase sales opportunities more and faster, and thus profit more with less effort.\n";
    String help_ans7="The seller must deactivate the advertisement after the sale of the goods to avoid receiving calls and notifications about them.";


    // about us =http://139.59.75.219/OpenBazar/public/about-us.html

    String about_us_str="<h1 style=\"text-align: center;\"><span style=\"color: #b40202;\"><strong>ABOUT</strong></span></h1>\n" +
            "<p>Open Bazar is an online classifieds platform in Europe that brings millions of users together without central institutions or intermediaries, to exchange their goods, products or find anything in their community. Users can browse a large variety of Goods, Services, Real Estate, Rentals, Cars, Electronics, Fashion, Jobs and more other items.</p>\n" +
            "<p>&nbsp;</p>\n" +
            "<h1 style=\"text-align: center;\"><span style=\"color: #b40202;\">MISSION</span></h1>\n" +
            "<p>Open Bazar aims to empower all who want to take part in shaping the future of finance by using our platform to distribute or redistribute their goods to fill a new need and deliver services through our diverse categories.</p>\n" +
            "<p>&nbsp;</p>\n" +
            "<h1 style=\"text-align: center;\"><span style=\"color: #b40202;\">VISION</span></h1>\n" +
            "<p>Open Bazar leads its strong passion for the sustainable development of an innovative platform, enabling one of the fastest growing network communities in Europe In a rapidly globalizing world, our goal is to become a meeting place for shopping lovers, wandering adventurers and all Internet users who are looking for something new to do or try, no matter where they are. One platform accommodates the buyers, the sellers and the merchants and meets their needs, where the merchant can be buyer and every user can sell.</p>";

    String about_us_str_1="Open Bazar is an online classifieds and advertising service platform in Europe that brings millions of users together without central institutions or intermediaries, to exchange their goods, productsor find anything in their community.The platform enables merchants -to-user and user-to-user benefit in combination of credits and OpenBazar’s Coinsthrough open bazar wallet and coins system. Users and merchants can browse through a large variety of Goods, Services, Real Estate, Rentals, Cars, Electronics, Fashion, Jobs and more other items." ;
    String about_us_str_2="Open Bazar aims to empower merchants and userswho want to take part in shaping the future of their finance by using our platform to buy, sell, rent, lend, offer service, open shop or find anything in their community. A community where underused goods are redistributed to fill a new need, and become wanted again, where non- product assets such as space, skills, money and coins are exchanged and traded in new ways of our Wallet and credits system that don’t always require centralized institutions or ‘middlemen’.";
    String about_us_str_3="Open Bazar leads its strong passion for the sustainable development of an innovative platform, enabling one of the fastest growing network communities in Europe In a rapidly globalizing world, our goal is to become a meeting place for shopping lovers, wandering adventurers and all Internet users who are looking for something new to do or try, no matter where they are. One platform accommodates the buyers, the sellers and the merchants and meets their needs, where the merchant can be buyer and every user can sell.";
}


