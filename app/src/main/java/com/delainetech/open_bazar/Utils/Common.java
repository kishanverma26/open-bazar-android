package com.delainetech.open_bazar.Utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.InputType;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


import com.android.volley.error.AuthFailureError;
import com.android.volley.error.NetworkError;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ParseError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;


import com.delainetech.open_bazar.Detail_Activity_User;
import com.delainetech.open_bazar.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

public class Common implements ParamKeys{


    String[] PERMISSI = {
            android.Manifest.permission.READ_CONTACTS,
            android.Manifest.permission.WRITE_CONTACTS,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.READ_SMS,
            android.Manifest.permission.CAMERA
    };


// onactivity result
//  requestCode=: 101 ,permissions=android.permission.WRITE_EXTERNAL_STORAGE ,grantResults=-1  deny
//requestCode=: 101 ,permissions=android.permission.WRITE_EXTERNAL_STORAGE ,grantResults=0 allow
    public static void  askpermission(AppCompatActivity activity, String[]  PERMISSIONS){
        int PERMISSION_ALL = 1;
        if(!hasPermissions(activity, PERMISSIONS)){
            ActivityCompat.requestPermissions(activity, PERMISSIONS, PERMISSION_ALL);
        }
    }



    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }



    public static boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }


    public static  boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
         }
        return true;
    }

    public static String ParseString(JSONObject obj, String Param) throws JSONException {
        if (obj.has(Param)) {
            String lastSeen = obj.getString(Param);
            if (lastSeen != null && !TextUtils.isEmpty(lastSeen) && !lastSeen.equalsIgnoreCase("null") && lastSeen.length()>0)
                return obj.getString(Param);
            else
                return "";

        } else
            return "";
    }


    public static String Parsintentvalue(String key,AppCompatActivity inte){
        String data;

        if (inte.getIntent().hasExtra(key)){
            data=inte.getIntent().getStringExtra(key);
        }else {
            data="";
        }

        return data;

    }


    public static void Snakbar(String msg, View v){
        Snackbar.make(v, msg, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    public static int checklength(EditText text){

        return text.getText().toString().trim().length();
    }



    public static String getTimeAgo(long time, Context ctx) {

//        if(date == null) {
//            return null;
//        }
//
//        long time = date.getTime();
//        Log.e("time",time+"");
        Date curDate = currentDate();
        long now = curDate.getTime();
        if (time > now || time <= 0) {
            return null;
        }

        int dim = getTimeDistanceInMinutes(time);

        String timeAgo = null;

        if (dim == 0) {
            timeAgo =  "Just Now";
        } else if (dim == 1) {
            return "1 " + ctx.getResources().getString(R.string.date_util_unit_minute);
        } else if (dim >= 2 && dim <= 44) {
            timeAgo = dim + " " + ctx.getResources().getString(R.string.date_util_unit_minutes);
        } else if (dim >= 45 && dim <= 89) {
            timeAgo = ctx.getResources().getString(R.string.date_util_prefix_about) + " "+ctx.getResources().getString(R.string.date_util_term_an)+ " " + ctx.getResources().getString(R.string.date_util_unit_hour);
        } else if (dim >= 90 && dim <= 1439) {
            timeAgo = ctx.getResources().getString(R.string.date_util_prefix_about) + " " + (Math.round(dim / 60)) + " " + ctx.getResources().getString(R.string.date_util_unit_hours);
        } else if (dim >= 1440 && dim <= 2519) {
            timeAgo = "1 " + ctx.getResources().getString(R.string.date_util_unit_day);
        } else if (dim >= 2520 && dim <= 43199) {
            timeAgo = (Math.round(dim / 1440)) + " " + ctx.getResources().getString(R.string.date_util_unit_days);
        } else if (dim >= 43200 && dim <= 86399) {
            timeAgo = ctx.getResources().getString(R.string.date_util_prefix_about) + " "+ctx.getResources().getString(R.string.date_util_term_a)+ " " + ctx.getResources().getString(R.string.date_util_unit_month);
        } else if (dim >= 86400 && dim <= 525599) {
            timeAgo = (Math.round(dim / 43200)) + " " + ctx.getResources().getString(R.string.date_util_unit_months);
        } else if (dim >= 525600 && dim <= 655199) {
            timeAgo = ctx.getResources().getString(R.string.date_util_prefix_about) + " "+ctx.getResources().getString(R.string.date_util_term_a)+ " " + ctx.getResources().getString(R.string.date_util_unit_year);
        } else if (dim >= 655200 && dim <= 914399) {
            timeAgo = ctx.getResources().getString(R.string.date_util_prefix_over) + " "+ctx.getResources().getString(R.string.date_util_term_a)+ " " + ctx.getResources().getString(R.string.date_util_unit_year);
        } else if (dim >= 914400 && dim <= 1051199) {
            timeAgo = ctx.getResources().getString(R.string.date_util_prefix_almost) + " 2 " + ctx.getResources().getString(R.string.date_util_unit_years);
        } else {
            timeAgo = ctx.getResources().getString(R.string.date_util_prefix_about) + " " + (Math.round(dim / 525600)) + " " + ctx.getResources().getString(R.string.date_util_unit_years);
        }
        if (timeAgo.equals("Just Now")){
            return timeAgo;
        }else {
            return timeAgo + " " + ctx.getResources().getString(R.string.date_util_suffix);
        }
        }
    public static Date timestamptodate(String inputDate){
        Log.e("timestamp",getDate(Long.parseLong(inputDate))+"");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss",Locale.US);
        Date date = null;
        try {
            date = simpleDateFormat.parse(inputDate);

        } catch (ParseException e) {
            e.printStackTrace();
            Log.e("dateexception","",e);
            //throw new IllegalAccessException("Error in parsing date");
        }
        return date;
    }
    public static Date getDate(long time) {
        logg("date3",time+"");
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        String date = DateFormat.format("yyyy-MM-dd hh:mm:ss", cal).toString();
        Log.e("date1",date);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss",Locale.US);
        Date datee = null;
        try {
            datee = simpleDateFormat.parse(date);
            Log.e("date2",datee+"");
        } catch (ParseException e) {
            e.printStackTrace();
            Log.e("dateexception","",e);
            //throw new IllegalAccessException("Error in parsing date");
        }
        return datee;
    }

    private static int getTimeDistanceInMinutes(long time) {
        long timeDistance = currentDate().getTime() - time;
        return Math.round((Math.abs(timeDistance) / 1000) / 60);
    }

    public static Date currentDate() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTime();
    }

    public static void hideSoftKeyboard(AppCompatActivity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        AppCompatActivity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getWindow().getDecorView().getRootView().getWindowToken(), 0);

    }

    public static String time_diff(long posttime)

    {
        String time = "";
        int numberOfDays;
        int numberOfHours;
        int numberOfMinutes;

        try {

            long difference = System.currentTimeMillis() - posttime;
            difference = difference / 1000;

            numberOfDays = (int) (difference / 86400);
            numberOfHours = (int) ((difference % 86400) / 3600);
            numberOfMinutes = (int) (((difference % 86400) % 3600) / 60);
            Date date = new Date(posttime);
            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd",Locale.US);
            SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm",Locale.US);
            sdf.setTimeZone(TimeZone.getDefault());
            sdf1.setTimeZone(TimeZone.getDefault());
            String java_date = sdf.format(date) + " at " + sdf1.format(date);
            Log.e("time_diff", numberOfDays + "," + numberOfHours + "," + numberOfMinutes);

            if (numberOfDays > 0) {

                if (numberOfDays > 0) {

                    time = sdf.format(date) + " at " + sdf1.format(date);
                }
            } else if (numberOfHours > 0) {
                // if (numberOfHours == 1) {
                time = numberOfHours + " hr";
//                } else {
//                    time = numberOfHours + " hours ago";
//                }

            } else if (numberOfMinutes > 0) {


//                if (numberOfMinutes == 1) {

                time = numberOfMinutes + " min";
//                } else {
//                    time = numberOfMinutes + " minutes ago";
//                }
            } else {
                time = "Just now";
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("excptn", "excptn", e);
        }

        return time;
    }

    public boolean isCallActive(Context context){
        AudioManager manager = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);

        return manager.getMode()==AudioManager.MODE_IN_CALL;
    }



    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager)context. getSystemService(Context.CONNECTIVITY_SERVICE);

        assert cm != null;
        return cm.getActiveNetworkInfo() != null;
    }


    public static byte[]  set_Image_bytearray(Uri ImageUri,Context context)
    {
        byte[] b = new byte[0];
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver() , ImageUri );

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
            Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
            scaled.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            b = baos.toByteArray();
            Log.e("byteArray",b+"");

        }

        catch (Exception e) {
            Log.e("e","e",e);
            e.printStackTrace();
        }
        return b;
    }

    public  static String Image_Loading_Url(String url){
        String URL="abc";
        if (url!=null){


        if (url.length()>0){
            if (url.contains("https://")){
                URL=url;
            } else if (url.contains("emulated/0")){
                URL=url;
            }else if (url.contains("file:///data/user/0/")){
                URL=url;
            }
            else {
                URL = imageurl + url;
            }
        }
        }
        return URL;
    }
    public static String volleyerror(VolleyError volleyError){
        String message = null;
        if (volleyError instanceof NetworkError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ServerError) {
            message = "The server could not be found. Please try again after some time!!";
        } else if (volleyError instanceof AuthFailureError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ParseError) {
            message = "Parsing error! Please try again after some time!!";
        } else if (volleyError instanceof NoConnectionError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof TimeoutError) {
            message = "Connection TimeOut! Please check your internet connection.";
        }
        return message;
    }
    public static void logg(String tag,String msg){
        Log.e(tag,msg);

    }




    public static void enableInputVisiblePassword(EditText editText,ImageView imageView) {
        final int INPUT_TYPE_VISIBLE_PASSWORD = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD;
        final int INPUT_TYPE_HIDDEN_PASSWORD = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD;

        if (editText.getInputType() == INPUT_TYPE_VISIBLE_PASSWORD){
            Typeface cache = editText.getTypeface();
            editText.setInputType(INPUT_TYPE_HIDDEN_PASSWORD);
            editText.setTypeface(cache);
            imageView.setImageResource(R.drawable.eye_vision_off);
        }else{
            Typeface cache = editText.getTypeface();
            editText.setInputType(INPUT_TYPE_VISIBLE_PASSWORD);
            editText.setTypeface(cache);
            imageView.setImageResource(R.drawable.eye_vision_on);}
    }


    public static String changeDateFormatFromAnother(String date){
        @SuppressLint("SimpleDateFormat") java.text.DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss",Locale.US);
        @SuppressLint("SimpleDateFormat") java.text.DateFormat outputFormat = new SimpleDateFormat("dd MMMM yyyy",Locale.US);
        String resultDate = "";
        try {
            resultDate=outputFormat.format(inputFormat.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return resultDate;
    }

    public static void create_dynamic_link(final String msg, String str, final Activity activity){

        Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse("https://openbazar.page.link?="+str))
                .setDomainUriPrefix("https://openbazar.page.link")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder("com.open_bazar")
                        .setFallbackUrl(Uri.parse("https://www.dropbox.com/s/o1z7e9w94g7twqz/app-debug.apk?dl=0"))
                        .build())
                // Set parameters
                // ...
                .buildShortDynamicLink()

                .addOnCompleteListener(activity, new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {
                            // Short link created
                            Uri shortLink = task.getResult().getShortLink();
                            Uri flowchartLink = task.getResult().getPreviewLink();

                            logg("shortLink",shortLink+" flowchartLink"+flowchartLink);


                            String shareBody2 = msg+" "+shortLink;
                            Intent sharingIntent2 = new Intent(android.content.Intent.ACTION_SEND);
                            sharingIntent2.setType("text/plain");
                            sharingIntent2.putExtra(android.content.Intent.EXTRA_SUBJECT, "Open Bazar");
                            sharingIntent2.putExtra(android.content.Intent.EXTRA_TEXT, shareBody2);
                        activity.    startActivity(Intent.createChooser(sharingIntent2, ""));

                        } else {
                            // Error
                            // ...
                            Toast.makeText(activity, "Somthing went wrong please try again", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


}
