package com.delainetech.open_bazar.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by Dell on 26-04-2017.
 */

public class UserSharedPreferences {
    public static   UserSharedPreferences preferences;
    SharedPreferences userPreferences,playeruserPreferences;

    SharedPreferences.Editor edit,playeredit;

    public UserSharedPreferences(Context context)
    {

        userPreferences =context.getSharedPreferences("UserSharedPreferences", Context.MODE_PRIVATE);
        edit= userPreferences.edit();
    }

    public static UserSharedPreferences getInstance(Context context) {

        if (preferences == null) {
            preferences = new UserSharedPreferences(context.getApplicationContext());
        }
        return preferences;
    }
    public UserSharedPreferences(Context context, String a)
    {

        playeruserPreferences =context.getSharedPreferences("UserSharedPreferencesplayer", Context.MODE_PRIVATE);
        playeredit= playeruserPreferences.edit();
    }

    public void Clear(){
        edit.clear();
        edit.commit();
    }



    public void setname(String name) {

        edit.putString("name",name);
        edit.commit();
    }

    public String getname() {

        return  userPreferences.getString("name","");
    }
   public void setnotificationcount(String notificationcount) {

        edit.putString("notificationcount",notificationcount);
        edit.commit();
    }

    public String getnotificationcount() {

        return  userPreferences.getString("notificationcount","0");
    }


    public void setgoogle_verification(boolean google_verification) {

        edit.putBoolean("google_verification",google_verification);
        edit.commit();
    }

    public boolean getgoogle_verification() {

        return userPreferences.getBoolean("google_verification", false);

    }

     public void setfacebook_verification(boolean facebook_verification) {

        edit.putBoolean("facebook_verification",facebook_verification);
        edit.commit();
    }

    public boolean getfacebook_verification() {

        return  userPreferences.getBoolean("facebook_verification",false);
    }


    public void setemail(String email) {
        Log.e("email",email);
        edit.putString("email",email);
        edit.commit();
    }

    public String getemail() {
        return  userPreferences.getString("email","");
    }

    public void setlogin(Boolean login) {
        edit.putBoolean("login",login);
        edit.commit();
    }

    public Boolean getlogin() {
        return  userPreferences.getBoolean("login",false);
    }



    public void setuserid(String id) {
        edit.putString("id",id);
        edit.commit();
    }

    public String getuserid() {
        return  userPreferences.getString("id","");
    }

  public void setrefferuserid(String refferuserid) {
        edit.putString("refferuserid",refferuserid);
        edit.commit();
    }

    public String getrefferuserid() {
        return  userPreferences.getString("refferuserid","");
    }

    public void setmobile(String mobile) {
        edit.putString("mobile",mobile);
        edit.commit();
    }

    public String getmobile() {
        return  userPreferences.getString("mobile","");
    }

    public void setadmobile(String admobile) {
        edit.putString("admobile",admobile);
        edit.commit();
    }

    public String getadmobile() {
        return  userPreferences.getString("admobile","");
    }

     public void setmobileverification(boolean mobileverification) {
        edit.putBoolean("mobileverification",mobileverification);
        edit.commit();
    }

    public boolean getmobileverification() {
        return  userPreferences.getBoolean("mobileverification",false);
    }



    public void setimage(String image) {
        edit.putString("image",image);
        edit.commit();
    }

    public String getimage() {
        return  userPreferences.getString("image","");
    }

     public void settoken(String token) {
        edit.putString("token",token);
        edit.commit();
    }

    public String gettoken() {
        return  userPreferences.getString("token","");
    }

    public void setaddress(String address) {
        edit.putString("address",address);
        edit.commit();
    }

    public String getaddress() {
        return  userPreferences.getString("address","");
    }

    public void setcreated_at(String created_at) {
        edit.putString("created_at",created_at);
        edit.commit();
    }

    public String getcreated_at() {
        return  userPreferences.getString("created_at","");
    }

    public void setindustry(String industry) {
        edit.putString("industry",industry);
        edit.commit();
    }

    public String getindustry() {
        return  userPreferences.getString("industry","");
    }

    public void setaudiorate(String audiorate) {
        edit.putString("audiorate",audiorate);
        edit.commit();
    }

    public String getaudiorate() {
        return  userPreferences.getString("audiorate","0");
    }
       public void setvideorate(String videorate) {
        edit.putString("videorate",videorate);
        edit.commit();
    }

    public String getvideorate() {
        return  userPreferences.getString("videorate","0");
    }



 public void setbio(String bio) {
        edit.putString("bio",bio);
        edit.commit();
    }

    public String getbio() {
        return  userPreferences.getString("bio","");
    }

    public void setdob(String dob) {
        edit.putString("dob",dob);
        edit.commit();
    }

    public String getdob() {
        return  userPreferences.getString("dob","");
    }

   public void setprofilesetup(Boolean profilesetup) {
        edit.putBoolean("profilesetup",profilesetup);
        edit.commit();
    }

    public Boolean getprofilesetup() {
        return  userPreferences.getBoolean("profilesetup",false);
    }

public void setsetuppin(Boolean setuppin) {
        edit.putBoolean("setuppin",setuppin);
        edit.commit();
    }

    public Boolean getsetuppin() {
        return  userPreferences.getBoolean("setuppin",false);
    }



    public void setplayerid(String playerid) {
        playeredit.putString("playerid",playerid);
        playeredit.commit();
    }

    public String getplayerid() {
        return  playeruserPreferences.getString("playerid","");
    }

    public void setlanguage(String language) {
        playeredit.putString("language",language);
        playeredit.commit();
    }

    public String getlanguage() {
        return  playeruserPreferences.getString("language","en");
    }

    public void setdeviceid(String deviceid) {
        playeredit.putString("deviceid",deviceid);
        playeredit.commit();
    }

    public String getdeviceid() {
        return  playeruserPreferences.getString("deviceid","");
    }

}
