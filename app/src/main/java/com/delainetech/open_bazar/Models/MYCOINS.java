package com.delainetech.open_bazar.Models;

public class MYCOINS {
    
    String  id ,user_id ,tital ,refer_id ,coins,timestamp,type;

    public MYCOINS(String id, String user_id, String tital, String refer_id, String coins, String timestamp, String type) {
        this.id = id;
        this.user_id = user_id;
        this.tital = tital;
        this.refer_id = refer_id;
        this.coins = coins;
        this.timestamp = timestamp;
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTital() {
        return tital;
    }

    public void setTital(String tital) {
        this.tital = tital;
    }

    public String getRefer_id() {
        return refer_id;
    }

    public void setRefer_id(String refer_id) {
        this.refer_id = refer_id;
    }

    public String getCoins() {
        return coins;
    }

    public void setCoins(String coins) {
        this.coins = coins;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
