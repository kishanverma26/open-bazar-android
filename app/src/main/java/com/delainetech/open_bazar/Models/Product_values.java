package com.delainetech.open_bazar.Models;

import java.io.Serializable;

public class Product_values implements Serializable {

    String tital,value,dataarray,image;

    public Product_values(String tital, String value, String dataarray) {
        this.tital = tital;
        this.value = value;
        this.dataarray = dataarray;
    }
  public Product_values(String tital, String value, String dataarray,String image) {
        this.tital = tital;
        this.value = value;
        this.dataarray = dataarray;
      this.image = image;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDataarray() {
        return dataarray;
    }

    public void setDataarray(String dataarray) {
        this.dataarray = dataarray;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTital() {
        return tital;
    }

    public void setTital(String tital) {
        this.tital = tital;
    }


}
