package com.delainetech.open_bazar.Models;

public class Location_Picker {

    String tital;
    boolean select;

    public Location_Picker(String tital, boolean select) {
        this.tital = tital;
        this.select = select;
    }

    public String getTital() {
        return tital;
    }

    public void setTital(String tital) {
        this.tital = tital;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }
}
