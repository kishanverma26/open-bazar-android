package com.delainetech.open_bazar.Models;

import java.io.Serializable;

public class ADS implements Serializable {

    String id,user_id ,ad_title,ad_description,mobile,price,images,category ,city,favorite_count,views_count,like_count,timestamp,
            fav_status,like_status,user_name,user_image,expired_date,offer_count,price_type,main_category,user_regiterdate,ads_count
            ,follow_user,verify_phone,hide_mobileno,shop_name,boost_type,category_id;

    public ADS() {
    }

    public ADS(String id, String user_id, String ad_title, String ad_description, String mobile, String price, String images,
               String category, String city, String favorite_count, String views_count, String like_count, String timestamp,
               String fav_status, String like_status, String user_name, String user_image, String expired_date, String offer_count
            , String price_type, String main_category, String user_regiterdate, String ads_count, String follow_user, String verify_phone
            , String hide_mobileno , String shop_name , String boost_type, String category_id) {
        this.id = id;
        this.user_id = user_id;
        this.ad_title = ad_title;
        this.ad_description = ad_description;
        this.mobile = mobile;
        this.price = price;
        this.images = images;
        this.category = category;
        this.city = city;
        this.favorite_count = favorite_count;
        this.views_count = views_count;
        this.like_count = like_count;
        this.timestamp = timestamp;
        this.fav_status = fav_status;
        this.like_status = like_status;
    this.user_name = user_name;
        this.user_image = user_image;
         this.expired_date = expired_date;
         this.offer_count = offer_count;
        this.price_type = price_type;
        this.main_category = main_category;
      this.user_regiterdate = user_regiterdate;
            this.ads_count = ads_count;
        this.follow_user = follow_user;
         this.verify_phone = verify_phone;
        this.hide_mobileno = hide_mobileno;
         this.shop_name = shop_name;
      this.boost_type = boost_type;
      this.category_id = category_id;


    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getBoost_type() {
        return boost_type;
    }

    public void setBoost_type(String boost_type) {
        this.boost_type = boost_type;
    }

    public String getHide_mobileno() {
        return hide_mobileno;
    }

    public void setHide_mobileno(String hide_mobileno) {
        this.hide_mobileno = hide_mobileno;
    }

    public String getVerify_phone() {
        return verify_phone;
    }

    public void setVerify_phone(String verify_phone) {
        this.verify_phone = verify_phone;
    }

    public String getFollow_user() {
        return follow_user;
    }

    public void setFollow_user(String follow_user) {
        this.follow_user = follow_user;
    }

    public String getUser_regiterdate() {
        return user_regiterdate;
    }

    public void setUser_regiterdate(String user_regiterdate) {
        this.user_regiterdate = user_regiterdate;
    }

    public String getAds_count() {
        return ads_count;
    }

    public void setAds_count(String ads_count) {
        this.ads_count = ads_count;
    }

    public String getMain_category() {
        return main_category;
    }

    public void setMain_category(String main_category) {
        this.main_category = main_category;
    }

    public void setPrice_type(String price_type) {
        this.price_type = price_type;
    }

    public String getPrice_type() {
        return price_type;
    }

    public String getOffer_count() {
        return offer_count;
    }

    public void setOffer_count(String offer_count) {
        this.offer_count = offer_count;
    }

    public String getExpired_date() {
        return expired_date;
    }

    public void setExpired_date(String expired_date) {
        this.expired_date = expired_date;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getFav_status() {
        return fav_status;
    }

    public void setFav_status(String fav_status) {
        this.fav_status = fav_status;
    }

    public String getLike_status() {
        return like_status;
    }

    public void setLike_status(String like_status) {
        this.like_status = like_status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getAd_title() {
        return ad_title;
    }

    public void setAd_title(String ad_title) {
        this.ad_title = ad_title;
    }

    public String getAd_description() {
        return ad_description;
    }

    public void setAd_description(String ad_description) {
        this.ad_description = ad_description;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getFavorite_count() {
        return favorite_count;
    }

    public void setFavorite_count(String favorite_count) {
        this.favorite_count = favorite_count;
    }

    public String getViews_count() {
        return views_count;
    }

    public void setViews_count(String views_count) {
        this.views_count = views_count;
    }

    public String getLike_count() {
        return like_count;
    }

    public void setLike_count(String like_count) {
        this.like_count = like_count;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }
}
