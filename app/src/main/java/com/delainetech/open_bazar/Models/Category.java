package com.delainetech.open_bazar.Models;

public class Category {
   String  id ,en_name ,ar_name ,image,next_status;

    public Category(String id, String en_name, String ar_name, String image, String next_status) {
        this.id = id;
        this.en_name = en_name;
        this.ar_name = ar_name;
        this.image = image;
        this.next_status = next_status;
    }

    public String getId() {
        return id;
    }

    public String getEn_name() {
        return en_name;
    }

    public String getAr_name() {
        return ar_name;
    }

    public String getImage() {
        return image;
    }

    public String getNext_status() {
        return next_status;
    }
}
