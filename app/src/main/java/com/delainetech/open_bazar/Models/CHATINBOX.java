package com.delainetech.open_bazar.Models;

public class CHATINBOX {

    String id,userid,postid;
    boolean seen;

    String username,last_msgusername,last_msg,ad_titel,ad_image,ad_price,ad_postid;
    public CHATINBOX(String id, String userid, String postid,boolean seen) {
        this.id = id;
        this.userid = userid;
        this.postid = postid;
        this.seen = seen;

    }

    public CHATINBOX(String id, String userid, String postid, boolean seen, String username,
                     String last_msgusername, String last_msg, String ad_titel) {
        this.id = id;
        this.userid = userid;
        this.postid = postid;
        this.seen = seen;
        this.username = username;
        this.last_msgusername = last_msgusername;
        this.last_msg = last_msg;
        this.ad_titel = ad_titel;
    }

    public String getAd_postid() {
        return ad_postid;
    }

    public void setAd_postid(String ad_postid) {
        this.ad_postid = ad_postid;
    }

    public String getAd_image() {
        return ad_image;
    }

    public void setAd_image(String ad_image) {
        this.ad_image = ad_image;
    }

    public String getAd_price() {
        return ad_price;
    }

    public void setAd_price(String ad_price) {
        this.ad_price = ad_price;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLast_msgusername() {
        return last_msgusername;
    }

    public void setLast_msgusername(String last_msgusername) {
        this.last_msgusername = last_msgusername;
    }

    public String getLast_msg() {
        return last_msg;
    }

    public void setLast_msg(String last_msg) {
        this.last_msg = last_msg;
    }

    public String getAd_titel() {
        return ad_titel;
    }

    public void setAd_titel(String ad_titel) {
        this.ad_titel = ad_titel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getPostid() {
        return postid;
    }

    public void setPostid(String postid) {
        this.postid = postid;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }
}
