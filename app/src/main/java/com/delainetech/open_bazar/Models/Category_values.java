package com.delainetech.open_bazar.Models;

import java.io.Serializable;

public class Category_values implements Serializable {

    String tital,seletedvalue,id,datavalue,image;

    public Category_values(String tital, String seletedvalue,String id,String datavalue) {
        this.datavalue = datavalue;
        this.tital = tital;
        this.seletedvalue = seletedvalue;
          this.id = id;
    }

    public Category_values(String tital, String seletedvalue, String id, String datavalue, String image) {
        this.tital = tital;
        this.seletedvalue = seletedvalue;
        this.id = id;
        this.datavalue = datavalue;
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSeletedvalue() {
        return seletedvalue;
    }

    public void setSeletedvalue(String seletedvalue) {
        this.seletedvalue = seletedvalue;
    }

    public String getDatavalue() {
        return datavalue;
    }

    public void setDatavalue(String datavalue) {
        this.datavalue = datavalue;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTital() {
        return tital;
    }

    public void setTital(String tital) {
        this.tital = tital;
    }

    public String getValue() {
        return seletedvalue;
    }

    public void setValue(String value) {
        this.seletedvalue = value;
    }
}
