package com.delainetech.open_bazar.Models;

/**
 * Created by AkshayeJH on 24/07/17.
 */

public class Messages {

    private String message, type,status,duration;
    private String  timestamp;
    private boolean seen;
    private String user;
    int view_type;

    public Messages(String message, String type, String timestamp, boolean seen, String user, int view_type,String status,String duration) {
        this.message = message;
        this.type = type;
        this.timestamp = timestamp;
        this.seen = seen;
        this.user = user;
        this.view_type = view_type;
        this.status = status;
        this.duration = duration;

    }

    public String getDuration() {
        return duration;
    }

    public String getStatus() {
        return status;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getView_type() {
        return view_type;
    }

    public void setView_type(int view_type) {
        this.view_type = view_type;
    }

    public Messages(String user) {
        this.user = user;
    }

    public String getuser() {
        return user;
    }

    public void setuser(String user) {
        this.user = user;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String gettimestamp() {
        return timestamp;
    }

    public void settimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }


}
