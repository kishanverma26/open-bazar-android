package com.delainetech.open_bazar.Models;

public class Category_Filter {
    String id,tital,count,image;

    public Category_Filter(String id, String tital, String count, String image) {
        this.id = id;
        this.tital = tital;
        this.count = count;
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTital() {
        return tital;
    }

    public void setTital(String tital) {
        this.tital = tital;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
