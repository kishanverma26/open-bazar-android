package com.delainetech.open_bazar.Models;

public class Gallery {

    String uri;
    boolean is_main;

    public Gallery(String uri, boolean is_main) {
        this.uri = uri;
        this.is_main = is_main;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public boolean isIs_main() {
        return is_main;
    }

    public void setIs_main(boolean is_main) {
        this.is_main = is_main;
    }
}
