package com.delainetech.open_bazar.Models;

public class Following {

    String following_id,name,profile_image,ads_count,followstatus,signupid;

    public Following(String following_id, String name, String profile_image, String ads_count,String followstatus,String signupid) {
        this.following_id = following_id;
        this.name = name;
        this.profile_image = profile_image;
        this.ads_count = ads_count;
        this.followstatus = followstatus;
      this.signupid = signupid;

    }

    public String getSignupid() {
        return signupid;
    }

    public String getFollowstatus() {
        return followstatus;
    }

    public void setFollowstatus(String followstatus) {
        this.followstatus = followstatus;
    }

    public String getFollowing_id() {
        return following_id;
    }

    public void setFollowing_id(String following_id) {
        this.following_id = following_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getAds_count() {
        return ads_count;
    }

    public void setAds_count(String ads_count) {
        this.ads_count = ads_count;
    }
}
