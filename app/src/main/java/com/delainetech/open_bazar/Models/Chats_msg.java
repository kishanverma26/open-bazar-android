package com.delainetech.open_bazar.Models;

public class Chats_msg {

    String messages,msgfrom,timestamp,mesage_id,image;

    public Chats_msg(String messages, String msgfrom, String timestamp, String mesage_id, String image) {
        this.messages = messages;
        this.msgfrom = msgfrom;
        this.timestamp = timestamp;
        this.mesage_id = mesage_id;
        this.image = image;

    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    public String getMsgfrom() {
        return msgfrom;
    }

    public void setMsgfrom(String msgfrom) {
        this.msgfrom = msgfrom;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getMesage_id() {
        return mesage_id;
    }

    public void setMesage_id(String mesage_id) {
        this.mesage_id = mesage_id;
    }


}

