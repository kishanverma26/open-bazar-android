package com.delainetech.open_bazar.Models;

public class Notifications {

    
  String  id,user_id,refer_id,callby,tital,type,timestamp,sender_name,sender_image,status;

    public Notifications(String id, String user_id, String refer_id, String callby, String tital, String type, String timestamp
            , String sender_name, String sender_image, String status) {
        this.id = id;
        this.user_id = user_id;
        this.refer_id = refer_id;
        this.callby = callby;
        this.tital = tital;
        this.type = type;
        this.timestamp = timestamp;
        this.sender_name = sender_name;
        this.sender_image = sender_image;
       this.status = status;

    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSender_name() {
        return sender_name;
    }

    public void setSender_name(String sender_name) {
        this.sender_name = sender_name;
    }

    public String getSender_image() {
        return sender_image;
    }

    public void setSender_image(String sender_image) {
        this.sender_image = sender_image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getRefer_id() {
        return refer_id;
    }

    public void setRefer_id(String refer_id) {
        this.refer_id = refer_id;
    }

    public String getCallby() {
        return callby;
    }

    public void setCallby(String callby) {
        this.callby = callby;
    }

    public String getTital() {
        return tital;
    }

    public void setTital(String tital) {
        this.tital = tital;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
