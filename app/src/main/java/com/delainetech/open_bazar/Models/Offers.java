package com.delainetech.open_bazar.Models;

public class Offers {
    
    String id,user_id ,bid_price ,timestamp,user_name,user_image;

    public Offers(String id, String user_id, String bid_price, String timestamp, String user_name, String user_image) {
        this.id = id;
        this.user_id = user_id;
        this.bid_price = bid_price;
        this.timestamp = timestamp;
        this.user_name = user_name;
        this.user_image = user_image;


    }

    public String getUser_name() {
        return user_name;
    }

    public String getUser_image() {
        return user_image;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getBid_price() {
        return bid_price;
    }

    public void setBid_price(String bid_price) {
        this.bid_price = bid_price;
    }
}
