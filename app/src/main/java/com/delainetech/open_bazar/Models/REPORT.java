package com.delainetech.open_bazar.Models;

public class REPORT {

    String tital;
    Boolean select;

    public REPORT(String tital, Boolean select) {
        this.tital = tital;
        this.select = select;
    }

    public String getTital() {
        return tital;
    }

    public void setTital(String tital) {
        this.tital = tital;
    }

    public Boolean getSelect() {
        return select;
    }

    public void setSelect(Boolean select) {
        this.select = select;
    }
}
