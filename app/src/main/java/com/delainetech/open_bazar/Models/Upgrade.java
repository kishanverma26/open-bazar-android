package com.delainetech.open_bazar.Models;

import java.util.ArrayList;

public class Upgrade {

    ArrayList<String> price,credits;
    String live_ads;

    public Upgrade(ArrayList<String> price, ArrayList<String> credits, String live_ads) {
        this.price = price;
        this.credits = credits;
        this.live_ads = live_ads;
    }

    public ArrayList<String> getPrice() {
        return price;
    }

    public ArrayList<String> getCredits() {
        return credits;
    }

    public String getLive_ads() {
        return live_ads;
    }
}
