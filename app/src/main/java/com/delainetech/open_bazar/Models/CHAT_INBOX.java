package com.delainetech.open_bazar.Models;

public class CHAT_INBOX {

    String image,price,title,postid;


    public CHAT_INBOX(String image, String price, String title, String postid) {
        this.image = image;
        this.price = price;
        this.title = title;
        this.postid = postid;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPostid() {
        return postid;
    }

    public void setPostid(String postid) {
        this.postid = postid;
    }
}
