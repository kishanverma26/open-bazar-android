package com.delainetech.open_bazar;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.delainetech.open_bazar.Adapter.My_Ads_adapter_Other_Users;
import com.delainetech.open_bazar.Custom_classes.DelayedProgressDialog;
import com.delainetech.open_bazar.Custom_classes.EndlessRecyclerViewScrollListener;
import com.delainetech.open_bazar.Models.ADS;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.delainetech.open_bazar.Utils.Common.ParseString;
import static com.delainetech.open_bazar.Utils.Common.logg;

public class Other_User_Profile  extends AppCompatActivity implements ParamKeys, SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {

    CircleImageView userimg;
    String id="";
    TextView tv_adcount,tv_following_count,tv_followers_count,tv_time;
    RelativeLayout rll_myfollowings,rll_myfollowers;
    RecyclerView reclcler;
    LinearLayoutManager linearLayoutManager;
    My_Ads_adapter_Other_Users adapter;
    int index=0;
    EndlessRecyclerViewScrollListener recyclerViewScrollListener;
    SwipeRefreshLayout swiperefresh;
    UserSharedPreferences preferences;
    Context mcontext;
    Toolbar toolbar;
    ArrayList<ADS> adsArrayList=new ArrayList<>();
    TextView tv_username;
    ImageView iv_emailverify,iv_phoneverify,iv_fbverify,iv_googleverify;
    String follow_user="false";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.other_user_profile);
        id=getIntent().getStringExtra("id");
        mcontext=Other_User_Profile.this;
        initia();
    }

    private void initia() {

        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));


        rll_myfollowings=(RelativeLayout) findViewById(R.id.rll_myfollowings);
        rll_myfollowings.setOnClickListener(this);
        rll_myfollowers=(RelativeLayout) findViewById(R.id.rll_myfollowers);
        rll_myfollowers.setOnClickListener(this);

        iv_emailverify=(ImageView) findViewById(R.id.iv_emailverify);
        iv_phoneverify=(ImageView) findViewById(R.id.iv_phoneverify);
        iv_fbverify=(ImageView) findViewById(R.id.iv_fbverify);
        iv_googleverify=(ImageView) findViewById(R.id.iv_googleverify);

       userimg=(CircleImageView)findViewById(R.id.userimg);
        tv_time=(TextView)findViewById(R.id.tv_time);
        tv_following_count=(TextView)findViewById(R.id.tv_following_count);
        tv_adcount=(TextView)findViewById(R.id.tv_adcount);
        tv_followers_count=(TextView)findViewById(R.id.tv_followers_count);
        tv_username=(TextView)findViewById(R.id.tv_username);


        preferences=new UserSharedPreferences(mcontext);

        swiperefresh=(SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        reclcler=(RecyclerView) findViewById(R.id.reclcler);
        linearLayoutManager=new LinearLayoutManager(mcontext);
        reclcler.setLayoutManager(linearLayoutManager);
        adapter=new My_Ads_adapter_Other_Users(mcontext,adsArrayList);
        reclcler.setAdapter(adapter);


        fetech_ads(index,id);
        recyclerViewScrollListener=new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (adsArrayList.size()%10==0){
                    index++;
                    fetech_ads(index,id);
                }

            }
        };
        reclcler.addOnScrollListener(recyclerViewScrollListener);
        reclcler.setNestedScrollingEnabled(false);
        swiperefresh.setOnRefreshListener(this);

        swiperefresh.setColorSchemeColors(getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorPrimary),getResources().getColor(R.color.colorPrimary));


    }


    private MenuItem menu1;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.user_profile,menu);
         menu1=menu.findItem(R.id.menu_follow);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.menu_follow:
                logg("followstatus",follow_user+"");
                if (follow_user.equals("true")) {
                   alert(Other_User_Profile.this,tv_username.getText().toString());
                }else {
                    follow_user(id);
                }
                break;
            case R.id.menu_report:
                Intent intent=new Intent(mcontext, Drawer_Frame_Activity.class);
                intent.putExtra(Pos,14);
                intent.putExtra("type","user");
                intent.putExtra(Title,"User Report");
                intent.putExtra("id",id);
                startActivity(intent);
                break;

        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onRefresh() {
        adsArrayList.clear();
        index=0;
        fetech_ads(index,id);
    }


    public void  fetech_ads(final int index,final String id){

        final DelayedProgressDialog pro=new DelayedProgressDialog();

        pro.show(getSupportFragmentManager(),"");


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "user_details", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                pro.cancel();
                logg("user_details",response);
                try {
                    swiperefresh.setRefreshing(false);
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){
                     tv_username.setText(ParseString(object.getJSONObject("user_detail"),"name"));
                        RequestOptions requestOptions=new RequestOptions().error(R.drawable.userimage).centerCrop();
                        Glide.with(Other_User_Profile.this).load(Common.Image_Loading_Url(
                                ParseString(object.getJSONObject("user_detail"),"profile_image")))
                                .apply(requestOptions).into(userimg);
                        tv_time.setText(getResources().getString(R.string.member_since)+" "+Common.changeDateFormatFromAnother(ParseString(object.getJSONObject("user_detail"),"created_at")));
                        tv_adcount.setText(ParseString(object.getJSONObject("user_detail"),"ads_count"));
                        if (ParseString(object.getJSONObject("user_detail"),"verify_phone").equals("true")){
                            iv_phoneverify.setVisibility(View.VISIBLE);
                        }
                        if (ParseString(object.getJSONObject("user_detail"),"verify_google").equals("true")){
                            iv_emailverify.setVisibility(View.VISIBLE);
                            iv_googleverify.setVisibility(View.VISIBLE);
                        }
                        if (ParseString(object.getJSONObject("user_detail"),"verify_facebook").equals("true")){
                            iv_fbverify.setVisibility(View.VISIBLE);
                        }

                        follow_user=ParseString(object.getJSONObject("user_detail"),"follow_user");
                        tv_followers_count.setText(ParseString(object.getJSONObject("user_detail"),"followers"));
                        tv_following_count.setText(ParseString(object.getJSONObject("user_detail"),"following"));

                        JSONArray jsonArray=object.getJSONArray("data");

                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject object1=jsonArray.getJSONObject(i);

                            adsArrayList.add(new ADS(ParseString(object1,"id"),ParseString(object1,"user_id"),ParseString(object1,"ad_title"),
                                    ParseString(object1,"ad_description"),ParseString(object1,"mobile"),ParseString(object1,"price")
                                    ,ParseString(object1,"images"),ParseString(object1,"category"),ParseString(object1,"city")
                                    ,ParseString(object1,"favorite_count"),ParseString(object1,"views_count"),ParseString(object1,"like_count")
                                    ,ParseString(object1,"timestamp"),ParseString(object1,"fav_stattus"),ParseString(object1,"like_stattus")
                                    ,ParseString(object1,"user_name"),ParseString(object1,"user_image"),
                                    ParseString(object1,"expired_date"), ParseString(object1,"offer_count"),
                                    ParseString(object1,"price_type"),ParseString(object1,"main_category"),
                            ParseString(object.getJSONObject("user_detail"),"created_at"),ParseString(object.getJSONObject("user_detail"),"ads_count")
                         ,follow_user,ParseString(object1,"mobile_verify"),ParseString(object1,"hide_mobileno")
                                    ,ParseString(object1,"shop_name"), ParseString(object1, "boost_type")
                                    , ParseString(object1, "category_id")));

                        }
                        if (follow_user.equals("true")){
                            menu1.setIcon(R.drawable.ic_followers_icon_white);
                        }else {
                            menu1.setIcon(R.drawable.ic_follow_icon);
                        }

                        adapter.notifyDataSetChanged();

                    }


                }
                catch (Exception e){
                    pro.cancel();
                    Log.e("error","e",e);

                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                pro.cancel();
              Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),reclcler);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put("index",index+"");
                params.put(Id,id+"");
                params.put(User_id,preferences.getuserid()+"");



                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        MySingleton.getInstance(context).addToRequestQueue(stringRequest);
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }


    public void  follow_user(final String userid){


        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "follow", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                pro.cancel();
                logg("follow",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){
                        if (object.getString("message").equals("Unfollow Successfully")){
                            menu1.setIcon(R.drawable.ic_follow_icon);
                            follow_user="false";
                        }else {
                            menu1.setIcon(R.drawable.ic_followers_icon_white);
                            follow_user="true";
                        }
                    }

                }
                catch (Exception e){
//                    pro.cancel();
                    Log.e("error","e",e);
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                Log.e("Volleyerror","e",volleyError);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(User_id,preferences.getuserid());
                params.put("following_id",userid);
                Log.e("params",params+"");
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }



    public void alert(Context mcoxt,String name){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mcoxt);

        alertDialog.setMessage("Are you sure you want to unfollow "+name+"?");
        alertDialog.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                follow_user(id);
            }
        });
        alertDialog.setNegativeButton(getResources().getString(R.string.No), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog1= alertDialog.create();
        alertDialog1.show();

        alertDialog1.getButton(alertDialog1.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorAccent));
        alertDialog1.getButton(alertDialog1.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorAccent));
    }

    @Override
    public void onBackPressed() {
        if (getIntent().hasExtra("splash")){
            startActivity(new Intent(Other_User_Profile.this, Home_Screen_Activity.class));
            finish();
        }else {
                finish();
        }
    }

    Intent i;
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rll_myfollowings:
                i=new Intent(mcontext,Drawer_Frame_Activity.class);
                i.putExtra("followingid",id);
                i.putExtra(Pos,18);
                i.putExtra(Title,"Following");
                startActivity(i);

                break;

            case R.id.rll_myfollowers:
                i=new Intent(mcontext,Drawer_Frame_Activity.class);
                i.putExtra("followingid",id);
                i.putExtra(Pos,19);
                i.putExtra(Title,"Followers");
                startActivity(i);

                break;

        }
    }
}
