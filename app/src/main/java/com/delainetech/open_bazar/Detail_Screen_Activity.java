package com.delainetech.open_bazar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import com.delainetech.open_bazar.Adapter.Detail_viewpager_adapter;
import com.delainetech.open_bazar.Models.ADS;
import com.delainetech.open_bazar.Utils.ParamKeys;

import java.util.ArrayList;

import static com.delainetech.open_bazar.Utils.Common.logg;

public class Detail_Screen_Activity extends AppCompatActivity implements ParamKeys {

    ViewPager viewpager;
    Context mcontext;
    Detail_viewpager_adapter detail_viewpager_adapter;
    ArrayList<ADS> myList;
    int pos;
    boolean delete_ad=false;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ad_detail_screen);
        mcontext=Detail_Screen_Activity.this;
        initial();
    }

    private void initial() {
        viewpager=(ViewPager)findViewById(R.id.viewpager);

        myList = (ArrayList<ADS>) getIntent().getSerializableExtra("list");
        pos=getIntent().getIntExtra("pos",0);
        detail_viewpager_adapter=new Detail_viewpager_adapter(getSupportFragmentManager(),myList,this);
        viewpager.setAdapter(detail_viewpager_adapter);
        viewpager.setCurrentItem(pos);
        logg("mylist",myList.size()+"");
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void deleteads(){
        myList.remove(viewpager.getCurrentItem());
        detail_viewpager_adapter.notifyDataSetChanged();
        delete_ad=true;
    }




    @Override
    public void onBackPressed() {
        logg("working","onbackpress");
        if (delete_ad)
        {
            logg("back_press","bck");
            Intent i=new Intent();
            setResult(RESULT_OK,i);
        }
        else
        {
            Intent i=new Intent();
            i.putExtra("mylist",myList);
            setResult(33,i);
        }
        finish();
    }
}
