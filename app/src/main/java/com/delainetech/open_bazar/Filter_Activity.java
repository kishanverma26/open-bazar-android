package com.delainetech.open_bazar;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.delainetech.open_bazar.Adapter.Custom_Spinner_Adapter;
import com.delainetech.open_bazar.Fragments.Select_category_dialog_fragment_filter;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;

import static com.delainetech.open_bazar.Utils.Common.logg;
import static com.delainetech.open_bazar.Utils.ParamKeys.FILTER_CAT;

public class Filter_Activity extends AppCompatActivity implements View.OnClickListener , ParamKeys {


    Spinner spinner,shortby_sp,condition_sp;
    Context mcontext;
    LinearLayout ll_price;
    String price_type="price",price="0";
    Toolbar toolbar;
    TextView tv_location,tv_cat;
    Button bt_filter,bt_filter_reset;
    AppCompatEditText et_pricefrom,et_priceto;
    EditText et_search;
   public String Main_cat_id="0",str_cat="",cat_id="";
   String str_search="",str_location="",str_pricetype="",str_price_from="",str_price_to="",sort_by="",condittion="";

    String arr[]={"Sort By","Price (Low - High)","Price (High - Low)", "Newest", "Oldest"};
    String ar[]={"Price Types","Price", "Ask For Price", "Exchange", "Free"};
    String arrr[]={"Condition","New","Used"};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filter_activity);
        mcontext=Filter_Activity.this;
        initil();
    }

    private void initil() {
        et_search=(EditText) findViewById(R.id.et_search);
        et_pricefrom=(AppCompatEditText) findViewById(R.id.et_pricefrom);
        et_priceto=(AppCompatEditText) findViewById(R.id.et_priceto);
        bt_filter=(Button) findViewById(R.id.bt_filter);
        bt_filter.setOnClickListener(this);
        bt_filter_reset=(Button) findViewById(R.id.bt_filter_reset);
        bt_filter_reset.setOnClickListener(this);
        tv_location=(TextView) findViewById(R.id.tv_location);
        tv_location.setOnClickListener(this);
        tv_cat=(TextView) findViewById(R.id.tv_cat);
        tv_cat.setOnClickListener(this);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);

        ll_price=(LinearLayout) findViewById(R.id.ll_price);
        spinner=(Spinner) findViewById(R.id.spinner);
        shortby_sp=(Spinner) findViewById(R.id.shortby_sp);
        condition_sp=(Spinner) findViewById(R.id.condition_sp);
        Custom_Spinner_Adapter adapterr=new Custom_Spinner_Adapter(this,arr,R.color.dark_text_color);
        shortby_sp.setAdapter(adapterr);

        Custom_Spinner_Adapter adapterrr=new Custom_Spinner_Adapter(this,arrr,R.color.dark_text_color);
        condition_sp.setAdapter(adapterrr);

        Custom_Spinner_Adapter adapter=new Custom_Spinner_Adapter(this,ar,R.color.dark_text_color);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position>1){
                    ll_price.setVisibility(View.GONE);
                }else {
                    ll_price.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_location:
                startActivityForResult(new Intent(mcontext,Location_picker_Activity.class),LOCTAION_CITY);

                break;
            case R.id.bt_filter:
                filter();
                break;
         case R.id.bt_filter_reset:
              alert(mcontext);

                break;

            case R.id.tv_cat:
                Bundle b=new Bundle();
                b.putString(Type,"filter");
                Select_category_dialog_fragment_filter dialogfrg = new Select_category_dialog_fragment_filter();
                dialogfrg.setArguments(b);
                FragmentManager frm = getSupportFragmentManager();
                dialogfrg.show(frm, "");
                break;

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        logg("requestCode1=",requestCode+", resultCode="+resultCode);
        if (requestCode==FILTER_CAT &&  Activity.RESULT_OK==resultCode){
            logg("data",data+"");
            logg("data",data.getStringExtra("name")+"");

            if (data.hasExtra("name")) {
                str_cat=data.getStringExtra("id");
                tv_cat.setText(data.getStringExtra("name"));
                cat_id=data.getStringExtra("id");


            }
        }

        if ((requestCode==LOCTAION_CITY) && (resultCode== RESULT_OK)){
            if (data!=null) {
                tv_location.setText(data.getStringExtra("city"));
                str_location=data.getStringExtra("city");
            }
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.add_ad,menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.item_post:
                filter();
                break;


            case R.id.item_reset:
//                 posted_poupup();
                alert(mcontext);

                break;



        }
        return super.onOptionsItemSelected(item);
    }


    public void alert(Context mcontext){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mcontext);
        alertDialog.setTitle(R.string.reset_all_fields);
        alertDialog.setMessage(getResources().getString(R.string.areyoushure_reset));
        alertDialog.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                resetdata();


            }
        });
        alertDialog.setNegativeButton(getResources().getString(R.string.No), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog1= alertDialog.create();
        alertDialog1.show();

        alertDialog1.getButton(alertDialog1.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorAccent));
        alertDialog1.getButton(alertDialog1.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorAccent));
    }


    private void resetdata(){
        Main_cat_id="0";
        str_cat="";
        cat_id="";
        str_search="";
        str_location="";
        str_pricetype="";
        str_price_from="";
        str_price_to="";
        sort_by="";
        condittion="";
        et_priceto.setText("");
        et_pricefrom.setText("");

        spinner.setSelection(0);
        shortby_sp.setSelection(0);
        condition_sp.setSelection(0);
        et_search.setText("");
        tv_cat.setText(getResources().getString(R.string.category));
        tv_location.setText(getResources().getString(R.string.cities));
        Common.Snakbar(getResources().getString(R.string.reset_all_fields),toolbar);

    }


    private void filter(){
        Intent i=new Intent();
        if (et_search.getText().toString().trim().length()>0){
            str_search=et_search.getText().toString().trim();
            i.putExtra("searchquery",str_search);
        }
        if (!Main_cat_id.equals("0")){
            i.putExtra("main_cat",Main_cat_id);
            i.putExtra("cat_id",cat_id);
        }

        if (str_location.length()>0){
            i.putExtra("location",str_location);
        }

        if (ar[spinner.getSelectedItemPosition()].equals("Price Types") ||
                ar[spinner.getSelectedItemPosition()].equals("Price"))
        {
            i.putExtra("price_type",ar[spinner.getSelectedItemPosition()]);
            i.putExtra("price_from", str_price_from);
            i.putExtra("price_to",str_price_to);

        }else {
            i.putExtra("price_type",ar[spinner.getSelectedItemPosition()]);

        }

        if (shortby_sp.getSelectedItemPosition()!=0){
            sort_by= arr[shortby_sp.getSelectedItemPosition()];
            i.putExtra("sort_by",sort_by);
        }
     if (condition_sp.getSelectedItemPosition()!=0){
                condittion= arrr[shortby_sp.getSelectedItemPosition()];
                i.putExtra("condittion",condittion);
            }

        setResult(RESULT_OK,i);
            finish();
    }

}
