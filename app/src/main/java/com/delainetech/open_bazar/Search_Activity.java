package com.delainetech.open_bazar;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.delainetech.open_bazar.Adapter.Search_adapter;
import com.delainetech.open_bazar.Custom_classes.EndlessRecyclerViewScrollListener;
import com.delainetech.open_bazar.Fragments.Select_category_dialog_fragment_Search_Activity;
import com.delainetech.open_bazar.Models.ADS;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import static com.delainetech.open_bazar.Utils.Common.ParseString;
import static com.delainetech.open_bazar.Utils.Common.logg;

public class Search_Activity extends AppCompatActivity implements View.OnClickListener,SwipeRefreshLayout.OnRefreshListener, ParamKeys
{
    View view;

    RecyclerView rview;
    GridLayoutManager gridLayoutManager;
    UserSharedPreferences userPrefernces;
    SwipeRefreshLayout swipeContainer;
    TextView EmptyView;
    Search_adapter home_adapter;
    Context mcontext;
    MaterialSpinner spinner;
    EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    ImageView iv_location,iv_notification;
    EditText etsearch;
    ArrayList<ADS> adsArrayList=new ArrayList<ADS>();
    int index=0;
    TextView tv_location,tv_cat,tv_filter;
    LinearLayout ll_filter;
    Intent i;
    String type="",str_city="",str_cat="";

    String str_search="",str_location="",str_pricetype="",str_price_from="",str_price_to="",sort_by="",condittion="",price_type="";
    boolean isfilter=false;
    String cat_id="";
    ImageView iv_back;
    TextView tvemptyview;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_activity);
        mcontext=this;
        init();
    }

    private void init()
    {
        tvemptyview=findViewById(R.id.tvemptyview);
        userPrefernces= UserSharedPreferences.getInstance(Search_Activity.this);
        etsearch=(EditText) findViewById(R.id.etsearch);
        iv_back=(ImageView)findViewById(R.id.iv_back);
        iv_back.setOnClickListener(this);
        ll_filter=(LinearLayout) findViewById(R.id.ll_filter);
        tv_filter=(TextView) findViewById(R.id.tv_filter);
        tv_filter.setOnClickListener(this);
        tv_location=(TextView)findViewById(R.id.tv_location);
        tv_location.setOnClickListener(this);
        tv_cat=(TextView)findViewById(R.id.tv_cat);
        tv_cat.setOnClickListener(this);


        spinner=(MaterialSpinner) findViewById(R.id.spinner);
        spinner.setItems("India", "USA", "UAE", "UK");

        rview=(RecyclerView)findViewById(R.id.recyclerview);
        swipeContainer=(SwipeRefreshLayout)findViewById(R.id.Swip_refreshlayout);
        swipeContainer.setOnRefreshListener(this);
        gridLayoutManager=new GridLayoutManager(this,2);
        rview.setLayoutManager(gridLayoutManager);

        home_adapter=new Search_adapter(mcontext,adsArrayList);
        rview.setAdapter(home_adapter);
//        fetech_ads(index,"");
        endlessRecyclerViewScrollListener=new EndlessRecyclerViewScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
              if ( (adsArrayList.size()%10)==0){
//                  index++;
//                  fetech_ads(index,"");
              }
            }
        };
        rview.addOnScrollListener(endlessRecyclerViewScrollListener);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorPrimary),getResources().getColor(R.color.colorPrimary));


        etsearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Common.hideSoftKeyboard(Search_Activity.this);
                    adsArrayList.clear();
                    index=0;
                    str_search=v.getText().toString().trim();
                    fetech_ads(index);
                    return true;
                }
                return false;
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){


            case R.id.tv_location:
                i=new Intent(mcontext,Location_picker_Activity.class);
                i.putExtra("filter",true);
                startActivityForResult(i,LOCTAION_CITY);
                break;

            case R.id.tv_filter:
                startActivityForResult(new Intent(mcontext, Filter_Activity.class),FILTER_ACTIVITY);

                break;
            case R.id.iv_back:
           finish();
            break;

            case R.id.fab_add_ad:
                Intent i=new Intent(mcontext, Adpost_Add_Activity.class);
                startActivityForResult(i,20);
                break;

            case R.id.tv_cat:
                Bundle b=new Bundle();
                b.putString(Type,"filter");
                Select_category_dialog_fragment_Search_Activity dialogfrg = new Select_category_dialog_fragment_Search_Activity();
                dialogfrg.setArguments(b);
                FragmentManager frm = getSupportFragmentManager();
                dialogfrg.show(frm, "");
                break;
        }
    }

    @Override
    public void onRefresh() {
        adsArrayList.clear();
        index=0;
        fetech_ads(index);
        swipeContainer.setRefreshing(false);
    }


    public void  fetech_ads(final int index)
    {
        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "search_ads", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                logg("search_ads",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("true")){

                        JSONArray jsonArray=object.getJSONArray("data");

                        if(jsonArray.length()>0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object1 = jsonArray.getJSONObject(i);

                                adsArrayList.add(new ADS(ParseString(object1, "id"), ParseString(object1, "user_id"), ParseString(object1, "ad_title"),
                                        ParseString(object1, "ad_description"), ParseString(object1, "mobile"), ParseString(object1, "price")
                                        , ParseString(object1, "images"), ParseString(object1, "category"), ParseString(object1, "city")
                                        , ParseString(object1, "favorite_count"), ParseString(object1, "views_count"), ParseString(object1, "like_count")
                                        , ParseString(object1, "timestamp"), ParseString(object1, "fav_stattus"), ParseString(object1, "like_stattus")
                                        , ParseString(object1, "user_name"), ParseString(object1, "user_image"),
                                        ParseString(object1, "expired_date"), ParseString(object1, "offer_count"),
                                        ParseString(object1, "price_type"), ParseString(object1, "main_category"),
                                        ParseString(object1, "user_regiterdate"), ParseString(object1, "ads_count")
                                        , ParseString(object1, "follow_user"), ParseString(object1, "mobile_verify")
                                        , ParseString(object1, "hide_mobileno"), ParseString(object1, "shop_name")
                                        , ParseString(object1, "boost_type"), ParseString(object1, "category_id")));
                            }
                            logg("size", adsArrayList.size() + "");
                            home_adapter = new Search_adapter(mcontext, adsArrayList);
                            rview.setAdapter(home_adapter);
                        }
                        else
                        {

                        }
                    }

                }
                catch (Exception e){ }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Common.Snakbar(Common.volleyerror(volleyError),iv_back);
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+userPrefernces.gettoken());
                logg("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put("index",index+"");
                params.put(User_id,userPrefernces.getuserid()+"");

                if (str_search.length()>0) {
                    params.put("search", str_search);
                }
                if (str_city.length()>0){
                    params.put("city", str_city);
                }
                if (str_cat.length()>0 ){

                    params.put("cat_id",str_cat);
                }
                logg("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        MySingleton.getInstance(context).addToRequestQueue(stringRequest);
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        logg("requestCode=",requestCode+", resultCode="+resultCode);
        if (requestCode==20 &&  Activity.RESULT_OK==resultCode)
        {

            adsArrayList.clear();
            index=0;
            fetech_ads(index);

        }
        else if (requestCode==ADS_DELETE_RESULT_CODE &&  Activity.RESULT_OK==resultCode)
        {
            adsArrayList.clear();
            index=0;
            fetech_ads(index);
        }
        else if (requestCode==LOCTAION_CITY &&  Activity.RESULT_OK==resultCode)
        {
            logg("data",data+"");
            logg("data",data.getStringExtra("city")+"");

            if (data.hasExtra("city"))
            {
                if(data.getStringExtra("city").equalsIgnoreCase("All Cities"))
                {
                    str_city = "";
                }
                else {
                    str_city = data.getStringExtra("city");
                }
                tv_location.setText(data.getStringExtra("city"));
            }
            adsArrayList.clear();
            index=0;
            filter(index);
        }
        else if (requestCode==FILTER_CAT &&  Activity.RESULT_OK==resultCode)
        {
            logg("data_id",data.getStringExtra("id")+"");
            logg("data",data.getStringExtra("name")+"");

            if (data.hasExtra("name")) {
                str_cat=data.getStringExtra("id");
                tv_cat.setText(data.getStringExtra("name"));
                str_cat=data.getStringExtra("id");
            }
            adsArrayList.clear();
            index=0;
            fetech_ads(index);
        }
        else if (requestCode==FILTER_ACTIVITY &&  Activity.RESULT_OK==resultCode)
        {
            str_search="";
            str_location="";
            str_pricetype="";
            str_price_from="";
            str_price_to="";
            sort_by="";
            condittion="";
            isfilter=true;
            index=0;
            if (data.hasExtra("location"))
            {
                str_city=data.getStringExtra("location");
                tv_location.setText(data.getStringExtra("location"));
            }

            if (data.hasExtra("searchquery"))
            {
                str_search=data.getStringExtra("searchquery");
            }
            if (data.hasExtra("main_cat") && data.hasExtra("cat_id"))
            {
                type=data.getStringExtra("main_cat");
                cat_id=data.getStringExtra("cat_id");
            }

            if (data.hasExtra("price_type"))
            {
                price_type=data.getStringExtra("price_type");
            }

            if (data.hasExtra("price_from")){
                str_price_from=data.getStringExtra("price_from");
            }
            if (data.hasExtra("price_to")){
                str_price_to=data.getStringExtra("price_to");
            }
            if (data.hasExtra("sort_by")){
                sort_by=data.getStringExtra("sort_by");
            }
            if (data.hasExtra("condittion")){
                sort_by=data.getStringExtra("condittion");
            }

            adsArrayList.clear();
            index=0;
            fetech_ads(index);
        }
    }


    public void  filter(final int index){

        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "search_ads", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                logg("search_ads",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("true")){

                        JSONArray jsonArray=object.getJSONArray("data");

                        if(jsonArray.length()>0)
                        {
                            swipeContainer.setVisibility(View.VISIBLE);
                            tvemptyview.setVisibility(View.GONE);
                            for (int i = 0; i < jsonArray.length(); i++)
                            {
                                JSONObject object1 = jsonArray.getJSONObject(i);

                                adsArrayList.add(new ADS(ParseString(object1, "id"), ParseString(object1, "user_id"), ParseString(object1, "ad_title"),
                                        ParseString(object1, "ad_description"), ParseString(object1, "mobile"), ParseString(object1, "price")
                                        , ParseString(object1, "images"), ParseString(object1, "category"), ParseString(object1, "city")
                                        , ParseString(object1, "favorite_count"), ParseString(object1, "views_count"), ParseString(object1, "like_count")
                                        , ParseString(object1, "timestamp"), ParseString(object1, "fav_stattus"), ParseString(object1, "like_stattus")
                                        , ParseString(object1, "user_name"), ParseString(object1, "user_image"),
                                        ParseString(object1, "expired_date"), ParseString(object1, "offer_count"),
                                        ParseString(object1, "price_type"), ParseString(object1, "main_category"),
                                        ParseString(object1, "user_regiterdate"), ParseString(object1, "ads_count")
                                        , ParseString(object1, "follow_user"), ParseString(object1, "mobile_verify")
                                        , ParseString(object1, "hide_mobileno"), ParseString(object1, "shop_name")
                                        , ParseString(object1, "boost_type"), ParseString(object1, "category_id")));
                            }
                            logg("size", adsArrayList.size() + "");
                            home_adapter = new Search_adapter(mcontext, adsArrayList);
                            rview.setAdapter(home_adapter);
                        }
                        else
                        {
                            tvemptyview.setVisibility(View.VISIBLE);
                            swipeContainer.setVisibility(View.GONE);
                        }
                    }

                }
                catch (Exception e){ }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Common.Snakbar(Common.volleyerror(volleyError),iv_back);
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+userPrefernces.gettoken());
                logg("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put("index",index+"");
                params.put(User_id,userPrefernces.getuserid()+"");
                if (str_search.length()>0) {
                    params.put("search", str_search);
                }
                if (str_city.length()>0){
                    params.put("city", str_city);
                }
                if (price_type.length()>0){
                    params.put("price_type", price_type);
                }
                if (str_price_from.length()>0){
                    params.put("price_min",str_price_from);
                }

                if (str_price_to.length()>0){
                    params.put("price_max",str_price_to);
                }
                if (condittion.length()>0){
                    params.put("condition",condittion);
                }
                if (sort_by.length()>0){
                    params.put("sort",sort_by);
                }


                logg("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }

}
