package com.delainetech.open_bazar;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.delainetech.open_bazar.Adapter.Notification_adapter;
import com.delainetech.open_bazar.Adapter.Offer_adapter;
import com.delainetech.open_bazar.Custom_classes.EndlessRecyclerViewScrollListener;
import com.delainetech.open_bazar.Models.ADS;
import com.delainetech.open_bazar.Models.Notifications;
import com.delainetech.open_bazar.Models.Offers;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import static com.delainetech.open_bazar.Utils.Common.ParseString;
import static com.delainetech.open_bazar.Utils.Common.logg;

public class All_offers_Activity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, ParamKeys, View.OnClickListener {

    UserSharedPreferences preferences;
    RecyclerView rv_notification;
    LinearLayoutManager manager;
    Offer_adapter notification_adapter;
    ArrayList<Offers> notifications;
    EndlessRecyclerViewScrollListener scrollListener;
    int index=0;
    Context mcontext;
    Toolbar toolbar;
    SwipeRefreshLayout swipeContainer;
    public String id="",bid_type="0";
    EditText et_offer;
    TextView tv_tital;
    ImageView ivsendButton;
    int offer_count=0;
    LinearLayout ll;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.all_offers_activity);
        initiliz();
    }

    private void initiliz()
    {
        mcontext=All_offers_Activity.this;
        preferences=UserSharedPreferences.getInstance(mcontext);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ivsendButton=(ImageView) findViewById(R.id.ivsendButton);
        ivsendButton.setOnClickListener(this);
        tv_tital=findViewById(R.id.tv_tital);

        et_offer=(EditText) findViewById(R.id.et_offer);
        swipeContainer=(SwipeRefreshLayout)findViewById(R.id.Swip_refreshlayout);
        swipeContainer.setOnRefreshListener(this);

        id=getIntent().getStringExtra("id");
        bid_type=getIntent().getStringExtra("bid_type");
        offer_count=getIntent().getIntExtra("count",0);
        logg("dataa",id+"----"+bid_type+"---"+offer_count);


        notifications=new ArrayList<>();
        ll=(LinearLayout) findViewById(R.id.ll);
        rv_notification=(RecyclerView) findViewById(R.id.recyclerview);
        manager=new LinearLayoutManager(mcontext);
        rv_notification.setLayoutManager(manager);
        notification_adapter=new Offer_adapter(mcontext,notifications,bid_type);
        rv_notification.setAdapter(notification_adapter);
        fetech_offers(index);

        scrollListener=new EndlessRecyclerViewScrollListener(manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if ( (notifications.size()%10)==0){
                    index++;
                    fetech_offers(index);
                }
            }
        };

        rv_notification.addOnScrollListener(scrollListener);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorPrimary),getResources().getColor(R.color.colorPrimary));


        if(bid_type.equalsIgnoreCase("1"))
        {
            tv_tital.setText("All Comments");
            et_offer.setHint("Enter Comment");
            et_offer.setInputType(InputType.TYPE_CLASS_TEXT);
        }
        else
        {
            tv_tital.setText("All Offers");
            et_offer.setHint("Enter The Amount Of Offer");
            et_offer.setInputType(InputType.TYPE_CLASS_NUMBER);
        }

        if (getIntent().hasExtra("type")){
            ll.setVisibility(View.GONE);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
              onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onRefresh() {
    index=0;
    notifications.clear();
    fetech_offers(index);
    }

    public void  fetech_offers(final int index){

        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "get_offer_bid", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                logg("get_offer_bid",response);
                try {
                    swipeContainer.setRefreshing(false);
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){

                        JSONArray jsonArray=object.getJSONArray("data");

                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject object1=jsonArray.getJSONObject(i);

                            notifications.add(new Offers(ParseString(object1,"id"),ParseString(object1,"user_id"),
                                    ParseString(object1,"bid_price"),ParseString(object1,"timestamp"),
                                    ParseString(object1,"user_name"),ParseString(object1,"user_image")));
                        }
                        notification_adapter.notifyDataSetChanged();

                    }


                }
                catch (Exception e){
                    Log.e("error","e",e);

                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("Volleyerror","e",volleyError);
                Common.Snakbar(Common.volleyerror(volleyError),rv_notification);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put("index",index+"");
                params.put("post_id",id);
                params.put("bid_type",bid_type);
                Log.e("params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }

    @Override
    public void onClick(View v)
    {
        if (et_offer.getText().toString().trim().length()>0)
        {
            offer_bid(id);
        }
        else {
            Common.Snakbar("Place a bid amount",et_offer);
        }
    }

    public void  offer_bid(final String postid){

        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "offer_bid", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                logg("insert_ads",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){

                        et_offer.setText("");
                        if (bid_type.equals("0")) {
                            Common.Snakbar("Your offer has been placed", et_offer);
                        }else {
                            Common.Snakbar("Your comment has been placed",et_offer);
                        }
                    notifications.add(0,new Offers(ParseString(object.getJSONObject("data"),"id"),ParseString(object.getJSONObject("data"),"user_id"),
                       ParseString(object.getJSONObject("data"),"bid_price"),ParseString(object.getJSONObject("data"),"timestamp"),
                            preferences.getname(),preferences.getimage()));

                        offer_count=offer_count+1;
                        notification_adapter.notifyDataSetChanged();

                    }

                }
                catch (Exception e){
                    Log.e("error","e",e);
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                Log.e("Volleyerror","e",volleyError);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(User_id,preferences.getuserid());
                params.put(Post_id,postid);
                params.put(Bid_type,bid_type);
                params.put(Bid_price,et_offer.getText().toString().trim());
                params.put(Ttimestamp,System.currentTimeMillis()+"");

                Log.e("params",params+"");
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontext);
        mRequestQueue.add(stringRequest);

    }


    public void delete(){

        offer_count=offer_count-1;
    }

    @Override
    public void onBackPressed() {
        Intent i=new Intent();
        i.putExtra("count",offer_count);
        setResult(RESULT_OK,i);

        logg("offer",offer_count+"");
        finish();
    }



}
