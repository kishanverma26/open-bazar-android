package com.delainetech.open_bazar;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.provider.Settings;
import android.util.Base64;
import android.util.Log;


import com.crashlytics.android.Crashlytics;
import com.delainetech.open_bazar.Custom_classes.ITimeCount;
import com.delainetech.open_bazar.Custom_classes.TimeCount;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;

import io.fabric.sdk.android.Fabric;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;

import static com.delainetech.open_bazar.Utils.Common.logg;
import static com.delainetech.open_bazar.Utils.ParamKeys.Pos;
import static com.delainetech.open_bazar.Utils.ParamKeys.Title;

public class Splash_Activity extends AppCompatActivity implements ITimeCount {
    String TAG="Splash_Activity";
    private TimeCount timeCount;
    UserSharedPreferences preferences;
    UserSharedPreferences userSharedPreferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.splash_screen);
        preferences=new UserSharedPreferences(Splash_Activity.this);
        userSharedPreferences=new UserSharedPreferences(Splash_Activity.this,"");
        String androidId = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        userSharedPreferences.setdeviceid(androidId);
        logg("deviceid",androidId);
        this.timeCount = new TimeCount(3000L, 1000L, (ITimeCount)this);

    }

    protected void onResume() {
        super.onResume();
        if(this.timeCount != null) {
            TimeCount var10000 = this.timeCount;
            if(this.timeCount == null) {

            }

            var10000.start();
        }

    }

    protected void onPause() {
        super.onPause();
        if(this.timeCount != null) {
            TimeCount var10000 = this.timeCount;
            if(this.timeCount == null) {

            }

            var10000.cancel();
//            video.pause();
        }

    }

    protected void onDestroy() {
        super.onDestroy();
        if(this.timeCount != null) {
            TimeCount var10000 = this.timeCount;
            if(this.timeCount == null) {

            }

            var10000.cancel();
        }

    }

    @Override
    public void OnTickListener(long timetick) {

    }

    @Override
    public void OnFinish() {

        get_dynamiclink();

    }

    protected static String printKeyHash(Context context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", "e1", e1);
        } catch (NoSuchAlgorithmException e) {
            Log.e("No such an al gorithm", "e", e);
        } catch (Exception e) {
            Log.e("Exception", "e", e);
        }

        return key;
    }



    private void get_dynamiclink(){
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        // Get deep link from result (may be null if no link is found)
                        Uri deepLink = null;
                        String id="";
                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();
                            logg("login",preferences.getlogin()+"");
                            if (preferences.getlogin()){
                            if (deepLink.toString().contains("https://openbazar.page.link?=myprofile/")){
                                id=deepLink.toString().replace("https://openbazar.page.link?=myprofile/","");
                                logg("deepLinkid3",id+"");
                                Intent ii=new Intent(Splash_Activity.this, Other_User_Profile.class);
                                ii.putExtra("id",id);
                                ii.putExtra("splash",true);
                                 startActivity(ii);
                                finish();
                            }else  if(deepLink.toString().contains("https://openbazar.page.link?=adpost/")){
                                id=deepLink.toString().replace("https://openbazar.page.link?=adpost/","");
                                logg("deepLinkid2",id+"");
                                List<String> animalList = Arrays.asList(id.split(","));
                               if (animalList.size()>1) {
                                   String user_id = animalList.get(1);
                                   String post_id = animalList.get(0);
                                   logg("deepLinkid2",user_id+""+post_id);

                                   if (preferences.getuserid().equals(user_id)){
                                     Intent  i=new Intent(Splash_Activity.this,Drawer_Frame_Activity.class);
                                       i.putExtra(Pos,0);
                                       i.putExtra(Title,"My Ads");
                                       startActivity(i);
                                   }else {
                                       Intent ii=new Intent(Splash_Activity.this, Ads_Detail_Activity.class);
                                       ii.putExtra("id",post_id);
                                       startActivity(ii);
                                       finish();
                                   }
                               }

                            } else  {
                                startActivity(new Intent(Splash_Activity.this, Home_Screen_Activity.class));
                                finish();
                            }

                            } else if (deepLink.toString().contains("https://openbazar.page.link?=reffer/")){
                                id=deepLink.toString().replace("https://openbazar.page.link?=reffer/","");
                                logg("deepLinkid1",id+"");
                                preferences.setrefferuserid(id);
                                    startActivity(new Intent(Splash_Activity.this, Login_Activity.class));
                                    finish();
                                    logg("login","33");
                            }else  {
                                startActivity(new Intent(Splash_Activity.this, Login_Activity.class));
                                finish();
                                logg("login","22");
                            }

                        }else {
                            if (preferences.getlogin()){
                                startActivity(new Intent(Splash_Activity.this, Home_Screen_Activity.class));
                                finish();
                            }else {
                                startActivity(new Intent(Splash_Activity.this, Login_Activity.class));
                                finish();
                                logg("login","11");
                            }
                        }
                        logg("deepLink",deepLink+"");


                        // Handle the deep link. For example, open the linked
                        // content, or apply promotional credit to the user's
                        // account.
                        // ...

                        // ...
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.v(TAG, "getDynamicLink:onFailure", e);
                    }
                });
    }
}
