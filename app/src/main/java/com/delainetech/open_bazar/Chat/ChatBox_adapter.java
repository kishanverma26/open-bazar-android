package com.delainetech.open_bazar.Chat;

import android.app.Dialog;
import android.content.Context;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.delainetech.open_bazar.Chat.ChatActivity;
import com.delainetech.open_bazar.Models.Messages;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;
import com.makeramen.roundedimageview.RoundedImageView;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
import static com.delainetech.open_bazar.Utils.Common.logg;

/**
 * Created by Kishan on 11-Mar-18.
 */

public class ChatBox_adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ParamKeys {

    ArrayList<Messages> chatlist;
    Context mcontex;
    String Ts="",pTs="";
    UserSharedPreferences preferences;
    RequestOptions options=new RequestOptions().error(R.drawable.logo_bazar_no_image_up).centerCrop();
    private MediaPlayer player = null;
    MediaPlayer mp;
    String ad_tital="",ad_image="",ad_price="";
    public ChatBox_adapter(ArrayList<Messages> chat, Context mcontex,String ad_tital,String ad_image,String ad_price) {
        Collections.reverse(chat);
        this.chatlist = chat;
        this.ad_tital = ad_tital;
        this.ad_image = ad_image;
        this.ad_price = ad_price;
        this.mcontex = mcontex;
        preferences=new UserSharedPreferences(mcontex);
        setHasStableIds(true);
        mp=new MediaPlayer();
    }

//   User 1= text ,3 =Image ,5=audio ,7=location ,9=quickreply ,11=offer
//Otheruser 2= Text ,4=Image ,6=audio, 8 = location,6=audio ,10=quickreply ,12=offer
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        logg("viewtype",viewType+"");
        switch (viewType) {
            case 1:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_view_user_text, parent, false);
                return new UserViewholder(view);
            case 2:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_view_otheruser_text, parent, false);
                return new VendorViewholder(view);
            case 3:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_view_user_image, parent, false);
                return new UserImageViewholder(view);
            case 4:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_view_otheruser_image, parent, false);
                return new OtherUserImageViewholder(view);
            case 5:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_view_user_audio, parent, false);
                return new UserAudioViewholder(view);
                 case 6:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_view_otheruser_audio, parent, false);
                return new UserAudioViewholder(view);
            case 7:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_view_user_image, parent, false);
                return new UserImageViewholder(view);
            case 8:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_view_otheruser_image, parent, false);
                return new OtherUserImageViewholder(view);
            case 9:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_view_user_quick_msg_lay, parent, false);
                return new QuickMsgViewholder(view);
      case 10:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_view_user_quick_msg_lay, parent, false);
                return new QuickMsgViewholder(view);
          case 11:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_view_user_offer_accept, parent, false);
                return new OfferViewholder(view);
         case 12:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_view_user_offer_accept, parent, false);
                return new OfferViewholder(view);

        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        logg("chatbox",position+",,"+chatlist.get(position).getMessage());
        switch (chatlist.get(position).getView_type()){
            case 1:
                ((UserViewholder)holder).tvmsg.setText(chatlist.get(position).getMessage());
                    ((UserViewholder) holder).tvtime.setText(unixtimetotime(Long.parseLong(chatlist.get(position).gettimestamp())));
                    setdatetiem(position,((UserViewholder)holder).date);

                    logg("boolen","pos="+position+" ,"+chatlist.get(position).isSeen()+"");
                    if (chatlist.get(position).isSeen()){
                        ((UserViewholder) holder).tvtime.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_chat_double_tick_indicator,0);

                    }else {
                        ((UserViewholder) holder).tvtime.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_chat_delivery_status_single_check_darkgrey,0);

                    }
                break;
            case 2:
                ((VendorViewholder)holder).tvmsg.setText(chatlist.get(position).getMessage());
                    ((VendorViewholder) holder).tvtime.setText(unixtimetotime(Long.parseLong(chatlist.get(position).gettimestamp())));
                    setdatetiem(position,((VendorViewholder)holder).date);


                break;
            case 3:

                    Glide.with(mcontex).load(Common.Image_Loading_Url(chatlist.get(position).getMessage())).apply(options)
                            .transition(withCrossFade())
                            .into(((UserImageViewholder) holder).ivmsg);

                    ((UserImageViewholder) holder).tvtime.setText(unixtimetotime(Long.parseLong(chatlist.get(position).gettimestamp())));
                    setdatetiem(position,((UserImageViewholder)holder).date);

                if (chatlist.get(position).isSeen()){
                    ((UserImageViewholder) holder).tvtime.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_chat_double_tick_indicator,0);

                }else {
                    ((UserImageViewholder) holder).tvtime.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_chat_delivery_status_single_check_white,0);

                }
                ((UserImageViewholder) holder).ivmsg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mediaDialog(Common.Image_Loading_Url(chatlist.get(position).getMessage()));
                    }
                });

                break;

         case 4:
                Glide.with(mcontex).load(Common.Image_Loading_Url(chatlist.get(position).getMessage())).apply(options)
                        .transition(withCrossFade())
                        .into(((OtherUserImageViewholder)holder).ivmsg);

                    ((OtherUserImageViewholder) holder).tvtime.setText(unixtimetotime(Long.parseLong(chatlist.get(position).gettimestamp())));
                    setdatetiem(position,((OtherUserImageViewholder)holder).date);
             ((OtherUserImageViewholder) holder).ivmsg.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     mediaDialog(Common.Image_Loading_Url(chatlist.get(position).getMessage()));
                 }
             });
                break;

     case 5:


         setAudioData(position,((UserAudioViewholder)holder));
         break;

            case 6:
                setAudioData(position,((UserAudioViewholder)holder));
                break;

            case 7:
                logg("urlmap",Googlemap_imgurl+chatlist.get(position).getMessage());

                Glide.with(mcontex).load(Googlemap_imgurl+chatlist.get(position).getMessage())
                        .transition(withCrossFade())
                        .apply(new RequestOptions().centerCrop().error(R.drawable.ic_google_map))
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                              logg("glideload","loadfail");

                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                logg("glideload","loadsuccess");
                                return false;
                            }
                        })
                            .into(((UserImageViewholder) holder).ivmsg);

                ((UserImageViewholder) holder).tvtime.setText(unixtimetotime(Long.parseLong(chatlist.get(position).gettimestamp())));
                setdatetiem(position,((UserImageViewholder)holder).date);


                ((UserImageViewholder)holder).ivmsg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Uri gmmIntentUri = Uri.parse("google.navigation:q="+chatlist.get(position).getMessage());
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                      mcontex.  startActivity(mapIntent);
                    }
                });

                if (chatlist.get(position).isSeen()){
                    ((UserImageViewholder) holder).tvtime.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_chat_double_tick_indicator,0);

                }else {
                    ((UserImageViewholder) holder).tvtime.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_chat_delivery_status_single_check_darkgrey,0);

                }
                break;
            case 8:
                logg("urlmap",Googlemap_imgurl+chatlist.get(position).getMessage());
                Glide.with(mcontex).load(Googlemap_imgurl+chatlist.get(position).getMessage())
                        .apply(new RequestOptions().centerCrop().error(R.drawable.ic_google_map))
                        .transition(withCrossFade())
                        .into(((OtherUserImageViewholder)holder).ivmsg);

                ((OtherUserImageViewholder) holder).tvtime.setText(unixtimetotime(Long.parseLong(chatlist.get(position).gettimestamp())));
                setdatetiem(position,((OtherUserImageViewholder)holder).date);

                ((OtherUserImageViewholder)holder).ivmsg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Uri gmmIntentUri = Uri.parse("google.navigation:q="+chatlist.get(position).getMessage());
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        mcontex.  startActivity(mapIntent);
                    }
                });
                break;
            case 9:

                set_quickreplydata((QuickMsgViewholder)holder,position );
                              break;
              case 10:

                set_quickreplydata((QuickMsgViewholder)holder,position );
                              break;

              case 11:
                  ((OfferViewholder)holder).tv_price.setText(chatlist.get(position).getMessage());
                  ((OfferViewholder) holder).tvtime.setText(unixtimetotime(Long.parseLong(chatlist.get(position).gettimestamp())));
                  setdatetiem(position,((OfferViewholder)holder).date);
                  ((OfferViewholder)holder).rl_accept.setVisibility(View.GONE);
                  ((OfferViewholder)holder).rl_reject.setVisibility(View.GONE);
                  ((OfferViewholder)holder).tv_headertext.setText(mcontex.getString(R.string.your_offer));

                  if (chatlist.get(position).getStatus().equals("pending")){
                      ((OfferViewholder)holder).rl_accept.setVisibility(View.GONE);
                      ((OfferViewholder)holder).rl_reject.setVisibility(View.GONE);
                      ((OfferViewholder)holder).tv_headertext.setText("You have offered "+chatlist.get(position).getMessage());
                      ((OfferViewholder)holder).tv_price.setText("Please wait for the user response");
                      ((OfferViewholder)holder).tv_price.setTextColor(mcontex.getResources().getColor(R.color.c1) );



                  }else if (chatlist.get(position).getStatus().equals("accepted")){
                      ((OfferViewholder)holder).rl_accept.setVisibility(View.VISIBLE);
                      ((OfferViewholder)holder).rl_reject.setVisibility(View.GONE);
                      ((OfferViewholder)holder).tv_pleaseconn.setVisibility(View.VISIBLE);
                      ((OfferViewholder)holder).tv_accept.setText("Accepted");

                  }else if (chatlist.get(position).getStatus().equals("rejected")){
                      ((OfferViewholder)holder).rl_accept.setVisibility(View.GONE);
                      ((OfferViewholder)holder).rl_reject.setVisibility(View.VISIBLE);
                      ((OfferViewholder)holder).tv_makeoffer.setVisibility(View.VISIBLE);

                      ((OfferViewholder)holder).tv_rejecttext.setText("Rejected");
                  }

                  break;
              case 12:
                  ((OfferViewholder)holder).tv_price.setText(chatlist.get(position).getMessage());
                  ((OfferViewholder) holder).tvtime.setText(unixtimetotime(Long.parseLong(chatlist.get(position).gettimestamp())));
                  setdatetiem(position,((OfferViewholder)holder).date);
                    if (chatlist.get(position).getStatus().equals("pending")){
                        ((OfferViewholder)holder).rl_accept.setVisibility(View.VISIBLE);
                        ((OfferViewholder)holder).rl_reject.setVisibility(View.VISIBLE);
                        ((OfferViewholder)holder).tv_accept.setText("Accept");
                        ((OfferViewholder)holder).tv_rejecttext.setText("Reject");

                    }else if (chatlist.get(position).getStatus().equals("accepted")){
                        ((OfferViewholder)holder).rl_accept.setVisibility(View.VISIBLE);
                        ((OfferViewholder)holder).rl_reject.setVisibility(View.GONE);
                        ((OfferViewholder)holder).tv_accept.setText("Accepted");

                        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                                RelativeLayout.LayoutParams.WRAP_CONTENT,
                                RelativeLayout.LayoutParams.WRAP_CONTENT,
                                2.0f
                        );

                        ((OfferViewholder)holder).rl_price.setLayoutParams(param);
                    }else if (chatlist.get(position).getStatus().equals("rejected")){
                        ((OfferViewholder)holder).rl_accept.setVisibility(View.GONE);
                        ((OfferViewholder)holder).rl_reject.setVisibility(View.VISIBLE);

                        ((OfferViewholder)holder).tv_rejecttext.setText("Rejected");
                    }

                  ((OfferViewholder)holder).tv_accept.setOnClickListener(new View.OnClickListener() {
                      @Override
                      public void onClick(View v) {
                          logg("accept",((OfferViewholder)holder).tv_accept.getText().toString());
                          if ( ((OfferViewholder)holder).tv_accept.getText().toString().equals("Accept")){
//                              Toast.makeText(mcontex, "click1", Toast.LENGTH_SHORT).show();

                              ((ChatActivity)mcontex).update_status(chatlist.get(position).gettimestamp(),"accepted");

                              ((OfferViewholder)holder).rl_accept.setVisibility(View.VISIBLE);
                              ((OfferViewholder)holder).rl_reject.setVisibility(View.GONE);
                              ((OfferViewholder)holder).tv_accept.setText("Accepted");
                          }
                      }
                  });
                  ((OfferViewholder)holder).tv_rejecttext.setOnClickListener(new View.OnClickListener() {
                      @Override
                      public void onClick(View v) {

                          if ( ((OfferViewholder)holder).tv_rejecttext.getText().toString().equals("Reject")){
                              Toast.makeText(mcontex, "click2", Toast.LENGTH_SHORT).show();

                              ((ChatActivity)mcontex).update_status(chatlist.get(position).gettimestamp(),"rejected");
                              ((OfferViewholder)holder).rl_accept.setVisibility(View.GONE);
                              ((OfferViewholder)holder).rl_reject.setVisibility(View.VISIBLE);
                              ((OfferViewholder)holder).tv_rejecttext.setText("Rejected");
                          }
                      }
                  });

                  break;


        }
          }

    @Override
    public int getItemCount() {
        return chatlist.size();
    }

    class UserViewholder extends RecyclerView.ViewHolder{
        TextView tvmsg,tvtime,date;
        public UserViewholder(View itemView) {
            super(itemView);
            date=itemView.findViewById(R.id.timestamp);
            tvmsg=itemView.findViewById(R.id.tvmsg);
            tvtime=itemView.findViewById(R.id.tvtime);


        }
    }
    class UserImageViewholder extends RecyclerView.ViewHolder{
        TextView tvtime,date;
        RoundedImageView ivmsg;
        public UserImageViewholder(View itemView) {
            super(itemView);
            date=itemView.findViewById(R.id.timestamp);
            ivmsg=(RoundedImageView)itemView.findViewById(R.id.ivmsg);
            tvtime=itemView.findViewById(R.id.tvtime);


        }
    }


    class UserAudioViewholder extends RecyclerView.ViewHolder{
        TextView tvtime,date,tv_duration;
        ImageView ivmsg;
        SeekBar seek;
         MediaPlayer player = null;
        public UserAudioViewholder(View itemView) {
            super(itemView);
            date=itemView.findViewById(R.id.timestamp);
            ivmsg=itemView.findViewById(R.id.ivmsg);
            tv_duration=itemView.findViewById(R.id.tv_duration);
            tvtime=itemView.findViewById(R.id.tvtime);
            seek=itemView.findViewById(R.id.seek);
            player = new MediaPlayer();

        }
    }

    class QuickMsgViewholder extends RecyclerView.ViewHolder{
        TextView tvtime,date,tv_headertext,tv_tital,tv_price,tv_date;
        ImageView iv_image;

        public QuickMsgViewholder(View itemView) {
            super(itemView);
            date=itemView.findViewById(R.id.timestamp);
            iv_image=itemView.findViewById(R.id.iv_image);
            tvtime=itemView.findViewById(R.id.tvtime);
            tv_headertext=itemView.findViewById(R.id.tv_headertext);
            tv_tital=itemView.findViewById(R.id.tv_tital);
            tv_price=itemView.findViewById(R.id.tv_price);
            tv_date=itemView.findViewById(R.id.tv_date);


        }
    }
    class OfferViewholder extends RecyclerView.ViewHolder{
        TextView date,tv_headertext,tv_tital,tv_date,tvtime;
        ImageView iv_image;
        RelativeLayout rl_reject,rl_price,rl_accept;
        TextView tv_rejecttext,tv_price,tv_accept,tv_makeoffer,tv_pleaseconn;
        public OfferViewholder(View itemView) {
            super(itemView);
            date=itemView.findViewById(R.id.timestamp);
            iv_image=itemView.findViewById(R.id.iv_image);
            tvtime=itemView.findViewById(R.id.tvtime);
            tv_headertext=itemView.findViewById(R.id.tv_headertext);
            tv_tital=itemView.findViewById(R.id.tv_tital);
            tv_price=itemView.findViewById(R.id.tv_price);
            tv_date=itemView.findViewById(R.id.tv_date);

            rl_reject=itemView.findViewById(R.id.rl_reject);
            rl_price=itemView.findViewById(R.id.rl_price);
            rl_accept=itemView.findViewById(R.id.rl_accept);

            tv_rejecttext=itemView.findViewById(R.id.tv_rejecttext);
            tv_accept=itemView.findViewById(R.id.tv_accept);
            tv_makeoffer=itemView.findViewById(R.id.tv_makeoffer);
            tv_pleaseconn=itemView.findViewById(R.id.tv_pleaseconn);

        }
    }


    class OtherUserImageViewholder extends RecyclerView.ViewHolder{
        TextView tvtime,date;
        RoundedImageView ivmsg;
        public OtherUserImageViewholder(View itemView) {
            super(itemView);
            date=itemView.findViewById(R.id.timestamp);
            ivmsg=(RoundedImageView)itemView.findViewById(R.id.ivmsg);
            tvtime=itemView.findViewById(R.id.tvtime);


        }
    }

    class VendorViewholder extends RecyclerView.ViewHolder{
        TextView tvmsg,tvtime,date;

        public VendorViewholder(View itemView) {
            super(itemView);
            date=itemView.findViewById(R.id.timestamp);

            tvmsg=itemView.findViewById(R.id.tvmsg);
            tvtime=itemView.findViewById(R.id.tvtime);

        }
    }

    @Override
    public int getItemViewType(int position) {
       return chatlist.get(position).getView_type();
    }


    private String unixtimetotime(long unixSeconds){

// convert seconds to milliseconds
        Date date = new Date(unixSeconds);
// the format of your date
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z", Locale.US);
// give a timezone reference for formatting (see comment at the bottom)
        sdf.setTimeZone(TimeZone.getTimeZone("GMT-4"));
        String formattedDate = sdf.format(date);
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(sdf.parse(sdf.format(date)));
//            Log.e("date","data="+formattedDate+"houre="+calendar.get(Calendar.HOUR_OF_DAY)+"min="+calendar.get(Calendar.MINUTE));

        }catch (Exception e){

        }

//        getTimeAgo(timestamptodate(rate.get(position).getRequest_date()),context);
//        updateTime();
        return updateTime(calendar.get(Calendar.HOUR_OF_DAY),calendar.get(Calendar.MINUTE));
    }




    public static Date currentDate() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTime();
    }
    private static int getTimeDistanceInMinutes(long time) {
        long timeDistance = currentDate().getTime() - time;
        return Math.round((Math.abs(timeDistance) / 1000) / 60);
    }

    public static Date timestamptodate(String inputDate){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss",Locale.US);
        Date date = null;
        try {
            date = simpleDateFormat.parse(inputDate);
        } catch (ParseException e) {
            e.printStackTrace();
            //throw new IllegalAccessException("Error in parsing date");
        }
        return date;
    }

    private String updateTime(int hours, int mins) {
//        time=  hours + ":" + mins;
        logg("houre",hours+"");
        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12) {
            timeSet = "PM";
        }
        else {
            timeSet = "AM";
        }

        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);

        // Append in a StringBuilder
        String aTime = new StringBuilder().append(hours).append(':')
                .append(minutes).append(" ").append(timeSet).toString();

      return  aTime;
    }


    private void setdatetiem(int position,TextView date){
        if(position>0) {
            Log.e("date",pTs+",,"+Ts);
            pTs = chatlist.get(position-1).gettimestamp();
            Ts = chatlist.get(position).gettimestamp();
        }
        else
        {
            Log.e("date1",pTs+",,"+Ts);
            pTs = chatlist.get(position).gettimestamp();
            Ts = chatlist.get(position).gettimestamp();
        }
        String tym="";
        Date d = null,d1=null;
        Date dp=null,dn=null;
        //Date date=new Date();
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:SS",Locale.US);
        SimpleDateFormat sdf1=new SimpleDateFormat("MMM dd, yyyy",Locale.US);
        SimpleDateFormat sdf2=new SimpleDateFormat("hh:mm a",Locale.US);
        try {
//            d=sdf.parse(pTs);
//            d1 = sdf.parse(Ts);

            try {
                long pdate=Long.parseLong(pTs);
                long ndate=Long.parseLong(Ts);
                d=new Date(pdate);
                d1=new Date(ndate);
//                d = new Date(pTs);
//                d1 = new Date(Ts);
            }
            catch (Exception e)
            {
                Log.e("ex","ex",e);
            }
            pTs=sdf1.format(d);
            Ts=sdf1.format(d1);
            tym=sdf2.format(d1);
            dp=sdf1.parse(pTs);
            dn=sdf1.parse(Ts);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //  holder.duration.setText("Duration : "+hours+" hours " +minute+" mi
        if(position==0)
        {
            date.setVisibility(View.VISIBLE);
            date.setText(Ts);
//            time.setText(tym);
        }
        else
        {

            try {

                Log.e("d",dp+","+dn);
                if(dn.compareTo(dp)==0)
                {
                    Log.e("enter","enter");

                    date.setVisibility(View.GONE);
//                    message.setText(chat.get(position).getMessage());
//                    time.setText(tym);
                }
                else
                {
                    Log.e("enter","enter1");
                    date.setVisibility(View.VISIBLE);
                    date.setText(Ts);
//                    message.setText(chat.get(position).getMessage());
//                    time.setText(tym);
                }

            } catch (Exception e) {
                e.printStackTrace();
                Log.e("e","excptn",e);
            }

        }

    }

    @Override
    public long getItemId(int position) {
        return position;
    }


     Handler mHandler;
    private void startPlaying(final SeekBar seekBar, final TextView duration, final MediaPlayer mediaPlayer) {

        player =mediaPlayer;

            player.start();

            mHandler = new Handler();
//Make sure you update Seekbar on UI thread
            ((ChatActivity)  mcontex).runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if(player != null){
                        int mCurrentPosition = player.getCurrentPosition() ;
                        logg("current_loc",mCurrentPosition+"");
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                                duration.setText(getDisplayValue((long) mCurrentPosition)+"");
                            }
                            seekBar.setProgress(mCurrentPosition/1000,true);
                        }else {
                            duration.setText(mCurrentPosition+"");
                            seekBar.setProgress(mCurrentPosition);
                        }

                        if (player.getCurrentPosition()==player.getDuration()){
                            mHandler.removeCallbacks(this);
                            logg("cancel","cancel");
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                                duration.setText(getDisplayValue(Long.valueOf(player.getDuration()))+"");
                            }
                            stopPlaying();

                        }else {
                            mHandler.postDelayed(this,1000);
                        }
                    }


                }
            });

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

            }
        };
        mHandler.postDelayed(runnable,1000);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
               logg("intint",progress+" fromuser="+fromUser);
                if(player != null && fromUser){

                    player.seekTo(progress * 1000);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


    }

    private void stopPlaying() {
        if (player!=null) {
            player.release();
            player = null;
        }
    }

boolean playpause=true;
    private void onPlay(boolean start,ImageView imageView,SeekBar seekBar,TextView duration,MediaPlayer media) {
        if (start) {

            startPlaying(seekBar,duration,media);
            playpause=false;
            imageView.setImageResource(R.drawable.ic_stop_white_24dp);
        } else {
            stopPlaying();
            playpause=true;
            imageView.setImageResource(R.drawable.ic_play_arrow_black_24dp);
        }
    }
//    Long seconds,minutes;
//    String str_sec,str_min;
//    @RequiresApi(api = Build.VERSION_CODES.O)
//    public String getDisplayValue(Long ms) {
//        Duration duration = Duration.ofMillis(ms);
//         minutes = duration.toMinutes();
//         seconds = duration.minusMinutes(minutes).getSeconds();
//
//        if (seconds<10){
//            str_sec="0"+seconds;
//        }else {
//            str_sec=seconds+"";
//        }
//        if (minutes<10){
//            str_min= ("0"+minutes);
//        }else {
//            str_min=minutes+"";
//        }
//
//        return (str_min + ":" + str_sec);
//    }

    Long seconds,minutes;
    String str_sec,str_min;
    public String timeduration(long timeInMilliSeconds){

        long seconds = timeInMilliSeconds / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;
        seconds= seconds % 60;
        minutes=minutes % 60;

        if (seconds<10){
            str_sec="0"+seconds;
        }else {
            str_sec=seconds+"";
        }
        if (minutes<10){
            str_min= ("0"+minutes);
        }else {
            str_min=minutes+"";
        }
        return (str_min + ":" + str_sec);
    }


    int playing_position;
    boolean audioPlaying;
    boolean isPlaying;
    Timer seekTimer;
    private void setAudioData(final int position, final UserAudioViewholder holder) {

        holder.tv_duration.setText(chatlist.get(position).getDuration());
if (chatlist.get(position).getView_type()==5) {
    if (chatlist.get(position).isSeen()) {
        holder.tvtime.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_chat_double_tick_indicator, 0);

    } else {
        holder.tvtime.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_chat_delivery_status_single_check_darkgrey, 0);

    }
}
     holder.tvtime.setText(unixtimetotime(Long.parseLong(chatlist.get(position).gettimestamp())));
                    setdatetiem(position,(holder.date));



        holder.ivmsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {


                    holder.seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                        @Override
                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                          holder.  tv_duration.setText(getDisplayValue(Long.valueOf(player.getDuration()))+"");

                            if (!fromUser) {
                                holder.tv_duration.setText(timeduration(Long.valueOf(progress)));

                            }
                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {

                        }

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {

                        }
                    });

                    if (playing_position != position) {


                        //Loge("not_position","not_position");

                        mp.reset();


                        if(seekTimer!=null)
                        {
                            seekTimer.cancel();
                        }
                        //mp=new MediaPlayer();


                        isPlaying=false;
                        audioPlaying=false;

                        notifyItemChanged(playing_position);
                        playing_position = position;

                    }




                    if (!isPlaying) {
                        isPlaying = true;

                        if (!audioPlaying) {
                            try {


                                    //Loge("play local audio", chatMessages.get(position).getFilename());
                                    mp.setDataSource(imageurl+chatlist.get(position).getMessage());

                                audioPlaying = true;

                                try {
                                    mp.prepareAsync();
                                } catch (Exception e) {
                                    //Loge("e", "e", e);
                                    e.printStackTrace();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {

                            //Loge("else","else");


                            seekTimer = new Timer();

                            seekTimer.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    int mCurrentPosition = mp.getCurrentPosition();

                                    if (playing_position == position)

                                        holder.seek.setProgress(mCurrentPosition);

                                }
                            }, 500, 500);

                            mp.start();


                        }


                        holder.ivmsg.setImageResource(R.drawable.audio_ic_round_pause_button);
                        holder.ivmsg.refreshDrawableState();

                        //Loge("prepared", "prepared");





                        mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(final MediaPlayer mp) {

                                holder.seek.setMax(mp.getDuration());
                                mp.start();

                                //Loge("started", "started");

                                seekTimer = new Timer();

                                seekTimer.schedule(new TimerTask() {
                                    @Override
                                    public void run() {
                                        int mCurrentPosition = mp.getCurrentPosition();

                                        if (playing_position == position)

                                            holder.seek.setProgress(mCurrentPosition);

                                    }
                                }, 500, 500);

                                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                                    @Override
                                    public void onCompletion(MediaPlayer mp) {


                                        seekTimer.cancel();
//                                        holder.audioSeek.setMax(1);
                                        holder.seek.setProgress(0);
//                                               playing = false;
                                        isPlaying = false;

                                        holder.ivmsg.setImageResource(R.drawable.audio_ic_play_rounded_button);
                                        holder.ivmsg.refreshDrawableState();
                                        holder.tv_duration.setText(chatlist.get(position).getDuration());

                                    }
                                });


                            }
                        });


                    } else {
                        mp.pause();
                        holder.ivmsg.setImageResource(R.drawable.audio_ic_play_rounded_button);
                        holder.ivmsg.refreshDrawableState();
                        isPlaying = false;
                    }


                    //Log.e("Seek", holder.audioSeek + " " + holder);


                }
                catch(Exception e)
                {
                    //Logd("Ex","e",e);
                }


            }



        });

    }


    private void mediaDialog(String imagePath) {
        final Dialog dialog = new Dialog(mcontex, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.setContentView(R.layout.showimagedialog);
        ImageView image = (ImageView) dialog.findViewById(R.id.media_image);

        Glide.with(mcontex).load(imagePath)
                .transition(withCrossFade())
                .into(image);

        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


    }


    public void set_quickreplydata(QuickMsgViewholder holder,int position){
        Glide.with(mcontex).load(Common.Image_Loading_Url(ad_image))
                .apply(new RequestOptions().centerCrop().error(R.drawable.logo_bazar_no_image))
                .transition(withCrossFade())
                .into(((QuickMsgViewholder)holder).iv_image);
        ((QuickMsgViewholder) holder).tv_price.setText(ad_price);
        ((QuickMsgViewholder) holder).tv_tital.setText(ad_tital);

        ((QuickMsgViewholder) holder).tvtime.setText(unixtimetotime(Long.parseLong(chatlist.get(position).gettimestamp())));
        setdatetiem(position,((QuickMsgViewholder)holder).date);
        ((QuickMsgViewholder)holder).tv_date.setText(((QuickMsgViewholder)holder).date.getText().toString());



        ((QuickMsgViewholder) holder).tv_headertext.setText(chatlist.get(position).getMessage());

        if (chatlist.get(position).getView_type()==9) {
            if (chatlist.get(position).isSeen()) {
                ((QuickMsgViewholder) holder).tvtime.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_chat_double_tick_indicator, 0);

            } else {
                ((QuickMsgViewholder) holder).tvtime.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_chat_delivery_status_single_check_darkgrey, 0);

            }
        }else {
            ((QuickMsgViewholder) holder).tvtime.setCompoundDrawablesWithIntrinsicBounds(0, 0,0, 0);

        }

    }

}
