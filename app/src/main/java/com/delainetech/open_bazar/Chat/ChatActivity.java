package com.delainetech.open_bazar.Chat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import de.hdodenhof.circleimageview.CircleImageView;

import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.SimpleMultiPartRequest;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.delainetech.open_bazar.Custom_classes.EndlessRecyclerViewScrollListener;
import com.delainetech.open_bazar.Custom_classes.GetTimeAgo;
import com.delainetech.open_bazar.Ads_Detail_Activity;
import com.delainetech.open_bazar.Models.Messages;
import com.delainetech.open_bazar.Other_User_Profile;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.PushService;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.zfdang.multiple_images_selector.ImagesSelectorActivity;
import com.zfdang.multiple_images_selector.SelectorSettings;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static com.delainetech.open_bazar.Utils.Common.logg;


public class ChatActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener, ParamKeys,
        GoogleApiClient.ConnectionCallbacks, LocationListener,
        GoogleApiClient.OnConnectionFailedListener{

    private String mChatUser;
    private Toolbar mChatToolbar;

    private DatabaseReference mRootRef;

    private TextView mTitleView;
    private TextView mLastSeenView;
    private ImageView ivback;
    CircleImageView mProfileImage;
    private FirebaseAuth mAuth;
    private String mCurrentUserId;

    private EditText mChatMessageView;
    RelativeLayout sendmsg;
    private RecyclerView mMessagesList;
    private SwipeRefreshLayout mRefreshLayout;

    private final ArrayList<Messages> messagesList = new ArrayList<>();
    private LinearLayoutManager mLinearLayout;
    private ChatBox_adapter mAdapter;

    private static final int TOTAL_ITEMS_TO_LOAD = 10;
    private int     mCurrentPage = 1;
    int viewtype=1;

    //New Solution
    private int itemPos = 0;

    private String mLastKey = "";
    private String mPrevKey = "";
    EndlessRecyclerViewScrollListener endlessRecyclerOnScrollListener;
    UserSharedPreferences preferences;

    ImageView iv_gallery,iv_camera,iv_mic,iv_location;

    String[] permissionsRequired = new String[]{
            android.Manifest.permission.CAMERA,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

//    voice recording
String[] recording = new String[]{
        Manifest.permission.RECORD_AUDIO,
        android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE
};

ImageView iv_online;

    String userimg="";


//    Post details
    String ad_tital="",ad_image="",ad_price="",ad_id="";
    TextView tv_tital,tv_price;
    ImageView iv_pro;

    String current_userchat="",other_user_chat;

//    Quick reply
    TextView tv_quickreply1,tv_quickreply2,tv_quickreply3,tv_quickreply4,tv_quickreply5;
    ImageView iv_delete_chat,iv_block,iv_call;

    private DatabaseReference mBlockDatabase;
    LinearLayout chat_ll;

    RelativeLayout header_lay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        preferences=new UserSharedPreferences(this);
        chat_ll = (LinearLayout) findViewById(R.id.chat_ll);
        header_lay = (RelativeLayout) findViewById(R.id.header_lay);
        header_lay.setOnClickListener(this);
        sendmsg = (RelativeLayout) findViewById(R.id.sendmsg);
        iv_online = (ImageView) findViewById(R.id.iv_online);

        iv_delete_chat = (ImageView) findViewById(R.id.iv_delete_chat);
        iv_block = (ImageView) findViewById(R.id.iv_block);
        iv_call = (ImageView) findViewById(R.id.iv_call);
        iv_delete_chat.setOnClickListener(this);
        iv_block.setOnClickListener(this);
        iv_call.setOnClickListener(this);

        mChatToolbar = (Toolbar) findViewById(R.id.chat_app_bar);
        setSupportActionBar(mChatToolbar);
        getWindow().setBackgroundDrawableResource(R.drawable.chat_bg);
        ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        mRootRef = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
//        mCurrentUserId = mAuth.getCurrentUser().getUid();
        mCurrentUserId="user"+preferences.getuserid();
//        register_user(mCurrentUserId);

        if (getIntent().getStringExtra("user_id").contains("user")){
            mChatUser=getIntent().getStringExtra("user_id");
        }else {
            mChatUser = "user" + getIntent().getStringExtra("user_id");
        }
        mProfileImage = (CircleImageView) findViewById(R.id.custom_bar_image);
        mProfileImage.setOnClickListener(this);

        String userName = getIntent().getStringExtra("user_name");


//        ads detail
        tv_tital=(TextView)findViewById(R.id.tv_tital);
        tv_price=(TextView)findViewById(R.id.tv_price);
        iv_pro=(ImageView)findViewById(R.id.iv_pro);
        if (getIntent().hasExtra("ad_tital")){
            ad_tital=getIntent().getStringExtra("ad_tital");
            ad_price=getIntent().getStringExtra("ad_price");
            ad_image=getIntent().getStringExtra("ad_image");
            ad_id=getIntent().getStringExtra("ad_id");

            logg("image",Common.Image_Loading_Url(ad_image));
            tv_tital.setText(ad_tital);
            tv_price.setText(ad_price);
            Glide.with(this).load(Common.Image_Loading_Url(ad_image))
            .apply(new RequestOptions().error(R.drawable.logo_bazar_no_image).centerCrop()).into(iv_pro);
        }

        current_userchat="ads"+ad_id+"_user"+preferences.getuserid();;
        other_user_chat="ads"+ad_id+"_user"+mChatUser.replace("user","");


        // ---- Custom Action bar Items ----

        mTitleView = (TextView) findViewById(R.id.custom_bar_title);
        mLastSeenView = (TextView) findViewById(R.id.custom_bar_seen);

        iv_gallery = (ImageView) findViewById(R.id.iv_gallery);
        iv_gallery.setOnClickListener(this);
        iv_camera = (ImageView) findViewById(R.id.iv_camera);
        iv_camera.setOnClickListener(this);
        iv_mic = (ImageView) findViewById(R.id.iv_mic);
        iv_mic.setOnClickListener(this);
        iv_location = (ImageView) findViewById(R.id.iv_location);
        iv_location.setOnClickListener(this);

        ivback = (ImageView) findViewById(R.id.ivback);
        ivback.setOnClickListener(this);
//        mChatAddBtn = (ImageButton) findViewById(R.id.chat_add_btn);
//        mChatSendBtn = (ImageButton) findViewById(R.id.chat_send_btn);
        mChatMessageView = (EditText) findViewById(R.id.chat_message_view);

        mAdapter = new ChatBox_adapter(messagesList,ChatActivity.this,ad_tital,ad_image,ad_price);

        mMessagesList = (RecyclerView) findViewById(R.id.messages_list);
        mRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.message_swipe_layout);
        mRefreshLayout.setOnRefreshListener(this);
        mRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorPrimary),getResources().getColor(R.color.colorPrimary));

        mLinearLayout = new LinearLayoutManager(this);
        mLinearLayout.setStackFromEnd(true);
//        mMessagesList.setHasFixedSize(true);
        mMessagesList.setLayoutManager(mLinearLayout);

        mMessagesList.setAdapter(mAdapter);


        mRootRef.child("Chat").child(mCurrentUserId).child(other_user_chat).child("seen").setValue(true);

//        quick msg reply
        tv_quickreply1=(TextView)findViewById(R.id.tv_quickreply1);
        tv_quickreply1.setOnClickListener(this);
        tv_quickreply2=(TextView)findViewById(R.id.tv_quickreply2);
        tv_quickreply2.setOnClickListener(this);
        tv_quickreply3=(TextView)findViewById(R.id.tv_quickreply3);
        tv_quickreply3.setOnClickListener(this);
        tv_quickreply4=(TextView)findViewById(R.id.tv_quickreply4);
        tv_quickreply4.setOnClickListener(this);
        tv_quickreply5=(TextView)findViewById(R.id.tv_quickreply5);
        tv_quickreply5.setOnClickListener(this);

        loadMessages();


        mTitleView.setText(userName);

        mRootRef.child("Users").child(mChatUser).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, Object> map=(Map<String, Object>)dataSnapshot.getValue();
                if (map!=null){
                if (map.containsKey("online")) {
                    String online = dataSnapshot.child("online").getValue().toString();
                    if (userimg.length()==0) {
                        userimg = dataSnapshot.child("image").getValue().toString();

              RequestOptions options = new RequestOptions().centerCrop().error(R.drawable.userimage);
                        if (!ChatActivity.this.isFinishing()) {
                            Glide.with(ChatActivity.this).load(Common.Image_Loading_Url(userimg)).apply(options).into(mProfileImage);

                        }
                    }
                    if (online.equals("true")) {

                        mLastSeenView.setText("Online");
                        iv_online.setVisibility(View.VISIBLE);
                    } else {

                        GetTimeAgo getTimeAgo = new GetTimeAgo();

                        long lastTime = Long.parseLong(online);

                        String lastSeenTime = getTimeAgo.getTimeAgo(lastTime, getApplicationContext());

                        mLastSeenView.setText(lastSeenTime);
                        iv_online.setVisibility(View.GONE);
                    }
                }
            }else {
                    mLastSeenView.setText("");
                    iv_online.setVisibility(View.GONE);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        mRootRef.child("Chat").child(mCurrentUserId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                logg("datachatvalue",dataSnapshot.getValue().toString());
                if(!dataSnapshot.hasChild(other_user_chat)){

                    Map chatAddMap = new HashMap();
                    chatAddMap.put("seen", false);
                    chatAddMap.put("timestamp", ServerValue.TIMESTAMP);

                    Map chatUserMap = new HashMap();
                    chatUserMap.put("Chat/" + mCurrentUserId + "/" + other_user_chat, chatAddMap);
                    chatUserMap.put("Chat/" + mChatUser + "/" + current_userchat, chatAddMap);

                    mRootRef.updateChildren(chatUserMap, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                            if(databaseError != null){

                                Log.d("CHAT_LOG", databaseError.getMessage().toString());

                            }

                        }
                    });

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        sendmsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String message = mChatMessageView.getText().toString();
                sendMessage(message,"text");

            }
        });

        setup_blockstatus();


    }



    private void loadMoreMessages() {

        DatabaseReference messageRef = mRootRef.child("messages").child(current_userchat).child(other_user_chat);

        Query messageQuery = messageRef.orderByKey().endAt(mLastKey).limitToLast(10);

        messageQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {


//                Messages message = dataSnapshot.getValue(Messages.class);
                String messageKey = dataSnapshot.getKey();
                String status="",duration="";
                if(!mPrevKey.equals(messageKey)){

                    Map<String, Object> map1=(Map<String, Object>)dataSnapshot.getValue();
                    if (map1.containsKey("type")) {
                        if (map1.get("user").toString().equals("user" + preferences.getuserid())) {
                            if (map1.get("type").toString().equals("text")) {
                                viewtype = 1;
                            } else if (map1.get("type").toString().equals("image")) {
                                viewtype = 3;
                            } else if (map1.get("type").toString().equals("audio")) {
                                viewtype = 5;
                                duration = map1.get("duration").toString();
                            } else if (map1.get("type").toString().equals("location")) {
                                viewtype = 7;
                            } else if (map1.get("type").toString().equals("quickreply")) {
                                viewtype = 9;
                            } else if (map1.get("type").toString().equals("offer")) {
                                viewtype = 11;
                                status = map1.get("status").toString();
                            }

                            messagesList.add(itemPos++, new Messages(map1.get("message").toString(), "text", map1.get("timestamp").toString(), Boolean.parseBoolean(map1.get("seen").toString()),
                                    map1.get("user").toString(), viewtype, status, duration));
                        } else {

                            update_messageseen(current_userchat, other_user_chat, map1.get("timestamp").toString());
                            if (map1.containsKey("type")) {
                                if (map1.get("type").toString().equals("text")) {
                                    viewtype = 2;
                                } else if (map1.get("type").toString().equals("image")) {
                                    viewtype = 4;
                                } else if (map1.get("type").toString().equals("audio")) {
                                    viewtype = 6;
                                    duration = map1.get("duration").toString();
                                } else if (map1.get("type").toString().equals("location")) {
                                    viewtype = 8;
                                } else if (map1.get("type").toString().equals("quickreply")) {
                                    viewtype = 10;
                                } else if (map1.get("type").toString().equals("offer")) {
                                    viewtype = 12;
                                    status = map1.get("status").toString();
                                }
                                messagesList.add(itemPos++, new Messages(map1.get("message").toString(), "text", map1.get("timestamp").toString(), Boolean.parseBoolean(map1.get("seen").toString()),
                                        map1.get("user").toString(), viewtype, status, duration));
                            }
                        }
                    }

                } else {

                    mPrevKey = mLastKey;

                }

                if(itemPos == 1) {

                    mLastKey = messageKey;

                }

                mAdapter.notifyDataSetChanged();

                mRefreshLayout.setRefreshing(false);

//                mLinearLayout.scrollToPositionWithOffset(10, 0);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void loadMessages() {

        DatabaseReference messageRef = mRootRef.child("messages").child(current_userchat).child(other_user_chat);

        Query messageQuery = messageRef.limitToLast(mCurrentPage * TOTAL_ITEMS_TO_LOAD);


        messageQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                String message1 = dataSnapshot.getValue().toString();

                Log.e("addlast_msg1",message1+"");
              Log.e("addlast_msg1key",dataSnapshot.getKey().toString()+"");

//                Messages message = dataSnapshot.getValue(Messages.class);
                String status="",duration="";
                itemPos++;
                logg("aaaaitemPos=",itemPos+"  ,key="+dataSnapshot.getKey());
                if(itemPos == 1){

                    String messageKey = dataSnapshot.getKey();

                    mLastKey = messageKey;
                    mPrevKey = messageKey;
                logg("aaaamLastKey=",mLastKey+"  ,mPrevKey="+mPrevKey);
                }
//                Map<String, Object> map=(Map<String, Object>)dataSnapshot.getValue();

                    Map<String, Object> map1=(Map<String, Object>)dataSnapshot.getValue();
                logg("map11",map1.values().toString()+"");
                if (map1.containsKey("type")) {

                    if (map1.get("user").toString().equals("user" + preferences.getuserid())) {
                        if (map1.get("type").toString().equals("text")) {
                            viewtype = 1;
                        } else if (map1.get("type").toString().equals("image")) {
                            viewtype = 3;
                        } else if (map1.get("type").toString().equals("audio")) {
                            viewtype = 5;
                            duration = map1.get("duration").toString();
                        } else if (map1.get("type").toString().equals("location")) {
                            viewtype = 7;
                        } else if (map1.get("type").toString().equals("quickreply")) {
                            viewtype = 9;
                        } else if (map1.get("type").toString().equals("offer")) {
                            viewtype = 11;
                            status = map1.get("status").toString();
                        }
                        logg("seen", map1.get("seen").toString());
                        logg("seen1", Boolean.parseBoolean(map1.get("seen").toString()) + "");

                        messagesList.add(new Messages(map1.get("message").toString(), "text", map1.get("timestamp").toString(), Boolean.parseBoolean(map1.get("seen").toString()),
                                map1.get("user").toString(), viewtype, status, duration));
                        mMessagesList.scrollToPosition(messagesList.size() - 1);
                    } else {

                        update_messageseen(current_userchat, other_user_chat, map1.get("timestamp").toString());


                        if (map1.get("type").toString().equals("text")) {
                            viewtype = 2;
                        } else if (map1.get("type").toString().equals("image")) {
                            viewtype = 4;
                        } else if (map1.get("type").toString().equals("audio")) {
                            viewtype = 6;
                            duration = map1.get("duration").toString();
                        } else if (map1.get("type").toString().equals("location")) {
                            viewtype = 8;
                        } else if (map1.get("type").toString().equals("quickreply")) {
                            viewtype = 10;
                        } else if (map1.get("type").toString().equals("offer")) {
                            viewtype = 12;
                            status = map1.get("status").toString();
                        }

                        messagesList.add(new Messages(map1.get("message").toString(), "text", map1.get("timestamp").toString(), Boolean.parseBoolean(map1.get("seen").toString()),
                                map1.get("user").toString(), viewtype, status, duration));
                        mMessagesList.scrollToPosition(messagesList.size() - 1);
                    }
                }
                if (mRootRef!=null) {
                    online_status("true");
                }
//                mAdapter.notifyDataSetChanged();

//                mMessagesList.scrollToPosition(messagesList.size() - 1);

                mRefreshLayout.setRefreshing(false);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void sendMessage(String msg,String type) {




        if(!TextUtils.isEmpty(msg)){

            String current_user_ref = "messages/" + current_userchat + "/" + other_user_chat;
            String chat_user_ref = "messages/" + other_user_chat + "/" + current_userchat;

            DatabaseReference user_message_push = mRootRef.child("messages")
                    .child(current_user_ref).child(chat_user_ref).push();

            String push_id = user_message_push.getKey();

            Map messageMap = new HashMap();
            messageMap.put("message", msg);
            if (iv_online.getVisibility()==View.VISIBLE){
                messageMap.put("seen", true);
            }else {
                messageMap.put("seen", false);
            }
            String timestamp=System.currentTimeMillis()+"";
            messageMap.put("type", type);
            messageMap.put("timestamp", timestamp);
            messageMap.put("user", mCurrentUserId);
            messageMap.put("user_name", preferences.getname());

            Map messageUserMap = new HashMap();
            messageUserMap.put(current_user_ref + "/" + timestamp, messageMap);
            messageUserMap.put(chat_user_ref + "/" + timestamp, messageMap);

            mChatMessageView.setText("");

            mRootRef.child("Chat").child(mCurrentUserId).child(other_user_chat).child("seen").setValue(true);
            mRootRef.child("Chat").child(mCurrentUserId).child(other_user_chat).child("timestamp").setValue(ServerValue.TIMESTAMP);

            mRootRef.child("Chat").child(mChatUser).child(current_userchat).child("seen").setValue(false);
            mRootRef.child("Chat").child(mChatUser).child(current_userchat).child("timestamp").setValue(ServerValue.TIMESTAMP);

            mRootRef.updateChildren(messageUserMap, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                    if(databaseError != null){

                        Log.d("CHAT_LOG", databaseError.getMessage().toString());

                    }

                }
            });

        }

    }
    DatabaseReference mDatabase;



    @Override
    public void onRefresh() {
        mCurrentPage++;

        itemPos = 0;

        loadMoreMessages();
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.header_lay:
                Intent i=new Intent(ChatActivity.this, Ads_Detail_Activity.class);
                i.putExtra("id",ad_id);
                i.putExtra(Type,"chat");

                startActivity(i);
                break;

            case R.id.custom_bar_image:
                Intent ii=new Intent(ChatActivity.this, Other_User_Profile.class);
                ii.putExtra("id",mChatUser.replace("user",""));
                startActivity(ii);
                break;

            case R.id.iv_delete_chat:
                delete_chat(ChatActivity.this);
                break;
            case R.id.iv_block:
                block_user(ChatActivity.this);
                break;
            case R.id.iv_call:
                finish();
                break;


            case R.id.ivback:
                finish();
                break;
            case R.id.iv_gallery:
                   openGallery();

                break;
            case R.id.iv_camera:
                if (Common.hasPermissions(ChatActivity.this,permissionsRequired)) {
                    open_camera();
                }else {
                    Common.askpermission(ChatActivity.this,permissionsRequired);
                }
                break;

            case R.id.iv_mic:
                if (Common.hasPermissions(ChatActivity.this,recording)) {
                    Record_audio_dialog_fragment fragment=new Record_audio_dialog_fragment();
                    FragmentManager frm = getSupportFragmentManager();
                    fragment.show(frm, "");
                }else {
                    Common.askpermission(ChatActivity.this,recording);
                }

                break;
            case R.id.iv_location:
                location_picker();
                break;

//                quick replu
            case R.id.tv_quickreply1:
               sendMessage(getResources().getString(R.string.hello_i_d_like_to_buy_it),"quickreply");
                break;
            case R.id.tv_quickreply2:
               sendMessage(getResources().getString(R.string.i_m_interested),"quickreply");
                break;
            case R.id.tv_quickreply3:
               sendMessage(getResources().getString(R.string.is_it_still_available),"quickreply");
                break;
            case R.id.tv_quickreply4:
               sendMessage(getResources().getString(R.string.is_it_possible_to_deliver_it),"quickreply");
                break;
            case R.id.tv_quickreply5:
               sendMessage(getResources().getString(R.string.can_we_negotiate_the_price),"quickreply");
                break;

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mRootRef!=null) {
            online_status("true");
        }
        PushService. chatactivitystr="in";
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mRootRef!=null) {
            online_status(ServerValue.TIMESTAMP);

        }
    }


    private Uri mOutputFileUri = null;
    private static final int REQUEST_CODE_IMAGE_CAPTURE = 12121;
    private static final int Replace_REQUEST_CODE_IMAGE_CAPTURE = 1211;

    private void open_camera(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStorageDirectory(),
                UUID.randomUUID().toString() + "img.jpg");
        mOutputFileUri = FileProvider.getUriForFile(ChatActivity.this, "com.open_bazar.fileprovider", file);
        Log.e("uri", ("file:" + file.getAbsolutePath()) + "");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mOutputFileUri);
        mOutputFileUri = Uri.parse("file:" + file.getAbsolutePath());
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(intent, REQUEST_CODE_IMAGE_CAPTURE);
    }

    private ArrayList<String> mResults = new ArrayList<>();
    int REQUEST_CODE_GALLERY = 1;
    private void openGallery() {
//        take_album_or_gallry(mcontext);

        Intent intent = new Intent(ChatActivity.this, ImagesSelectorActivity.class);
        // max number of images to be selected
        intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 1);
        // min size of image which will be shown; to filter tiny images (mainly icons)
        intent.putExtra(SelectorSettings.SELECTOR_MIN_IMAGE_SIZE, 100000);
        // show camera or not
        intent.putExtra(SelectorSettings.SELECTOR_SHOW_CAMERA, false);
        // pass current selected images as the initial value
        intent.putStringArrayListExtra(SelectorSettings.SELECTOR_INITIAL_SELECTED_LIST, mResults);
        // start the selector
        startActivityForResult(intent, REQUEST_CODE_GALLERY);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == REQUEST_CODE_GALLERY) {
            if (data != null) {

                if (resultCode == RESULT_OK) {
                    mResults = data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS);
                    assert mResults != null;

                    // show results in textview
                    StringBuilder sb = new StringBuilder();
                    sb.append(String.format("Totally %d images selected:", mResults.size())).append("\n");
                    for (String result : mResults) {
                        upload_media(result,"image","");
                        sb.append(result).append("\n");
                    }
                    mResults.clear();

                }


            }

        }

        if (requestCode == REQUEST_CODE_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            if (mOutputFileUri != null) {
                String uri = mOutputFileUri.toString();
                Log.e("uri", uri);

                upload_media(uri,"image","");

            }
        }

    }



    public void upload_media(String uri, final String type,final String duration){

        SimpleMultiPartRequest request = new SimpleMultiPartRequest(Request.Method.POST, baseurl+"uploadimage", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                logg("response",response+"");
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("success")){

                        if (type.equals("audio")){
                            if (you_block_user_status){
                                Toast.makeText(ChatActivity.this, "User is blocked", Toast.LENGTH_SHORT).show();
                            }else {
                                sendaudioMessage(object.getString("image"), type, duration);
                            }
                        }else {
                            logg("jsonarrayimg", object.getString("image") + "");
                            sendMessage(object.getString("image"), type);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Common.volleyerror(error);

            }
        });
        request.addFile("image", Uri.parse(uri.replace("file:",""))+"");

        logg("params",request.getFilesToUpload()+"");
        request.setOnProgressListener(new Response.ProgressListener() {
            @Override
            public void onProgress(long transferredBytes, long totalSize) {
                int percentage = (int) ((transferredBytes / ((float) totalSize)) * 100);
                logg("percentage",percentage+"");
//                progress.setProgress(percentage);
            }
        });
        RequestQueue mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        mRequestQueue.add(request);



    }



//   Current location


    private GoogleApiClient googleApiClient;
    boolean ask_gps=true;
    ProgressDialog progressDialog;

    private void location_picker() {
        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Getting And Sharing Your Current Location Please Wait .......");
        buildGoogleApiClient();
        createConnectionsLocation();
        check_Runtime_Permissions();
    }

    //get current location enable api client
    private void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(ChatActivity.this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    LocationRequest mLocationRequest;

    private void createConnectionsLocation() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(10000 / 2);
    }

    private void check_Runtime_Permissions() {
        if (Common.hasPermissions(ChatActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                android.Manifest.permission.ACCESS_COARSE_LOCATION})) {
            checkGPSstatus();
        } else {
            Common.askpermission(ChatActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION});
        }
    }

    private void checkGPSstatus() {
        logg("gpsstatus", checkGPSStatus(this) + "");
        if (!(checkGPSStatus(this))) {

            if (ask_gps) {
                ask_gps=false;
                buildAlertMessageNoGps();
            }
        } else if (googleApiClient.isConnected()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    Activity#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for Activity#requestPermissions for more details.
                    return;
                }
            }
            progressDialog.show();
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, mLocationRequest, this);

        }
    }

    public static boolean checkGPSStatus(Context mContext) {
        LocationManager locationManager = null;
        boolean gps_enabled = true;

        if (locationManager == null) {
            locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        }

        if ((locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)))
            gps_enabled = true;
        else
            gps_enabled = false;
        logg("gps_enabled", ":2:" + gps_enabled);
        return gps_enabled;
    }


    Location mLocation;

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    Activity#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                return;
            }
        }
        mLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        logg("connected","connected");
        checkGPSstatus();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }



    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        ask_gps=true;
                        startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS),10);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        ask_gps=true;
                        Toast.makeText(ChatActivity.this, "GPS need to be enable", Toast.LENGTH_SHORT).show();

                        dialog.cancel();
                    }
                })
                .setNeutralButton("Exit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
        ;
        final AlertDialog alert = builder.create();
        alert.show();
    }


    @Override
    public void onLocationChanged(Location location) {
        progressDialog.dismiss();
        logg("location",location.getLatitude()+","+location.getLongitude());

//        Intent i=new Intent();
//        i.putExtra("latlong",location.getLatitude()+","+location.getLongitude());
//        setResult(Activity.RESULT_OK,i);
//
//        finish();
        sendMessage(location.getLatitude()+","+location.getLongitude(),"location");
        stopLocationUpdates();
        progressDialog.dismiss();
    }



    //Stop Location Update Method
    protected void stopLocationUpdates() {
        try {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    googleApiClient, (LocationListener) this);
        }
        catch  (Exception ex)
        {}
    }



    private void update_messageseen(String current_userid,String chatuser,String timestamp){
        mRootRef.child("messages").child(current_userid).child(chatuser).child(timestamp).child("seen").setValue(true);
        mRootRef.child("messages").child(chatuser).child(current_userid).child(timestamp).child("seen").setValue(true);

    }


    private void online_status(Object status){
        mRootRef.child("Users").child("user"+preferences.getuserid()).child("online").setValue(status);

    }


    public void update_status(String timestamp,String status){
        mRootRef.child("messages").child(current_userchat).child(other_user_chat).child(timestamp).child("status").setValue(status);
        mRootRef.child("messages").child(other_user_chat).child(current_userchat).child(timestamp).child("status").setValue(status);

    }


    private void sendaudioMessage(String msg,String type,String duration) {

        if(!TextUtils.isEmpty(msg)){

            String current_user_ref = "messages/" + current_userchat + "/" + other_user_chat;
            String chat_user_ref = "messages/" + other_user_chat + "/" + current_userchat;

            DatabaseReference user_message_push = mRootRef.child("messages")
                    .child(current_user_ref).child(chat_user_ref).push();

            String push_id = user_message_push.getKey();

            Map messageMap = new HashMap();
            messageMap.put("message", msg);
            if (iv_online.getVisibility()==View.VISIBLE){
                messageMap.put("seen", true);
            }else {
                messageMap.put("seen", false);
            }
            String timestamp=System.currentTimeMillis()+"";
            messageMap.put("type", type);
            messageMap.put("timestamp", timestamp);
            messageMap.put("user", mCurrentUserId);
            messageMap.put("duration", duration);
            messageMap.put("user_name", preferences.getname());

            Map messageUserMap = new HashMap();
            messageUserMap.put(current_user_ref + "/" + timestamp, messageMap);
            messageUserMap.put(chat_user_ref + "/" + timestamp, messageMap);

            mChatMessageView.setText("");

            mRootRef.child("Chat").child(mCurrentUserId).child(other_user_chat).child("seen").setValue(true);
            mRootRef.child("Chat").child(mCurrentUserId).child(other_user_chat).child("timestamp").setValue(ServerValue.TIMESTAMP);

            mRootRef.child("Chat").child(mChatUser).child(current_userchat).child("seen").setValue(false);
            mRootRef.child("Chat").child(mChatUser).child(current_userchat).child("timestamp").setValue(ServerValue.TIMESTAMP);

            mRootRef.updateChildren(messageUserMap, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                    if(databaseError != null){

                        Log.d("CHAT_LOG", databaseError.getMessage().toString());

                    }

                }
            });

        }

    }

    boolean chat_delete_status=false;
    public void delete_chat(final Context mcoxt){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mcoxt);

        alertDialog.setMessage("Are your sure you want to delete conversation?");
        alertDialog.setPositiveButton(mcoxt.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                mRootRef.child("messages").child(current_userchat).child(other_user_chat).removeValue();
                mRootRef.child("Chat").child("user"+preferences.getuserid()).child(other_user_chat).removeValue();

                Intent i=new Intent();
                i.putExtra("otherid",other_user_chat);
                setResult(RESULT_OK,i);
                finish();
            }
        });
        alertDialog.setNegativeButton(mcoxt.getResources().getString(R.string.No), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog1= alertDialog.create();
        alertDialog1.show();

        alertDialog1.getButton(alertDialog1.BUTTON_NEGATIVE).setTextColor(mcoxt.getResources().getColor(R.color.colorAccent));
        alertDialog1.getButton(alertDialog1.BUTTON_POSITIVE).setTextColor(mcoxt.getResources().getColor(R.color.colorAccent));
    }



    public void block_user(final Context mcoxt){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mcoxt);

        alertDialog.setMessage("Are your sure you want to block?");
        alertDialog.setPositiveButton(mcoxt.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                block_user(getIntent().getStringExtra("user_id").replace("user",""),"block");
            }
        });
        alertDialog.setNegativeButton(mcoxt.getResources().getString(R.string.No), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog1= alertDialog.create();
        alertDialog1.show();

        alertDialog1.getButton(alertDialog1.BUTTON_NEGATIVE).setTextColor(mcoxt.getResources().getColor(R.color.colorAccent));
        alertDialog1.getButton(alertDialog1.BUTTON_POSITIVE).setTextColor(mcoxt.getResources().getColor(R.color.colorAccent));
    }


    public void  block_user(final String userid,final String blockstatus){
        final ProgressDialog progressDialog=new ProgressDialog(ChatActivity.this);
        progressDialog.setMessage("Please Wait.....");
        progressDialog.show();
        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "block_user", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                pro.cancel();
                progressDialog.dismiss();
                logg("follow",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("true")){
                        mRootRef.child("Block").child(mCurrentUserId).child(mChatUser).child("blocked"+mChatUser).setValue(true);
                        mRootRef.child("Block").child(mChatUser).child(mCurrentUserId).child("blocked"+mChatUser).setValue(true);

                        finish();
                        Toast.makeText(ChatActivity.this, "User is blocked now", Toast.LENGTH_SHORT).show();
                    }

                }
                catch (Exception e){
//                    pro.cancel();
                    progressDialog.dismiss();

                    Log.e("error","e",e);
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                progressDialog.dismiss();

                Log.e("Volleyerror","e",volleyError);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(User_id,preferences.getuserid());
                params.put("block_id",userid);
                params.put("type",blockstatus);

                Log.e("params",params+"");
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue mRequestQueue = Volley.newRequestQueue(ChatActivity.this);
        mRequestQueue.add(stringRequest);

    }

    boolean you_block_user_status=false;
    boolean user_block_u_status=false;
    private void setup_blockstatus(){
        mBlockDatabase = FirebaseDatabase.getInstance().getReference().child("Block").child("user"+preferences.getuserid());

        mBlockDatabase.child(mChatUser).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot!=null) {
//                            if (dataSnapshot.getKey())
                    if (dataSnapshot.hasChild("blocked"+ mChatUser)) {

                    logg("blocked","otheruser");
                        if (dataSnapshot.child("blocked"+ mChatUser).getValue().toString().equals("true")){
                            chat_ll.setVisibility(View.GONE);

                        }else {
                            chat_ll.setVisibility(View.VISIBLE);
//                            you_block_user_status=false;
                        }
                    }

                    if (dataSnapshot.hasChild("blocked"+ mCurrentUserId)){
                        logg("blocked","i am blocked");
                        if (dataSnapshot.child("blocked"+ mCurrentUserId).getValue().toString().equals("true")){
                            chat_ll.setVisibility(View.GONE);
                            block_user_status(ChatActivity.this,"You are blocked by this user");
                        }else {
                            chat_ll.setVisibility(View.VISIBLE);
//                            iv_block.setImageResource(R.drawable.chat_ic_unbloack);
//                            you_block_user_status=true;
                        }

                    }else {
                        chat_ll.setVisibility(View.VISIBLE);
//                        iv_block.setImageResource(R.drawable.chat_ic_unbloack);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        mRootRef.child("Chat").child(mCurrentUserId).child(other_user_chat).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                logg("chideadded",dataSnapshot.getValue().toString());
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                logg("chidremove",dataSnapshot.getValue().toString());
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    public void block_user_status(final Context mcoxt,String user){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mcoxt);

        alertDialog.setMessage(user);
        alertDialog.setPositiveButton(mcoxt.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        });

        AlertDialog alertDialog1= alertDialog.create();
        alertDialog1.show();

        alertDialog1.getButton(alertDialog1.BUTTON_NEGATIVE).setTextColor(mcoxt.getResources().getColor(R.color.colorAccent));
        alertDialog1.getButton(alertDialog1.BUTTON_POSITIVE).setTextColor(mcoxt.getResources().getColor(R.color.colorAccent));
    }
}

