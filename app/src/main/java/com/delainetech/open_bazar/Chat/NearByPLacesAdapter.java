//package com.delainetech.open_bazar.Chat;
//
//import android.content.Context;
//import android.support.v7.widget.RecyclerView;
//import android.text.TextUtils;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.girlfriend.R;
//import com.squareup.picasso.Picasso;
//
//import java.util.ArrayList;
//
//public class NearByPLacesAdapter extends  RecyclerView.Adapter<NearByPLacesAdapter.CustomViewHolder>
//{
//    Context c;
//
//
//  //  String image_url="https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&key=AIzaSyC92Rdnlx0I1AUkmeCYhGYmG7nxYmflxMY&photoreference=";
//
//
//    ArrayList<Places> places;
//
//
//
//
//    public NearByPLacesAdapter(Context c, ArrayList<Places> places) {
//
//        this.places=places;
//        this.c = c;
//    }
//
//    @Override
//    public NearByPLacesAdapter.CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
//    {
//        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.nearbylocationadapter, null);
//        NearByPLacesAdapter.CustomViewHolder viewHolder = new NearByPLacesAdapter.CustomViewHolder(view);
//        return viewHolder;
//    }
//
//    @Override
//    public void onBindViewHolder(NearByPLacesAdapter.CustomViewHolder holder, final int position)
//    {
//        //holder.image.setText(add_interests[position]);
//
//
//        NearByPLacesAdapter.CustomViewHolder vh0 = (NearByPLacesAdapter.CustomViewHolder) holder;
//        if (places!=null) {
//
//
//           //Loge("place_name", places.get(position).getName()+" nameee");
////            if (places.get(position).getName() != "null" & !(TextUtils.isEmpty(places.get(position).getName())
////                    & !(places.get(position).getName().equalsIgnoreCase("null")))) {
//                vh0.Name.setText(places.get(position).getName());
////            }
//
//            vh0.Address.setText(places.get(position).getAddress());
//
//          // //Loge("im_url", image_url + places.get(position).getImage());
//
//            if (places.get(position).getImage() != "null" & !(TextUtils.isEmpty(places.get(position).getImage())
//                    & !(places.get(position).getImage().equalsIgnoreCase("null")))) {
//
//                Picasso.get()
//                        .load( places.get(position).getImage())
//                        .error(R.drawable.ic_pin_white)
//                        .placeholder(R.drawable.ic_pin_white)
//                        .into(vh0.image);
//
//            }
//        }
//
//        vh0.layout1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ((SelectLocation)c).sendLocation(places.get(position).getLat(),
//                        places.get(position).getLng(),
//                        places.get(position).getName(),
//                        places.get(position).getAddress());
//            }
//        });
//
//
//    }
//
//
//
//
//
//
//
//
//
//    @Override
//    public int getItemCount()
//    {
//        return places.size();
//    }
//    public class CustomViewHolder extends RecyclerView.ViewHolder {
//
//        ImageView image;
//         TextView Name,Address;
//         RelativeLayout layout1;
//
//        public CustomViewHolder(View view) {
//            super(view);
//            Address=(TextView)view.findViewById(R.id.tv_address);
//            image=(ImageView)view.findViewById(R.id.image);
//            Name=(TextView)view.findViewById(R.id.tv_name);
//            layout1=(RelativeLayout)view.findViewById(R.id.layout1);
//        }
//    }
//
//
//}
