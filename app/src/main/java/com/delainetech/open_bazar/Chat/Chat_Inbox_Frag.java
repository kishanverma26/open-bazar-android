package com.delainetech.open_bazar.Chat;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.delainetech.open_bazar.Adpost_Add_Activity;
import com.delainetech.open_bazar.Chat.ChatActivity;
import com.delainetech.open_bazar.Custom_classes.EndlessRecyclerViewScrollListener;
import com.delainetech.open_bazar.Home_Screen_Activity;
import com.delainetech.open_bazar.Models.ADS;
import com.delainetech.open_bazar.Models.CHAT_INBOX;
import com.delainetech.open_bazar.Models.Conv;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.PushService;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

import static com.delainetech.open_bazar.Utils.Common.logg;

public class Chat_Inbox_Frag extends Fragment implements ParamKeys,SwipeRefreshLayout.OnRefreshListener,View.OnClickListener{


    View v;
    Context mcontext;
    UserSharedPreferences preferences;

    SwipeRefreshLayout swiperefresh;
    RecyclerView reclcler;
    ArrayList<CHAT_INBOX> chat_inboxArrayList=new ArrayList<>();
    EndlessRecyclerViewScrollListener recyclerViewScrollListener;

    TextView tvemptyview;
    ImageView home_notification;


    // firbase chat
    private DatabaseReference mConvDatabase;
    private DatabaseReference mMessageDatabase;
    private DatabaseReference mUsersDatabase,mAdsDatabase,mBlockDatabase;

    private String mCurrent_user_id,nameuser="";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.chat_inbox_frag, container, false);
        mcontext=getActivity();
        FirebaseApp.initializeApp(getActivity());
        initial();

        return v;
    }



    private void initial() {
       home_notification=(ImageView) v.findViewById(R.id.home_notification);
        home_notification.setOnClickListener(this);
        preferences=new UserSharedPreferences(mcontext);
        tvemptyview=v.findViewById(R.id.tvemptyview);
//        tvemptyview.setOnClickListener(this);
        swiperefresh=v.findViewById(R.id.swiperefresh);
        reclcler=v.findViewById(R.id.reclcler);

        swiperefresh.setOnRefreshListener(this);

        swiperefresh.setColorSchemeColors(getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorPrimary),getResources().getColor(R.color.colorPrimary));

        ((Home_Screen_Activity)mcontext).fab_add_ad.setOnClickListener(this);

        mRootRef = FirebaseDatabase.getInstance().getReference();
//        firbase
        mCurrent_user_id="user"+preferences.getuserid();
        register_user(preferences.getname());
        mConvDatabase = FirebaseDatabase.getInstance().getReference().child("Chat").child(mCurrent_user_id);

        mConvDatabase.keepSynced(false);
        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
        mAdsDatabase = FirebaseDatabase.getInstance().getReference().child("Ads");
        mBlockDatabase = FirebaseDatabase.getInstance().getReference().child("Block").child("user"+preferences.getuserid());
        mMessageDatabase = FirebaseDatabase.getInstance().getReference().child("messages");
        mUsersDatabase.keepSynced(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);

        reclcler.setHasFixedSize(true);
        reclcler.setLayoutManager(linearLayoutManager);

    }



    @Override
    public void onRefresh() {
    swiperefresh.setRefreshing(false);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.home_notification:

                break;

            case R.id.fab_add_ad:
                Intent i=new Intent(mcontext, Adpost_Add_Activity.class);
                startActivityForResult(i,10);
                break;

        }
    }

    public void register_user(final String name){

        FirbaseCommon.register_user("user"+preferences.getuserid(),preferences.getname(),preferences.getimage(),true);
    }


    @Override
    public void onStart() {
        super.onStart();

        Query conversationQuery = mConvDatabase.orderByChild("timestamp");

        FirebaseRecyclerAdapter<Conv, ConvViewHolder> firebaseConvAdapter = new FirebaseRecyclerAdapter<Conv, ConvViewHolder>(
                Conv.class,
                R.layout.chat_inbox_adapter,
                ConvViewHolder.class,
                conversationQuery
        ) {
            @Override
            protected void populateViewHolder(final ConvViewHolder convViewHolder, final Conv conv, final int i) {

                final String list_user_id = getRef(i).getKey();

                String[] parts = list_user_id.split("_");
                String post_id="",userid="";
                if (parts.length>0) {
                    post_id = parts[0];
                    userid = parts[1];
                }
                Log.e("list_user_id",list_user_id+" ,userid="+userid);
                Query lastMessageQuery = mMessageDatabase.child(post_id+"_"+mCurrent_user_id).child(list_user_id).limitToLast(1);

                lastMessageQuery.addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                        logg("datasnapshot1",dataSnapshot.getValue().toString());
                        String msg = dataSnapshot.child("message").getValue().toString();
                        if (getActivity()!=null) {
                            convViewHolder.setMessage(msg, conv.isSeen(), getActivity(),
                                    dataSnapshot.child("user_name").getValue().toString(),
                                    dataSnapshot.child("timestamp").getValue().toString(),
                                    dataSnapshot.child("type").getValue().toString());
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


                final String finalUserid = userid;
                mUsersDatabase.child(userid).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot!=null) {
                            if (dataSnapshot.hasChild("name")) {
                                logg("datasnapshot",dataSnapshot.getValue().toString());

                                final String userName = dataSnapshot.child("name").getValue().toString();
                                final String userThumb = dataSnapshot.child("image").getValue().toString();

                                if (dataSnapshot.hasChild("online")) {

                                    String userOnline = dataSnapshot.child("online").getValue().toString();

                                    convViewHolder.setUserOnline(userOnline);

                                }

                                convViewHolder.setName(userName);
                                if (getActivity()!=null) {
                                    convViewHolder.setUserImage(userThumb, getActivity());
                                }

                                convViewHolder.rv.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        if (chat_inboxArrayList.size()>i) {
                                            Intent chatIntent = new Intent(getActivity(), ChatActivity.class);
                                            chatIntent.putExtra("user_id", finalUserid);
                                            chatIntent.putExtra("user_name", userName);
                                            chatIntent.putExtra("userimage", userThumb);
                                            chatIntent.putExtra("name", nameuser);
                                            chatIntent.putExtra("id", preferences.getuserid());
                                            chatIntent.putExtra("ad_tital", chat_inboxArrayList.get(i).getTitle());
                                            chatIntent.putExtra("ad_image", chat_inboxArrayList.get(i).getImage());
                                            chatIntent.putExtra("ad_price", "€ " + chat_inboxArrayList.get(i).getPrice());
                                            chatIntent.putExtra("ad_id", chat_inboxArrayList.get(i).getPostid());

                                            Log.e("ides", chatIntent.getExtras() + ",--pos=" + i);
                                            startActivityForResult(chatIntent,10);
                                        }
                                    }
                                });
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


                mAdsDatabase.child(post_id).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot!=null) {
                            if (dataSnapshot.hasChild("title"))
                            {
                                logg("datasnapshotpost",dataSnapshot.getValue().toString());

                                chat_inboxArrayList.add(0,new CHAT_INBOX(dataSnapshot.child("image").getValue().toString(),
                                        dataSnapshot.child("price").getValue().toString(),dataSnapshot.child("title").getValue().toString(),
                                        dataSnapshot.child("ad_id").getValue().toString()));

                                convViewHolder.settital(dataSnapshot.child("title").getValue().toString());
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                logg("firbasereff",userid);
                final String finalUserid1 = userid;
                mBlockDatabase.child(userid).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot!=null) {
//                            if (dataSnapshot.getKey())
                            if (dataSnapshot.hasChild("blocked"+ finalUserid1)) {
                                logg("dataa1", dataSnapshot.getValue().toString() + "");
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

                logg("firbasereff",mBlockDatabase.getDatabase().toString()+"");
            }

        };

        reclcler.setAdapter(firebaseConvAdapter);

    }

    public static class ConvViewHolder extends RecyclerView.ViewHolder {

        View mView;
        RelativeLayout rv;
        public ConvViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

            rv=itemView.findViewById(R.id.rv);
        }

        public void setMessage(String message, boolean isSeen,Context context,String lastmsgusername,String timestamp,String type){

            TextView userStatusView = (TextView) mView.findViewById(R.id.lastmsg1);
            TextView lastmsg = (TextView) mView.findViewById(R.id.lastmsg);
            TextView tv_time = (TextView) mView.findViewById(R.id.tv_time);
            lastmsg.setText(lastmsgusername+" :");
            tv_time.setText(Common.getTimeAgo(Long.parseLong(timestamp),context));

            if (type.equals("image")){
                message="Shared Image";
            }else if (type.equals("audio")){
                message="Shared Audio";
            }else if (type.equals("location")){
                message="Shared Location";
            }
            userStatusView.setText(message);

            if(!isSeen){
                userStatusView.setTypeface(userStatusView.getTypeface(), Typeface.BOLD);
                if (context!=null) {
                    userStatusView.setTextColor(context.getResources().getColor(R.color.colorAccent));
                }
            } else {
                userStatusView.setTypeface(userStatusView.getTypeface(), Typeface.NORMAL);
            }

        }

        public void setName(String name){

            TextView userNameView = (TextView) mView.findViewById(R.id.tvchattername);
            userNameView.setText(name);

        }
    RequestOptions requestOptions=new RequestOptions().error(R.drawable.userimage).centerCrop();
        public void setUserImage(String thumb_image, Context ctx){

            CircleImageView userImageView = (CircleImageView) mView.findViewById(R.id.iv_circle);
//            Picasso.with(ctx).load(thumb_image).placeholder(R.drawable.default_avatar).into(userImageView);
            Log.e( "setUserImage: ", Common.Image_Loading_Url(thumb_image) );

            Glide.with(ctx).load(Common.Image_Loading_Url(thumb_image)).apply(requestOptions).into(userImageView);

        }

        public void settital(String tital){
            TextView tv_ad_tital = (TextView) mView.findViewById(R.id.tv_ad_tital);
            tv_ad_tital.setText(tital);

        }

        public void setUserOnline(String online_status) {

            ImageView userOnlineView = (ImageView) mView.findViewById(R.id.iv_online);

            if(online_status.equals("true")){

                userOnlineView.setVisibility(View.VISIBLE);

            } else {

                userOnlineView.setVisibility(View.INVISIBLE);

            }

        }


    }


    private DatabaseReference mRootRef;

    @Override
    public void onResume() {
        super.onResume();
        if (mRootRef!=null) {
            online_status("true");
        }
        PushService. chatactivitystr="in";

        if (!Common.isNetworkConnected(getActivity())){
            Toast.makeText(getActivity(), No_Internet_Connection, Toast.LENGTH_SHORT).show();
        }
    }

//    @Override
//    public void onStop() {
//        super.onStop();
//        if (mRootRef!=null) {
//            online_status(ServerValue.TIMESTAMP);
//        }
//    }


    private void online_status(Object status){
        mRootRef.child("Users").child("user"+preferences.getuserid()).child("online").setValue(status);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        logg("requestCode=",requestCode+", resultCode="+resultCode);
        if (requestCode==10 && resultCode == Activity.RESULT_OK){
            if (data!=null) {
                logg("otheruserid",data.getStringExtra("otherid"));
                mRootRef.child("Chat").child("user" + preferences.getuserid()).child(data.getStringExtra("otherid")).removeValue();
            }
        }
    }



}
