package com.delainetech.open_bazar.Chat;

import android.app.ProgressDialog;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.delainetech.open_bazar.Chat.ChatActivity;
import com.delainetech.open_bazar.Models.CHATINBOX;
import com.delainetech.open_bazar.Models.CHAT_INBOX;
import com.delainetech.open_bazar.Models.Chats_msg;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import static com.delainetech.open_bazar.Utils.Common.logg;
import static com.delainetech.open_bazar.Utils.ParamKeys.User_id;
import static com.delainetech.open_bazar.Utils.ParamKeys.baseurl;

/**
 * Created by Kishan on 11-Mar-18.
 */

public class Inbox_Chat_adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    ArrayList<CHATINBOX> chatlist;
    Context mcontex;
    private DatabaseReference mUsersDatabase,mAdsDatabase,mBlockDatabase;
    private DatabaseReference mMessageDatabase;
    UserSharedPreferences preferences;
    ArrayList<CHAT_INBOX> chat_inboxArrayList=new ArrayList<>();
    private DatabaseReference mRootRef;
    public Inbox_Chat_adapter(ArrayList<CHATINBOX> chat, Context mcontex) {
//        Collections.reverse(chat);
        this.chatlist = chat;
        this.mcontex = mcontex;
        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
        mUsersDatabase.keepSynced(true);
        mMessageDatabase = FirebaseDatabase.getInstance().getReference().child("messages");
        mMessageDatabase.keepSynced(true);
        preferences=UserSharedPreferences.getInstance(mcontex);

        mAdsDatabase = FirebaseDatabase.getInstance().getReference().child("Ads");
        mBlockDatabase = FirebaseDatabase.getInstance().getReference().child("Block").child("user"+preferences.getuserid());
        mRootRef = FirebaseDatabase.getInstance().getReference();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_inbox_adapter, parent, false);
        return new UserViewholder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        mUsersDatabase.child(chatlist.get(position).getUserid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot!=null) {
                    if (dataSnapshot.hasChild("name")) {
                        logg("datasnapshot",dataSnapshot.getValue().toString()+" pos="+position);

                        final String userName = dataSnapshot.child("name").getValue().toString();
                        final String userThumb = dataSnapshot.child("image").getValue().toString();
                        chatlist.get(position).setUsername(userName);

                        if (dataSnapshot.hasChild("online")) {

                            String userOnline = dataSnapshot.child("online").getValue().toString();

                            ( (UserViewholder)holder ) .setUserOnline(userOnline);

                        }

                        ( (UserViewholder)holder ) .setName(userName);

                        ( (UserViewholder)holder ) .setUserImage(userThumb, mcontex);

//                        Glide.with(Userlist_activity.this).load(Constant.imageUri+userThumb).into(holder.imageView);

                        ( (UserViewholder)holder ) .rv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                if (chat_inboxArrayList.size()>position) {
                                    Intent chatIntent = new Intent(mcontex, ChatActivity.class);
                                    chatIntent.putExtra("user_id", chatlist.get(position).getUserid());
                                    chatIntent.putExtra("user_name", userName);
                                    chatIntent.putExtra("userimage", userThumb);
                                    chatIntent.putExtra("id", preferences.getuserid());
                                    chatIntent.putExtra("ad_tital1", chat_inboxArrayList.get(position).getTitle());
                                    chatIntent.putExtra("ad_image1", chat_inboxArrayList.get(position).getImage());
                                    chatIntent.putExtra("ad_price1", "€ " + chat_inboxArrayList.get(position).getPrice());
                                    chatIntent.putExtra("ad_id1", chat_inboxArrayList.get(position).getPostid());
                                    chatIntent.putExtra("ad_tital", chatlist.get(position).getAd_titel());
                                    chatIntent.putExtra("ad_image", chatlist.get(position).getAd_image());
                                    chatIntent.putExtra("ad_price", "€ " + chatlist.get(position).getAd_price());
                                    chatIntent.putExtra("ad_id", chatlist.get(position).getAd_postid());

                                    Log.e("ides", chatIntent.getExtras() + ",--pos=" + position);
                                    mcontex. startActivity(chatIntent);
                                }
                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        Query lastMessageQuery = mMessageDatabase.child(chatlist.get(position).getPostid()+"_user"+preferences.getuserid())
                .child(chatlist.get(position).getId()).limitToLast(1);

        lastMessageQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                logg("datasnapshot1",dataSnapshot.getValue().toString());
                if (dataSnapshot.hasChild("message")) {
                    String msg = dataSnapshot.child("message").getValue().toString();

                    ((UserViewholder) holder).setMessage(msg, Boolean.parseBoolean(dataSnapshot.child("seen").getValue().toString()), mcontex,
                            dataSnapshot.child("user_name").getValue().toString(),
                            dataSnapshot.child("timestamp").getValue().toString(),
                            dataSnapshot.child("type").getValue().toString());
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Log.e("loadmore_adapterchange",dataSnapshot.getValue().toString()+"");

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        mAdsDatabase.child(chatlist.get(position).getPostid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot!=null) {
                    if (dataSnapshot.hasChild("title")) {
                        logg("datasnapshotpost",dataSnapshot.getValue().toString());
                        chatlist.get(position).setAd_titel(dataSnapshot.child("title").getValue().toString());
                        chatlist.get(position).setAd_image(dataSnapshot.child("image").getValue().toString());
                        chatlist.get(position).setAd_price(dataSnapshot.child("price").getValue().toString());
                        chatlist.get(position).setAd_postid(dataSnapshot.child("ad_id").getValue().toString());

                        chat_inboxArrayList.add(0,new CHAT_INBOX(dataSnapshot.child("image").getValue().toString(),
                                dataSnapshot.child("price").getValue().toString(),dataSnapshot.child("title").getValue().toString(),
                                dataSnapshot.child("ad_id").getValue().toString()));

                        ((UserViewholder)holder).settital(dataSnapshot.child("title").getValue().toString());
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mBlockDatabase.child(chatlist.get(position).getUserid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot!=null) {
//                            if (dataSnapshot.getKey())
                    if (dataSnapshot.hasChild("blocked"+ chatlist.get(position).getUserid())) {
                        logg("dataa1", dataSnapshot.getValue().toString() + "");
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        logg("firbasereff",mBlockDatabase.getDatabase().toString()+"");


        ((UserViewholder)holder).rv.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                CharSequence colors[] = new CharSequence[]{"Delete conversation", "Block user"};

                AlertDialog.Builder builder = new AlertDialog.Builder(mcontex);
                builder.setTitle("The Conversation");
                builder.setItems(colors, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.e("value is", "" + which);
                        switch (which) {
                            case 0:
                                delete_chat(mcontex,chatlist.get(position).getPostid()+"_user"+preferences.getuserid()
                                        ,chatlist.get(position).getId(),position);
                                break;
                            case 1:
                                block_user(mcontex,position);
                                break;

                        }
                    }
                });
                builder.show();
                return true;
            }
        });

        ((UserViewholder)holder).setseenunseenstatus(chatlist.get(position).isSeen());
    }




    @Override
    public int getItemCount() {
        logg("size",chatlist.size()+"");
        return chatlist.size();
    }

    class UserViewholder extends RecyclerView.ViewHolder{
        RelativeLayout rv;
        View mView;
        TextView userStatusView;

        public UserViewholder(View itemView) {
            super(itemView);
            mView = itemView;
            rv=itemView.findViewById(R.id.rv);
            userStatusView = (TextView) mView.findViewById(R.id.lastmsg1);
        }

        public void setMessage(String message, boolean isSeen,Context context,String lastmsgusername,String timestamp,String type){

            TextView lastmsg = (TextView) mView.findViewById(R.id.lastmsg);
            TextView tv_time = (TextView) mView.findViewById(R.id.tv_time);
            lastmsg.setText(lastmsgusername+" :");
            tv_time.setText(Common.getTimeAgo(Long.parseLong(timestamp),context));

            if (type.equals("image")){
                message="Shared Image";
            }else if (type.equals("audio")){
                message="Shared Audio";
            }else if (type.equals("location")){
                message="Shared Location";
            }
            userStatusView.setText(message);

//            if(!isSeen){
//                userStatusView.setTypeface(userStatusView.getTypeface(), Typeface.BOLD);
//                if (context!=null) {
//                    userStatusView.setTextColor(context.getResources().getColor(R.color.colorAccent));
//                }
//            } else {
//                userStatusView.setTypeface(userStatusView.getTypeface(), Typeface.NORMAL);
//            }

        }

        public void setseenunseenstatus(boolean isSeen){
            if(!isSeen){
                userStatusView.setTypeface(userStatusView.getTypeface(), Typeface.BOLD);
                if (mcontex!=null) {
                    userStatusView.setTextColor(mcontex.getResources().getColor(R.color.colorAccent));
                }
            } else {
                userStatusView.setTypeface(userStatusView.getTypeface(), Typeface.NORMAL);
            }
        }

        public void setName(String name){

            TextView userNameView = (TextView) mView.findViewById(R.id.tvchattername);
            userNameView.setText(name);

        }
        RequestOptions requestOptions=new RequestOptions().error(R.drawable.userimage).centerCrop();
        public void setUserImage(String thumb_image, Context ctx){

            CircleImageView userImageView = (CircleImageView) mView.findViewById(R.id.iv_circle);
//            Picasso.with(ctx).load(thumb_image).placeholder(R.drawable.default_avatar).into(userImageView);
            Log.e( "setUserImage: ", Common.Image_Loading_Url(thumb_image) );

            Glide.with(ctx).load(Common.Image_Loading_Url(thumb_image)).apply(requestOptions).into(userImageView);

        }

        public void settital(String tital){
            TextView tv_ad_tital = (TextView) mView.findViewById(R.id.tv_ad_tital);
            tv_ad_tital.setText(tital);

        }

        public void setUserOnline(String online_status) {

            ImageView userOnlineView = (ImageView) mView.findViewById(R.id.iv_online);

            if(online_status.equals("true")){

                userOnlineView.setVisibility(View.VISIBLE);

            } else {

                userOnlineView.setVisibility(View.INVISIBLE);

            }

        }

    }

    public void delete_chat(final Context mcoxt, final String current, final String otheruser, final int pos){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mcoxt);

        alertDialog.setMessage("Are your sure you want to delete conversation?");
        alertDialog.setPositiveButton(mcoxt.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                mRootRef.child("messages").child(current).child(otheruser).removeValue();
                mRootRef.child("Chat").child("user"+preferences.getuserid()).child(otheruser).removeValue();

                chatlist.remove(pos);
                notifyDataSetChanged();

            }
        });
        alertDialog.setNegativeButton(mcoxt.getResources().getString(R.string.No), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog1= alertDialog.create();
        alertDialog1.show();

        alertDialog1.getButton(alertDialog1.BUTTON_NEGATIVE).setTextColor(mcoxt.getResources().getColor(R.color.colorAccent));
        alertDialog1.getButton(alertDialog1.BUTTON_POSITIVE).setTextColor(mcoxt.getResources().getColor(R.color.colorAccent));
    }


    public void block_user(final Context mcoxt, final int pos){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mcoxt);

        alertDialog.setMessage("Are your sure you want to block?");
        alertDialog.setPositiveButton(mcoxt.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                block_user(chatlist.get(pos).getUserid(),"block");
            }
        });
        alertDialog.setNegativeButton(mcoxt.getResources().getString(R.string.No), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog1= alertDialog.create();
        alertDialog1.show();

        alertDialog1.getButton(alertDialog1.BUTTON_NEGATIVE).setTextColor(mcoxt.getResources().getColor(R.color.colorAccent));
        alertDialog1.getButton(alertDialog1.BUTTON_POSITIVE).setTextColor(mcoxt.getResources().getColor(R.color.colorAccent));
    }

    public void  block_user(final String userid,final String blockstatus){
        final ProgressDialog progressDialog=new ProgressDialog(mcontex);
        progressDialog.setMessage("Please Wait.....");
        progressDialog.show();
        final  String mChatUser="user"+userid;
        final StringRequest stringRequest=new StringRequest(Request.Method.POST, baseurl + "block_user", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                pro.cancel();
                progressDialog.dismiss();
                logg("follow",response);
                try {
                    JSONObject object=new JSONObject(response);
                    if (object.getString("status").equals("true")){
                        mRootRef.child("Block").child("user"+preferences.getuserid()).child(mChatUser).child("blocked"+mChatUser).setValue(true);
                        mRootRef.child("Block").child(mChatUser).child("user"+preferences.getuserid()).child("blocked"+mChatUser).setValue(true);


                        Toast.makeText(mcontex, "User is blocked now", Toast.LENGTH_SHORT).show();
                    }

                }
                catch (Exception e){
//                    pro.cancel();
                    progressDialog.dismiss();

                    Log.e("error","e",e);
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                pro.cancel();
                progressDialog.dismiss();

                Log.e("Volleyerror","e",volleyError);

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization","Bearer "+preferences.gettoken());
                Log.e("header",header+"");
                return header;
            }

            @Override
            protected Map<String, String> getParams() {
                //set post params
                Map<String, String> params = new HashMap<>();
                params.put(User_id,preferences.getuserid());
                params.put("block_id",userid);
                params.put("type",blockstatus);

                Log.e("params",params+"");
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue mRequestQueue = Volley.newRequestQueue(mcontex);
        mRequestQueue.add(stringRequest);

    }



}
