package com.delainetech.open_bazar.Chat;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

import java.util.HashMap;
import java.util.Map;

public class FirbaseCommon {

    public static String ADS="Ads";




    public static void regiter_adpost(final String postid,String tital,String image,String price,String mobileno,String mobileno_status){

        DatabaseReference mDatabase;

        mDatabase = FirebaseDatabase.getInstance().getReference().child(ADS).child("ads"+postid);
//        String device_token = FirebaseInstanceId.getInstance().getToken();

        HashMap<Object, Object> userMap = new HashMap<>();
        userMap.put("title", tital);
        userMap.put("image", image);
        userMap.put("price", price);
        userMap.put("ad_id", postid);
        userMap.put("mobileno", mobileno);
        userMap.put("mobileno_status", mobileno_status);

        mDatabase.setValue(userMap);

    }



    public static void register_user(String id,String username,String userimage,boolean isonline){

        DatabaseReference mDatabase= FirebaseDatabase.getInstance().getReference().child("Users").child(id);
//        String device_token = FirebaseInstanceId.getInstance().getToken();

        HashMap<Object, Object> userMap = new HashMap<>();
        userMap.put("name", username);
        userMap.put("image",userimage);
        userMap.put("online", isonline);

        mDatabase.setValue(userMap);
    }

    public static void sendMessage(String msg,String type,String posid,String userid,String otheruserid,String username) {
        DatabaseReference  mRootRef = FirebaseDatabase.getInstance().getReference();

        String current_userchat="";
         String other_user_chat="";
        current_userchat="ads"+posid+"_user"+userid;;
        other_user_chat="ads"+posid+"_user"+otheruserid;
        String mCurrentUserId="user"+userid;
        String mChatUser="user"+otheruserid;

        if(!TextUtils.isEmpty(msg)){

            String current_user_ref = "messages/" + current_userchat + "/" + other_user_chat;
            String chat_user_ref = "messages/" + other_user_chat + "/" + current_userchat;

            DatabaseReference user_message_push = mRootRef.child("messages")
                    .child(current_user_ref).child(chat_user_ref).push();


            Map messageMap = new HashMap();
            messageMap.put("message", msg);

                messageMap.put("seen", false);

            String timestamp=System.currentTimeMillis()+"";
            messageMap.put("type", type);
            messageMap.put("timestamp", timestamp);
            messageMap.put("user", mCurrentUserId);
            messageMap.put("user_name", username);
            messageMap.put("status", "pending");

            Map messageUserMap = new HashMap();
            messageUserMap.put(current_user_ref + "/" + timestamp, messageMap);
            messageUserMap.put(chat_user_ref + "/" + timestamp, messageMap);


            mRootRef.child("Chat").child(mCurrentUserId).child(other_user_chat).child("seen").setValue(true);
            mRootRef.child("Chat").child(mCurrentUserId).child(other_user_chat).child("timestamp").setValue(ServerValue.TIMESTAMP);

            mRootRef.child("Chat").child(mChatUser).child(current_userchat).child("seen").setValue(false);
            mRootRef.child("Chat").child(mChatUser).child(current_userchat).child("timestamp").setValue(ServerValue.TIMESTAMP);

            mRootRef.updateChildren(messageUserMap, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                    if(databaseError != null){

                        Log.d("CHAT_LOG", databaseError.getMessage().toString());

                    }

                }
            });

        }

    }

}
