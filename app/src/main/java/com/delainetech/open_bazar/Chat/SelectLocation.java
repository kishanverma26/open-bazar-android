//package com.delainetech.open_bazar.Chat;
//
//import android.Manifest;
//import android.annotation.SuppressLint;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentSender;
//import android.content.SharedPreferences;
//import android.content.pm.PackageManager;
//import android.graphics.Bitmap;
//import android.graphics.Canvas;
//import android.graphics.drawable.Drawable;
//import android.location.LocationManager;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.Menu;
//import android.view.MenuItem;
//import android.view.View;
//import android.widget.ProgressBar;
//
//import com.android.volley.DefaultRetryPolicy;
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.RetryPolicy;
//import com.android.volley.toolbox.Volley;
//import com.delainetech.open_bazar.R;
//import com.delainetech.open_bazar.Utils.UserSharedPreferences;
//import com.google.android.gms.common.ConnectionResult;
//import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
//import com.google.android.gms.common.GooglePlayServicesRepairableException;
//import com.google.android.gms.common.api.GoogleApiClient;
//import com.google.android.gms.common.api.PendingResult;
//import com.google.android.gms.common.api.ResultCallback;
//import com.google.android.gms.common.api.Status;
//import com.google.android.gms.location.LocationListener;
//import com.google.android.gms.location.LocationRequest;
//import com.google.android.gms.location.LocationSettingsRequest;
//import com.google.android.gms.location.LocationSettingsResult;
//import com.google.android.gms.location.LocationSettingsStatusCodes;
//import com.google.android.gms.location.places.Places;
//import com.google.android.gms.maps.GoogleMap;
//import com.google.android.gms.maps.OnMapReadyCallback;
//import com.google.android.gms.maps.SupportMapFragment;
//
//import org.json.JSONArray;
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Map;
//
//import androidx.annotation.Nullable;
//import androidx.appcompat.app.AppCompatActivity;
//import androidx.appcompat.widget.Toolbar;
//import androidx.recyclerview.widget.LinearLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//
//
//import static com.google.android.gms.location.LocationServices.*;
//
//public class SelectLocation extends AppCompatActivity implements OnMapReadyCallback,GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,LocationListener
//{
//
//
//    android.location.Location mLastLocation;
//    private static int UPDATE_INTERVAL = 10000; // 10 sec
//    private static int FATEST_INTERVAL = 5000; // 5 sec
//    private static int DISPLACEMENT = 10; // 10 meters
//    private LocationRequest mLocationRequest;
//    GoogleMap mMap;
//    SupportMapFragment map;
//    int PLACE_AUTOCOMPLETE_REQUEST_CODE=1;
//    View view;
//   // MaterialSearchView searchView;
//    double lat,lng;
//    Toolbar toolbar;
//    String Location_Name="";
//    public GoogleApiClient mGoogleApiClient;
//    final static int REQUEST_LOCATION = 199;
//    final static int MY_PERMISSIONS_REQUEST_LOCATION = 1999;
//    //Marker marker;
//    ProgressBar progress;
//    RecyclerView Location_rv;
//    ArrayList<Places> places;
//    UserSharedPreferences userPref;
//    NearByPLacesAdapter adapter;
//    LinearLayoutManager lManager;
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.select_location);
//        init(view);
//    }
//
//    @Override
//    protected void onStart()
//    {
//        super.onStart();
//        if (mGoogleApiClient != null)
//        {
//            mGoogleApiClient.connect();
//        }
//    }
//
//    @Override
//    protected void onStop()
//    {
//        super.onStop();
//    }
//
//    @Override
//    protected void onDestroy()
//    {
//        super.onDestroy();
//        if (mGoogleApiClient != null) {
//            mGoogleApiClient.disconnect();
//        }
//    }
//    private void init(View view) {
//        toolbar=(Toolbar)findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        getSupportActionBar().setHomeButtonEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setTitle("Select Loaction");
//        map = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
//        map.getMapAsync(this);
//        progress=(ProgressBar)findViewById(R.id.progress);
//        Location_rv=(RecyclerView)findViewById(R.id.location_rv);
//        lManager=new LinearLayoutManager(SelectLocation.this);
//        Location_rv.setLayoutManager(lManager);
//
//       /// searchView = (MaterialSearchView) findViewById(R.id.search_view);
//        userPref= UserSharedPreferences.getInstance(SelectLocation.this);
//
//        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE );
//        buildGoogleApiClient();
//        createLocationRequest();
//        OnUpdateListener();
//
//    }
//    protected synchronized void buildGoogleApiClient()
//    {
//        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .addConnectionCallbacks(this)
//                .addOnConnectionFailedListener(this)
//                .addApi(API).build();
//    }
//
//
//    protected void createLocationRequest()
//    {
//        mLocationRequest = new LocationRequest();
//        mLocationRequest.setInterval(UPDATE_INTERVAL);
//        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
//        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
//    }
//
//
//    @SuppressLint("MissingPermission")
//    protected void startLocationUpdates() {
//
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
//        {
//            return;
//        }
//        FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest,this);
//
//    }
//
//    @SuppressLint("MissingPermission")
//    private void displayLocation()
//    {
//
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
//        {
//            return;
//        }
//        mLastLocation = FusedLocationApi.getLastLocation(mGoogleApiClient);
//
//        if (mLastLocation != null)
//        {
//            double latitude = mLastLocation.getLatitude();
//            double longitude = mLastLocation.getLongitude();
//
//            lat= mLastLocation.getLatitude();
//            lng= mLastLocation.getLongitude();
//           //Loge("Location_gps11111111111",latitude + ", " + longitude);
//
//           // findGirlfriendbydefaultlocation(lat,lng);
//        } else {
//           //Loge("check","location");
//        }
//    }
//
//    @Override
//    public void onConnected(@Nullable Bundle bundle) {
//        displayLocation();
//    }
//
//    @Override
//    public void onConnectionSuspended(int i) {
//
//    }
//
//    @Override
//    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
//
//    }
//
//    @Override
//    public void onLocationChanged(android.location.Location location) {
//        displayLocation();
//
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == RESULT_OK && data !=null) {
//            Place place = PlaceAutocomplete.getPlace(this, data);
//            /*Get Location*/
//            if (place!=null) {
//                mMap.clear();
//                mMap.addMarker(new MarkerOptions()
//                        .title(place.getName() + "")
//                        .icon(bitmapDescriptorFromVector(SelectLocation.this, R.drawable.pin))
//                        .position(place.getLatLng()));
//                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 15));
//                Location_Name = place.getName() + "";
//                getPlaces(place.getLatLng().latitude,place.getLatLng().longitude,
//                        place.getName()+"",place.getAddress()+"");
//            }
//
//        } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
//            Status status = PlaceAutocomplete.getStatus(this, data);
//            // TODO: Handle the error.
//           //Loge("error status", status.getStatusMessage());
//
//        } else if (resultCode == RESULT_CANCELED) {
//            // The user canceled the operation.
//        }
//    }
//
//    public void OnUpdateListener()
//    {
//        if(Constants.checkGPSPermission(getApplicationContext()))
//        {
//            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION))
//            {
//                ActivityCompat.requestPermissions(SelectLocation.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},
//                        MY_PERMISSIONS_REQUEST_LOCATION);
//            }
//            else
//            {
//                ActivityCompat.requestPermissions(SelectLocation.this,
//                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
//                        MY_PERMISSIONS_REQUEST_LOCATION);
//            }
//        }
//        else if(Constants.checkGPSStatus(SelectLocation.this))
//            turnGPSOn();
//        else if (mGoogleApiClient.isConnected())
//        {
//            startLocationUpdates();
//        }
//
//    }
//
//    public void turnGPSOn()
//    {
//        mGoogleApiClient.connect();
//        LocationRequest locationRequest = LocationRequest.create();
//        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//        locationRequest.setInterval(500);
//        locationRequest.setFastestInterval(1000);
//        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
//
//        builder.setAlwaysShow(true);
//
//        PendingResult<LocationSettingsResult> result =
//                SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
//        result.setResultCallback(new ResultCallback<LocationSettingsResult>()
//        {
//            @Override
//            public void onResult(LocationSettingsResult result)
//            {
//                final Status status = result.getStatus();
//                switch (status.getStatusCode())
//                {
//                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
//                        try
//                        {
//                            status.startResolutionForResult(SelectLocation.this, REQUEST_LOCATION);
//                        }
//                        catch (@SuppressLint("NewApi") IntentSender.SendIntentException e)
//                        {
//
//                        }
//                        break;
//                }
//            }
//        });
//    }
//
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        double latitude , longitude ;
//
//
//        mMap = googleMap;
//        if (lat!=0.0 && lng!=0.0)
//        {
//            latitude=lat;
//            longitude=lng;
//        }
//        else
//        {
//            latitude=Double.parseDouble(userPref.getLatitude());
//            longitude=Double.parseDouble(userPref.getLongitude());
//        }
//
//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude,
//                longitude), 15));
//        mMap.clear();
//         mMap.addMarker(new MarkerOptions()
//        .position(new LatLng(latitude,longitude))
//        .icon(bitmapDescriptorFromVector(SelectLocation.this,R.drawable.pin)));
//         getPlaces(latitude,longitude,"","");
//
//    }
//
//    private void getPlaces(double latitude,double longitude,String place,String address) {
//        Location_rv.setVisibility(View.GONE);
//        progress.setVisibility(View.VISIBLE);
//        places=new ArrayList<>();
//        RequestQueue queue= Volley.newRequestQueue(SelectLocation.this);
//        String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + latitude + "," + longitude + "&radius=500&key=AIzaSyC92Rdnlx0I1AUkmeCYhGYmG7nxYmflxMY&sensor=true";
//       //Loge("url", url);
//        StringRequest req = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String s) {
//                //pDialog.dismiss();
//                progress.setVisibility(View.GONE);
//               //Loge("places_res", s);
//
//               // swipeContainer.setRefreshing(false);
//                try {
//
//                    JSONObject obj = new JSONObject(s);
//
//                    JSONArray json_Array = obj.getJSONArray("results");
//
//                    if (json_Array.length() > 0) {
//                        //places.clear();
//
//                        for (int i = 0; i < json_Array.length(); i++) {
//                            JSONObject jobj = json_Array.getJSONObject(i);
//
//                            String name="",address="",icon="";
//                            if (jobj.has("name"))
//                            {
//                                name=jobj.getString("name");
//                            }
//                            if (jobj.has("vicinity"))
//                            {
//                                address=jobj.getString("vicinity");
//                            }
//                            if (jobj.has("icon"))
//                            {
//                                icon=jobj.getString("icon");
//                            }
//                            places.add(new Places(name,
//                                    address,
//                                    icon,
//                                    jobj.getJSONObject("geometry").getJSONObject("location").getDouble("lat"),
//                                    jobj.getJSONObject("geometry").getJSONObject("location").getDouble("lng")));
//                            mMap.addMarker(new MarkerOptions()
//                                    .icon(bitmapDescriptorFromVector(SelectLocation.this,R.drawable.ic_marker))
//                                    .title(jobj.getString("name"))
//                                    .position(new LatLng( jobj.getJSONObject("geometry").getJSONObject("location").getDouble("lat"),
//                                            jobj.getJSONObject("geometry").getJSONObject("location").getDouble("lng"))));
//                        }
//
//                        adapter=new NearByPLacesAdapter(SelectLocation.this,places);
//                        Location_rv.setAdapter(adapter);
//                        Location_rv.setVisibility(View.VISIBLE);
//                    }
//
//
//
//
//                } catch (Exception e) {
//                   //Loge("e", "e", e);
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//                volleyError.printStackTrace();
//                //pDialog.dismiss();
//                progress.setVisibility(View.GONE);
//               //Loge("error", volleyError.getMessage() + "e");
//
//
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<String, String>();
//
//                return params;
//            }
////            @Override
////            public Map getHeaders() throws AuthFailureError {
////                Map headers = new HashMap();
////                headers.put("x-authorization", userSharedPreferences.getApiKey());
////
////                return headers;
////            }
//
//        };
//        int socketTimeout = 50000;
//        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//        req.setRetryPolicy(policy);
//        queue.add(req);
//    }
//
//    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
//        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
//        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
//        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
//        Canvas canvas = new Canvas(bitmap);
//        vectorDrawable.draw(canvas);
//        return BitmapDescriptorFactory.fromBitmap(bitmap);
//    }
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.findmygirlfriend_menu,menu);
//        //menu.findItem(R.id.findmygirlfriend_menu).setVisible(false);
//        MenuItem item = menu.findItem(R.id.action_search);
//       // searchView.setMenuItem(item);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item)
//    {
//        switch (item.getItemId())
//        {
//            case R.id.action_search:
//                Intent intent = null;
//                try
//                {
//                    intent=new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).build(SelectLocation.this);
//                }
//                catch (GooglePlayServicesRepairableException e)
//                {
//                    e.printStackTrace();
//                }
//                catch (GooglePlayServicesNotAvailableException e)
//                {
//                    e.printStackTrace();
//                }
//                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
//                break;
//
//        }
//        return super.onOptionsItemSelected(item);
//
//    }
//
//
//    public  void sendLocation(double lat,double lng, String name, String address)
//    {
//
//        Intent in=new Intent(SelectLocation.this,Activity_Chat.class);
//        in.putExtra("lat",lat);
//        in.putExtra("lng",lng);
//        in.putExtra("name",name);
//        in.putExtra("address",address);
//        setResult(RESULT_OK,in);
//        finish();
////        mMap.addMarker(new MarkerOptions()
////                .icon(bitmapDescriptorFromVector(SelectLocation.this,R.drawable.ic_pin))
////                .title(jobj.getString("name"))
////                .position(new LatLng( jobj.getJSONObject("geometry").getJSONObject("location").getDouble("lat"),
////                        jobj.getJSONObject("geometry").getJSONObject("location").getDouble("lng"))));
//    }
//}
