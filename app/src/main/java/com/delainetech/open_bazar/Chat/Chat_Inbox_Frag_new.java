package com.delainetech.open_bazar.Chat;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.delainetech.open_bazar.Adpost_Add_Activity;
import com.delainetech.open_bazar.Custom_classes.EndlessRecyclerViewScrollListener;
import com.delainetech.open_bazar.Home_Screen_Activity;
import com.delainetech.open_bazar.Models.CHATINBOX;
import com.delainetech.open_bazar.Models.CHAT_INBOX;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;
import com.delainetech.open_bazar.Utils.PushService;
import com.delainetech.open_bazar.Utils.UserSharedPreferences;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.delainetech.open_bazar.Utils.Common.logg;

public class Chat_Inbox_Frag_new extends Fragment implements ParamKeys,SwipeRefreshLayout.OnRefreshListener,View.OnClickListener{


    View v;
    Context mcontext;
    UserSharedPreferences preferences;

    RecyclerView reclcler;
//    LinearLayoutManager linearLayoutManager;
//    Inbox_adapter adapter;
    int index=0;
    EndlessRecyclerViewScrollListener recyclerViewScrollListener;
    SwipeRefreshLayout swiperefresh;

    LinearLayout tvemptyview;
    boolean isViewShown=false;
    ImageView home_notification;


//    firbase chat
    private DatabaseReference mConvDatabase;
    private DatabaseReference mMessageDatabase;
    private DatabaseReference mUsersDatabase,mAdsDatabase,mBlockDatabase;

    private FirebaseAuth mAuth;

    private String mCurrent_user_id,nameuser="";

    String current_userchat="",other_user_chat;
    ArrayList<CHAT_INBOX> chat_inboxArrayList=new ArrayList<>();

    ArrayList<CHATINBOX>  chatinboxes=new ArrayList<>();
    Inbox_Chat_adapter adapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.chat_inbox_frag, container, false);
        mcontext=getActivity();
        FirebaseApp.initializeApp(getActivity());
        initial();

        return v;
    }



    private void initial() {
       home_notification=(ImageView) v.findViewById(R.id.home_notification);
        home_notification.setOnClickListener(this);
        preferences=new UserSharedPreferences(mcontext);
        tvemptyview=v.findViewById(R.id.tvemptyview);
        tvemptyview.setOnClickListener(this);
        swiperefresh=v.findViewById(R.id.swiperefresh);
        reclcler=v.findViewById(R.id.reclcler);

        swiperefresh.setOnRefreshListener(this);

        swiperefresh.setColorSchemeColors(getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorPrimary),getResources().getColor(R.color.colorPrimary));

        ((Home_Screen_Activity)mcontext).fab_add_ad.setOnClickListener(this);

        mRootRef = FirebaseDatabase.getInstance().getReference();
//        firbase
        mCurrent_user_id="user"+preferences.getuserid();
        register_user(preferences.getname());
        mConvDatabase = FirebaseDatabase.getInstance().getReference().child("Chat").child(mCurrent_user_id);

        mConvDatabase.keepSynced(false);
        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
        mAdsDatabase = FirebaseDatabase.getInstance().getReference().child("Ads");
        mBlockDatabase = FirebaseDatabase.getInstance().getReference().child("Block").child("user"+preferences.getuserid());
        mMessageDatabase = FirebaseDatabase.getInstance().getReference().child("messages");
        mUsersDatabase.keepSynced(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);

        reclcler.setHasFixedSize(true);
        reclcler.setLayoutManager(linearLayoutManager);
        adapter=new Inbox_Chat_adapter(chatinboxes,mcontext);
        reclcler.setAdapter(adapter);
        loadMessages();

    }



    @Override
    public void onRefresh() {
    swiperefresh.setRefreshing(false);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.home_notification:

                break;

            case R.id.fab_add_ad:
                Intent i=new Intent(mcontext, Adpost_Add_Activity.class);
                startActivityForResult(i,10);
                break;

        }
    }

    private  final int TOTAL_ITEMS_TO_LOAD = 50;
    private int  mCurrentPage = 1;

    private void loadMessages()
    {
        mRootRef.keepSynced(true);
        DatabaseReference messageRef = mRootRef.child("Chat").child(mCurrent_user_id);



        Query messageQuery = messageRef.orderByChild("timestamp").limitToLast(mCurrentPage * TOTAL_ITEMS_TO_LOAD);

        messageQuery.addChildEventListener(new ChildEventListener()
        {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s)
            {
                String message1 = dataSnapshot.getValue().toString();

                Log.e("loadmore_chat",message1+"");
                Log.e("loadmore_key",dataSnapshot.getKey().toString()+"");

                String[] parts = dataSnapshot.getKey().toString().split("_");
                String post_id="",userid="";
                if (parts.length>0)
                {
                    post_id = parts[0];
                    userid = parts[1];
                }
                chatinboxes.add(new CHATINBOX(dataSnapshot.getKey().toString(),userid,post_id,Boolean.parseBoolean(dataSnapshot.child("seen").getValue().toString())));
                Log.e("list_user_id",dataSnapshot.getKey().toString()+" ,userid="+userid);


                adapter.notifyDataSetChanged();

                if(chatinboxes.size()>0)
                {
                    reclcler.setVisibility(View.VISIBLE);
                    tvemptyview.setVisibility(View.GONE);
                }
                logg("size14",chatinboxes.size()+"");

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Log.e("loadmore_keychange",dataSnapshot.getValue().toString()+"");

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



    }




    public void register_user(final String name){

        FirbaseCommon.register_user("user"+preferences.getuserid(),preferences.getname(),preferences.getimage(),true);


    }




//    @Override
//    public void onStart() {
//        super.onStart();
//
////        Query conversationQuery = mConvDatabase.orderByChild("timestamp");
////
////        FirebaseRecyclerAdapter<Conv, ConvViewHolder> firebaseConvAdapter = new FirebaseRecyclerAdapter<Conv, ConvViewHolder>(
////                Conv.class,
////                R.layout.chat_inbox_adapter,
////                ConvViewHolder.class,
////                conversationQuery
////        ) {
////            @Override
////            protected void populateViewHolder(final ConvViewHolder convViewHolder, final Conv conv, final int i) {
////
////
////
////                final String list_user_id = getRef(i).getKey();
////
////                String[] parts = list_user_id.split("_");
////                String post_id="",userid="";
////                if (parts.length>0) {
////                    post_id = parts[0];
////                    userid = parts[1];
////                }
////                Log.e("list_user_id",list_user_id+" ,userid="+userid);
////                Query lastMessageQuery = mMessageDatabase.child(post_id+"_"+mCurrent_user_id).child(list_user_id).limitToLast(1);
////
////                lastMessageQuery.addChildEventListener(new ChildEventListener() {
////                    @Override
////                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
////
////                        logg("datasnapshot1",dataSnapshot.getValue().toString());
////                        String msg = dataSnapshot.child("message").getValue().toString();
////                        if (getActivity()!=null) {
////                            convViewHolder.setMessage(msg, conv.isSeen(), getActivity(),
////                                    dataSnapshot.child("user_name").getValue().toString(),
////                                    dataSnapshot.child("timestamp").getValue().toString(),
////                                    dataSnapshot.child("type").getValue().toString());
////                        }
////                    }
////
////                    @Override
////                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
////
////                    }
////
////                    @Override
////                    public void onChildRemoved(DataSnapshot dataSnapshot) {
////
////                    }
////
////                    @Override
////                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
////
////                    }
////
////                    @Override
////                    public void onCancelled(DatabaseError databaseError) {
////
////                    }
////                });
////
////
////                final String finalUserid = userid;
////                mUsersDatabase.child(userid).addValueEventListener(new ValueEventListener() {
////                    @Override
////                    public void onDataChange(DataSnapshot dataSnapshot) {
////                        if (dataSnapshot!=null) {
////                            if (dataSnapshot.hasChild("name")) {
////                                logg("datasnapshot",dataSnapshot.getValue().toString());
////
////                                final String userName = dataSnapshot.child("name").getValue().toString();
////                                final String userThumb = dataSnapshot.child("image").getValue().toString();
////
////                                if (dataSnapshot.hasChild("online")) {
////
////                                    String userOnline = dataSnapshot.child("online").getValue().toString();
////
////                                    convViewHolder.setUserOnline(userOnline);
////
////                                }
////
////                                convViewHolder.setName(userName);
////                                if (getActivity()!=null) {
////                                    convViewHolder.setUserImage(userThumb, getActivity());
////                                }
//////                        Glide.with(Userlist_activity.this).load(Constant.imageUri+userThumb).into(holder.imageView);
////
////                                convViewHolder.rv.setOnClickListener(new View.OnClickListener() {
////                                    @Override
////                                    public void onClick(View view) {
////
////                                        if (chat_inboxArrayList.size()>i) {
////                                            Intent chatIntent = new Intent(getActivity(), ChatActivity.class);
////                                            chatIntent.putExtra("user_id", finalUserid);
////                                            chatIntent.putExtra("user_name", userName);
////                                            chatIntent.putExtra("userimage", userThumb);
////                                            chatIntent.putExtra("name", nameuser);
////                                            chatIntent.putExtra("id", preferences.getuserid());
////                                            chatIntent.putExtra("ad_tital", chat_inboxArrayList.get(i).getTitle());
////                                            chatIntent.putExtra("ad_image", chat_inboxArrayList.get(i).getImage());
////                                            chatIntent.putExtra("ad_price", "€ " + chat_inboxArrayList.get(i).getPrice());
////                                            chatIntent.putExtra("ad_id", chat_inboxArrayList.get(i).getPostid());
////
////                                            Log.e("ides", chatIntent.getExtras() + ",--pos=" + i);
////                                            startActivityForResult(chatIntent,10);
////                                        }
////                                    }
////                                });
////                            }
////                        }
////                    }
////
////                    @Override
////                    public void onCancelled(DatabaseError databaseError) {
////
////                    }
////                });
////
////
////                mAdsDatabase.child(post_id).addValueEventListener(new ValueEventListener() {
////                    @Override
////                    public void onDataChange(DataSnapshot dataSnapshot) {
////                        if (dataSnapshot!=null) {
////                            if (dataSnapshot.hasChild("title")) {
////                                logg("datasnapshotpost",dataSnapshot.getValue().toString());
////
////                                chat_inboxArrayList.add(0,new CHAT_INBOX(dataSnapshot.child("image").getValue().toString(),
////                                        dataSnapshot.child("price").getValue().toString(),dataSnapshot.child("title").getValue().toString(),
////                                        dataSnapshot.child("ad_id").getValue().toString()));
////
////                                convViewHolder.settital(dataSnapshot.child("title").getValue().toString());
////                            }
////                        }
////                    }
////
////                    @Override
////                    public void onCancelled(DatabaseError databaseError) {
////
////                    }
////                });
////
////                logg("firbasereff",userid);
////                final String finalUserid1 = userid;
////                mBlockDatabase.child(userid).addValueEventListener(new ValueEventListener() {
////                    @Override
////                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
////                        if (dataSnapshot!=null) {
//////                            if (dataSnapshot.getKey())
////                            if (dataSnapshot.hasChild("blocked"+ finalUserid1)) {
////                                logg("dataa1", dataSnapshot.getValue().toString() + "");
////                            }
////                        }
////                    }
////
////                    @Override
////                    public void onCancelled(@NonNull DatabaseError databaseError) {
////
////                    }
////                });
////
////                logg("firbasereff",mBlockDatabase.getDatabase().toString()+"");
////            }
////
////        };
////
////        reclcler.setAdapter(firebaseConvAdapter);
//
//    }

    public static class ConvViewHolder extends RecyclerView.ViewHolder {

        View mView;
        RelativeLayout rv;
        public ConvViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

            rv=itemView.findViewById(R.id.rv);
        }

        public void setMessage(String message, boolean isSeen,Context context,String lastmsgusername,String timestamp,String type){

            TextView userStatusView = (TextView) mView.findViewById(R.id.lastmsg1);
            TextView lastmsg = (TextView) mView.findViewById(R.id.lastmsg);
            TextView tv_time = (TextView) mView.findViewById(R.id.tv_time);
            lastmsg.setText(lastmsgusername+" :");
            tv_time.setText(Common.getTimeAgo(Long.parseLong(timestamp),context));

            if (type.equals("image")){
                message="Shared Image";
            }else if (type.equals("audio")){
                message="Shared Audio";
            }else if (type.equals("location")){
                message="Shared Location";
            }
            userStatusView.setText(message);

            if(!isSeen){
                userStatusView.setTypeface(userStatusView.getTypeface(), Typeface.BOLD);
                if (context!=null) {
                    userStatusView.setTextColor(context.getResources().getColor(R.color.colorAccent));
                }
            } else {
                userStatusView.setTypeface(userStatusView.getTypeface(), Typeface.NORMAL);
            }

        }

        public void setName(String name){

            TextView userNameView = (TextView) mView.findViewById(R.id.tvchattername);
            userNameView.setText(name);

        }
    RequestOptions requestOptions=new RequestOptions().error(R.drawable.userimage).centerCrop();
        public void setUserImage(String thumb_image, Context ctx){

            CircleImageView userImageView = (CircleImageView) mView.findViewById(R.id.iv_circle);
//            Picasso.with(ctx).load(thumb_image).placeholder(R.drawable.default_avatar).into(userImageView);
            Log.e( "setUserImage: ", Common.Image_Loading_Url(thumb_image) );

            Glide.with(ctx).load(Common.Image_Loading_Url(thumb_image)).apply(requestOptions).into(userImageView);

        }

        public void settital(String tital){
            TextView tv_ad_tital = (TextView) mView.findViewById(R.id.tv_ad_tital);
            tv_ad_tital.setText(tital);

        }

        public void setUserOnline(String online_status) {

            ImageView userOnlineView = (ImageView) mView.findViewById(R.id.iv_online);

            if(online_status.equals("true")){

                userOnlineView.setVisibility(View.VISIBLE);

            } else {

                userOnlineView.setVisibility(View.INVISIBLE);

            }

        }


    }


    private DatabaseReference mRootRef;

    @Override
    public void onResume() {
        super.onResume();
        if (mRootRef!=null) {
            online_status("true");
        }
        PushService. chatactivitystr="in";

        if (!Common.isNetworkConnected(getActivity())){
            Toast.makeText(getActivity(), No_Internet_Connection, Toast.LENGTH_SHORT).show();
        }
    }

//    @Override
//    public void onStop() {
//        super.onStop();
//        if (mRootRef!=null) {
//            online_status(ServerValue.TIMESTAMP);
//        }
//    }


    private void online_status(Object status){
        mRootRef.child("Users").child("user"+preferences.getuserid()).child("online").setValue(status);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        logg("requestCode=",requestCode+", resultCode="+resultCode);
        if (requestCode==10 && resultCode == Activity.RESULT_OK){
            if (data!=null) {
                logg("otheruserid",data.getStringExtra("otherid"));
                mRootRef.child("Chat").child("user" + preferences.getuserid()).child(data.getStringExtra("otherid")).removeValue();
            }
        }
    }



}
