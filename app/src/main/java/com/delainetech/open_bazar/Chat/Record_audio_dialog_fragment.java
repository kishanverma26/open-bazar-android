package com.delainetech.open_bazar.Chat;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.delainetech.open_bazar.Chat.ChatActivity;
import com.delainetech.open_bazar.R;
import com.delainetech.open_bazar.Utils.ParamKeys;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.DialogFragment;
import pl.bclogic.pulsator4droid.library.PulsatorLayout;

import static com.delainetech.open_bazar.Utils.Common.logg;


/**
 * Created by Rsss on 2/5/2018.
 */

public class Record_audio_dialog_fragment extends DialogFragment implements ParamKeys, View.OnClickListener {


    View v;
    private static String fileName = null;
      private MediaRecorder recorder = null;


    Context mcontext;

    LinearLayout ll;
//   CardView cvimageViewAudio,cvimageViewsend,cvimageViewAudioplay;
    ImageView imageAudio,imageAudioplay;
    private Rect rect;

//   Time
    TextView tv_time;
private int audioTotalTime;
    private TimerTask timerTask;
    private Timer audioTimer;
    private SimpleDateFormat timeFormatter = new SimpleDateFormat("m:ss", Locale.getDefault());
    private Handler handler;
    private MediaPlayer   player = null;

    boolean playpause=true;
    String str_duration="00:00";

    private PulsatorLayout mPulsator;
    TextView tv_tap;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        v=inflater.inflate(R.layout.record_audio_dialog_fragment,container,false);
        mcontext=getActivity();
        initi();
        return v;
    }

    private void initi()
    {
        ll=(LinearLayout)v.findViewById(R.id.ll);
        imageAudio=(ImageView) v.findViewById(R.id.imageAudio);
        tv_tap=(TextView) v.findViewById(R.id.tv_tap);
//        cvimageViewAudio=(CardView) v.findViewById(R.id.cvimageViewAudio);
//        cvimageViewsend=(CardView) v.findViewById(R.id.cvimageViewsend);
        mPulsator = (PulsatorLayout)v. findViewById(R.id.pulsator);

//        cvimageViewAudioplay=(CardView) v.findViewById(R.id.cvimageViewAudioplay);
//        cvimageViewAudioplay.setOnClickListener(this);
//        cvimageViewsend.setOnClickListener(this);

        tv_time=(TextView) v.findViewById(R.id.tv_time);
        handler = new Handler(Looper.getMainLooper());




//        cvimageViewAudio.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v)
//            {
//                if (cvimageViewAudio.getTag()!=null) {
//                    if (cvimageViewAudio.getTag().equals("true")) {
//                        stop_recording();
//
//                    } else {
//                        if (playpause){
//                            startrecording();
//
//                        }else {
//                            onPlay(false);
//                        }
//                    }
//                }else {
//                    if (playpause){
//                        startrecording();
//
//                    }else {
//                        onPlay(false);
//                    }
//                }
//            }
//        });

        imageAudio.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                logg("action",event.getAction()+"");
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    //Start recording
                    rect = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());

                    mPulsator.start();
                    startrecording();
                    tv_tap.setText("To Cancel swipe down");
                    return true;
                }
                else if(event.getAction() == MotionEvent.ACTION_UP){
                    //Stop recording and send it
                    if(rect.contains(v.getLeft() + (int) event.getX(), v.getTop() + (int) event.getY())){

                        mPulsator.stop();
                        stop_recording();
                        onPlay(false);
                        str_duration=tv_time.getText().toString();
                        ( (ChatActivity)getActivity()).upload_media(fileName,"audio",str_duration);


                    }

                    return true;
                }else if (event.getAction() == MotionEvent.ACTION_MOVE){
                    if(!rect.contains(v.getLeft() + (int) event.getX(), v.getTop() + (int) event.getY())){
                        //Stop recording and cancel
                        tv_tap.setText("Tap to start recording");
                        mPulsator.stop();
                        onPlay(false);

                        stop_recording();
                        tv_time.setText("00:00");
                    }
                    return true;
                }
                return false;
            }
        });

    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        Dialog dialog = super.onCreateDialog(savedInstanceState);
        WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.gravity = Gravity.CENTER;
        wlmp.dimAmount = 0.0F;
        dialog.getWindow().setAttributes(wlmp);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return dialog;
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 1.00);
        int screenHeight = (int) (metrics.heightPixels * 1.00);

        getDialog().getWindow().setLayout(screenWidth, screenHeight);
    }



    private void startrecording(){
        fileName =getActivity(). getExternalCacheDir().getAbsolutePath();

        fileName += "/"+System.currentTimeMillis()+".3gp";
//        cvimageViewsend.setVisibility(View.GONE);
//        cvimageViewAudioplay.setVisibility(View.GONE);
//        imageAudio.setImageResource(R.drawable.ic_stop_white_24dp);
//        cvimageViewAudio.setTag("true");
//        cvimageViewAudio.animate().scaleXBy(0.5f).scaleYBy(0.5f).setDuration(200).setInterpolator(new OvershootInterpolator()).start();

        if (audioTimer == null) {
            audioTimer = new Timer();
            timeFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        }

        timerTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        tv_time.setText(timeFormatter.format(new Date(audioTotalTime * 1000)));
                        audioTotalTime++;
                    }
                });
            }
        };

        audioTotalTime = 0;
        audioTimer.schedule(timerTask, 0, 1000);
        startRecording();
    }


    private void stop_recording(){
        timerTask.cancel();
//        cvimageViewAudio.animate().scaleX(0f).scaleY(0f).setDuration(100).setInterpolator(new LinearInterpolator()).start();
//        cvimageViewAudio.animate().scaleX(1f).scaleY(1f).translationX(0).translationY(0).setDuration(100).setInterpolator(new LinearInterpolator()).start();
//        cvimageViewAudio.setTag("false");
//        cvimageViewsend.setVisibility(View.VISIBLE);
//        cvimageViewAudioplay.setVisibility(View.VISIBLE);
            stopRecording();
    }



    private void startRecording() {


        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder.setOutputFile(fileName);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            recorder.prepare();
        } catch (IOException e) {
            logg("record", "prepare() failed");
        }

        recorder.start();
    }

    private void stopRecording() {
        if (recorder!=null) {
            recorder.stop();
            recorder.release();
            recorder = null;
            logg("filename", fileName + "");
        }
       dismiss();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
//            case R.id.cvimageViewsend:
//                onPlay(false);
//                str_duration=tv_time.getText().toString();
//                ( (ChatActivity)getActivity()).upload_media(fileName,"audio",str_duration);
//                dismiss();
//               break;


        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (recorder != null) {
            recorder.release();
            recorder = null;
        }

        if (player != null) {
            player.release();
            player = null;
        }
    }

    private void startPlaying() {
        player = new MediaPlayer();
        try {
            player.setDataSource(fileName);
            player.prepare();
            player.start();
        } catch (IOException e) {
            Log.e("play", "prepare() failed");
        }
    }

    private void stopPlaying() {
        if (player!=null) {
            player.release();
            player = null;
        }
    }


    private void onPlay(boolean start) {
        if (start) {

            startPlaying();
            playpause=false;
//            imageAudioplay.setImageResource(R.drawable.ic_stop_white_24dp);
        } else {
            stopPlaying();
            playpause=true;
//            imageAudioplay.setImageResource(R.drawable.ic_play_arrow_black_24dp);
        }
    }
}
