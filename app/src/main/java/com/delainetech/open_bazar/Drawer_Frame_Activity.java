package com.delainetech.open_bazar;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.delainetech.open_bazar.Fragments.About_us_fragment;
import com.delainetech.open_bazar.Fragments.Account_fragment;
import com.delainetech.open_bazar.Fragments.Blocked_User_Frag;
import com.delainetech.open_bazar.Fragments.Boost_Ads_Fragment;
import com.delainetech.open_bazar.Fragments.Buy_Package_fragment;
import com.delainetech.open_bazar.Fragments.Comming_Soon_fragment;
import com.delainetech.open_bazar.Fragments.Contact_us_Fragment;
import com.delainetech.open_bazar.Fragments.Favorite_Ads_Frag;
import com.delainetech.open_bazar.Fragments.Followers_Fragment;
import com.delainetech.open_bazar.Fragments.Following_Ads_Frag;
import com.delainetech.open_bazar.Fragments.Help_fragment;
import com.delainetech.open_bazar.Fragments.MY_ADS_Frag;
import com.delainetech.open_bazar.Fragments.My_Coin_Faq_fragment;
import com.delainetech.open_bazar.Fragments.My_Offer_Ads_Frag;
import com.delainetech.open_bazar.Fragments.My_Wallet_Faq_fragment;
import com.delainetech.open_bazar.Fragments.Following_Frag;
import com.delainetech.open_bazar.Fragments.Other_Followers_Fragment;
import com.delainetech.open_bazar.Fragments.Other_Following_Frag;
import com.delainetech.open_bazar.Fragments.Recently_View_Frag;
import com.delainetech.open_bazar.Fragments.Report_Ad_fragment;
import com.delainetech.open_bazar.Fragments.Upgrade_Account_Fragment;
import com.delainetech.open_bazar.Utils.Common;
import com.delainetech.open_bazar.Utils.ParamKeys;

public class Drawer_Frame_Activity extends AppCompatActivity implements ParamKeys {

    Toolbar toolbar;
    Context mcontext;
    TextView tv_tital;
    String id="",type="",boostaddid="";
    int pos=0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_fram_layout);
        mcontext=Drawer_Frame_Activity.this;
        inti();
    }

    private void inti() {
        tv_tital=(TextView) findViewById(R.id.tv_tital);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);

        tv_tital.setText(Common.Parsintentvalue(Title,Drawer_Frame_Activity.this));

        pos=getIntent().getIntExtra(Pos,0);

        if(getIntent().hasExtra("add_id"))
        {
            boostaddid=getIntent().getStringExtra("add_id");
        }

        if (getIntent().hasExtra("id"))
        {
         id=getIntent().getStringExtra("id");
         type=getIntent().getStringExtra("type");

        }

        if (getIntent().hasExtra("followingid"))
        {
            id=getIntent().getStringExtra("followingid");

        }
        switch_view(pos);
    }




    private void switch_view(int pos){

        switch (pos){
            case 0:
             //Recently
              getSupportFragmentManager().beginTransaction().replace(R.id.fram_lay,new MY_ADS_Frag()).commit();
             break;
            case 1:
             //Recently
              getSupportFragmentManager().beginTransaction().replace(R.id.fram_lay,new Following_Frag()).commit();
             break;
          case 2:
             //Recently
              getSupportFragmentManager().beginTransaction().replace(R.id.fram_lay,new Contact_us_Fragment()).commit();
             break;
          case 3:
             //Recently
              getSupportFragmentManager().beginTransaction().replace(R.id.fram_lay,new My_Wallet_Faq_fragment()).commit();
             break;
            case 4:
             //Recently
              getSupportFragmentManager().beginTransaction().replace(R.id.fram_lay,new My_Coin_Faq_fragment()).commit();
             break;
        case 5:
             //Recently
              getSupportFragmentManager().beginTransaction().replace(R.id.fram_lay,new Account_fragment()).commit();
             break;
        case 6:
             //Recently
              getSupportFragmentManager().beginTransaction().replace(R.id.fram_lay,new About_us_fragment()).commit();
             break;
       case 7:
             //Recently
              getSupportFragmentManager().beginTransaction().replace(R.id.fram_lay,new Buy_Package_fragment()).commit();
             break;
            case 8:
             //Recently
              getSupportFragmentManager().beginTransaction().replace(R.id.fram_lay,new Recently_View_Frag()).commit();
             break;
             case 9:
             //Recently
              getSupportFragmentManager().beginTransaction().replace(R.id.fram_lay,new Favorite_Ads_Frag()).commit();
             break;
          case 10:
             //Recently
              getSupportFragmentManager().beginTransaction().replace(R.id.fram_lay,new My_Offer_Ads_Frag()).commit();
             break;
       case 11:
             //Recently
              getSupportFragmentManager().beginTransaction().replace(R.id.fram_lay,new Following_Ads_Frag()).commit();
             break;
         case 12:
             //Recently
              getSupportFragmentManager().beginTransaction().replace(R.id.fram_lay,new Followers_Fragment()).commit();
             break;
            case 13:
                //Recently
                getSupportFragmentManager().beginTransaction().replace(R.id.fram_lay,new Help_fragment()).commit();
                break;
        case 14:
            //Recently
            getSupportFragmentManager().beginTransaction().replace(R.id.fram_lay,new Report_Ad_fragment(id,type)).commit();
                break;
   case 15:
            //blocked user
            getSupportFragmentManager().beginTransaction().replace(R.id.fram_lay,new Blocked_User_Frag()).commit();
                break;
  case 16:
            //blocked user
            getSupportFragmentManager().beginTransaction().replace(R.id.fram_lay,new Upgrade_Account_Fragment()).commit();
                break;

 case 17:
            //blocked user
            getSupportFragmentManager().beginTransaction().replace(R.id.fram_lay,new Boost_Ads_Fragment(boostaddid)).commit();
                break;
 case 18:
            //blocked user
            getSupportFragmentManager().beginTransaction().replace(R.id.fram_lay,new Other_Following_Frag(id)).commit();
                break;

  case 19:
            //blocked user
            getSupportFragmentManager().beginTransaction().replace(R.id.fram_lay,new Other_Followers_Fragment(id)).commit();
                break;
case 20:
            //comming Soon
            getSupportFragmentManager().beginTransaction().replace(R.id.fram_lay,new Comming_Soon_fragment()).commit();
                break;


        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
}
        return super.onOptionsItemSelected(item);
    }
}
