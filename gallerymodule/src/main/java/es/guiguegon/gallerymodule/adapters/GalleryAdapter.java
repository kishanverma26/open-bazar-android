package es.guiguegon.gallerymodule.adapters;

import android.content.Context;
import android.net.Uri;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import es.guiguegon.gallerymodule.R;
import es.guiguegon.gallerymodule.model.GalleryMedia;
import es.guiguegon.gallerymodule.utils.ImageUtils;
import es.guiguegon.gallerymodule.utils.ScreenUtils;
import es.guiguegon.gallerymodule.utils.TextureCameraPreview;
import es.guiguegon.gallerymodule.utils.TimeUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import static es.guiguegon.gallerymodule.GalleryFragment.SelectedImage;
import static es.guiguegon.gallerymodule.GalleryFragment.video;

public class GalleryAdapter extends SelectableAdapter<RecyclerView.ViewHolder> {

    private final static int VIEW_HOLDER_TYPE_HEADER = 1;
    private final static int VIEW_HOLDER_TYPE_ITEM = 2;
    private final String TAG = "[" + this.getClass().getSimpleName() + "]";
    private ArrayList<GalleryMedia> galleryMedias;
    private WeakReference<OnGalleryClickListener> onGalleryClickListenerWeak;
    private boolean multiselection;
    private int itemWidth;
    ArrayList<GalleryMedia> selected_items;
    private int itemHeight;
    private int maxSelectedItems = Integer.MAX_VALUE;

    public GalleryAdapter(Context context, int columns) {
        galleryMedias = new ArrayList<>();
        itemWidth = ScreenUtils.getScreenWidth(context) / columns;
        itemHeight = context.getResources().getDimensionPixelSize(R.dimen.gallery_item_height);
    }

    public void setMultiselection(boolean multiselection) {
        this.multiselection = multiselection;
    }

    public void setMaxSelectedItems(int maxSelectedItems) {
        this.maxSelectedItems = maxSelectedItems;
    }

    public void setOnGalleryClickListener(OnGalleryClickListener onGalleryClickListener) {
        this.onGalleryClickListenerWeak = new WeakReference<>(onGalleryClickListener);
    }

    public void addGalleryImage(GalleryMedia galleryMedia) {



        this.galleryMedias.add(0, galleryMedia);
        notifySelectableAdapterItemInserted(1);
    }

    public void addGalleryImage(List<GalleryMedia> galleryMedias) {
        this.galleryMedias.addAll(galleryMedias);
        notifyItemRangeInserted(1, galleryMedias.size());
    }

    public ArrayList<GalleryMedia> getSelectedItems() {
        ArrayList<GalleryMedia> galleryMedias = new ArrayList<>();
        for (Integer position : getSelectedItemsPosition()) {
            galleryMedias.add(this.galleryMedias.get(position - 1));
        }
        return galleryMedias;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v;
        switch (viewType) {
            case VIEW_HOLDER_TYPE_HEADER:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_gallery_header, viewGroup, false);
                return new GalleryHeaderViewHolder(v);
            case VIEW_HOLDER_TYPE_ITEM:
            default:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_gallery, viewGroup, false);
                return new GalleryItemViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        try {


            final ViewGroup.LayoutParams layoutParams = viewHolder.itemView.getLayoutParams();
            StaggeredGridLayoutManager.LayoutParams sglayoutParams =
                    (StaggeredGridLayoutManager.LayoutParams) layoutParams;
            if (viewHolder instanceof GalleryHeaderViewHolder) {
                Log.e("adapter",position+"");
                sglayoutParams.setFullSpan(true);
                fill((GalleryHeaderViewHolder) viewHolder);
            } else {
                sglayoutParams.setFullSpan(false);
                fill((GalleryItemViewHolder) viewHolder, galleryMedias.get(position - 1), position);
            }
            viewHolder.itemView.setLayoutParams(sglayoutParams);
        } catch (Exception e) {
            Log.e(TAG, "[onBindViewHolder] ", e);
        }
    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return VIEW_HOLDER_TYPE_HEADER;
            default:
                return VIEW_HOLDER_TYPE_ITEM;
        }
    }

    @Override
    public int getItemCount() {
        return galleryMedias.size() + 1;
    }

    public void fill(GalleryItemViewHolder galleryItemViewHolder, final GalleryMedia galleryMedia, int position) {
        galleryItemViewHolder.galleryItemSelected.setSelected(isSelected(position));

        Context context = galleryItemViewHolder.itemView.getContext();
        ImageUtils.loadImageFromUri(context, galleryMedia.mediaUri(),
                galleryItemViewHolder.galleryItem, itemWidth,
                itemHeight);

        if (galleryMedia.isVideo()) {
            galleryItemViewHolder.galleryItemVideoDuration.setText(
                    TimeUtils.getTimeFromVideoDuration(galleryMedia.duration()));
            galleryItemViewHolder.galleryItemVideoDuration.setVisibility(View.VISIBLE);
        } else {
            galleryItemViewHolder.galleryItemVideoDuration.setVisibility(View.GONE);
        }


        /*******/


        /*********/

        Log.e("method","called");

        Log.e("selected_items_sizeee",getSelectedItems().size()+"");

        if(isSelected(position))
        {
            galleryItemViewHolder.SelectedImage.setVisibility(View.VISIBLE);

        }

        else

        {
            galleryItemViewHolder.SelectedImage.setVisibility(View.GONE);

        }


      //  if (getSelectedItemsPo().get(position).isVideo()) {
          if(getSelectedItemsPosition().size()>0)
        {
            if (getSelectedItems().get(getSelectedItemsPosition().size() - 1).isVideo()) {
                video.setVisibility(View.VISIBLE);
                SelectedImage.setVisibility(View.GONE);
                video.setVideoURI(Uri.parse(getSelectedItems().get(getSelectedItemsPosition().size() - 1).mediaUri()));
                video.start();

            } else {
                RequestOptions requestOptions=new RequestOptions().centerCrop();
                Glide.with(context).load(getSelectedItems().get(getSelectedItemsPosition().size() - 1).mediaUri()).apply(requestOptions).into(SelectedImage);
                SelectedImage.setVisibility(View.VISIBLE);
                video.setVisibility(View.GONE);
            }
        }
        else
          {
              video.setVisibility(View.GONE);
              SelectedImage.setVisibility(View.GONE);
          }
        galleryItemViewHolder.galleryItemLayout.setOnClickListener(v -> {
//            if (isSelected(position)){
//           if(position < getSelectedItems().size()) {
//
//                   if (getSelectedItems().get(position).isVideo()) {
//                       video.setVisibility(View.VISIBLE);
//                       SelectedImage.setVisibility(View.GONE);
//                       video.setVideoURI(Uri.parse(getSelectedItems().get(position).mediaUri()));
//                       video.start();
//
//                   } else {
//                       Glide.with(context).load(getSelectedItems().get(position).mediaUri()).centerCrop().into(SelectedImage);
//                       SelectedImage.setVisibility(View.VISIBLE);
//                       video.setVisibility(View.GONE);
//                   }
//               }
//           }
            if (multiselection && canSelectItem(position)) {
                toggleSelection(position);

                notifyDataSetChanged();
//
//                if(position < getSelectedItems().size())
//                {
//                   if (getSelectedItems().get(position).isVideo()) {
//                       video.setVisibility(View.VISIBLE);
//                       SelectedImage.setVisibility(View.GONE);
//                       video.setVideoURI(Uri.parse(getSelectedItems().get(position).mediaUri()));
//                       video.start();
//
//                   } else {
//                       Glide.with(context).load(getSelectedItems().get(position).mediaUri()).centerCrop().into(SelectedImage);
//                       SelectedImage.setVisibility(View.VISIBLE);
//                       video.setVisibility(View.GONE);
//                   }
//               }




              //  if(isSelected(position-1)) {
//                    if (galleryMedias.get(position-1).isVideo()) {
//                        video.setVisibility(View.VISIBLE);
//                        SelectedImage.setVisibility(View.GONE);
//                        video.setVideoURI(Uri.parse(galleryMedias.get(position-1).mediaUri()));
//                        video.start();
//
//                    } else {
//                        Glide.with(context).load(galleryMedias.get(position-1).mediaUri()).centerCrop().into(SelectedImage);
//                        SelectedImage.setVisibility(View.VISIBLE);
//                        video.setVisibility(View.GONE);
//                    }
              //  }

            }

            onGalleryClickListenerWeak.get().onGalleryClick(galleryMedia);
        });
    }

    private boolean canSelectItem(int position) {
        return isSelected(position) || getSelectedItemCount() < maxSelectedItems;
    }

    public void fill(GalleryHeaderViewHolder galleryHeaderViewHolder) {

        galleryHeaderViewHolder.galleryCameraLayout.setOnClickListener(
                v -> onGalleryClickListenerWeak.get().onCameraClick());
    }

    public interface OnGalleryClickListener {
        void onGalleryClick(GalleryMedia galleryMedia);

        void onCameraClick();
    }

    public class GalleryItemViewHolder extends RecyclerView.ViewHolder {

        View galleryItemSelected;
        ImageView galleryItem;
        ImageView galleryGradient;
        ImageView SelectedImage;
        FrameLayout galleryItemLayout;
        TextView galleryItemVideoDuration;

        public GalleryItemViewHolder(View v) {
            super(v);
            SelectedImage=(ImageView)v.findViewById(R.id.selected_image);
            galleryItem = (ImageView) v.findViewById(R.id.gallery_item);
            galleryGradient = (ImageView) v.findViewById(R.id.gallery_gradient);
            galleryItemVideoDuration = (TextView) v.findViewById(R.id.gallery_video_duration);
            galleryItemLayout = (FrameLayout) v.findViewById(R.id.gallery_item_layout);
            galleryItemSelected = v.findViewById(R.id.gallery_item_selected);
            galleryItem.getLayoutParams().width = itemWidth;
            galleryGradient.getLayoutParams().width = itemWidth;
        }
    }




    private class GalleryHeaderViewHolder extends RecyclerView.ViewHolder {
        FrameLayout galleryCameraLayout;
        TextureCameraPreview galleryCameraPreview;
        ImageView selected_image;
        public GalleryHeaderViewHolder(View v) {
            super(v);
            selected_image=(ImageView)v.findViewById(R.id.gallery_camera);
            galleryCameraLayout = (FrameLayout) v.findViewById(R.id.gallery_camera_layout);
           galleryCameraPreview = (TextureCameraPreview) v.findViewById(R.id.gallery_camera_preview);
        }
    }


    }

