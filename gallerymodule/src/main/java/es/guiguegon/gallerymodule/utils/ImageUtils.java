package es.guiguegon.gallerymodule.utils;
/**
 * Created by guillermoguerrero on 2/6/16.
 */

import android.content.Context;
import android.util.Log;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import es.guiguegon.gallerymodule.R;

public class ImageUtils {

    private static final String TAG = "[ScreenUtils]";

    private ImageUtils() {
        //empty contructor
    }

    public static void loadImageFromUri(Context context, String imageUri, ImageView imageView, int width, int height) {
        try {
            RequestOptions requestOptions=new RequestOptions().override(width, height);
            Glide.with(context).load(imageUri).apply(requestOptions).into(imageView);
        } catch (Exception e) {
            Log.e(TAG, "[loadImageFromUri]", e);
        }
    }
}
